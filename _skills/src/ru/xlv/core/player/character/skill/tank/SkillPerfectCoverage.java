package ru.xlv.core.player.character.skill.tank;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import ru.xlv.core.common.XlvsCoreCommon;
import ru.xlv.core.event.EntityDamagePlayerEvent;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.mochar.player.character.skill.Skill;
import ru.xlv.mochar.player.character.skill.SkillType;

import javax.annotation.Nonnull;

public class SkillPerfectCoverage extends Skill {

    private final double chance;

    private ServerPlayer serverPlayer;

    public SkillPerfectCoverage(SkillType skillType) {
        super(skillType);
        chance = skillType.getCustomParam("chance", double.class);
    }

    @Override
    public void onSelected(ServerPlayer serverPlayer) {
        super.onSelected(serverPlayer);
        this.serverPlayer = serverPlayer;
        XlvsCoreCommon.EVENT_BUS.register(this);
    }

    @Override
    public void onDeselected(ServerPlayer serverPlayer) {
        super.onDeselected(serverPlayer);
        XlvsCoreCommon.EVENT_BUS.unregister(this);
    }

    @SuppressWarnings("unused")
    @SubscribeEvent
    public void event(EntityDamagePlayerEvent.Pre event) {
        if(event.getServerPlayer() == serverPlayer) {
            int i = event.getServerPlayer().getEntityPlayer().worldObj.rand.nextInt(100);
            if(i < chance) {
                event.setCanceled(true);
            }
        }
    }

    @Nonnull
    @Override
    public SkillPerfectCoverage clone() {
        return new SkillPerfectCoverage(getSkillType());
    }
}
