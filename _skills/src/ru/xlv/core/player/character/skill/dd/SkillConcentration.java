package ru.xlv.core.player.character.skill.dd;

import ru.xlv.core.common.player.character.CharacterAttributeMod;
import ru.xlv.core.common.player.character.CharacterAttributeType;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.mochar.player.character.skill.ActivableSkill;
import ru.xlv.mochar.player.character.skill.SkillType;

import javax.annotation.Nonnull;

public class SkillConcentration extends ActivableSkill {

    private final double ballisticMod;
    private final double energyMod;
    private final double recoilMod;

    public SkillConcentration(SkillType skillType) {
        super(skillType);
        ballisticMod = 1D + skillType.getCustomParam("outcome_ballistic_damage_value", double.class) / 100D;
        energyMod = 1D + skillType.getCustomParam("outcome_energy_damage_value", double.class) / 100D;
        recoilMod = 1D - skillType.getCustomParam("recoil_modifier", double.class) / 100D;
    }

    @Override
    public void onActivate(ServerPlayer serverPlayer) {
        serverPlayer.getSelectedCharacter().addAttributeMod(CharacterAttributeMod.builder()
                .attributeType(CharacterAttributeType.BALLISTIC_DAMAGE)
                .valueMod(ballisticMod)
                .period(getSkillType().getDuration())
                .build());
        serverPlayer.getSelectedCharacter().addAttributeMod(CharacterAttributeMod.builder()
                .attributeType(CharacterAttributeType.ENERGY_DAMAGE)
                .valueMod(energyMod)
                .period(getSkillType().getDuration())
                .build());
        serverPlayer.getSelectedCharacter().addAttributeMod(CharacterAttributeMod.builder()
                .attributeType(CharacterAttributeType.WEAPON_RECOIL_MOD)
                .valueMod(recoilMod)
                .period(getSkillType().getDuration())
                .build());
    }

    @Nonnull
    @Override
    public SkillConcentration clone() {
        return new SkillConcentration(getSkillType());
    }
}
