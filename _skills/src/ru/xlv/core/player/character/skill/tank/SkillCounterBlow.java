package ru.xlv.core.player.character.skill.tank;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import ru.xlv.core.common.XlvsCoreCommon;
import ru.xlv.core.event.EntityDamagePlayerEvent;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.core.util.DamageUtils;
import ru.xlv.mochar.player.character.skill.ActivableSkill;
import ru.xlv.mochar.player.character.skill.SkillType;

import javax.annotation.Nonnull;

public class SkillCounterBlow extends ActivableSkill {

    private final double damageBackMod;

    private ServerPlayer serverPlayer;

    public SkillCounterBlow(SkillType skillType) {
        super(skillType);
        damageBackMod = skillType.getCustomParam("counterblow_value", double.class) / 100D;
    }

    @Override
    public void onActivate(ServerPlayer serverPlayer) {
        this.serverPlayer = serverPlayer;
        XlvsCoreCommon.EVENT_BUS.register(this);
    }

    @Override
    protected void onDeactivated(ServerPlayer serverPlayer) {
        super.onDeactivated(serverPlayer);
        XlvsCoreCommon.EVENT_BUS.unregister(this);
    }

    @SuppressWarnings("unused")
    @SubscribeEvent
    public void event(EntityDamagePlayerEvent.Post event) {
        if(event.getServerPlayer() == serverPlayer) {
            DamageUtils.damageEntity(serverPlayer.getEntityPlayer(), event.getEntityLivingBase(), (float) (event.getAmount() * damageBackMod), true);
        }
    }

    @Nonnull
    @Override
    public SkillCounterBlow clone() {
        return new SkillCounterBlow(getSkillType());
    }
}
