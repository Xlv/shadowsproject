package ru.xlv.core.player.character.skill.dd;

import ru.xlv.core.common.player.character.CharacterAttributeMod;
import ru.xlv.core.common.player.character.CharacterAttributeType;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.core.player.character.skill.SkillUtils;
import ru.xlv.core.util.ScheduledConsumableTask;
import ru.xlv.mochar.player.character.skill.ISkillUpdatable;
import ru.xlv.mochar.player.character.skill.Skill;
import ru.xlv.mochar.player.character.skill.SkillType;

import javax.annotation.Nonnull;
import java.util.Set;
import java.util.UUID;

public class SkillCorrosion extends Skill implements ISkillUpdatable {

    private static final UUID MOD_UUID = UUID.randomUUID();

    private final ScheduledConsumableTask<ServerPlayer> scheduledConsumableTask;

    public SkillCorrosion(SkillType skillType) {
        super(skillType);
        double radius = skillType.getCustomParam("range", double.class);
        double protectMod = 1D + skillType.getCustomParam("protect_add_enemy_all_value", double.class) / 100;
        CharacterAttributeMod characterAttributeMod = CharacterAttributeMod.builder()
                .attributeType(CharacterAttributeType.DAMAGE_BASE_PROTECTION)
                .valueMod(protectMod)
                .period(1000L)
                .uuid(MOD_UUID)
                .build();
        scheduledConsumableTask = new ScheduledConsumableTask<>(1000L, serverPlayer -> {
            Set<ServerPlayer> enemyServerPlayersAround = SkillUtils.getEnemyServerPlayersAround(serverPlayer, radius);
            enemyServerPlayersAround.forEach(serverPlayer1 -> serverPlayer1.getSelectedCharacter().addAttributeMod(characterAttributeMod, true));
        });
    }

    @Override
    public void update(ServerPlayer serverPlayer) {
        scheduledConsumableTask.update(serverPlayer);
    }

    @Nonnull
    @Override
    public SkillCorrosion clone() {
        return new SkillCorrosion(getSkillType());
    }
}
