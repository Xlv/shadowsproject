package ru.xlv.core.player.character.skill.dd;

import ru.xlv.core.common.player.character.CharacterAttributeMod;
import ru.xlv.core.common.player.character.CharacterAttributeType;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.core.util.ScheduledConsumableTask;
import ru.xlv.mochar.player.character.skill.ISkillUpdatable;
import ru.xlv.mochar.player.character.skill.Skill;
import ru.xlv.mochar.player.character.skill.SkillType;

import javax.annotation.Nonnull;
import java.util.UUID;

public class SkillCommando extends Skill implements ISkillUpdatable {

    private static final UUID MOD_UUID = UUID.randomUUID();
    private static final UUID MOD_UUID1 = UUID.randomUUID();

    private final ScheduledConsumableTask<ServerPlayer> scheduledConsumableTask;

    public SkillCommando(SkillType skillType) {
        super(skillType);
        double recoilMod = 1D + skillType.getCustomParam("shotrecoil_add_value", double.class) / 100D;
        double speedMod = 1D + skillType.getCustomParam("speed_add_self_value", double.class) / 100D;
        CharacterAttributeMod characterAttributeMod = CharacterAttributeMod.builder()
                .attributeType(CharacterAttributeType.WEAPON_RECOIL_MOD)
                .valueMod(recoilMod)
                .period(1000L)
                .uuid(MOD_UUID)
                .build();
        CharacterAttributeMod characterAttributeMod1 = CharacterAttributeMod.builder()
                .attributeType(CharacterAttributeType.MOVE_SPEED)
                .valueMod(speedMod)
                .period(1000L)
                .uuid(MOD_UUID1)
                .build();
        scheduledConsumableTask = new ScheduledConsumableTask<>(1000L, serverPlayer -> {
            serverPlayer.getSelectedCharacter().addAttributeMod(characterAttributeMod, true);
            serverPlayer.getSelectedCharacter().addAttributeMod(characterAttributeMod1, true);
        });
    }

    @Override
    public void update(ServerPlayer serverPlayer) {
        scheduledConsumableTask.update(serverPlayer);
    }

    @Nonnull
    @Override
    public SkillCommando clone() {
        return new SkillCommando(getSkillType());
    }
}
