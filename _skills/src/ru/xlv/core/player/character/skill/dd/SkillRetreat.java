package ru.xlv.core.player.character.skill.dd;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraftforge.common.MinecraftForge;
import ru.xlv.core.common.XlvsCoreCommon;
import ru.xlv.core.common.event.PlayerShootsWeaponEvent;
import ru.xlv.core.common.player.character.CharacterAttributeMod;
import ru.xlv.core.common.player.character.CharacterAttributeType;
import ru.xlv.core.event.PlayerDamageEntityEvent;
import ru.xlv.core.event.PlayerUsedSkillEvent;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.mochar.player.character.skill.ActivableSkill;
import ru.xlv.mochar.player.character.skill.SkillType;

import javax.annotation.Nonnull;

public class SkillRetreat extends ActivableSkill {

    private final double speedMod;

    private ServerPlayer serverPlayer;

    public SkillRetreat(SkillType skillType) {
        super(skillType);
        speedMod = 1D + skillType.getCustomParam("speed_add_self_value", double.class) / 100D;
    }

    @Override
    public void onActivate(ServerPlayer serverPlayer) {
        serverPlayer.getSelectedCharacter().addAttributeMod(CharacterAttributeMod.builder()
                .attributeType(CharacterAttributeType.MOVE_SPEED)
                .valueMod(speedMod)
                .period(getSkillType().getDuration())
                .build());
        serverPlayer.getEntityPlayer().addPotionEffect(new PotionEffect(Potion.invisibility.id, (int) (20 * (getSkillType().getDuration() / 1000)), 1));
        this.serverPlayer = serverPlayer;
        MinecraftForge.EVENT_BUS.register(this);
        XlvsCoreCommon.EVENT_BUS.register(this);
    }

    @SuppressWarnings("unused")
    @SubscribeEvent
    public void event(PlayerShootsWeaponEvent event) {
        if(event.getEntityPlayer() == serverPlayer.getEntityPlayer()) {
            deactivate();
        }
    }

    @SuppressWarnings("unused")
    @SubscribeEvent
    public void event(PlayerDamageEntityEvent.Pre event) {
        if(event.getServerPlayer() == serverPlayer) {
            deactivate();
        }
    }

    @SuppressWarnings("unused")
    @SubscribeEvent
    public void event(PlayerUsedSkillEvent event) {
        if(event.getServerPlayer() == serverPlayer) {
            deactivate();
        }
    }

    private void deactivate() {
        deactivateSkill(serverPlayer);
        MinecraftForge.EVENT_BUS.unregister(this);
        XlvsCoreCommon.EVENT_BUS.unregister(this);
    }

    @Nonnull
    @Override
    public SkillRetreat clone() {
        return new SkillRetreat(getSkillType());
    }
}
