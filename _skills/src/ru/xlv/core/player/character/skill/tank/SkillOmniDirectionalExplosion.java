package ru.xlv.core.player.character.skill.tank;

import ru.xlv.core.common.player.character.CharacterAttributeMod;
import ru.xlv.core.common.player.character.CharacterAttributeType;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.core.player.character.skill.SkillUtils;
import ru.xlv.core.util.DamageUtils;
import ru.xlv.mochar.player.character.skill.ActivableSkill;
import ru.xlv.mochar.player.character.skill.SkillType;

import javax.annotation.Nonnull;
import java.util.Set;

public class SkillOmniDirectionalExplosion extends ActivableSkill {

    private final double explosionDamage;
    private final double radius;

    private final CharacterAttributeMod characterAttributeMod;

    public SkillOmniDirectionalExplosion(SkillType skillType) {
        super(skillType);
        final double damageMod = 1D + skillType.getCustomParam("income_explosion_damage_value", double.class) / 100;
        explosionDamage = skillType.getCustomParam("explosion_damage", double.class);
        radius = skillType.getCustomParam("explosion_range", double.class);
        characterAttributeMod = CharacterAttributeMod.builder()
                .attributeType(CharacterAttributeType.EXPLOSION_PROTECTION)
                .valueMod(damageMod)
                .period(skillType.getDuration())
                .build();
    }

    @Override
    public void onActivate(ServerPlayer serverPlayer) {
        serverPlayer.getSelectedCharacter().addAttributeMod(characterAttributeMod, true);
        Set<ServerPlayer> enemyServerPlayersAround = SkillUtils.getEnemyServerPlayersAround(serverPlayer, radius);
        enemyServerPlayersAround.forEach(serverPlayer1 -> DamageUtils.damageEntity(serverPlayer.getEntityPlayer(), serverPlayer1.getEntityPlayer(), CharacterAttributeType.EXPLOSION_DAMAGE, explosionDamage));
    }

    @Nonnull
    @Override
    public SkillOmniDirectionalExplosion clone() {
        return new SkillOmniDirectionalExplosion(getSkillType());
    }
}
