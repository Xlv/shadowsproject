package ru.xlv.trades.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import ru.xlv.core.common.network.IPacketOut;

import java.io.IOException;

@NoArgsConstructor
public class PacketTradeConfirm implements IPacketOut {

    private String targetName;

    public PacketTradeConfirm(String targetName) {
        this.targetName = targetName;
    }

    @Override
    public void write(ByteBufOutputStream bbos) throws IOException {
        bbos.writeUTF(targetName);
    }
}
