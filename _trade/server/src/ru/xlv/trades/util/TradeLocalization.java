package ru.xlv.trades.util;

import lombok.Getter;
import ru.xlv.core.common.util.config.Configurable;
import ru.xlv.core.util.Localization;

import java.io.File;

@Getter
@Configurable
public class TradeLocalization extends Localization {

    private String responseTradeInviteResultSuccessMessage = "responseTradeInviteResultSuccessMessage";
    private String responseTradeInviteResultPlayerNotFoundMessage = "responseTradeInviteResultPlayerNotFoundMessage";
    private String responseTradeInviteResultUnknownMessage = "responseTradeInviteResultUnknownMessage";
    private String responseTradeInviteResultCooldownMessage = "responseTradeInviteResultCooldownMessage";

    private String playerCancelledTradeMessage = "{0} cancelled the trade.";
    private String youCancelledTradeMessage = "You successfully cancelled the trade.";
    private String tradeConfirmedMessage = "tradeConfirmedMessage";

    @Override
    public File getConfigFile() {
        return new File("config/trade/localization.json");
    }
}
