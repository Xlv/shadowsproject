package ru.xlv.gun.render.guns.machinegun;

import cpw.mods.fml.client.FMLClientHandler;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.settings.GameSettings;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.IItemRenderer;
import net.minecraftforge.client.model.IModelCustom;
import net.minecraftforge.client.model.obj.WavefrontObject;
import org.lwjgl.opengl.GL11;
import ru.xlv.core.model.ModelLoader;
import ru.xlv.gun.item.AttachmentType;
import ru.xlv.gun.item.ItemWeapon;
import ru.xlv.gun.item.Items;
import ru.xlv.gun.render.ModelWrapperDisplayList;
import ru.xlv.gun.render.RenderAttachments;
import ru.xlv.gun.render.RenderWeaponThings;
import ru.xlv.gun.util.Utils;

public class RPKRender implements IItemRenderer {
	
	  public static final ResourceLocation tex = new ResourceLocation("batmod", "models/guns/rpk/texture.png");
	  public static final ResourceLocation mag = new ResourceLocation("batmod", "models/guns/rpk/mag.png");
	    private static RenderAttachments renderAtt;
	    
		Minecraft mc = Minecraft.getMinecraft();
		RenderWeaponThings rWT = new RenderWeaponThings();
		public static IModelCustom modelpreload;
		public static IModelCustom model;
	
	public RPKRender() {
		renderAtt = new RenderAttachments();
		model = ModelLoader.loadModel(new ResourceLocation("batmod:models/guns/rpk/model.sp"));
        model = new ModelWrapperDisplayList((WavefrontObject)this.model);
	}
	public boolean handleRenderType(ItemStack is, ItemRenderType type) {
		switch (type) {
		case ENTITY:
			return true;
		case EQUIPPED:
			return true;
		case EQUIPPED_FIRST_PERSON:
			return true;
		default:
			return false;
		}
	}
	public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack is, ItemRendererHelper helper) {
		switch (type){
		case EQUIPPED_FIRST_PERSON: return true;
		case ENTITY:
			return false;
		case EQUIPPED:
			return false;
		default:
			return false;
		}
		
	}


    
    
	
	
	public void renderItem(ItemRenderType type, ItemStack is, Object ... data) {
		
		GameSettings gameSettings = FMLClientHandler.instance().getClient().gameSettings;
		
		ItemWeapon weapon = (ItemWeapon)is.getItem();
		int shootTimer = (int)weapon.otdachaTimer;
		//анимация прицеливания
		//анимация прицеливания
		//анимация бега
		//анимация выстрела
		//анимация перезарядки
	    //анимация ливинг
		float aimAnim = rWT.lastRunProgress + (rWT.runProgress - rWT.lastRunProgress) * RenderWeaponThings.renderTicks;
	
		float runAnim = rWT.lastRun1Progress + (rWT.run1Progress - rWT.lastRun1Progress)* RenderWeaponThings.renderTicks;
	
		float shootAnim = rWT.lastRun2Progress + (rWT.run2Progress - rWT.lastRun2Progress)* RenderWeaponThings.renderTicks;

	    float reloadAnim = rWT.lastRun3Progress + (rWT.run3Progress - rWT.lastRun3Progress)* RenderWeaponThings.renderTicks;
	 
	    float livingAnim = rWT.lastRun4Progress + (rWT.run4Progress - rWT.lastRun4Progress)* RenderWeaponThings.renderTicks;
		switch (type) {
		
		case EQUIPPED_FIRST_PERSON: {
			
			rWT.doAnimations();
 			
			float x = 0;
			float y = 0;
			float z = 0;
			GL11.glPushMatrix();
			RenderHelper.disableStandardItemLighting();
			GL11.glColor4f(0.9f, 0.9f, 0.9f, 1f);
			if(AttachmentType.checkAttachment(is,"scope", Items.pkas)){
				//gameSettings.fovSetting = 90 - 20 * aimAnim;
				x = -0.156F;
				y = -0.181F;
				z = 0.111F;
			}
			if(AttachmentType.checkAttachment(is,"scope", Items.kobra)){
				//gameSettings.fovSetting = 90 -20 * aimAnim;
				x = -0.053F;
				y = -0.148F;
				z = 0.026F;
			}
			if(AttachmentType.checkAttachment(is,"scope", Items.pso)){
				//gameSettings.fovSetting = 90 - 60 * aimAnim;
				x = 0.208F;
				y = -0.163F;
				z = -0.175F;
			}
			float dSWS = 1F;
			float f1 = 1F;
			if(Utils.isPlayerAiming()){
			   dSWS = 0.2F;
			   f1 = 1F;
			}
			float fov = gameSettings.fovSetting/10000;
			if(!Utils.isPlayerAiming())
				GL11.glTranslatef(0,0.05F*livingAnim - aimAnim/30,0);
			else
				GL11.glTranslatef(0,-0.6F*livingAnim*fov,0);
			GL11.glRotatef(65*reloadAnim, 0, 0, 1);
			GL11.glRotatef(-25*runAnim, 1, 0, 0);
			GL11.glRotatef(-15*runAnim, 0, 0, 1);
			GL11.glTranslatef(-0.4F*runAnim, -0.8F*runAnim, 0.5F*runAnim);
			GL11.glRotated(0.75F*aimAnim, 1.0, 0.0, 1.0);
			GL11.glTranslatef(-0.562F * aimAnim+ x*aimAnim, 0.135F * aimAnim + y*aimAnim, 0.070F * aimAnim + z*aimAnim);
			GL11.glRotated(4.0F*shootAnim*dSWS, 1.0, 0.0, 1.0);
			GL11.glRotated(-1.0F*shootAnim*dSWS, 0.0, 1.0, 1.0);
			GL11.glTranslatef(-0.085F * shootAnim*dSWS, 0.1F*shootAnim*dSWS, 0.084F * shootAnim*dSWS);
			
			GL11.glPushMatrix();
			GL11.glScalef(1.39F, 1.39F, 1.39F);
			
			GL11.glRotated(0.0, 1.0, 0.0, 0.0);
			GL11.glRotated(45.0, 0.0, 1.0, 0.0);
			GL11.glRotated(0.0, 0.0, 0.0, 1.0);
			//GL11.glTranslatef(-0.4F * aimAnim, 0.203F * aimAnim, -0.570F * aimAnim);
			GL11.glTranslatef(-0.42F, 0.568F, -0.235F);
			NBTTagCompound tagz = is.getTagCompound();
			if(tagz != null) {	
				if(tagz.getString("scope").length() > 2 || tagz.getString("stvol").length() > 2 || tagz.getString("laser").length() > 2) {
					renderAttachments(type, is);
				}
			}
			
			ru.xlv.core.util.Utils.bindTexture(tex);
			model.renderPart("gun");
			ru.xlv.core.util.Utils.bindTexture(mag);
			model.renderPart("mag");
			if(shootTimer == weapon.shootSpeed-1){
				GL11.glPushMatrix();
				GL11.glScalef(0.13F, 0.13F, 0.13F);
				GL11.glRotatef(90, 0, 1, 0);
				GL11.glTranslatef(-0.1F, 3.2F, 30F);
				rWT.flash();
				GL11.glPopMatrix();
			}	
			
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR); 
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
			
			GL11.glPopMatrix();
			
			GL11.glPushMatrix();
			rWT.renderRightArm(0.3F, -1.8F, 0.92F, -90, -5, -25, 2.1F);
			GL11.glPopMatrix();
			GL11.glPushMatrix();
			rWT.renderLeftArm(-0.72F, -0.49F, -0.38F, 20, 27, -80, 2.3F);
			GL11.glPopMatrix();
			
		    
			GL11.glPopMatrix();
		}
		break;
		case EQUIPPED: {
			GL11.glRotatef(35.0F, 1.0F, 0.0F, 0.0F);
		    GL11.glRotatef(-73.0F, 0.0F, 1.0F, 0.0F);
		    GL11.glRotatef(21.0F, 0.0F, 0.0F, 1.0F);
		    GL11.glTranslatef(0.2F, -0.05F, -0.5F);
	        GL11.glRotatef(-90.0F, 1.0F, 0.0F, 0.0F);
	        
			renderEntityAndEquipped(type, is);
		}
		break;
		case ENTITY: {
			
			 
			renderEntityAndEquipped(type, is);
		}
		break;
		default:
			break;
		}
		
		
	}
	public void renderAttachments(ItemRenderType type, ItemStack is){
		NBTTagCompound nbtstack = is.getTagCompound();
		if(is != null) {
			if(nbtstack != null) {
				if(is.stackTagCompound != null) {
						if(nbtstack.getString("scope").length() > 2) {
							if(ItemStack.loadItemStackFromNBT(is.stackTagCompound.getCompoundTag("scope")).getItem() == Items.pkas){
								GL11.glPushMatrix();
								GL11.glTranslatef(0.1F, -0.07F, 0.01F);
								renderAtt.renderPKAS(Utils.isPlayerAiming());
								GL11.glPopMatrix();
							}
							if(ItemStack.loadItemStackFromNBT(is.stackTagCompound.getCompoundTag("scope")).getItem() == Items.kobra){
								GL11.glPushMatrix();
								GL11.glTranslatef(0.15F, -0.08F, -0.005F);
								renderAtt.renderKobra(Utils.isPlayerAiming());
								GL11.glPopMatrix();
							}
							if(ItemStack.loadItemStackFromNBT(is.stackTagCompound.getCompoundTag("scope")).getItem() == Items.pso){
								GL11.glPushMatrix();
								GL11.glTranslatef(0.1F, -0.1F, -0.038F);
								//GL11.glScalef(0.9F, 0.9F, 0.9F);
								renderAtt.renderPSO(Utils.isPlayerAiming());
								GL11.glPopMatrix();
							}
						
						}
						if(nbtstack.getString("stvol").length() > 2 && ItemStack.loadItemStackFromNBT(is.stackTagCompound.getCompoundTag("stvol")).getItem() == Items.pbs4
								){	
							 GL11.glPushMatrix();
							 GL11.glTranslatef(0.47F, -0.215F, -0.05F);
							 renderAtt.renderPBS4();
							 GL11.glPopMatrix();
							
						}		
						if(nbtstack.getString("grip").length() > 2 && ItemStack.loadItemStackFromNBT(is.stackTagCompound.getCompoundTag("grip")).getItem() == Items.laser){
							 GL11.glPushMatrix();
							 GL11.glScalef(0.05F, 0.05F, 0.05F);
							 GL11.glTranslatef(15.7F, -12.8F, -2.07F);
							 renderAtt.renderLaser(type);
							 GL11.glPopMatrix();
						 }
				}
			}
		}

	}
    public void renderEntityAndEquipped(ItemRenderType type, ItemStack is){
    	
    	GL11.glPushMatrix();
		ru.xlv.core.util.Utils.bindTexture(tex);
		GL11.glScalef(0.5F, 0.5F, 0.5F);
		GL11.glRotatef(90.0F, 1.0F, 0.0F, 0.0F);
		GL11.glTranslatef(0, 0, 0);
		model.renderPart("gun");
		ru.xlv.core.util.Utils.bindTexture(mag);
		model.renderPart("mag");
		NBTTagCompound tagz = is.getTagCompound();
		if(tagz != null) {	
			if(tagz.getString("scope").length() > 2 || tagz.getString("stvol").length() > 2 || tagz.getString("laser").length() > 2) {
				renderAttachments(type, is);
			}
		}
		
		GL11.glPopMatrix();
    }

}
