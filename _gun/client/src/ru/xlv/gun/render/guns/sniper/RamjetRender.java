package ru.xlv.gun.render.guns.sniper;

import cpw.mods.fml.client.FMLClientHandler;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.settings.GameSettings;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.IItemRenderer;
import net.minecraftforge.client.model.IModelCustom;
import net.minecraftforge.client.model.obj.WavefrontObject;
import org.lwjgl.opengl.GL11;
import ru.xlv.core.model.ModelLoader;
import ru.xlv.gun.item.AttachmentType;
import ru.xlv.gun.item.ItemWeapon;
import ru.xlv.gun.item.Items;
import ru.xlv.gun.render.ModelWrapperDisplayList;
import ru.xlv.gun.render.RenderAttachments;
import ru.xlv.gun.render.RenderWeaponThings;
import ru.xlv.gun.util.Utils;

public class RamjetRender implements IItemRenderer {
	
	
	
	public IModelCustom scopelinsa = ModelLoader.loadModel(new ResourceLocation("batmod", "models/attachments/reflex/modellinsa.sp"));
	//public static final IModelCustom model = spproject.loaderModel.Modell.loadModel(new ResourceLocation("batmod", "models/guns/ak74.sp"));
	
    public static final ResourceLocation tex = new ResourceLocation("batmod", "models/guns/ramjet/texture.png");
    
    //public static final ResourceLocation stvol = new ResourceLocation("batmod", "models/guns/vssk/silencer.png");
    RenderWeaponThings rWT = new RenderWeaponThings();
    private static RenderAttachments renderAtt;
   
	Minecraft mc = Minecraft.getMinecraft();
	private boolean reloading;
	private static float handBolt = 0F;
	private static float handBolt1 = 0F;
	private static float handBolt2 = 0F;
	IModelCustom model;
	
	
	public RamjetRender() {
		
		renderAtt = new RenderAttachments();
		this.model = ModelLoader.loadModel(new ResourceLocation("batmod:models/guns/ramjet/model.sp"));
        this.model = new ModelWrapperDisplayList((WavefrontObject)this.model);
        scopelinsa = new ModelWrapperDisplayList((WavefrontObject)this.scopelinsa);
	}
	public boolean handleRenderType(ItemStack is, ItemRenderType type) {
		switch (type) {
		case ENTITY:
			return true;
		case EQUIPPED:
			return true;
		case EQUIPPED_FIRST_PERSON:
			return true;
		default:
			return false;
		}
	}
	public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack is, ItemRendererHelper helper) {
		switch (type){
		case EQUIPPED_FIRST_PERSON: return true;
		case ENTITY:
			return false;
		case EQUIPPED:
			return false;
		default:
			return false;
		}
		
	}


    
    
	
	
	public void renderItem(ItemRenderType type, ItemStack is, Object ... data) {
		
		GameSettings gameSettings = FMLClientHandler.instance().getClient().gameSettings;

		ItemWeapon weapon = (ItemWeapon)is.getItem();
		int shootTimer = (int)weapon.otdachaTimer;
		//анимация прицеливания
		//анимация прицеливания
		//анимация бега
		//анимация выстрела
		//анимация перезарядки
	    //анимация ливинг
		float aimAnim = rWT.lastRunProgress + (rWT.runProgress - rWT.lastRunProgress) * RenderWeaponThings.renderTicks;
	
		float runAnim = rWT.lastRun1Progress + (rWT.run1Progress - rWT.lastRun1Progress)* RenderWeaponThings.renderTicks;
	
		float shootAnim = rWT.lastRun2Progress + (rWT.run2Progress - rWT.lastRun2Progress)* RenderWeaponThings.renderTicks;

	    float reloadAnim = rWT.lastRun3Progress + (rWT.run3Progress - rWT.lastRun3Progress)* RenderWeaponThings.renderTicks;
	 
	    float livingAnim = rWT.lastRun4Progress + (rWT.run4Progress - rWT.lastRun4Progress)* RenderWeaponThings.renderTicks;
	      //анимация перезарядки болта
	    float boltAnim = rWT.lastRun5Progress + (rWT.run5Progress - rWT.lastRun5Progress);
	
		switch (type) {
		case EQUIPPED_FIRST_PERSON: {
			rWT.doAnimations();
 			if(shootTimer == weapon.shootSpeed-1){
			GL11.glPushMatrix();
			
			GL11.glRotated(1.0F*shootAnim, 1.0, 0.0, 1.0); 
			GL11.glTranslatef(0.1F * aimAnim, 0.203F * aimAnim, -1.570F * aimAnim);
			GL11.glScalef(0.25F, 0.25F, 0.25F);
			GL11.glRotatef(135, 0, 1, 0);
			GL11.glTranslatef(-0.5F, 3.7F, 65F);
			rWT.flash();
			GL11.glPopMatrix();
			}		
			float x = 0;
			float y = 0;
			float z = 0;
			GL11.glPushMatrix();
			RenderHelper.disableStandardItemLighting();
			GL11.glColor4f(0.9f, 0.9f, 0.9f, 1f);
			if(AttachmentType.checkAttachment(is,"scope", Items.leupold)){
				//gameSettings.fovSetting = 90 - 40 * aimAnim;
				x = 0.004F;
				y = -0.095F;
				z = 0.0F;
			}
			if(AttachmentType.checkAttachment(is,"scope", Items.susat)){
				//gameSettings.fovSetting = 90 - 40 * aimAnim;
				x = -0.173F;
				y = -0.19F;
				z = 0.18F;
			}
			if(AttachmentType.checkAttachment(is,"scope", Items.barska)){
				//gameSettings.fovSetting = 90 - 40 * aimAnim;
				x = -0.196F;
				y = -0.105F;
				z = 0.193F;
			}
			if(AttachmentType.checkAttachment(is,"scope", Items.trAc)){
				//gameSettings.fovSetting = 90 - 40 * aimAnim;
				x = 0.051F;
				y = -0.065F;
				z = -0.05F;
			}
			float dSWS = 1F;
			float f1 = 1F;
			if((is.getTagCompound().getString("scope") != null || is.getTagCompound().getString("planka") != null) && Utils.isPlayerAiming()){
			   dSWS = 0.4F;
			   f1 = 1F;
			}
			float fov = gameSettings.fovSetting/10000;
			if(!Utils.isPlayerAiming())
				GL11.glTranslatef(0,0.05F*livingAnim - aimAnim/30,0);
			else
				GL11.glTranslatef(0,-0.6F*livingAnim*fov,0);
			GL11.glRotatef(65*reloadAnim, 0, 0, 1);
			GL11.glRotatef(-25*runAnim, 1, 0, 0);
			//GL11.glRotatef(-15*runAnim, 0, 0, 1);
			GL11.glTranslatef(-0.4F*runAnim, -0.8F*runAnim, 0.5F*runAnim);
			GL11.glRotated(-0.95F*aimAnim, 0.0, 0.0, 1.0);
			GL11.glTranslatef(-0.128F * aimAnim+ x*aimAnim, -0.052F * aimAnim + y*aimAnim, -0.53F * aimAnim + z*aimAnim);
			GL11.glRotated(4.0F*shootAnim*dSWS, 1.0, 0.0, 1.0);
			GL11.glRotated(-1.0F*shootAnim*dSWS, 0.0, 1.0, 1.0);
			GL11.glTranslatef(-0.185F * shootAnim*dSWS, 0.2F*shootAnim*dSWS, 0.184F * shootAnim*dSWS);
			
			GL11.glPushMatrix();
			GL11.glScalef(1.05F, 1.05F, 1.05F);
			GL11.glRotated(0.0, 1.0, 0.0, 0.0);
			GL11.glRotated(45.0, 0.0, 1.0, 0.0);
			
		
			GL11.glTranslatef(-0.42F, 0.72F, -0.345F);
			
			GL11.glPushMatrix(); 
			
		    //GL11.glMatrixMode(GL11.GL_TEXTURE); 
			GL11.glPushMatrix(); 
			
			ru.xlv.core.util.Utils.bindTexture(new ResourceLocation("batmod:textures/models/attachments/reflex/blank.png"));
			int tex1 = mc.renderEngine.getTexture(new ResourceLocation("batmod:textures/models/attachments/reflex/blank.png")).getGlTextureId();
			GL11.glViewport(0, 0, mc.displayWidth, mc.displayHeight); 
			
			GL11.glBindTexture(GL11.GL_TEXTURE_2D, tex1); 
			int width = mc.displayWidth; 
			int height = mc.displayHeight; 
			
			GL11.glDisable(GL11.GL_LIGHTING);
			float zoom = 250.5F; 
			if(Utils.isPlayerAiming()){
			zoom = 4.5F; 
			}
			int size = (int) (Math.min(width, height) / zoom); 
			GL11.glCopyTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGB, width / 2 - size / 2, height / 2 - size / 2, size, size, 0); 
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR); 
			GL11.glPopMatrix(); 
			GL11.glScalef(0.235F, 0.235F, 0.235F);
			GL11.glTranslatef(-2.5F, 4.86F, 0.46F);
			GL11.glRotatef(180, 1, 0, 0);
			GL11.glMatrixMode(GL11.GL_MODELVIEW); 
			GL11.glDisable(GL11.GL_CULL_FACE); 
			scopelinsa.renderAll();
			GL11.glEnable(GL11.GL_CULL_FACE); 
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST); 
			GL11.glColor4f(1, 1, 1, 0.49F);
			ru.xlv.core.util.Utils.bindTexture(new ResourceLocation("batmod:textures/gui/sigsight.png"));
			GL11.glEnable(GL11.GL_BLEND); 
			GL11.glDisable(GL11.GL_CULL_FACE); 
			scopelinsa.renderAll();
			GL11.glPopMatrix();
			GL11.glDisable(GL11.GL_BLEND); 
			GL11.glEnable(GL11.GL_LIGHTING);
			GL11.glEnable(GL11.GL_CULL_FACE); 
			
			renderAttachments(type, is);
			ru.xlv.core.util.Utils.bindTexture(tex);
			model.renderAll();
		
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR); 
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
			
			GL11.glPopMatrix();
			
			GL11.glPushMatrix();
			rWT.renderRightArm(0.42F, -1.75F, 0.97F, -90, -5, -35, 1.9F);
			GL11.glPopMatrix();
			GL11.glPushMatrix();
			rWT.renderLeftArm(-0.7F, -0.45F, -0.31F, 10, 25, -75, 2.2F);
			GL11.glPopMatrix();
			
			
			GL11.glPopMatrix();
		}
		break;
		case EQUIPPED: {
			
			GL11.glRotatef(35.0F, 1.0F, 0.0F, 0.0F);
		    GL11.glRotatef(-73.0F, 0.0F, 1.0F, 0.0F);
		    GL11.glRotatef(21.0F, 0.0F, 0.0F, 1.0F);
		    GL11.glTranslatef(0.2F, -0.05F, -0.5F);
	        GL11.glRotatef(-90.0F, 1.0F, 0.0F, 0.0F);
	        GL11.glScalef(1.2F, 1.2F, 1.2F);
	        renderEntityAndEquipped(type, is);
		}
		break;
		case ENTITY: {
			renderEntityAndEquipped(type, is);
		}
		break;
		default:
			break;
		}
		
		
	}
	public void renderAttachments(ItemRenderType type, ItemStack is){
		
			if(AttachmentType.checkAttachment(is,"scope", Items.leupold)){
			GL11.glPushMatrix();
			GL11.glScalef(1.2F, 1.2F, 1.2F);
			GL11.glTranslatef(-0.05F, -0.065F, 0.094F);
			renderAtt.renderLeupold(Utils.isPlayerAiming());
			GL11.glPopMatrix();
			}
			else if(AttachmentType.checkAttachment(is,"scope", Items.susat)){
				GL11.glPushMatrix();
				GL11.glScalef(1.2F, 1.2F, 1.2F);
				GL11.glTranslatef(-0.01F, -0.06F, 0.096F);
				renderAtt.renderSUSAT(Utils.isPlayerAiming());
				GL11.glPopMatrix();
			}
			else if(AttachmentType.checkAttachment(is,"scope", Items.trAc)){
				GL11.glPushMatrix();
				GL11.glScalef(1.2F, 1.2F, 1.2F);
				GL11.glTranslatef(0.05F, -0.06F, 0.0945F);
				renderAtt.renderTRAC(Utils.isPlayerAiming());
				GL11.glPopMatrix();
			}
			else if(AttachmentType.checkAttachment(is,"scope", Items.barska)){
				GL11.glPushMatrix();
				GL11.glScalef(1.2F, 1.2F, 1.2F);
				GL11.glTranslatef(-0.05F, -0.07F, 0.1F);
				renderAtt.renderBarska(Utils.isPlayerAiming());
				GL11.glPopMatrix();
			}
			if(AttachmentType.checkAttachment(is,"grip", Items.laser)){
				 GL11.glPushMatrix();
				 GL11.glScalef(0.05F, 0.05F, 0.05F);
				 GL11.glTranslatef(23.7F, -8.8F, -1.97F);
	            renderAtt.renderLaser(type);
	            GL11.glPopMatrix();
			 }
	}
    public void renderEntityAndEquipped(ItemRenderType type, ItemStack is){
    	
    	GL11.glPushMatrix();
		GL11.glScalef(0.3F, 0.3F, 0.3F);
		GL11.glRotatef(90.0F, 1.0F, 0.0F, 0.0F);
		GL11.glTranslatef(0, 0, 0);
		NBTTagCompound tagz = is.getTagCompound();
		if(tagz != null) {	
			if(tagz.getString("scope").length() > 0 || tagz.getString("stvol").length() > 0 || tagz.getString("laser").length() > 0) {
				renderAttachments(type, is);
			}
		}
		ru.xlv.core.util.Utils.bindTexture(tex);
		model.renderAll();
		GL11.glPopMatrix();
    }
    
}
