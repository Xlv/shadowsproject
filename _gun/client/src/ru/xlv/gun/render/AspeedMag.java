package ru.xlv.gun.render;

import net.minecraft.client.Minecraft;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.IItemRenderer;
import net.minecraftforge.client.model.IModelCustom;
import net.minecraftforge.client.model.obj.WavefrontObject;
import org.lwjgl.opengl.GL11;

public class AspeedMag implements IItemRenderer {
	
	
	

	Minecraft mc = Minecraft.getMinecraft();
	RenderWeaponThings rWT = new RenderWeaponThings();
	public static IModelCustom model;
	public int typeGun = 0;
	
	public AspeedMag(int type) {
		this.typeGun = type;
		this.model = ResourcesSPLocation.aspeedmag;
        this.model = new ModelWrapperDisplayList((WavefrontObject)this.model);
	}
	public boolean handleRenderType(ItemStack is, ItemRenderType type) {
		switch (type) {
		case ENTITY:
			return false;
		case EQUIPPED:
			return true;
		case EQUIPPED_FIRST_PERSON:
			return true;
		default:
			return false;
		}
	}
	public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack is, ItemRendererHelper helper) {
		switch (type){
		case EQUIPPED_FIRST_PERSON: return true;
		case ENTITY:
			return false;
		case EQUIPPED:
			return false;
		default:
			return false;
		}
		
	}
	
	public void renderItem(ItemRenderType type, ItemStack is, Object ... data) {
		switch (type) {
		case EQUIPPED_FIRST_PERSON: {
			GL11.glPushMatrix();
	    	if(typeGun == 2) {
	    		ru.xlv.core.util.Utils.bindTexture(AspeedGrayRender.tex);
	    	}
	    	if(typeGun == 1) {
	    		ru.xlv.core.util.Utils.bindTexture(AspeedRender.tex);
	    	}
			GL11.glTranslatef(1f, 1.3f, 1.0f);
			GL11.glRotatef(-100, 0, 1, 0);
			model.renderAll();
			GL11.glPopMatrix();
		}
		break;
		case EQUIPPED: {
			GL11.glRotatef(35.0F, 1.0F, 0.0F, 0.0F);
		    GL11.glRotatef(-73.0F, 0.0F, 1.0F, 0.0F);
		    GL11.glRotatef(21.0F, 0.0F, 0.0F, 1.0F);
		    GL11.glTranslatef(-0.48F, 0.19F, -0.5F);
	        GL11.glRotatef(-90.0F, 1.0F, 0.0F, 0.0F);
			renderEntityAndEquipped(type, is);
		}
		break;
		case ENTITY: {
			renderEntityAndEquipped(type, is);
		}
		break;
		default:
			break;
		}
	}
    public void renderEntityAndEquipped(ItemRenderType type, ItemStack is){

    	GL11.glPushMatrix();
    	if(typeGun == 2) {
    		ru.xlv.core.util.Utils.bindTexture(AspeedGrayRender.tex);
    	}
    	if(typeGun == 1) {
    		ru.xlv.core.util.Utils.bindTexture(AspeedRender.tex);
    	}
		GL11.glScalef(0.2F, 0.2F, 0.2F);
		GL11.glRotatef(90.0F, 1.0F, 0.0F, 0.0F);
		GL11.glRotated(180.0, 0.0, 1.0, 0.0);
		GL11.glTranslatef(-2f, 1.2f, -0.5f);
		model.renderAll();
		GL11.glPopMatrix();
    }

}
