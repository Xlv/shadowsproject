package ru.xlv.gun.render.armor;

import net.minecraft.client.renderer.entity.RenderPlayer;
import net.minecraft.item.Item;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public interface IArmorRenderer {

    Map<Class<? extends Item>, IArmorRenderer> registry = Collections.synchronizedMap(new HashMap<>());

    void init();

    void render(RenderPlayer renderPlayer);
}
