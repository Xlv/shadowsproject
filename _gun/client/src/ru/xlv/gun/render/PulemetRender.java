package ru.xlv.gun.render;

import cpw.mods.fml.client.FMLClientHandler;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.settings.GameSettings;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.IItemRenderer;
import net.minecraftforge.client.model.IModelCustom;
import net.minecraftforge.client.model.obj.WavefrontObject;
import org.lwjgl.opengl.GL11;
import ru.xlv.core.model.ModelLoader;
import ru.xlv.gun.item.AttachmentType;
import ru.xlv.gun.item.ItemWeapon;
import ru.xlv.gun.item.Items;
import ru.xlv.gun.render.ModelWrapperDisplayList;
import ru.xlv.gun.render.RenderAttachments;
import ru.xlv.gun.render.RenderWeaponThings;
import ru.xlv.gun.util.Utils;

public class PulemetRender implements IItemRenderer {
	
	
	
    public static final ResourceLocation tex = new ResourceLocation("sp", "models/guns/pulemet/pulemettexture.png");

    private static RenderAttachments renderAtt;
    
	Minecraft mc = Minecraft.getMinecraft();
	RenderWeaponThings rWT = new RenderWeaponThings();
	public static IModelCustom model;
	
	public PulemetRender() {
		renderAtt = new RenderAttachments();
		model = ResourcesSPLocation.pulemet;
        model = new ModelWrapperDisplayList((WavefrontObject) model);
	}
	public boolean handleRenderType(ItemStack is, ItemRenderType type) {
		switch (type) {
		case ENTITY:
			return true;
		case EQUIPPED:
			return true;
		case EQUIPPED_FIRST_PERSON:
			return true;
		default:
			return false;
		}
	}
	public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack is, ItemRendererHelper helper) {
		switch (type){
		case EQUIPPED_FIRST_PERSON: return true;
		case ENTITY:
			return false;
		case EQUIPPED:
			return false;
		default:
			return false;
		}
		
	}
	
	public void renderItem(ItemRenderType type, ItemStack is, Object ... data) {
		
		GameSettings gameSettings = FMLClientHandler.instance().getClient().gameSettings;
		
		ItemWeapon weapon = (ItemWeapon)is.getItem();
		int shootTimer = (int)weapon.otdachaTimer;
		//анимация прицеливания
		//анимация прицеливания
		//анимация бега
		//анимация выстрела
		//анимация перезарядки
	    //анимация ливинг
		float aimAnim = RenderWeaponThings.lastRunProgress + (RenderWeaponThings.runProgress - RenderWeaponThings.lastRunProgress) * RenderWeaponThings.renderTicks;
	
		float runAnim = RenderWeaponThings.lastRun1Progress + (RenderWeaponThings.run1Progress - RenderWeaponThings.lastRun1Progress)* RenderWeaponThings.renderTicks;
	
		float shootAnim = RenderWeaponThings.lastRun2Progress + (RenderWeaponThings.run2Progress - RenderWeaponThings.lastRun2Progress)* RenderWeaponThings.renderTicks;

	    float reloadAnim = RenderWeaponThings.lastRun3Progress + (RenderWeaponThings.run3Progress - RenderWeaponThings.lastRun3Progress)* RenderWeaponThings.renderTicks;
	 
	    float livingAnim = RenderWeaponThings.lastRun4Progress + (RenderWeaponThings.run4Progress - RenderWeaponThings.lastRun4Progress)* RenderWeaponThings.renderTicks;

		switch (type) {
		
		case EQUIPPED_FIRST_PERSON: {
			
			rWT.doAnimations();
 			
			float x = 0;
			float y = 0;
			float z = 0;
			GL11.glPushMatrix();
			RenderHelper.disableStandardItemLighting();
			GL11.glColor4f(0.9f, 0.9f, 0.9f, 1f);
			float dSWS = 1F;
			float f1 = 1F;
			if(Utils.isPlayerAiming()){
			   dSWS = 0.2F;
			   f1 = 1F;
			}
			float fov = gameSettings.fovSetting/10000;
			if(!Utils.isPlayerAiming())
				GL11.glTranslatef(0,0.05F*livingAnim - aimAnim/30,0);
			else
				GL11.glTranslatef(-0.01f,-0.6F*livingAnim*fov,0);
			GL11.glRotatef(65*reloadAnim, 0, 0, 1);
			GL11.glRotatef(-25*runAnim, 1, 0, 0);
			GL11.glRotatef(-15*runAnim, 0, 0, 1);
			GL11.glTranslatef(-0.4F*runAnim, -0.8F*runAnim, 0.5F*runAnim);
			GL11.glRotated(-0.3F*aimAnim, 1.0, 0.0, 1.0);
			GL11.glTranslatef(-0.722F * aimAnim+ x*aimAnim, 0.065F * aimAnim + y*aimAnim, 0.070F * aimAnim + z*aimAnim);
			GL11.glRotated(4.0F*shootAnim*dSWS, 1.0, 0.0, 1.0);
			GL11.glRotated(-1.0F*shootAnim*dSWS, 0.0, 1.0, 1.0);
			GL11.glTranslatef(-0.085F * shootAnim*dSWS, 0.1F*shootAnim*dSWS, 0.084F * shootAnim*dSWS);
			GL11.glPushMatrix();
			GL11.glScalef(0.23F, 0.23F, 0.23F);
			GL11.glRotated(-1.2, 1.0, 0.0, 1.0);
			GL11.glRotated(224.5, 0.0, 1.0, 0.0);
			GL11.glRotated(-0.0, 0.0, 0.0, 1.0);
			GL11.glTranslatef(0.3F * aimAnim, 0, 0);
			GL11.glTranslatef(-0.02F, 6F, 0.96F);
			
			ru.xlv.core.util.Utils.bindTexture(tex);
			model.renderAll();
			if(shootTimer == weapon.shootSpeed-1){
				GL11.glPushMatrix();
				GL11.glScalef(1.0F, 1.0F, 1.0F);
				GL11.glRotatef(90, 0, 1, 0);
				if(!Utils.isPlayerAiming()) {
					GL11.glTranslatef(4F, -2F, -30F);
				}else {
					GL11.glTranslatef(1F, -2F, -30F);
				}
				rWT.flash();
				GL11.glPopMatrix();
			}
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR); 
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
			GL11.glPopMatrix();
			GL11.glPushMatrix();
			rWT.renderRightArm(0.4F, -1.8F, 0.72F, -90, -5, -25, 2.1F);
			GL11.glPopMatrix();
			GL11.glPushMatrix();
			rWT.renderLeftArm(-0.55F, -0.30F, -0.35F, 15, 18, -79, 2.5F);
			GL11.glPopMatrix();
			GL11.glPopMatrix();
		}
		break;
		case EQUIPPED: {
			GL11.glRotatef(35.0F, 1.0F, 0.0F, 0.0F);
		    GL11.glRotatef(-73.0F, 0.0F, 1.0F, 0.0F);
		    GL11.glRotatef(21.0F, 0.0F, 0.0F, 1.0F);
		    GL11.glTranslatef(0.2F, -0.05F, -0.5F);
	        GL11.glRotatef(-90.0F, 1.0F, 0.0F, 0.0F);
	        
			renderEntityAndEquipped(type, is);
		}
		break;
		case ENTITY: {
			
			 
			renderEntityAndEquipped(type, is);
		}
		break;
		default:
			break;
		}
		
		
	}
	public void renderAttachments(ItemRenderType type, ItemStack is){
	}
    public void renderEntityAndEquipped(ItemRenderType type, ItemStack is){
    	
    	GL11.glPushMatrix();
		ru.xlv.core.util.Utils.bindTexture(tex);
		GL11.glScalef(0.13F, 0.13F, 0.13F);
		GL11.glRotatef(90.0F, 1.0F, 0.0F, 0.0F);
		GL11.glRotated(180.0, 0.0, 1.0, 0.0);
		GL11.glTranslatef(-4.5f, 1.5f, 0);
		model.renderAll();
		NBTTagCompound tagz = is.getTagCompound();
		GL11.glPopMatrix();
    }

}
