package ru.xlv.gun;

import cpw.mods.fml.client.registry.ClientRegistry;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkRegistry;
import net.minecraft.client.Minecraft;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.Util;
import net.minecraftforge.client.MinecraftForgeClient;
import net.minecraftforge.common.MinecraftForge;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;
import ru.krogenit.utils.Utils;
import ru.xlv.core.util.CoreUtils;
import ru.xlv.gun.gui.GameOverlay;
import ru.xlv.gun.item.Items;
import ru.xlv.gun.network.PacketHandler;
import ru.xlv.gun.render.*;
import ru.xlv.gun.render.guns.machinegun.PKMRender;
import ru.xlv.gun.render.guns.machinegun.RPKRender;
import ru.xlv.gun.render.guns.pistol.*;
import ru.xlv.gun.render.guns.rifle.*;
import ru.xlv.gun.render.guns.shotgun.AA12Render;
import ru.xlv.gun.render.guns.shotgun.M1014Render;
import ru.xlv.gun.render.guns.shotgun.MP133Render;
import ru.xlv.gun.render.guns.shotgun.TOZ34Render;
import ru.xlv.gun.render.guns.smg.KedrRender;
import ru.xlv.gun.render.guns.smg.P90Render;
import ru.xlv.gun.render.guns.smg.PP2000Render;
import ru.xlv.gun.render.guns.smg.UMPRender;
import ru.xlv.gun.render.guns.sniper.*;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

@Mod(
        name = "XlvsGunMod",
        modid = XlvsMainMod.MODID,
        version = "1.0"
)
public class XlvsMainMod {

    public static final String MODID = "xlvsgunmod";

    @Mod.Instance(MODID)
    public static XlvsMainMod instance;

    public static KeyBinding modGuiKey = new KeyBinding("Открыть панель модификации оружия", Keyboard.KEY_X, "ShadowsProject");
    public static KeyBinding reloadKey = new KeyBinding("Перезарядить оружие", Keyboard.KEY_R, "ShadowsProject");
    public static KeyBinding unloadKey = new KeyBinding("Разарядить оружие", Keyboard.KEY_GRAVE, "ShadowsProject");

    @Mod.EventHandler
    public void event(FMLPreInitializationEvent event) {
        Items.loadItems();

        PacketHandler.registryPackets();

        ClientRegistry.registerKeyBinding(modGuiKey);
        ClientRegistry.registerKeyBinding(reloadKey);
        ClientRegistry.registerKeyBinding(unloadKey);

        MinecraftForge.EVENT_BUS.register(new ClientEventHandler());
        MinecraftForge.EVENT_BUS.register(new PlayerRenderHandler());
        MinecraftForge.EVENT_BUS.register(new GameOverlay());
        MinecraftForge.EVENT_BUS.register(new CommonEventHandler());
        FMLCommonHandler.instance().bus().register(new PlayerRenderHandler());
        FMLCommonHandler.instance().bus().register(new CommonEventHandler());
        FMLCommonHandler.instance().bus().register(new ClientEventHandler());
        FMLCommonHandler.instance().bus().register(new GameOverlay());
        NetworkRegistry.INSTANCE.registerGuiHandler(this, new GuiHandlerClient());
        setProgramIconAndTitle();
    }

    @Mod.EventHandler
    public void event(FMLPostInitializationEvent event) {
        setProgramIconAndTitle();
        registerGunRenders();

//        System.out.println("Armor registering...");
//        for (Item item : Items.getItemsSubscribed(IItemModelProvider.class)) {
//            try {
//                Class<?> clazz = Class.forName(((IItemModelProvider) item).getRenderClassPath());
//                IArmorRenderer iArmorRenderer = (IArmorRenderer) clazz.newInstance();
//                iArmorRenderer.init();
//                IArmorRenderer.registry.put(item.getClass(), iArmorRenderer);
//                System.out.println("Armor registered: " + item.getClass());
//            } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
//                e.printStackTrace();
//            }
//        }

//        ModelRenderInfo info = new ModelRenderInfo(BlockModel.ModelRenderType.ADVANCED, 1f);
//        info.setRenderer(new BlockBizonRenderer(info));
//
//        BlocksModels.registerNewModel("batmod:models/guns/bizon/model_new.obj", "Бизон", info);
//
//        info = new ModelRenderInfo(BlockModel.ModelRenderType.ADVANCED, 1f);
//        info.setRenderer(new BlockArmorMedicRender(info));
//        BlocksModels.registerNewModel("sp:models/armor/medic.sp", "Броня", info);
    }

    public static void registerGunRenders() {
        MinecraftForgeClient.registerItemRenderer(Items.ak74, new AK74Render());
        MinecraftForgeClient.registerItemRenderer(Items.akm, new AKMRender());
        MinecraftForgeClient.registerItemRenderer(Items.ak74u, new AK74URender());
        MinecraftForgeClient.registerItemRenderer(Items.toz34, new TOZ34Render());
        MinecraftForgeClient.registerItemRenderer(Items.ramjet, new RamjetRender());
        MinecraftForgeClient.registerItemRenderer(Items.mp133, new MP133Render());
        MinecraftForgeClient.registerItemRenderer(Items.sv98, new SV98Render());
        MinecraftForgeClient.registerItemRenderer(Items.m4, new M4Render());
        MinecraftForgeClient.registerItemRenderer(Items.pm, new PMRender());
        MinecraftForgeClient.registerItemRenderer(Items.svd, new SVDRender());
        MinecraftForgeClient.registerItemRenderer(Items.val, new VALRender());
        MinecraftForgeClient.registerItemRenderer(Items.l85, new L85Render());
        MinecraftForgeClient.registerItemRenderer(Items.kedr, new KedrRender());
        MinecraftForgeClient.registerItemRenderer(Items.abakan, new AbakanRender());
        MinecraftForgeClient.registerItemRenderer(Items.glock, new GlockRender());
        MinecraftForgeClient.registerItemRenderer(Items.aa12, new AA12Render());
        MinecraftForgeClient.registerItemRenderer(Items.pkm, new PKMRender());
        MinecraftForgeClient.registerItemRenderer(Items.usp, new USPRender());
        MinecraftForgeClient.registerItemRenderer(Items.m9, new M9Render());
        MinecraftForgeClient.registerItemRenderer(Items.ump, new UMPRender());
        MinecraftForgeClient.registerItemRenderer(Items.m1014, new M1014Render());
        MinecraftForgeClient.registerItemRenderer(Items.l96, new L96Render());
        MinecraftForgeClient.registerItemRenderer(Items.ebr, new EBRRender());
        MinecraftForgeClient.registerItemRenderer(Items.p90, new P90Render());
//        MinecraftForgeClient.registerItemRenderer(Items.bizon, new BizonRender());
        MinecraftForgeClient.registerItemRenderer(Items.fal, new FALRender());
        MinecraftForgeClient.registerItemRenderer(Items.ak105, new AK105Render());
        MinecraftForgeClient.registerItemRenderer(Items.aek, new AEKRender());
        MinecraftForgeClient.registerItemRenderer(Items.oc33, new OC33Render());
        MinecraftForgeClient.registerItemRenderer(Items.vss, new VSSRender());
        MinecraftForgeClient.registerItemRenderer(Items.rpk, new RPKRender());
        MinecraftForgeClient.registerItemRenderer(Items.pp2000, new PP2000Render());
        MinecraftForgeClient.registerItemRenderer(Items.pulemet, new PulemetRender());
        MinecraftForgeClient.registerItemRenderer(Items.aspeed, new AspeedRender());
        MinecraftForgeClient.registerItemRenderer(Items.aspeedgray, new AspeedGrayRender());
        MinecraftForgeClient.registerItemRenderer(Items.smg, new SmgRender());
        MinecraftForgeClient.registerItemRenderer(Items.bulletAspeedGray, new AspeedMag(1));
        MinecraftForgeClient.registerItemRenderer(Items.bulletAspeedRed, new AspeedMag(2));
    }

    private static void setProgramIconAndTitle() {
        Display.setTitle("ShadowsProject");
        if (Util.getOSType() != Util.EnumOS.OSX) {
            try {
                Display.setIcon(new ByteBuffer[]{
                        readImage(CoreUtils.getResourceInputStream("sp/textures/gui/icon_16x16.png", false)),
                        readImage(CoreUtils.getResourceInputStream("sp/textures/gui/icon_32x32.png", false))
                });
            } catch (IOException ioexception) {
                ioexception.printStackTrace();
            }
        }
    }

    public static ByteBuffer readImage(InputStream par1File) throws IOException {
        BufferedImage bufferedimage = ImageIO.read(par1File);
        int[] aint = bufferedimage.getRGB(0, 0, bufferedimage.getWidth(), bufferedimage.getHeight(), null, 0, bufferedimage.getWidth());
        ByteBuffer bytebuffer = ByteBuffer.allocate(4 * aint.length);
        int i = aint.length;
        for (int k : aint) {
            bytebuffer.putInt(k << 8 | k >> 24 & 0xFF);
        }
        bytebuffer.flip();
        return bytebuffer;
    }
}
