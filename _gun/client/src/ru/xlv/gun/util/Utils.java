package ru.xlv.gun.util;

import cpw.mods.fml.client.FMLClientHandler;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.util.IIcon;
import org.lwjgl.input.Mouse;
import ru.xlv.gun.item.ItemWeapon;

public class Utils {
    public static boolean isPlayerAiming(){
        if ((FMLClientHandler.instance().getClientPlayerEntity().inventory.getCurrentItem() != null)
                && (FMLClientHandler.instance().getClientPlayerEntity().inventory.getCurrentItem().getItem() instanceof ItemWeapon)
                && (Mouse.isButtonDown(1)) && (!FMLClientHandler.instance().getClientPlayerEntity().isSprinting()
                && (FMLClientHandler.instance().getClient().currentScreen == null))) {
            return true;
        } else {
            return false;
        }
    }

    public static void renderIcon(double x, double y, double z, IIcon icon, int width, int height) {
       Tessellator tessellator = Tessellator.instance;
       tessellator.startDrawingQuads();
       tessellator.addVertexWithUV(x, y + (double)height, z, (double)icon.getMinU(), (double)icon.getMaxV());
       tessellator.addVertexWithUV(x + (double)width, y + (double)height, z, (double)icon.getMaxU(), (double)icon.getMaxV());
       tessellator.addVertexWithUV(x + (double)width, y, z, (double)icon.getMaxU(), (double)icon.getMinV());
       tessellator.addVertexWithUV(x, y, z, (double)icon.getMinU(), (double)icon.getMinV());
       tessellator.draw();
    }
}
