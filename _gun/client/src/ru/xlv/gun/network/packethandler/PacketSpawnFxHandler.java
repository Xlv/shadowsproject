package ru.xlv.gun.network.packethandler;

import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.gun.entity.EntityShootFX;
import ru.xlv.gun.network.packets.PacketSpawnFx;

public class PacketSpawnFxHandler implements IMessageHandler<PacketSpawnFx, IMessage> {

	@SideOnly(Side.CLIENT)
	@Override
	public IMessage onMessage(PacketSpawnFx message, MessageContext ctx) {
		EntityPlayer shooter = getClientPlayer().getEntityWorld().getPlayerEntityByName(message.shooter);
		EntityPlayer clientPlayer = getClientPlayer();
		double y = -0.1;
		try {
			if (shooter != null && clientPlayer != null) {
				if (clientPlayer.getCommandSenderName() != shooter.getCommandSenderName()) {
					if (shooter.isSneaking()) {
						y = -0.3;
					}
					if (message.isCrawl) {
						y += -0.6;
					}
					EntityShootFX shootfx = new EntityShootFX(shooter, shooter.worldObj, shooter.posX, y + shooter.posY + shooter.getEyeHeight() - 0.20000000298023224D, shooter.posZ);
					shooter.worldObj.spawnEntityInWorld(shootfx);
				}
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

	@SideOnly(Side.CLIENT)
	private EntityPlayer getClientPlayer() {
		return Minecraft.getMinecraft().thePlayer;
	}
}
