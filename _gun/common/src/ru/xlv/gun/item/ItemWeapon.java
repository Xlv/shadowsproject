package ru.xlv.gun.item;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.StatCollector;
import net.minecraft.world.World;
import ru.xlv.gun.util.ExtendedPlayer;

import java.util.List;

public class ItemWeapon extends Item {

	private int shots;
	private int ammo;
	private float damage;
	private float spread;
	public float shootSpeed;
	private String icon;
	private float reloadTime;
	private float recoil;
	private String shootSound;
	private String silencerSound;
	public float otdachaTimer;
	//1 - тип глушителя(0 - нет/1 - автоматный/2 -пистолетный/3 - дробовик)
	//3 - можно ли ставить прицелы (0 - нет, 1 - пикатинни, 2- Ласточкин хвост)
	public Item[] attachments;
	private Item Ammo;
	private boolean isAuto;

	public ItemWeapon(Item[] attachments, Item Ammo, int shots, int ammo, float damage, float spread, float recoil, float shootSpeed, float reloadTime, String shootSound, String silencerSound, String reloadSound, String icon, boolean isAuto) {
		super();
		this.attachments = attachments;
		this.Ammo = Ammo;
		this.shots = shots;
		this.ammo = ammo;
		this.damage = damage;
		this.spread = spread;
		this.shootSpeed = shootSpeed;
		this.reloadTime = reloadTime;
		this.shootSound = shootSound;
		this.silencerSound = silencerSound;
		this.icon = icon;
		this.recoil = recoil;
		this.isAuto = isAuto;
		this.setMaxStackSize(1);
		this.setCreativeTab(Tabs.tabGuns);
	}

	@SuppressWarnings("unchecked")
	public void addInformation(ItemStack stack, EntityPlayer player, List lines, boolean advancedTooltips) {
		NBTTagCompound tagCompound = stack.getTagCompound();
		if (tagCompound != null) {
			lines.add("    ");
			lines.add(StatCollector.translateToLocal(EnumChatFormatting.WHITE + "Калибр: " + Ammo.getItemStackDisplayName(new ItemStack(Ammo))));
			lines.add(StatCollector.translateToLocal(EnumChatFormatting.WHITE + "Патрон: " + stack.getTagCompound().getInteger("ammo") + "/" + this.ammo));
			lines.add(StatCollector.translateToLocal(EnumChatFormatting.YELLOW + "Урон: " + round(stack.getTagCompound().getFloat("damage") * this.shots)));
			lines.add(StatCollector.translateToLocal(EnumChatFormatting.YELLOW + "Скорострельность: " + 1200 / this.shootSpeed + " выс/мин."));
			lines.add(StatCollector.translateToLocal(EnumChatFormatting.YELLOW + "Время перезарядки: " + this.reloadTime / 20 + " с."));
			lines.add(StatCollector.translateToLocal(EnumChatFormatting.YELLOW + "Точность: " + round(stack.getTagCompound().getFloat("spread"))));
			lines.add(StatCollector.translateToLocal(EnumChatFormatting.YELLOW + "Отдача: " + round(stack.getTagCompound().getFloat("recoil"))));
			if (tagCompound.getCompoundTag("att").getFloat("despread") > 0) {
				lines.add(StatCollector.translateToLocal(EnumChatFormatting.RED + "Понижен разброс от обвесов на: " + tagCompound.getCompoundTag("att").getFloat("despread")));
			}
			if (tagCompound.getCompoundTag("att").getFloat("derecoil") > 0) {
				lines.add(StatCollector.translateToLocal(EnumChatFormatting.RED + "Понижена отдача от обвесов: " + tagCompound.getCompoundTag("att").getFloat("derecoil")));
			}
			if (tagCompound.getCompoundTag("att").getFloat("dedamage") > 0) {
				lines.add(StatCollector.translateToLocal(EnumChatFormatting.RED + "Понижена урона от обвесов: " + tagCompound.getCompoundTag("att").getFloat("dedamage")));
			}
			lines.add(StatCollector.translateToLocal(EnumChatFormatting.WHITE + "Установленные модификации:"));
			NBTTagCompound nbtstack = stack.getTagCompound();
			if (nbtstack != null) {
				if (stack.stackTagCompound != null) {
					if (nbtstack.getString("scope").length() > 2) {
						lines.add(ItemStack.loadItemStackFromNBT(stack.getTagCompound().getCompoundTag("scope")).getDisplayName());
					}
					if (nbtstack.getString("stvol").length() > 2) {
						lines.add(ItemStack.loadItemStackFromNBT(stack.getTagCompound().getCompoundTag("stvol")).getDisplayName());
					}
					if (nbtstack.getString("grip").length() > 2) {
						lines.add(ItemStack.loadItemStackFromNBT(stack.getTagCompound().getCompoundTag("grip")).getDisplayName());
					}
				}
			}
		}
	}

	public void onUpdate(ItemStack itemStack, World world, Entity entity, int par4, boolean par5) {
		super.onUpdate(itemStack, world, entity, par4, par5);
		NBTTagCompound tag = itemStack.getTagCompound();
		if (tag == null) {
			NBTTagCompound newTags = new NBTTagCompound();
			newTags.setFloat("damage", damage);
			newTags.setFloat("recoil", recoil);
			newTags.setFloat("spread", spread);
			newTags.setInteger("ammo", this.ammo);
			newTags.setBoolean("auto", this.isAuto);
			newTags.setInteger("no_drop", 1);
			newTags.setInteger("unreload", 0);
			newTags.setString("rarity", "null");
			newTags.setInteger("unreload", 0);
			itemStack.stackTagCompound = newTags;
		} else {
			String govno = "" + itemStack.getTagCompound().getTag("att");
			if (govno.length() < 5) {
				NBTTagCompound attachmentTags = new NBTTagCompound();
				attachmentTags.setFloat("despreadstvol", 0);
				attachmentTags.setFloat("derecoilstvol", 0);
				attachmentTags.setFloat("dedamagestvol", 0);
				attachmentTags.setFloat("despreadscope", 0);
				attachmentTags.setFloat("derecoilscope", 0);
				attachmentTags.setFloat("dedamagescope", 0);
				attachmentTags.setFloat("despreadgrip", 0);
				attachmentTags.setFloat("derecoilgrip", 0);
				attachmentTags.setFloat("dedamagegrip", 0);
				attachmentTags.setFloat("despread", 0);
				attachmentTags.setFloat("derecoil", 0);
				attachmentTags.setFloat("dedamage", 0);
				itemStack.getTagCompound().setTag("att", attachmentTags);
			}

			if (tag.getCompoundTag("stvol") != null) {
				if (tag.getString("stvol").length() > 2) {
					ItemStack obves = ItemStack.loadItemStackFromNBT(tag.getCompoundTag("stvol"));
					tag.getCompoundTag("att").setFloat("despreadstvol", ((ItemAttachment) obves.getItem()).despread);
					tag.getCompoundTag("att").setFloat("derecoilstvol", ((ItemAttachment) obves.getItem()).derecoil);
					tag.getCompoundTag("att").setFloat("dedamagestvol", ((ItemAttachment) obves.getItem()).dedamage);
				} else {
					tag.getCompoundTag("att").setFloat("despreadstvol", 0);
					tag.getCompoundTag("att").setFloat("derecoilstvol", 0);
					tag.getCompoundTag("att").setFloat("dedamagestvol", 0);
				}
			} else {
				tag.getCompoundTag("att").setFloat("despreadstvol", 0);
				tag.getCompoundTag("att").setFloat("derecoilstvol", 0);
				tag.getCompoundTag("att").setFloat("dedamagestvol", 0);
			}
			if (tag.getCompoundTag("scope") != null) {
				if (tag.getString("scope").length() > 2) {
					ItemStack obves = ItemStack.loadItemStackFromNBT(tag.getCompoundTag("scope"));
					tag.getCompoundTag("att").setFloat("despreadscope", ((ItemAttachment) obves.getItem()).despread);
					tag.getCompoundTag("att").setFloat("derecoilscope", ((ItemAttachment) obves.getItem()).derecoil);
					tag.getCompoundTag("att").setFloat("dedamagescope", ((ItemAttachment) obves.getItem()).dedamage);
				} else {
					tag.getCompoundTag("att").setFloat("despreadscope", 0);
					tag.getCompoundTag("att").setFloat("derecoilscope", 0);
					tag.getCompoundTag("att").setFloat("dedamagescope", 0);
				}
			} else {
				tag.getCompoundTag("att").setFloat("despreadscope", 0);
				tag.getCompoundTag("att").setFloat("derecoilscope", 0);
				tag.getCompoundTag("att").setFloat("dedamagescope", 0);
			}
			if (tag.getCompoundTag("grip") != null) {
				if (tag.getString("grip").length() > 2) {
					ItemStack obves = ItemStack.loadItemStackFromNBT(tag.getCompoundTag("grip"));
					tag.getCompoundTag("att").setFloat("despreadgrip", ((ItemAttachment) obves.getItem()).despread);
					tag.getCompoundTag("att").setFloat("derecoilgrip", ((ItemAttachment) obves.getItem()).derecoil);
					tag.getCompoundTag("att").setFloat("dedamagegrip", ((ItemAttachment) obves.getItem()).dedamage);
				} else {
					tag.getCompoundTag("att").setFloat("despreadgrip", 0);
					tag.getCompoundTag("att").setFloat("derecoilgrip", 0);
					tag.getCompoundTag("att").setFloat("dedamagegrip", 0);
				}
			} else {
				tag.getCompoundTag("att").setFloat("despreadgrip", 0);
				tag.getCompoundTag("att").setFloat("derecoilgrip", 0);
				tag.getCompoundTag("att").setFloat("dedamagegrip", 0);
			}

			tag.getCompoundTag("att").setFloat("despread", tag.getCompoundTag("att").getFloat("despreadstvol") + tag.getCompoundTag("att").getFloat("despreadscope") + tag.getCompoundTag("att").getFloat("despreadgrip"));
			tag.getCompoundTag("att").setFloat("derecoil", tag.getCompoundTag("att").getFloat("derecoilstvol") + tag.getCompoundTag("att").getFloat("derecoilscope") + tag.getCompoundTag("att").getFloat("derecoilgrip"));
			tag.getCompoundTag("att").setFloat("dedamage", tag.getCompoundTag("att").getFloat("dedamagestvol") + tag.getCompoundTag("att").getFloat("dedamagescope") + tag.getCompoundTag("att").getFloat("dedamagegrip"));

			tag.setFloat("recoil", (getRecoil() - tag.getCompoundTag("att").getFloat("derecoil")));
			tag.setFloat("damage", (getDamage() - tag.getCompoundTag("att").getFloat("dedamage")));
			tag.setFloat("spread", (getSpread() - tag.getCompoundTag("att").getFloat("despread")));

			if (itemStack.getTagCompound().getFloat("reloadTime") > 0) {
				itemStack.getTagCompound().setFloat("reloadTime", itemStack.getTagCompound().getFloat("reloadTime") - 1);
			}
			if (itemStack.getTagCompound().getInteger("ammo") > this.ammo) {
				itemStack.getTagCompound().setInteger("ammo", this.ammo);
			}

			EntityPlayer entityPlayer = (EntityPlayer) entity;
			ExtendedPlayer extendedPlayer = ExtendedPlayer.get(entityPlayer);
			if (extendedPlayer != null && ((EntityPlayer) entity).inventory.getCurrentItem() != null) {
				if (extendedPlayer.getReloadKeyPressed() && itemStack.getTagCompound().getInteger("ammo") < this.ammo
						&& par5 && itemStack.getTagCompound().getFloat("reloadTime") == 0) {
					int bestSlot = -1;
					int bulletsInBestSlot = 0;
					for (int newBulletStack = 0; newBulletStack < entityPlayer.inventory.getSizeInventory(); ++newBulletStack) {
						ItemStack bullet = entityPlayer.inventory.getStackInSlot(newBulletStack);
						if (bullet != null && bullet.getItem() == this.Ammo) {
							int stackToLoad = bullet.getMaxDamage() - bullet.getItemDamage();
							if (stackToLoad > bulletsInBestSlot) {
								bestSlot = newBulletStack;
								bulletsInBestSlot = stackToLoad;
							}
						}
					}
					if (bestSlot != -1) {
						ItemStack ammo = entityPlayer.inventory.getStackInSlot(bestSlot);
						entityPlayer.inventory.setInventorySlotContents(bestSlot, null);
						itemStack.getTagCompound().setFloat("reloadTime", this.reloadTime);
						if (itemStack.getTagCompound().getInteger("ammo") != 0) {
							ItemStack ammomagundo = new ItemStack(this.Ammo);
							ammomagundo.setItemDamage(this.ammo - itemStack.getTagCompound().getInteger("ammo"));
							entityPlayer.inventory.addItemStackToInventory(ammomagundo);
						}
						itemStack.getTagCompound().setInteger("ammo", this.ammo - ammo.getItemDamage());
					}
				}

				if (itemStack.getTagCompound().getInteger("unreload") == 1 && itemStack.getTagCompound().getInteger("ammo") != 0) {
					itemStack.getTagCompound().setInteger("unreload", 0);
					ItemStack ammomagundo = new ItemStack(this.Ammo);
					ammomagundo.setItemDamage(this.ammo - itemStack.getTagCompound().getInteger("ammo"));
					itemStack.getTagCompound().setInteger("ammo", 0);
					entityPlayer.inventory.addItemStackToInventory(ammomagundo);
				}

				if (!world.isRemote) {
					extendedPlayer.consumeST();
				}
			}
		}
	}

	@Override
	public void getSubItems(Item item, CreativeTabs tabs, List list) {
		ItemStack gunStack = new ItemStack(item, 1, 0);
		NBTTagCompound tags = new NBTTagCompound();
		tags.setFloat("damage", damage);
		tags.setFloat("recoil", recoil);
		tags.setFloat("spread", spread);
		tags.setInteger("ammo", this.ammo);
		tags.setBoolean("auto", this.isAuto);
		tags.setInteger("unreload", 0);
		tags.setString("rarity", "null");
		gunStack.stackTagCompound = tags;
		list.add(gunStack);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister icon) {
		itemIcon = icon.registerIcon("batmod:" + this.icon);
	}

	@Override
	public boolean onBlockStartBreak(ItemStack itemstack, int X, int Y, int Z, EntityPlayer player) {
		return true;
	}

	@Override
	public boolean onEntitySwing(EntityLivingBase entityLiving, ItemStack stack) {
		return true;
	}

	public int getAmmo() {
		return this.ammo;
	}

	public float getOtdachaTimer() {
		return otdachaTimer;
	}

	public String getShootSound() {
		return this.shootSound;
	}

	public String getSilSound() {
		return this.silencerSound;
	}

	public float getDamage() {
		return this.damage;
	}

	public float getSpread() {
		return this.spread;
	}

	public int getShots() {
		return this.shots;
	}

	public float getShootSpeed() {
		return this.shootSpeed;
	}

	public boolean isAutoMode() {
		return this.isAuto;
	}

	public float getRecoil() {
		return this.recoil;
	}

	public float getReloadTime() {
		return this.reloadTime;
	}

	public void setOtdachaTimer() {
		this.otdachaTimer = this.shootSpeed;
	}

	private static double round(double value) {
		return Math.round(value * Math.pow(10, 2)) / Math.pow(10, 2);
	}
}