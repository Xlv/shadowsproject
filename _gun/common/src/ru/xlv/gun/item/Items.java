package ru.xlv.gun.item;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.item.Item;
import net.minecraft.item.ItemArmor;
import net.minecraftforge.common.util.EnumHelper;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Items {

	public static Item armorRem = new Item().setUnlocalizedName("Ремкомплект для брони").setTextureName("batmod:armorRem").setCreativeTab(Tabs.tabItems);
	public static Item leupold = new ItemAttachment("leupold", 0.7F, 0.0F, 0.0F, 2.8F, AttachmentType.scope).setUnlocalizedName("Leupold HAMR");
	public static Item pkas = new ItemAttachment("pkas", 0.7F, 0.0F, 0.0F, 1.5F, AttachmentType.scope).setUnlocalizedName("Прицел ПК-АС");
	public static Item pso = new ItemAttachment("pso", 0.7F, 0.0F, 0.0F, 2.8F, AttachmentType.scope).setUnlocalizedName("Прицел ПСО-1");
	public static Item laser = new ItemAttachment("laser", 0.4F, 0.4F, 0.2F, 1, AttachmentType.grip).setUnlocalizedName("Лазер");
	public static Item natoSilencer = new ItemAttachment("glushak", 1.24F, 0.5F, 3.0F, 1, AttachmentType.stvol).setUnlocalizedName("OTAN Silencer");
	public static Item pbs4 = new ItemAttachment("pbs4", 1.2F, 0.5F, 3.0F, 1, AttachmentType.stvol).setUnlocalizedName("ПБС-4");
	public static Item susat = new ItemAttachment("susat", 0.7F, 0.0F, 0.0F, 1.5F, AttachmentType.scope).setUnlocalizedName("SUSAT");
	public static Item kobra = new ItemAttachment("kobra", 0.7F, 0.0F, 0.0F, 1F, AttachmentType.scope).setUnlocalizedName("Прицел Кобра");
	public static Item barska = new ItemAttachment("barska", 0.7F, 0.0F, 0.0F, 1F, AttachmentType.scope).setUnlocalizedName("Barska Sight");
	public static Item trAc = new ItemAttachment("trAc", 0.7F, 0.0F, 0.0F, 4.2F, AttachmentType.scope).setUnlocalizedName("TR20");
	public static Item posp = new ItemAttachment("posp", 0.7F, 0.0F, 0.0F, 4.2F, AttachmentType.scope).setUnlocalizedName("Прицел ПОСП");
	public static Item osprey = new ItemAttachment("osprey", 1.15F, 0.5F, 3.0F, 1, AttachmentType.stvol).setUnlocalizedName("Osprey");
	public static Item aac762sdn = new ItemAttachment("aac762sdn", 1.6F, 0.5F, 3.0F, 1, AttachmentType.stvol).setUnlocalizedName("7x62 AAC SDN");
	public static Item acog = new ItemAttachment("acog", 0.7F, 0.0F, 0.0F, 3.4F, AttachmentType.scope).setUnlocalizedName("ACOG");

	public static Item Magpulemet = new ItemAmmo("magazine/pulemet", 100).setUnlocalizedName("magpulemet");
	public static Item bulletAspeedRed = new ItemAmmo("magazine/aspeedred", 45).setUnlocalizedName("magaspeedred");
	public static Item bulletAspeedGray = new ItemAmmo("magazine/aspeedgray", 40).setUnlocalizedName("magaspeedgray");
	public static Item Magak74 = new ItemAmmo("magazine/ak74", 30).setUnlocalizedName("magak74");
	public static Item Magmp133 = new ItemAmmo("magazine/drob", 4).setUnlocalizedName("magmp133gun");
	public static Item Magsvd = new ItemAmmo("magazine/svd", 10).setUnlocalizedName("magsvd");
	public static Item Magak74u = new ItemAmmo("magazine/ak74y", 30).setUnlocalizedName("magak74y");
	public static Item Magakm = new ItemAmmo("magazine/akm", 30).setUnlocalizedName("magakm");
	public static Item Magsv98 = new ItemAmmo("magazine/sv-98", 10).setUnlocalizedName("magsv98");
	public static Item Magm4 = new ItemAmmo("magazine/m4", 30).setUnlocalizedName("magm4a1");
	public static Item Magpm = new ItemAmmo("magazine/makarov", 8).setUnlocalizedName("magmakarov");
	public static Item Magtoz34 = new ItemAmmo("magazine/toz", 2).setUnlocalizedName("magtoz");
	public static Item Magval = new ItemAmmo("magazine/asval", 20).setUnlocalizedName("magval");
	public static Item Magl85 = new ItemAmmo("magazine/l85", 30).setUnlocalizedName("magl85");
	public static Item Magkedr = new ItemAmmo("magazine/kedr", 20).setUnlocalizedName("magkedr");
	public static Item Magabakan = new ItemAmmo("magazine/ah94", 30).setUnlocalizedName("magah94");
	public static Item Magglock = new ItemAmmo("magazine/glock17", 17).setUnlocalizedName("magglock17");
	public static Item Magaa12 = new ItemAmmo("magazine/aa12", 8).setUnlocalizedName("magaa12");
	public static Item Magpkm = new ItemAmmo("magazine/pkm", 100).setUnlocalizedName("magpkm");
	public static Item Magusp = new ItemAmmo("magazine/usp", 12).setUnlocalizedName("magusp");
	public static Item Magm9 = new ItemAmmo("magazine/m9", 15).setUnlocalizedName("magm9");
	public static Item Magump = new ItemAmmo("magazine/ump", 25).setUnlocalizedName("magump");
	public static Item Magm1014 = new ItemAmmo("magazine/m1014", 7).setUnlocalizedName("magm1014");
	public static Item Magl96 = new ItemAmmo("magazine/l96", 10).setUnlocalizedName("magl96");
	public static Item Magebr = new ItemAmmo("magazine/mk14", 10).setUnlocalizedName("magmk14");
	public static Item Magp90 = new ItemAmmo("magazine/p90", 50).setUnlocalizedName("magp90");
	public static Item Magbizon = new ItemAmmo("magazine/bizon", 64).setUnlocalizedName("magbizon");
	public static Item Magfal = new ItemAmmo("magazine/fnfal", 20).setUnlocalizedName("magfnfal");
	public static Item Magoc33 = new ItemAmmo("magazine/pern", 18).setUnlocalizedName("magperna4");
	public static Item Magvss = new ItemAmmo("magazine/vintores", 10).setUnlocalizedName("magvintores");
	public static Item Magpp2000 = new ItemAmmo("magazine/pp2000", 20).setUnlocalizedName("magpp2000");
	public static Item Magak105 = new ItemAmmo("magazine/ak105", 30).setUnlocalizedName("magak105");
	public static Item Magaek = new ItemAmmo("magazine/aek971", 30).setUnlocalizedName("magaek971");
	public static Item Magrpk = new ItemAmmo("magazine/rpk", 45).setUnlocalizedName("magrpk");
	public static Item Magramjet = new ItemAmmo("magazine/ramjet", 30).setUnlocalizedName("mag 20 mm Hispano");
	public static Item Magsmg = new ItemAmmo("magazine/smg", 30).setUnlocalizedName("magsmg");

	public static Item ak74 = new ItemWeapon(new Item[]{pbs4, laser, pkas, pso, kobra}, Magak74, 1, 30, 22.3F, 2.52F, 2.65F, 2, 76, "batmod:ak74_shoot", "batmod:ak_sil", "batmod:reload", "ak74", true).setUnlocalizedName("AK-74");
	public static Item mp133 = new ItemWeapon(new Item[]{laser}, Magmp133, 10, 4, 7F, 7.7F, 4.5F, 18, 86, "batmod:mp133_shoot", "batmod:duplet_sil", "batmod:mp133_reload", "mp133", false).setUnlocalizedName("МР-133");
	public static Item svd = new ItemWeapon(new Item[]{laser, aac762sdn, pkas, pso, posp}, Magsvd, 1, 10, 59.8F, 1.32F, 4.71F, 12, 82, "batmod:svd_shoot", "batmod:sil1", "batmod:reload", "svd", false).setUnlocalizedName("СВД");
	public static Item ak74u = new ItemWeapon(new Item[]{pbs4, laser, pkas, pso, kobra}, Magak74u, 1, 30, 23.7F, 2.62F, 2.61F, 2, 74, "batmod:aks74u_shoot", "batmod:ak_sil", "batmod:reload", "aksu", true).setUnlocalizedName("АК-74У");
	public static Item akm = new ItemWeapon(new Item[]{pbs4, laser, pkas, pso, kobra}, Magakm, 1, 30, 26.5F, 2.56F, 2.89F, 2, 76, "batmod:akm_shoot", "batmod:ak_sil", "batmod:reload", "akm", true).setUnlocalizedName("АКМ");
	public static Item sv98 = new ItemWeapon(new Item[]{aac762sdn, laser, leupold, susat, acog, barska, trAc}, Magsv98, 1, 10, 109.0F, 1.26F, 7F, 40, 82, "batmod:ventil_shoot", "batmod:ventil_sil", "batmod:ventil_reload", "sv98", false).setUnlocalizedName("СВ-98");
	public static Item m4 = new ItemWeapon(new Item[]{natoSilencer, laser, leupold, acog, susat, barska}, Magm4, 1, 30, 24.5F, 2.17F, 2.05F, 2, 74, "batmod:m4_shoot", "batmod:sil1", "batmod:reload", "m4", true).setUnlocalizedName("M4A1");
	public static Item pm = new ItemWeapon(new Item[]{osprey}, Magpm, 1, 8, 25.0F, 2.88F, 2.32F, 4, 60, "batmod:pm_shoot", "batmod:sil3", "batmod:pistol_reload", "pm", false).setUnlocalizedName("Пистолет Макарова");
	public static Item toz34 = new ItemWeapon(new Item[]{laser}, Magtoz34, 10, 2, 8F, 7.2F, 6.1F, 3, 68, "batmod:toz34_shoot", "batmod:duplet_sil", "batmod:toz34_reload", "toz34", false).setUnlocalizedName("ТОЗ-34");
	public static Item val = new ItemWeapon(new Item[]{laser, pkas, pso, kobra}, Magval, 1, 20, 24.5F, 2.22F, 2.5F, 2, 70, "batmod:vss_shoot", "batmod:vss_shoot", "batmod:reload", "val", true).setUnlocalizedName("АС ВАЛ");
	public static Item l85 = new ItemWeapon(new Item[]{natoSilencer, laser, leupold, acog, susat, barska}, Magl85, 1, 30, 21.5F, 2.54F, 2.62F, 2, 78, "batmod:l85_shoot", "batmod:sil3", "batmod:reload", "l85", true).setUnlocalizedName("L85A2");
	public static Item kedr = new ItemWeapon(new Item[]{laser, osprey}, Magkedr, 1, 20, 19.5F, 2.74F, 2.4F, 2, 68, "batmod:kedr_shoot", "batmod:sil1", "batmod:reload", "kedr", true).setUnlocalizedName("ПП-91 Кедр");
	public static Item abakan = new ItemWeapon(new Item[]{pbs4, laser, pkas, pso, kobra}, Magabakan, 1, 30, 22.0F, 2.38F, 2.62F, 2, 74, "batmod:abakan_shoot", "batmod:ak_sil", "batmod:reload", "abakan", true).setUnlocalizedName("АН-94");
	public static Item glock = new ItemWeapon(new Item[]{osprey, laser}, Magglock, 1, 17, 25.0F, 2.68F, 2.2F, 3, 64, "batmod:glock_shoot", "batmod:sil3", "batmod:pistol_reload", "glock", false).setUnlocalizedName("Glock 17");
	public static Item aa12 = new ItemWeapon(new Item[]{laser}, Magaa12, 10, 8, 6F, 7F, 5.5F, 4, 80, "batmod:aa12_shoot", "batmod:duplet_sil", "batmod:reload", "aa12", false).setUnlocalizedName("AA-12");
	public static Item pkm = new ItemWeapon(new Item[]{laser, pkas}, Magpkm, 1, 100, 30.0F, 3.02F, 3.05F, 2, 140, "batmod:pkm_shoot", "batmod:pkm_shoot", "batmod:rpd_reload", "pkm", true).setUnlocalizedName("ПКМ");
	public static Item usp = new ItemWeapon(new Item[]{osprey, laser}, Magusp, 1, 12, 26.0F, 2.66F, 2.25F, 4, 66, "batmod:usp_shoot", "batmod:sil3", "batmod:pistol_reload", "usp", false).setUnlocalizedName("USP");
	public static Item m9 = new ItemWeapon(new Item[]{laser, osprey}, Magm9, 1, 15, 23.0F, 2.76F, 2.25F, 3, 65, "batmod:m9_shoot", "batmod:sil3", "batmod:pistol_reload", "m9", false).setUnlocalizedName("M9");
	public static Item ump = new ItemWeapon(new Item[]{laser, osprey, barska, leupold, acog, susat}, Magump, 1, 25, 20.0F, 2.72F, 2.37F, 2, 70, "batmod:usp_shoot", "batmod:ak_sil", "batmod:reload", "ump", true).setUnlocalizedName("HK UMP");
	public static Item m1014 = new ItemWeapon(new Item[]{laser}, Magm1014, 10, 7, 7F, 7.5F, 6.2F, 6, 90, "batmod:m1014_shoot", "batmod:duplet_sil", "batmod:shorty_reload", "m1014", false).setUnlocalizedName("M1014");
	public static Item l96 = new ItemWeapon(new Item[]{aac762sdn, laser, leupold, acog, susat, barska, trAc}, Magl96, 1, 10, 97.5F, 1.58F, 5.61F, 40, 82, "batmod:l96_shoot", "batmod:ventil_sil", "batmod:ventil_reload", "l96", false).setUnlocalizedName("L96");
	public static Item ebr = new ItemWeapon(new Item[]{laser, aac762sdn, barska, leupold, acog, susat, trAc}, Magebr, 1, 10, 46.5F, 1.81F, 3.91F, 6, 84, "batmod:ebr_shoot", "batmod:sil1", "batmod:reload", "ebr", false).setUnlocalizedName("MK14 EBR");
	public static Item p90 = new ItemWeapon(new Item[]{laser, osprey, barska, leupold, acog, susat}, Magp90, 1, 50, 29.47F, 2.78F, 2.3F, 2F, 80, "batmod:glock_shoot", "batmod:sil5", "batmod:reload", "p90", true).setUnlocalizedName("P90");
	public static Item bizon = new ItemWeapon(new Item[]{osprey, laser, pkas, pso, kobra}, Magbizon, 1, 64, 22.5F, 2.68F, 2.5F, 2, 78, "batmod:pm_shoot", "batmod:sil4", "batmod:reload", "bizon", true).setUnlocalizedName("ПП-19 Бизон");
	public static Item fal = new ItemWeapon(new Item[]{natoSilencer, laser, leupold, acog, susat, barska}, Magfal, 1, 20, 27.0F, 2.52F, 2.87F, 2, 80, "batmod:fal_shoot", "batmod:scar_sil", "batmod:reload", "fal", true).setUnlocalizedName("FN FAL");
	public static Item oc33 = new ItemWeapon(new Item[]{laser, osprey}, Magoc33, 1, 18, 18.0F, 2.78F, 2.22F, 2, 64, "batmod:oc33_shoot", "batmod:sil3", "batmod:pistol_reload", "oc33", true).setUnlocalizedName("ОЦ-33 Пернач");
	public static Item vss = new ItemWeapon(new Item[]{laser, pkas, pso, kobra, posp}, Magvss, 1, 10, 29.0F, 2.02F, 2.3F, 2, 74, "batmod:vss_shoot", "batmod:vss_shoot", "batmod:reload", "vss", true).setUnlocalizedName("ВСС Винторез");
	public static Item pp2000 = new ItemWeapon(new Item[]{laser, osprey, leupold, susat, acog, barska}, Magpp2000, 1, 20, 18.0F, 2.61F, 2.3F, 2, 72, "batmod:pp2000_shoot", "batmod:sil1", "batmod:smg_reload", "pp2000", true).setUnlocalizedName("ПП-2000");
	public static Item ak105 = new ItemWeapon(new Item[]{pbs4, laser, pkas, pso, kobra}, Magak105, 1, 30, 22.0F, 2.42F, 2.53F, 2, 72, "batmod:2012_shoot", "batmod:ak_sil", "batmod:reload", "ak105", true).setUnlocalizedName("АК-105");
	public static Item aek = new ItemWeapon(new Item[]{pbs4, laser, pkas, pso, kobra}, Magaek, 1, 30, 22.5F, 2.34F, 2.48F, 2, 74, "batmod:aek_shoot", "batmod:ak_sil", "batmod:reload", "aek", true).setUnlocalizedName("АЕК-971");
	public static Item rpk = new ItemWeapon(new Item[]{pbs4, laser, pkas, pso, kobra}, Magrpk, 1, 45, 22.0F, 2.76F, 2.65F, 2, 82, "batmod:rpk_shoot", "batmod:sil4", "batmod:rpk_reload", "rpk", true).setUnlocalizedName("РПК");
	public static Item ramjet = new ItemWeapon(new Item[]{laser}, Magramjet, 1, 1, 200.0F, 1.02F, 7.91F, 2, 350, "batmod:rt_shoot", "batmod:sil1", "batmod:rt_reload", "ramjet", false).setUnlocalizedName("Ramjet");

	public static Item pulemet = new ItemWeapon(new Item[]{}, Magpulemet, 1, 100, 31F, 3.2F, 3.5F, 2, 150, "batmod:xm8_shoot", "batmod:sil2", "batmod:reload", "pulemet", true).setUnlocalizedName("Пулемёт");
	public static Item aspeed = new ItemWeapon(new Item[]{}, bulletAspeedRed, 1, 45, 33F, 1F, 0.9F, 2, 70, "batmod:xm8_shoot", "batmod:sil2", "batmod:reload", "aspeedred", true).setUnlocalizedName("AspeedRed");
	public static Item aspeedgray = new ItemWeapon(new Item[]{}, bulletAspeedGray, 1, 40, 31F, 2F, 1.25F, 2, 80, "batmod:xm8_shoot", "batmod:sil2", "batmod:reload", "aspeedgray", true).setUnlocalizedName("AspeedGray");
	public static Item smg = new ItemWeapon(new Item[]{}, Magsmg, 1, 30, 27F, 2.2F, 2.7F, 2, 60, "batmod:xm8_shoot", "batmod:sil2", "batmod:reload", "smg", true).setUnlocalizedName("smg");

	public static ItemTestArmor testArmor = new ItemTestArmor(ArmorMaterials.testMaterial, 1, 1,"medicGray");

	public static void loadItems() {
		for (Field field : Items.class.getFields()) {
			try {
				registerItem((Item) field.get(null));
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
	}

	public static <T> List<Item> getItemsSubscribed(T parentClass) {
		List<Item> list = new ArrayList<>();
		for (Field field : Items.class.getFields()) {
			for (Class<?> anInterface : field.getType().getInterfaces()) {
				if(anInterface == parentClass) {
					try {
						list.add((Item) field.get(null));
					} catch (IllegalAccessException e) {
						e.printStackTrace();
					}
				}
			}
		}
		return list;
	}

	private static void registerItem(Item item) {
		GameRegistry.registerItem(item, item.getUnlocalizedName());
	}
}
