package ru.xlv.gun;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.TickEvent;
import cpw.mods.fml.relauncher.ReflectionHelper;
import net.minecraft.crash.CrashReport;
import net.minecraft.crash.CrashReportCategory;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.potion.Potion;
import net.minecraft.util.EntityDamageSource;
import net.minecraft.util.ReportedException;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.entity.living.LivingEvent;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.event.entity.player.EntityItemPickupEvent;

import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.Callable;

public class CommonEventHandler {

    @SubscribeEvent
    public void onEntityJoin(EntityJoinWorldEvent event) {
        if (!event.entity.worldObj.isRemote && event.entity instanceof EntityLivingBase) {
            EntityLivingBase entity = (EntityLivingBase) event.entity;
            entity.renderDistanceWeight = 2.0D;
            entity.getEntityAttribute(SharedMonsterAttributes.knockbackResistance).setBaseValue(2D);
        }
        if (event.entity instanceof EntityPlayer) {
            EntityPlayer player = (EntityPlayer) event.entity;
            if (player.getMaxHealth() != 100) {
                player.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(100);
                if (player.shouldHeal()) {
                    player.setHealth(player.getMaxHealth());
                }
            }
        }
    }

    @SubscribeEvent
    public void removePotionEffectRegeneration(LivingHurtEvent event) {
        if (event.source instanceof EntityDamageSource) {
            EntityDamageSource dmgSource = (EntityDamageSource) event.source;
            Entity from = dmgSource.getEntity();
            EntityLivingBase to = event.entityLiving;
            if (to.getActivePotionEffect(Potion.regeneration) != null) {
                to.removePotionEffect(Potion.regeneration.id);
            }
            if (to.getActivePotionEffect(Potion.heal) != null) {
                to.removePotionEffect(Potion.heal.id);
            }
        }

    }

    @SubscribeEvent
    public void onItemPickup(EntityItemPickupEvent event) {
        if (event.entity instanceof EntityPlayer) {
            event.setCanceled(true);
        }
    }


    @SubscribeEvent
    public void entityHealthCheck(LivingEvent.LivingUpdateEvent event) {
        if (event.entity instanceof EntityPlayer) {
            EntityPlayer player = (EntityPlayer) event.entity;
            if (player.getCurrentArmor(2) == null) {
                if (player.getEntityAttribute(SharedMonsterAttributes.movementSpeed).getBaseValue() < 0.100 || player.getEntityAttribute(SharedMonsterAttributes.movementSpeed).getBaseValue() > 1.1) {
                    player.getEntityAttribute(SharedMonsterAttributes.movementSpeed).setBaseValue(0.10000000149011612);
                }
                if (player.getMaxHealth() >= 110) {
                    player.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(100);
                }
                if (player.getHealth() > 100 && player.getMaxHealth() != 300) {
                    player.setHealth(100);
                }
            }
        }
    }

    @SubscribeEvent
    public void onPlayerTick(TickEvent.PlayerTickEvent e) {
        if (!e.side.isClient() && e.phase == TickEvent.Phase.END) {
            InventoryPlayer inventory = e.player.inventory;
            for (int i = 0; i < inventory.getSizeInventory(); i++) {
                ItemStack item = inventory.getStackInSlot(i);
                if (item == null) {
                    continue;
                }
                if (item.getItem() instanceof ItemArmor) {

                } else {
                    if (item.stackSize > item.getMaxStackSize()) {
                        inventory.setInventorySlotContents(i, null);

                        if (!addItemStackToInventory(inventory, item)) {
                            EntityItem entityItem = e.player.dropPlayerItemWithRandomChoice(item, false);
                            entityItem.func_145797_a(e.player.getCommandSenderName());
                        }
                    }
                }
            }
        }
    }

    public boolean addItemStackToInventory(InventoryPlayer inventory, final ItemStack item) {
        if (item != null && item.stackSize != 0 && item.getItem() != null) {
            try {
                int i;

                do {
                    i = item.stackSize;
                    item.stackSize = storePartialItemStack(inventory, item);
                } while (item.stackSize > 0 && item.stackSize < i);

                return item.stackSize < i;
            } catch (Throwable throwable) {
                CrashReport crashreport = CrashReport.makeCrashReport(throwable, "Adding item to inventory");
                CrashReportCategory crashreportcategory = crashreport.makeCategory("Item being added");
                crashreportcategory.addCrashSection("Item ID", Item.getIdFromItem(item.getItem()));
                crashreportcategory.addCrashSection("Item data", item.getItemDamage());
                crashreportcategory.addCrashSectionCallable("Item name", new Callable() {
                    //private static final String __OBFID = "CL_00001710";

                    public String call() {
                        return item.getDisplayName();
                    }
                });
                throw new ReportedException(crashreport);
            }
        } else {
            return false;
        }
    }

    private int storePartialItemStack(InventoryPlayer inventory, ItemStack p_70452_1_)
            throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        Item item = p_70452_1_.getItem();
        int i = p_70452_1_.stackSize;
        int j;
        j = (int) ReflectionHelper
                .findMethod(InventoryPlayer.class, inventory, new String[]{"storeItemStack", "func_70432_d"}, ItemStack.class)
                .invoke(inventory, p_70452_1_);

        if (j < 0) {
            j = inventory.getFirstEmptyStack();
        }

        if (j < 0) {
            return i;
        } else {
            if (inventory.mainInventory[j] == null) {
                inventory.mainInventory[j] = new ItemStack(item, 0, p_70452_1_.getItemDamage());

                if (p_70452_1_.hasTagCompound()) {
                    inventory.mainInventory[j].setTagCompound((NBTTagCompound) p_70452_1_.getTagCompound().copy());
                }
            }

            int k = i;

            if (i > inventory.mainInventory[j].getMaxStackSize() - inventory.mainInventory[j].stackSize) {
                k = inventory.mainInventory[j].getMaxStackSize() - inventory.mainInventory[j].stackSize;
            }

            if (k > inventory.getInventoryStackLimit() - inventory.mainInventory[j].stackSize) {
                k = inventory.getInventoryStackLimit() - inventory.mainInventory[j].stackSize;
            }

            if (k == 0) {
                return i;
            } else {
                i -= k;
                inventory.mainInventory[j].stackSize += k;
                inventory.mainInventory[j].animationsToGo = 5;
                return i;
            }
        }
    }
}
