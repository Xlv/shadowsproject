package ru.xlv.gun.network.packets;

import cpw.mods.fml.common.network.simpleimpl.IMessage;
import io.netty.buffer.ByteBuf;

public class PacketReload implements IMessage {

	public int toSend;

	public PacketReload() {
	}

	public PacketReload(int toSend) {
		this.toSend = toSend;
	}

	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeInt(toSend);
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		toSend = buf.readInt();
	}
}