package ru.xlv.gun.network.packets;

import cpw.mods.fml.common.network.simpleimpl.IMessage;
import io.netty.buffer.ByteBuf;


public class PacketSpawnBlood implements IMessage {

    public double x;
    public double y;
    public double z;
    public boolean isHead;

    public PacketSpawnBlood() {
    }

    public PacketSpawnBlood(double posXFloat, double posYFloat, double posZFloat, boolean head) {
        this.x = posXFloat;
        this.y = posYFloat;
        this.z = posZFloat;
        this.isHead = head;
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeDouble(x);
        buf.writeDouble(y);
        buf.writeDouble(z);
        buf.writeBoolean(isHead);
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        x = buf.readDouble();
        y = buf.readDouble();
        z = buf.readDouble();
        isHead = buf.readBoolean();
    }
}