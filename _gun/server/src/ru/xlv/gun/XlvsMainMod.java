package ru.xlv.gun;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkRegistry;
import net.minecraftforge.common.MinecraftForge;
import ru.xlv.gun.item.Items;
import ru.xlv.gun.network.PacketHandler;

@Mod(
        name = "XlvsGunMod",
        modid = XlvsMainMod.MODID,
        version = "1.0"
)
public class XlvsMainMod {

    public static final String MODID = "xlvsgunmod";

    @Mod.Instance(MODID)
    public static XlvsMainMod instance;

    @Mod.EventHandler
    public void event(FMLPreInitializationEvent event) {
        Items.loadItems();
        PacketHandler.registryPackets();
        NetworkRegistry.INSTANCE.registerGuiHandler(this, new CommonGuiHandler());
        MinecraftForge.EVENT_BUS.register(new CommonEventHandler());
        FMLCommonHandler.instance().bus().register(new CommonEventHandler());
    }
}
