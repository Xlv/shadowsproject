package ru.xlv.gun.network.packethandler;

import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import ru.xlv.gun.item.Items;
import ru.xlv.gun.network.packets.PacketDetach;

public class PacketDetachHandler implements IMessageHandler<PacketDetach, IMessage> {

	@Override
	public IMessage onMessage(PacketDetach message, MessageContext ctx) {
		EntityPlayerMP serverPlayer = ctx.getServerHandler().playerEntity;
		String attachname = message.attachname;
		if (!serverPlayer.worldObj.isRemote) {
			NBTTagCompound nbtStack = serverPlayer.getHeldItem().getTagCompound();
			if (attachname.equals("Прицел ПК-АС")) {
				if (nbtStack.getString("scope") != null) {
					serverPlayer.inventory.addItemStackToInventory(new ItemStack(Items.pkas, 1));
					serverPlayer.getCurrentEquippedItem().getTagCompound().setString("scope", "");
					serverPlayer.inventoryContainer.detectAndSendChanges();
				}
			}
			if (attachname.equals("Прицел ПСО-1")) {
				if (nbtStack.getString("scope") != null) {
					serverPlayer.inventory.addItemStackToInventory(new ItemStack(Items.pso, 1));
					nbtStack.setString("scope", "");
					serverPlayer.inventoryContainer.detectAndSendChanges();
				}
			}
			if (attachname.equals("SUSAT")) {
				if (nbtStack.getString("scope") != null) {
					serverPlayer.inventory.addItemStackToInventory(new ItemStack(Items.susat, 1));
					nbtStack.setString("scope", "");
					serverPlayer.inventoryContainer.detectAndSendChanges();
				}
			}
			if (attachname.equals("Barska Sight")) {
				if (nbtStack.getString("scope") != null) {
					serverPlayer.inventory.addItemStackToInventory(new ItemStack(Items.barska, 1));
					;
					nbtStack.setString("scope", "");
					serverPlayer.inventoryContainer.detectAndSendChanges();
				}
			}
			if (attachname.equals("Leupold HAMR")) {
				if (nbtStack.getString("scope") != null) {
					serverPlayer.inventory.addItemStackToInventory(new ItemStack(Items.leupold, 1));
					nbtStack.setString("scope", "");
					serverPlayer.inventoryContainer.detectAndSendChanges();
				}
			}
			if (attachname.equals("ACOG")) {
				if (nbtStack.getString("scope") != null) {
					serverPlayer.inventory.addItemStackToInventory(new ItemStack(Items.acog, 1));
					nbtStack.setString("scope", "");
					serverPlayer.inventoryContainer.detectAndSendChanges();
				}
			}
			if (attachname.equals("Прицел ПОСП")) {
				if (nbtStack.getString("scope") != null) {
					serverPlayer.inventory.addItemStackToInventory(new ItemStack(Items.posp, 1));
					nbtStack.setString("scope", "");
					serverPlayer.inventoryContainer.detectAndSendChanges();
				}
			}
			if (attachname.equals("TR20")) {
				if (nbtStack.getString("scope") != null) {
					serverPlayer.inventory.addItemStackToInventory(new ItemStack(Items.trAc, 1));
					nbtStack.setString("scope", "");
					serverPlayer.inventoryContainer.detectAndSendChanges();
				}
			}
			if (attachname.equals("Прицел Кобра")) {
				if (nbtStack.getString("scope") != null) {
					serverPlayer.inventory.addItemStackToInventory(new ItemStack(Items.kobra, 1));
					nbtStack.setString("scope", "");
					serverPlayer.inventoryContainer.detectAndSendChanges();
				}
			} else if (attachname.equals("OTAN Silencer")) {
				if (nbtStack.getString("stvol") != null) {
					serverPlayer.inventory.addItemStackToInventory(new ItemStack(Items.natoSilencer, 1));
					nbtStack.setString("stvol", "");
					serverPlayer.inventoryContainer.detectAndSendChanges();
				}
			} else if (attachname.equals("Osprey")) {
				if (nbtStack.getString("stvol") != null) {
					serverPlayer.inventory.addItemStackToInventory(new ItemStack(Items.osprey, 1));
					nbtStack.setString("stvol", "");
					serverPlayer.inventoryContainer.detectAndSendChanges();
				}
			} else if (attachname.equals("ПБС-4")) {
				if (nbtStack.getString("stvol") != null) {
					serverPlayer.inventory.addItemStackToInventory(new ItemStack(Items.pbs4, 1));
					nbtStack.setString("stvol", "");
					serverPlayer.inventoryContainer.detectAndSendChanges();
				}
			} else if (attachname.equals("7x62 AAC SDN")) {
				if (nbtStack.getString("stvol") != null) {
					serverPlayer.inventory.addItemStackToInventory(new ItemStack(Items.aac762sdn, 1));
					nbtStack.setString("stvol", "");
					serverPlayer.inventoryContainer.detectAndSendChanges();
				}
			} else if (attachname.equals("Лазер")) {
				if (nbtStack.getString("laser") != null) {
					serverPlayer.inventory.addItemStackToInventory(new ItemStack(Items.laser, 1));
					nbtStack.setString("laser", "");
					serverPlayer.inventoryContainer.detectAndSendChanges();
				}
			}
		}
		return null;
	}
}