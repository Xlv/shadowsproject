package ru.xlv.gun.network.packethandler;

import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import ru.xlv.gun.item.ItemWeapon;
import ru.xlv.gun.network.packets.PacketShSound;
import ru.xlv.gun.util.ExtendedPlayer;

public class PacketShSoundHandler implements IMessageHandler<PacketShSound, IMessage> {

    @Override
    public IMessage onMessage(PacketShSound message, MessageContext ctx) {
        EntityPlayerMP sp = ctx.getServerHandler().playerEntity;
        ExtendedPlayer ep = ExtendedPlayer.get(sp);
        World w = sp.worldObj;
        if (!w.isRemote && sp.getCurrentEquippedItem() != null && sp.getCurrentEquippedItem().getItem() instanceof ItemWeapon) {
            ItemStack gs = sp.getCurrentEquippedItem();
            ItemWeapon gun = (ItemWeapon) gs.getItem();
            if (gs.getTagCompound().getInteger("ammo") > 0 && gs.getTagCompound().getFloat("reloadTime") == 0) {
                if (!sp.capabilities.isCreativeMode) {
                    gs.getTagCompound().setInteger("ammo", gs.getTagCompound().getInteger("ammo") - 1);
                }
                if (!(gs.getTagCompound().getString("stvol").length() > 2))
                    sp.playSound(gun.getShootSound(), 3F, 1F);
                else
                    sp.playSound(gun.getSilSound(), 1F, 1F);
            } else if (ep.getST() <= 0 && gs.getTagCompound().getInteger("ammo") == 0 && gs.getTagCompound().getFloat("reloadTime") == 0) {
                w.playSoundAtEntity(sp, "batmod:empty", 1.0F, 1.0F);
                ep.upST(7);
            }
        }
        return null;
    }
}
