package noppes.npcs;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.StatCollector;
import noppes.npcs.entity.EntityNPCInterface;

import java.awt.*;
import java.awt.datatransfer.*;
import java.io.IOException;
import java.util.Arrays;

public class NoppesStringUtils
{
    static final int[] illegalChars = new int[] {34, 60, 62, 124, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 58, 42, 63, 92, 47};

    public static String cleanFileName(String badFileName)
    {
        StringBuilder cleanName = new StringBuilder();

        for (int i = 0; i < badFileName.length(); ++i)
        {
            char c = badFileName.charAt(i);

            if (Arrays.binarySearch(illegalChars, c) < 0)
            {
                cleanName.append((char)c);
            }
        }

        return cleanName.toString();
    }

    public static String formatText(String text, Object ... obs)
    {
        Object[] var2 = obs;
        int var3 = obs.length;

        for (int var4 = 0; var4 < var3; ++var4)
        {
            Object ob = var2[var4];

            if (ob instanceof EntityPlayer)
            {
                String username = ((EntityPlayer)ob).getDisplayName();
                text = text.replace("{player}", username);
                text = text.replace("@p", username);
            }
            else if (ob instanceof EntityNPCInterface)
            {
                text = text.replace("@npc", ((EntityNPCInterface)ob).getCommandSenderName());
            }
        }

        text = text.replace("&", Character.toChars(167)[0] + "");
        return text;
    }

    public static void setClipboardContents(String aString)
    {
        StringSelection stringSelection = new StringSelection(aString);
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        clipboard.setContents(stringSelection, new ClipboardOwner()
        {
            public void lostOwnership(Clipboard arg0, Transferable arg1) {}
        });
    }

    public static String getClipboardContents()
    {
        String result = "";
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        Transferable contents = clipboard.getContents((Object)null);
        boolean hasTransferableText = contents != null && contents.isDataFlavorSupported(DataFlavor.stringFlavor);

        if (hasTransferableText)
        {
            try
            {
                result = (String)contents.getTransferData(DataFlavor.stringFlavor);
            }
            catch (UnsupportedFlavorException var5)
            {
                System.err.println(var5);
                var5.printStackTrace();
            }
            catch (IOException var6)
            {
                System.err.println(var6);
                var6.printStackTrace();
            }
        }

        return result;
    }

    public static String translate(Object ... arr)
    {
        String s = "";
        Object[] var2 = arr;
        int var3 = arr.length;

        for (int var4 = 0; var4 < var3; ++var4)
        {
            Object str = var2[var4];
            s = s + StatCollector.translateToLocal(str.toString());
        }

        return s;
    }

    static
    {
        Arrays.sort(illegalChars);
    }
}
