package noppes.npcs;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.*;
import cpw.mods.fml.common.network.FMLEventChannel;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.registry.EntityRegistry;
import foxz.command.CommandNoppes;
import net.minecraft.block.Block;
import net.minecraft.block.BlockIce;
import net.minecraft.block.BlockLeavesBase;
import net.minecraft.block.BlockVine;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityList;
import net.minecraft.server.MinecraftServer;
import net.minecraftforge.common.ForgeChunkManager;
import net.minecraftforge.common.MinecraftForge;
import noppes.npcs.config.ConfigLoader;
import noppes.npcs.config.ConfigProp;
import noppes.npcs.controllers.*;
import noppes.npcs.enchants.EnchantInterface;
import noppes.npcs.entity.*;
import noppes.npcs.entity.old.*;

import java.io.File;
import java.util.Iterator;
import java.util.Set;

@Mod(
    modid = "customnpcs",
    name = "CustomNpcsServer",
    version = "1.7.1d"
)
public class CustomNpcs
{
    @ConfigProp(
        info = "Disable Chat Bubbles"
    )
    public static boolean EnableChatBubbles = true;
    private static int NewEntityStartId = 0;
    @ConfigProp(
        info = "Navigation search range for NPCs. Not recommended to increase if you have a slow pc or on a server"
    )
    public static int NpcNavRange = 32;
    @ConfigProp(
        info = "Set to true if you want the dialog command option to be able to use op commands like tp etc"
    )
    public static boolean NpcUseOpCommands = false;
    @ConfigProp
    public static boolean InventoryGuiEnabled = true;
    @ConfigProp
    public static boolean DisableExtraItems = false;
    @ConfigProp
    public static boolean DisableExtraBlock = false;
    public static long ticks;
    @SidedProxy(
        clientSide = "noppes.npcs.client.ClientProxy",
        serverSide = "noppes.npcs.CommonProxy"
    )
    public static CommonProxy proxy;
    @ConfigProp(
        info = "Enables CustomNpcs startup update message"
    )
    //public static boolean EnableUpdateChecker = true;
    public static CustomNpcs instance;
    public static boolean FreezeNPCs = false;
    @ConfigProp(
        info = "Only ops can create and edit npcs"
    )
    public static boolean OpsOnly = false;
    @ConfigProp(
        info = "Default interact line. Leave empty to not have one"
    )
    public static String DefaultInteractLine = "Hello @p";
    @ConfigProp
    public static boolean DisableEnchants = false;
    @ConfigProp(
        info = "Start Id for enchants. IDs can only range from 0-256"
    )
    public static int EnchantStartId = 100;
    @ConfigProp(
        info = "Number of chunk loading npcs that can be active at the same time"
    )
    public static int ChuckLoaders = 20;
    public static File Dir;
    @ConfigProp(
        info = "Set to false if you want to disable guns"
    )
    public static boolean GunsEnabled = true;
    @ConfigProp(
        info = "Enables leaves decay"
    )
    public static boolean LeavesDecayEnabled = true;
    @ConfigProp(
        info = "Enables Vine Growth"
    )
    public static boolean VineGrowthEnabled = true;
    @ConfigProp(
        info = "Enables Ice Melting"
    )
    public static boolean IceMeltsEnabled = true;
    @ConfigProp(
        info = "Normal players can use soulstone on animals"
    )
    public static boolean SoulStoneAnimals = true;
    @ConfigProp(
        info = "Normal players can use soulstone on all npcs"
    )
    public static boolean SoulStoneNPCs = false;
    @ConfigProp(
        info = "When set to Minecraft it will use minecrafts font, when Default it will use OpenSans. Can only use fonts installed on your PC"
    )
    public static String FontType = "Default";
    @ConfigProp(
        info = "Font size for custom fonts (doesn\'t work with minecrafts font)"
    )
    public static int FontSize = 18;
    public static FMLEventChannel Channel;
    public static FMLEventChannel ChannelPlayer;
    public static ConfigLoader Config;

    public CustomNpcs()
    {
        instance = this;
    }

    @EventHandler
    public void load(FMLPreInitializationEvent ev)
    {
        Channel = NetworkRegistry.INSTANCE.newEventDrivenChannel("CustomNPCs");
        ChannelPlayer = NetworkRegistry.INSTANCE.newEventDrivenChannel("CustomNPCsPlayer");
        MinecraftServer server = MinecraftServer.getServer();
        String dir = "";

        if (server != null)
        {
            dir = (new File(".")).getAbsolutePath();
        }
        else
        {
            dir = Minecraft.getMinecraft().mcDataDir.getAbsolutePath();
        }

        Dir = new File(dir, "customnpcs");
        Dir.mkdir();
        Config = new ConfigLoader(this.getClass(), new File(dir, "config"), "CustomNpcs");
        Config.loadConfig();

        if (NpcNavRange < 16)
        {
            NpcNavRange = 16;
        }

        EnchantInterface.load();
        CustomItems.load();
        proxy.load();
        NetworkRegistry.INSTANCE.registerGuiHandler(this, proxy);
        MinecraftForge.EVENT_BUS.register(new ServerEventsHandler());
        MinecraftForge.EVENT_BUS.register(new ScriptController());
        FMLCommonHandler.instance().bus().register(new ServerTickHandler());
        this.registerNpc(EntityNPCHumanMale.class, "npchumanmale");
        this.registerNpc(EntityNPCVillager.class, "npcvillager");
        this.registerNpc(EntityNpcPony.class, "npcpony");
        this.registerNpc(EntityNPCHumanFemale.class, "npchumanfemale");
        this.registerNpc(EntityNPCDwarfMale.class, "npcdwarfmale");
        this.registerNpc(EntityNPCFurryMale.class, "npcfurrymale");
        this.registerNpc(EntityNpcMonsterMale.class, "npczombiemale");
        this.registerNpc(EntityNpcMonsterFemale.class, "npczombiefemale");
        this.registerNpc(EntityNpcSkeleton.class, "npcskeleton");
        this.registerNpc(EntityNPCDwarfFemale.class, "npcdwarffemale");
        this.registerNpc(EntityNPCFurryFemale.class, "npcfurryfemale");
        this.registerNpc(EntityNPCOrcMale.class, "npcorcfmale");
        this.registerNpc(EntityNPCOrcFemale.class, "npcorcfemale");
        this.registerNpc(EntityNPCElfMale.class, "npcelfmale");
        this.registerNpc(EntityNPCElfFemale.class, "npcelffemale");
        this.registerNpc(EntityNpcCrystal.class, "npccrystal");
        this.registerNpc(EntityNpcEnderchibi.class, "npcenderchibi");
        this.registerNpc(EntityNpcNagaMale.class, "npcnagamale");
        this.registerNpc(EntityNpcNagaFemale.class, "npcnagafemale");
        this.registerNpc(EntityNpcSlime.class, "NpcSlime");
        this.registerNpc(EntityNpcDragon.class, "NpcDragon");
        this.registerNpc(EntityNPCEnderman.class, "npcEnderman");
        this.registerNpc(EntityNPCGolem.class, "npcGolem");
        this.registerNpc(EntityCustomNpc.class, "CustomNpc");
        this.registerNewEntity(EntityChairMount.class, "CustomNpcChairMount", 64, 10, false);
        this.registerNewEntity(EntityProjectile.class, "throwableitem", 64, 3, true);
        this.registerNewEntity(EntityMagicProjectile.class, "magicprojectile", 64, 3, true);
        new RecipeController();
        ForgeChunkManager.setForcedChunkLoadingCallback(this, new ChunkController());
        new CustomNpcsPermissions();
        PixelmonHelper.load();
    }

    @EventHandler
    public void setAboutToStart(FMLServerAboutToStartEvent event)
    {
        ChunkController.instance.clear();
        new QuestController();
        new PlayerDataController();
        new FactionController();
        new TransportController();
        new GlobalDataController();
        new SpawnController();
        new LinkedNpcController();
        ScriptController.Instance.loadStoredData();
        ScriptController.HasStart = false;
        Set names = Block.blockRegistry.getKeys();
        Iterator var3 = names.iterator();

        while (var3.hasNext())
        {
            String name = (String)var3.next();
            Block block = (Block)Block.blockRegistry.getObject(name);

            if (block instanceof BlockLeavesBase)
            {
                block.setTickRandomly(LeavesDecayEnabled);
            }

            if (block instanceof BlockVine)
            {
                block.setTickRandomly(VineGrowthEnabled);
            }

            if (block instanceof BlockIce)
            {
                block.setTickRandomly(IceMeltsEnabled);
            }
        }
    }

    @EventHandler
    public void started(FMLServerStartedEvent event)
    {
        RecipeController.instance.load();
        new DialogController();
        new BankController();
        QuestController.instance.load();
        ScriptController.HasStart = true;
        ServerCloneController.Instance = new ServerCloneController();
    }

    @EventHandler
    public void stopped(FMLServerStoppedEvent event)
    {
        ServerCloneController.Instance = null;
    }

    @EventHandler
    public void serverstart(FMLServerStartingEvent event)
    {
        event.registerServerCommand(new CommandNoppes());
    }

    private void registerNpc(Class <? extends Entity > cl, String name)
    {
        EntityRegistry.registerModEntity(cl, name, NewEntityStartId++, this, 64, 3, true);
        EntityList.stringToClassMapping.put(name, cl);
    }

    private void registerNewEntity(Class <? extends Entity > cl, String name, int range, int update, boolean velocity)
    {
        EntityRegistry.registerModEntity(cl, name, NewEntityStartId++, this, range, update, velocity);
    }

    public static File getWorldSaveDirectory()
    {
        MinecraftServer server = MinecraftServer.getServer();
        File saves = new File(".");

        if (server != null && !server.isDedicatedServer())
        {
            saves = new File(Minecraft.getMinecraft().mcDataDir, "saves");
        }

        if (server != null)
        {
            File savedir = new File(new File(saves, server.getFolderName()), "customnpcs");

            if (!savedir.exists())
            {
                savedir.mkdir();
            }

            return savedir;
        }
        else
        {
            return null;
        }
    }
}
