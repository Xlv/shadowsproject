package noppes.npcs.containers;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;

public class InventoryNpcTrader implements IInventory
{
    private String inventoryTitle;
    private int slotsCount;
    private ItemStack[] inventoryContents;
    private ContainerNPCTrader con;

    public InventoryNpcTrader(String s, int i, ContainerNPCTrader con)
    {
        this.con = con;
        this.inventoryTitle = s;
        this.slotsCount = i;
        this.inventoryContents = new ItemStack[i];
    }

    /**
     * Returns the stack in slot i
     */
    public ItemStack getStackInSlot(int i)
    {
        ItemStack toBuy = this.inventoryContents[i];
        return toBuy == null ? null : ItemStack.copyItemStack(toBuy);
    }

    /**
     * Removes from an inventory slot (first arg) up to a specified number (second arg) of items and returns them in a
     * new stack.
     */
    public ItemStack decrStackSize(int i, int j)
    {
        if (this.inventoryContents[i] != null)
        {
            ItemStack itemstack = this.inventoryContents[i];
            return ItemStack.copyItemStack(itemstack);
        }
        else
        {
            return null;
        }
    }

    /**
     * Sets the given item stack to the specified slot in the inventory (can be crafting or armor sections).
     */
    public void setInventorySlotContents(int i, ItemStack itemstack)
    {
        if (itemstack != null)
        {
            this.inventoryContents[i] = itemstack.copy();
        }

        this.markDirty();
    }

    /**
     * Returns the number of slots in the inventory.
     */
    public int getSizeInventory()
    {
        return this.slotsCount;
    }

    /**
     * Returns the maximum stack size for a inventory slot.
     */
    public int getInventoryStackLimit()
    {
        return 64;
    }

    /**
     * Do not make give this method the name canInteractWith because it clashes with Container
     */
    public boolean isUseableByPlayer(EntityPlayer entityplayer)
    {
        return true;
    }

    /**
     * When some containers are closed they call this on each slot, then drop whatever it returns as an EntityItem -
     * like when you close a workbench GUI.
     */
    public ItemStack getStackInSlotOnClosing(int i)
    {
        return null;
    }

    /**
     * Returns true if automation is allowed to insert the given stack (ignoring stack size) into the given slot.
     */
    public boolean isItemValidForSlot(int i, ItemStack itemstack)
    {
        return true;
    }

    /**
     * Returns the name of the inventory
     */
    public String getInventoryName()
    {
        return this.inventoryTitle;
    }

    /**
     * Returns if the inventory name is localized
     */
    public boolean hasCustomInventoryName()
    {
        return true;
    }

    /**
     * Called when an the contents of an Inventory change, usually
     */
    public void markDirty()
    {
        this.con.onCraftMatrixChanged(this);
    }

    public void openInventory() {}

    public void closeInventory() {}
}
