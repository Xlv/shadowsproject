package noppes.npcs.blocks.tiles;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.AxisAlignedBB;

public class TileBanner extends TileColorable
{
    public ItemStack icon;
    public long time = 0L;

    public void readFromNBT(NBTTagCompound compound)
    {
        super.readFromNBT(compound);
        this.icon = ItemStack.loadItemStackFromNBT(compound.getCompoundTag("BannerIcon"));
    }

    public void writeToNBT(NBTTagCompound compound)
    {
        super.writeToNBT(compound);

        if (this.icon != null)
        {
            compound.setTag("BannerIcon", this.icon.writeToNBT(new NBTTagCompound()));
        }
    }

    public AxisAlignedBB getRenderBoundingBox()
    {
        return AxisAlignedBB.getBoundingBox((double)this.xCoord, (double)this.yCoord, (double)this.zCoord, (double)(this.xCoord + 1), (double)(this.yCoord + 2), (double)(this.zCoord + 1));
    }

    public boolean canEdit()
    {
        return System.currentTimeMillis() - this.time < 10000L;
    }
}
