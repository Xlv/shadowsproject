package noppes.npcs.roles;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import noppes.npcs.CustomItems;
import noppes.npcs.constants.EnumBardInstrument;
import noppes.npcs.entity.EntityNPCInterface;

public class JobBard extends JobInterface
{
    public int minRange = 2;
    public int maxRange = 64;
    public boolean isStreamer = true;
    public boolean hasOffRange = true;
    public String song = "";
    private EnumBardInstrument instrument;
    private long ticks;

    public JobBard(EntityNPCInterface npc)
    {
        super(npc);
        this.instrument = EnumBardInstrument.Banjo;
        this.ticks = 0L;

        if (CustomItems.banjo != null)
        {
            this.mainhand = new ItemStack(CustomItems.banjo);
            this.overrideMainHand = this.overrideOffHand = true;
        }
    }

    public NBTTagCompound writeToNBT(NBTTagCompound nbttagcompound)
    {
        nbttagcompound.setString("BardSong", this.song);
        nbttagcompound.setInteger("BardMinRange", this.minRange);
        nbttagcompound.setInteger("BardMaxRange", this.maxRange);
        nbttagcompound.setInteger("BardInstrument", this.instrument.ordinal());
        nbttagcompound.setBoolean("BardStreamer", this.isStreamer);
        nbttagcompound.setBoolean("BardHasOff", this.hasOffRange);
        return nbttagcompound;
    }

    public void readFromNBT(NBTTagCompound nbttagcompound)
    {
        this.song = nbttagcompound.getString("BardSong");
        this.minRange = nbttagcompound.getInteger("BardMinRange");
        this.maxRange = nbttagcompound.getInteger("BardMaxRange");
        this.setInstrument(nbttagcompound.getInteger("BardInstrument"));
        this.isStreamer = nbttagcompound.getBoolean("BardStreamer");
        this.hasOffRange = nbttagcompound.getBoolean("BardHasOff");
    }

    public void setInstrument(int i)
    {
        if (CustomItems.banjo != null)
        {
            this.instrument = EnumBardInstrument.values()[i];
            this.overrideMainHand = this.overrideOffHand = this.instrument != EnumBardInstrument.None;

            switch (this.instrument.ordinal())
            {
                case 1:
                    this.mainhand = null;
                    this.offhand = null;
                    break;

                case 2:
                    this.mainhand = new ItemStack(CustomItems.banjo);
                    this.offhand = null;
                    break;

                case 3:
                    this.mainhand = new ItemStack(CustomItems.violin);
                    this.offhand = new ItemStack(CustomItems.violinbow);
                    break;

                case 4:
                    this.mainhand = new ItemStack(CustomItems.guitar);
                    this.offhand = null;
                    break;

                case 5:
                    this.mainhand = new ItemStack(CustomItems.harp);
                    this.offhand = null;
                    break;

                case 6:
                    this.mainhand = new ItemStack(CustomItems.frenchHorn);
                    this.offhand = null;
            }
        }
    }

    public EnumBardInstrument getInstrument()
    {
        return this.instrument;
    }

    public void onLivingUpdate() { }

    public void killed()
    {
        this.delete();
    }

    public void delete() { }
}
