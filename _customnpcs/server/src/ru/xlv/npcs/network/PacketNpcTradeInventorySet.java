package ru.xlv.npcs.network;

import cpw.mods.fml.common.network.ByteBufUtils;
import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import noppes.npcs.entity.EntityNPCInterface;
import ru.xlv.core.common.network.IPacketInOnServer;
import ru.xlv.core.common.network.IPacketOut;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.npcs.TraderNpcConfig;

import java.io.IOException;
import java.util.List;

@NoArgsConstructor
public class PacketNpcTradeInventorySet implements IPacketOut, IPacketInOnServer {

    private int entityId;
    private List<ItemStack> itemStackList;

    public PacketNpcTradeInventorySet(int entityId, List<ItemStack> itemStackList) {
        this.entityId = entityId;
        this.itemStackList = itemStackList;
    }

    @Override
    public void write(ByteBufOutputStream bbos) throws IOException {
        bbos.writeInt(entityId);
        writeCollection(bbos, itemStackList, (byteBufOutputStream, itemStack) -> ByteBufUtils.writeItemStack(bbos.buffer(), itemStack));
    }

    @Override
    public void read(EntityPlayer entityPlayer, ByteBufInputStream bbis) throws IOException {
        if(!entityPlayer.capabilities.isCreativeMode) {
            return;
        }
        int entityId = bbis.readInt();
        List<ItemStack> itemStacks = readList(bbis, byteBufInputStream -> ByteBufUtils.readItemStack(bbis.getBuffer()));
        Entity entityByID = entityPlayer.worldObj.getEntityByID(entityId);
        if (entityByID instanceof EntityNPCInterface) {
            ((EntityNPCInterface) entityByID).getMatrixInventory().clear();
            itemStacks.forEach(((EntityNPCInterface) entityByID).getMatrixInventory()::addItem);
            TraderNpcConfig.save(entityByID.getCommandSenderName(), itemStacks);
        }
    }
}
