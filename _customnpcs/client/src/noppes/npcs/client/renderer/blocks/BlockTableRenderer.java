package noppes.npcs.client.renderer.blocks;

import cpw.mods.fml.client.registry.RenderingRegistry;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.entity.Entity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import noppes.npcs.CustomItems;
import noppes.npcs.blocks.BlockTable;
import noppes.npcs.blocks.tiles.TileColorable;
import noppes.npcs.client.model.blocks.ModelTable;
import org.lwjgl.opengl.GL11;

public class BlockTableRenderer extends BlockRendererInterface
{
    private final ModelTable model = new ModelTable();
    private static final ResourceLocation resource1 = new ResourceLocation("customnpcs", "textures/cache/planks_oak.png");
    private static final ResourceLocation resource2 = new ResourceLocation("customnpcs", "textures/cache/planks_big_oak.png");
    private static final ResourceLocation resource3 = new ResourceLocation("customnpcs", "textures/cache/planks_spruce.png");
    private static final ResourceLocation resource4 = new ResourceLocation("customnpcs", "textures/cache/planks_birch.png");
    private static final ResourceLocation resource5 = new ResourceLocation("customnpcs", "textures/cache/planks_acacia.png");
    private static final ResourceLocation resource6 = new ResourceLocation("customnpcs", "textures/cache/planks_jungle.png");

    public BlockTableRenderer()
    {
        ((BlockTable)CustomItems.table).renderId = RenderingRegistry.getNextAvailableRenderId();
        RenderingRegistry.registerBlockHandler(this);
    }

    public void renderTileEntityAt(TileEntity tileEntity, double x, double y, double z, float timeTick)
    {
        TileColorable tile = (TileColorable) tileEntity;
        GL11.glPushMatrix();
        GL11.glTranslatef((float) x + 0.5F, (float) y + 1.5F, (float) z + 0.5F);
        GL11.glRotatef(180.0F, 0.0F, 0.0F, 1.0F);
        GL11.glColor3f(1.0F, 1.0F, 1.0F);
        boolean south = tileEntity.getWorldObj().getBlock(tileEntity.xCoord + 1, tileEntity.yCoord, tileEntity.zCoord) == CustomItems.table;
        boolean north = tileEntity.getWorldObj().getBlock(tileEntity.xCoord - 1, tileEntity.yCoord, tileEntity.zCoord) == CustomItems.table;
        boolean east = tileEntity.getWorldObj().getBlock(tileEntity.xCoord, tileEntity.yCoord, tileEntity.zCoord + 1) == CustomItems.table;
        boolean west = tileEntity.getWorldObj().getBlock(tileEntity.xCoord, tileEntity.yCoord, tileEntity.zCoord - 1) == CustomItems.table;
        this.model.Shape1.showModel = !south && !east;
        this.model.Shape3.showModel = !north && !west;
        this.model.Shape4.showModel = !north && !east;
        this.model.Shape5.showModel = !south && !west;
        this.setWoodTexture(tileEntity.getBlockMetadata());
        this.model.render((Entity)null, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
        GL11.glRotatef((float)(90 * tile.rotation), 0.0F, 1.0F, 0.0F);
        this.model.Table.render(0.0625F);
        GL11.glPopMatrix();
    }

    public void renderTileEntityAtPost(TileEntity tileEntity, double x, double y, double z, float timeTick) {

    }

    public void renderInventoryBlock(Block block, int metadata, int modelId, RenderBlocks renderer)
    {
        GL11.glPushMatrix();
        GL11.glTranslatef(0.0F, 0.9F, 0.0F);
        GL11.glRotatef(180.0F, 0.0F, 0.0F, 1.0F);
        GL11.glRotatef(180.0F, 0.0F, 1.0F, 0.0F);
        this.setWoodTexture(metadata);
        GL11.glColor3f(1.0F, 1.0F, 1.0F);
        this.model.Table.render(0.0625F);
        this.model.Shape1.showModel = true;
        this.model.Shape3.showModel = true;
        this.model.Shape4.showModel = true;
        this.model.Shape5.showModel = true;
        this.model.render((Entity)null, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
        GL11.glPopMatrix();
    }

    public int getRenderId()
    {
        return CustomItems.table.getRenderType();
    }
}
