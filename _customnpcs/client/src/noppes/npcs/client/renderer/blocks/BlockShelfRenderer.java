package noppes.npcs.client.renderer.blocks;

import cpw.mods.fml.client.registry.RenderingRegistry;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.entity.Entity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import noppes.npcs.CustomItems;
import noppes.npcs.blocks.BlockRotated;
import noppes.npcs.blocks.tiles.TileColorable;
import noppes.npcs.blocks.tiles.TileShelf;
import noppes.npcs.client.model.blocks.ModelShelf;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

public class BlockShelfRenderer extends BlockRendererInterface
{
    private final ModelShelf model = new ModelShelf();

    public BlockShelfRenderer()
    {
        ((BlockRotated)CustomItems.shelf).renderId = RenderingRegistry.getNextAvailableRenderId();
        RenderingRegistry.registerBlockHandler(this);
    }

    public void renderTileEntityAt(TileEntity tileEntity, double x, double y, double z, float timeTick)
    {
        TileColorable tile = (TileColorable) tileEntity;
        GL11.glDisable(GL12.GL_RESCALE_NORMAL);
        GL11.glPushMatrix();
        GL11.glTranslatef((float) x + 0.5F, (float) y + 1.5F, (float) z + 0.5F);
        GL11.glRotatef(180.0F, 0.0F, 0.0F, 1.0F);
        GL11.glRotatef((float)(90 * tile.rotation), 0.0F, 1.0F, 0.0F);
        GL11.glColor3f(1.0F, 1.0F, 1.0F);
        boolean drawLeft = true;
        boolean drawRight = true;

        if (tile.rotation == 3)
        {
            drawLeft = this.shouldDraw(tileEntity.getWorldObj(), tileEntity.xCoord, tileEntity.yCoord, tileEntity.zCoord - 1, 3);
            drawRight = this.shouldDraw(tileEntity.getWorldObj(), tileEntity.xCoord, tileEntity.yCoord, tileEntity.zCoord + 1, 3);
        }
        else if (tile.rotation == 1)
        {
            drawLeft = this.shouldDraw(tileEntity.getWorldObj(), tileEntity.xCoord, tileEntity.yCoord, tileEntity.zCoord + 1, 1);
            drawRight = this.shouldDraw(tileEntity.getWorldObj(), tileEntity.xCoord, tileEntity.yCoord, tileEntity.zCoord - 1, 1);
        }
        else if (tile.rotation == 0)
        {
            drawLeft = this.shouldDraw(tileEntity.getWorldObj(), tileEntity.xCoord + 1, tileEntity.yCoord, tileEntity.zCoord, 0);
            drawRight = this.shouldDraw(tileEntity.getWorldObj(), tileEntity.xCoord - 1, tileEntity.yCoord, tileEntity.zCoord, 0);
        }
        else if (tile.rotation == 2)
        {
            drawLeft = this.shouldDraw(tileEntity.getWorldObj(), tileEntity.xCoord - 1, tileEntity.yCoord, tileEntity.zCoord, 2);
            drawRight = this.shouldDraw(tileEntity.getWorldObj(), tileEntity.xCoord + 1, tileEntity.yCoord, tileEntity.zCoord, 2);
        }

        this.model.SupportLeft1.showModel = this.model.SupportLeft2.showModel = drawLeft;
        this.model.SupportRight1.showModel = this.model.SupportRight2.showModel = drawRight;
        this.setWoodTexture(tileEntity.getBlockMetadata());
        this.model.render((Entity)null, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
        GL11.glPopMatrix();
    }

    public void renderTileEntityAtPost(TileEntity tileEntity, double x, double y, double z, float timeTick) {

    }

    private boolean shouldDraw(World world, int x, int y, int z, int rotation)
    {
        TileEntity tile = world.getTileEntity(x, y, z);
        return tile != null && tile instanceof TileShelf ? ((TileShelf)tile).rotation != rotation : true;
    }

    public void renderInventoryBlock(Block block, int metadata, int modelId, RenderBlocks renderer)
    {
        GL11.glPushMatrix();
        GL11.glTranslatef(0.0F, 0.6F, 0.0F);
        GL11.glScalef(1.0F, 1.0F, 1.0F);
        GL11.glRotatef(180.0F, 0.0F, 0.0F, 1.0F);
        GL11.glRotatef(180.0F, 0.0F, 1.0F, 0.0F);
        this.setWoodTexture(metadata);
        GL11.glColor3f(1.0F, 1.0F, 1.0F);
        this.model.SupportLeft1.showModel = this.model.SupportLeft2.showModel = true;
        this.model.SupportRight1.showModel = this.model.SupportRight2.showModel = true;
        this.model.render((Entity)null, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
        GL11.glPopMatrix();
    }

    public int getRenderId()
    {
        return CustomItems.shelf.getRenderType();
    }
}
