package noppes.npcs.client;

import cpw.mods.fml.common.ObfuscationReflectionHelper;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.network.FMLNetworkEvent.ClientCustomPacketEvent;
import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.achievement.GuiAchievement;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ChatComponentTranslation;
import net.minecraft.util.StatCollector;
import net.minecraft.village.MerchantRecipeList;
import noppes.npcs.*;
import noppes.npcs.client.controllers.MusicController;
import noppes.npcs.client.gui.GuiNpcMobSpawnerAdd;
import noppes.npcs.client.gui.player.GuiBook;
import noppes.npcs.client.gui.util.*;
import noppes.npcs.constants.EnumGuiType;
import noppes.npcs.constants.EnumPacketClient;
import noppes.npcs.controllers.RecipeCarpentry;
import noppes.npcs.controllers.RecipeController;
import noppes.npcs.entity.EntityDialogNpc;
import noppes.npcs.entity.EntityNPCInterface;
import noppes.npcs.util.EntityUtil;

import java.io.IOException;
import java.util.HashMap;

public class PacketHandlerClient extends PacketHandlerServer
{
    @SubscribeEvent
    public void onPacketData(ClientCustomPacketEvent event)
    {
        EntityClientPlayerMP player = Minecraft.getMinecraft().thePlayer;
        ByteBuf buffer = event.packet.payload();

        try
        {
            this.client(buffer, player, EnumPacketClient.values()[buffer.readInt()]);
        }
        catch (IOException var5)
        {
            var5.printStackTrace();
        }
    }

    private void client(ByteBuf buffer, EntityPlayer player, EnumPacketClient type) throws IOException
    {
        Entity config;

        if (type == EnumPacketClient.CHATBUBBLE)
        {
            config = Minecraft.getMinecraft().theWorld.getEntityByID(buffer.readInt());

            if (config == null || !(config instanceof EntityNPCInterface))
            {
                return;
            }

            EntityNPCInterface font = (EntityNPCInterface)config;

            if (font.messages == null)
            {
                font.messages = new RenderChatMessages();
            }

            String size = NoppesStringUtils.formatText(Server.readString(buffer), new Object[] {player, font});
            font.messages.addMessage(size, font);

            if (buffer.readBoolean())
            {
                player.addChatMessage(new ChatComponentTranslation(font.getCommandSenderName() + ": " + size, new Object[0]));
            }
        }
        else
        {
            String var7;
            String var16;

            if (type == EnumPacketClient.CHAT)
            {
                for (var7 = ""; (var16 = Server.readString(buffer)) != null && !var16.isEmpty(); var7 = var7 + StatCollector.translateToLocal(var16))
                {
                    ;
                }

                player.addChatMessage(new ChatComponentTranslation(var7, new Object[0]));
            }
            else if (type == EnumPacketClient.MESSAGE)
            {
                var7 = StatCollector.translateToLocal(Server.readString(buffer));
                var16 = Server.readString(buffer);
                QuestAchievement var20 = new QuestAchievement(var16, var7);
                Minecraft.getMinecraft().guiAchievement.func_146256_a(var20);
                ObfuscationReflectionHelper.setPrivateValue(GuiAchievement.class, Minecraft.getMinecraft().guiAchievement, var20.getDescription(), 4);
            }
            else
            {
                int var17;

                if (type == EnumPacketClient.SYNCRECIPES_ADD)
                {
                    NBTTagList var8 = Server.readNBT(buffer).getTagList("recipes", 10);

                    if (var8 == null)
                    {
                        return;
                    }

                    for (var17 = 0; var17 < var8.tagCount(); ++var17)
                    {
                        RecipeCarpentry var21 = RecipeCarpentry.read(var8.getCompoundTagAt(var17));
                        RecipeController.syncRecipes.put(Integer.valueOf(var21.id), var21);
                    }
                }
                else if (type == EnumPacketClient.SYNCRECIPES_WORKBENCH)
                {
                    RecipeController.reloadGlobalRecipes(RecipeController.syncRecipes);
                    RecipeController.syncRecipes = new HashMap();
                }
                else if (type == EnumPacketClient.SYNCRECIPES_CARPENTRYBENCH)
                {
                    RecipeController.instance.anvilRecipes = RecipeController.syncRecipes;
                    RecipeController.syncRecipes = new HashMap();
                }
                else if (type == EnumPacketClient.DIALOG)
                {
                    config = Minecraft.getMinecraft().theWorld.getEntityByID(buffer.readInt());

                    if (config == null || !(config instanceof EntityNPCInterface))
                    {
                        return;
                    }

                    NoppesUtil.openDialog(Server.readNBT(buffer), (EntityNPCInterface)config, player);
                }
                else if (type == EnumPacketClient.DIALOG_DUMMY)
                {
                    EntityDialogNpc var9 = new EntityDialogNpc(player.worldObj);
                    var9.display.name = Server.readString(buffer);
                    EntityUtil.Copy(player, var9);
                    NoppesUtil.openDialog(Server.readNBT(buffer), var9, player);
                }
                else if (type == EnumPacketClient.QUEST_COMPLETION)
                {
                    NoppesUtil.guiQuestCompletion(player, Server.readNBT(buffer));
                }
                else if (type == EnumPacketClient.EDIT_NPC)
                {
                    config = Minecraft.getMinecraft().theWorld.getEntityByID(buffer.readInt());

                    if (config == null || !(config instanceof EntityNPCInterface))
                    {
                        return;
                    }

                    NoppesUtil.setLastNpc((EntityNPCInterface)config);
                }
                else if (type == EnumPacketClient.PLAY_MUSIC)
                {
                    MusicController.Instance.playMusic(Server.readString(buffer), player);
                }
                else if (type == EnumPacketClient.PLAY_SOUND)
                {
                    MusicController.Instance.playSound(Server.readString(buffer), buffer.readFloat(), buffer.readFloat(), buffer.readFloat());
                }
                else
                {
                    NBTTagCompound var10;
                    Entity var18;

                    if (type == EnumPacketClient.UPDATE_NPC)
                    {
                        var10 = Server.readNBT(buffer);
                        var18 = Minecraft.getMinecraft().theWorld.getEntityByID(var10.getInteger("EntityId"));

                        if (var18 == null || !(var18 instanceof EntityNPCInterface))
                        {
                            return;
                        }

                        ((EntityNPCInterface)var18).readSpawnData(var10);
                    }
                    else if (type == EnumPacketClient.ROLE)
                    {
                        var10 = Server.readNBT(buffer);
                        var18 = Minecraft.getMinecraft().theWorld.getEntityByID(var10.getInteger("EntityId"));

                        if (var18 == null || !(var18 instanceof EntityNPCInterface))
                        {
                            return;
                        }

                        ((EntityNPCInterface)var18).advanced.setRole(var10.getInteger("Role"));
                        ((EntityNPCInterface)var18).roleInterface.readFromNBT(var10);
                        NoppesUtil.setLastNpc((EntityNPCInterface)var18);
                    }
                    else if (type == EnumPacketClient.GUI)
                    {
                        EnumGuiType var11 = EnumGuiType.values()[buffer.readInt()];
                        CustomNpcs.proxy.openGui(NoppesUtil.getLastNpc(), var11, buffer.readInt(), buffer.readInt(), buffer.readInt());
                    }
                    else if (type == EnumPacketClient.PARTICLE)
                    {
                        NoppesUtil.spawnParticle(buffer);
                    }
                    else if (type == EnumPacketClient.DELETE_NPC)
                    {
                        config = Minecraft.getMinecraft().theWorld.getEntityByID(buffer.readInt());

                        if (config == null || !(config instanceof EntityNPCInterface))
                        {
                            return;
                        }

                        ((EntityNPCInterface)config).delete();
                    }
                    else if (type == EnumPacketClient.SCROLL_LIST)
                    {
                        NoppesUtil.setScrollList(buffer);
                    }
                    else if (type == EnumPacketClient.SCROLL_DATA)
                    {
                        NoppesUtil.setScrollData(buffer);
                    }
                    else if (type == EnumPacketClient.SCROLL_DATA_PART)
                    {
                        NoppesUtil.addScrollData(buffer);
                    }
                    else
                    {
                        GuiScreen var12;

                        if (type == EnumPacketClient.SCROLL_SELECTED)
                        {
                            var12 = Minecraft.getMinecraft().currentScreen;

                            if (var12 == null || !(var12 instanceof IScrollData))
                            {
                                return;
                            }

                            var16 = Server.readString(buffer);
                            ((IScrollData)var12).setSelected(var16);
                        }
                        else if (type == EnumPacketClient.GUI_REDSTONE)
                        {
                            NoppesUtil.saveRedstoneBlock(player, Server.readNBT(buffer));
                        }
                        else if (type == EnumPacketClient.GUI_WAYPOINT)
                        {
                            NoppesUtil.saveWayPointBlock(player, Server.readNBT(buffer));
                        }
                        else if (type == EnumPacketClient.CLONE)
                        {
                            var10 = Server.readNBT(buffer);
                            NoppesUtil.openGUI(player, new GuiNpcMobSpawnerAdd(var10));
                        }
                        else if (type == EnumPacketClient.GUI_DATA)
                        {
                            Object var13 = Minecraft.getMinecraft().currentScreen;

                            if (var13 == null)
                            {
                                return;
                            }

                            if (var13 instanceof GuiNPCInterface && ((GuiNPCInterface)var13).hasSubGui())
                            {
                                var13 = ((GuiNPCInterface)var13).getSubGui();
                            }
                            else if (var13 instanceof GuiContainerNPCInterface && ((GuiContainerNPCInterface)var13).hasSubGui())
                            {
                                var13 = ((GuiContainerNPCInterface)var13).getSubGui();
                            }

                            if (var13 instanceof IGuiData)
                            {
                                ((IGuiData)var13).setGuiData(Server.readNBT(buffer));
                            }
                        }
                        else
                        {
                            NBTTagCompound var22;

                            if (type == EnumPacketClient.GUI_ERROR)
                            {
                                var12 = Minecraft.getMinecraft().currentScreen;

                                if (var12 == null || !(var12 instanceof IGuiError))
                                {
                                    return;
                                }

                                var17 = buffer.readInt();
                                var22 = Server.readNBT(buffer);
                                ((IGuiError)var12).setError(var17, var22);
                            }
                            else if (type == EnumPacketClient.GUI_CLOSE)
                            {
                                var12 = Minecraft.getMinecraft().currentScreen;

                                if (var12 == null)
                                {
                                    return;
                                }

                                if (var12 instanceof IGuiClose)
                                {
                                    var17 = buffer.readInt();
                                    var22 = Server.readNBT(buffer);
                                    ((IGuiClose)var12).setClose(var17, var22);
                                }

                                Minecraft var19 = Minecraft.getMinecraft();
                                var19.displayGuiScreen((GuiScreen)null);
                                var19.setIngameFocus();
                            }
                            else if (type == EnumPacketClient.VILLAGER_LIST)
                            {
                                MerchantRecipeList var14 = MerchantRecipeList.func_151390_b(new PacketBuffer(buffer));
                                ServerEventsHandler.Merchant.setRecipes(var14);
                            }
                            else
                            {
                                int var15;
                                int var23;

                                if (type == EnumPacketClient.OPEN_BOOK)
                                {
                                    var15 = buffer.readInt();
                                    var17 = buffer.readInt();
                                    var23 = buffer.readInt();
                                    NoppesUtil.openGUI(player, new GuiBook(player, ItemStack.loadItemStackFromNBT(Server.readNBT(buffer)), var15, var17, var23));
                                }
                                else if (type == EnumPacketClient.CONFIG)
                                {
                                    var15 = buffer.readInt();

                                    if (var15 == 0)
                                    {
                                        var16 = Server.readString(buffer);
                                        var23 = buffer.readInt();

                                        if (!var16.isEmpty())
                                        {
                                            CustomNpcs.FontType = var16;
                                            CustomNpcs.FontSize = var23;
                                            ClientProxy.Font = new ClientProxy.FontContainer(CustomNpcs.FontType, CustomNpcs.FontSize);
                                            CustomNpcs.Config.updateConfig();
                                            player.addChatMessage(new ChatComponentTranslation("Font set to %s", new Object[] {ClientProxy.Font.getName()}));
                                        }
                                        else
                                        {
                                            player.addChatMessage(new ChatComponentTranslation("Current font is %s", new Object[] {ClientProxy.Font.getName()}));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
