package noppes.npcs.client.model.util;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.renderer.entity.NPCRendererHelper;
import net.minecraft.client.renderer.entity.RendererLivingEntity;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;

public class ModelRenderPassHelper extends ModelBase
{
    public RendererLivingEntity renderer;
    public EntityLivingBase entity;

    /**
     * Sets the models various rotation angles then renders the model.
     */
    public void render(Entity par1Entity, float par2, float par3, float par4, float par5, float par6, float par7)
    {
        ModelBase model = NPCRendererHelper.getPassModel(this.renderer);
        model.isChild = this.isChild;
        model.render(this.entity, par2, par3, par4, par5, par6, par7);
    }

    /**
     * Used for easily adding entity-dependent animations. The second and third float params here are the same second
     * and third as in the setRotationAngles method.
     */
    public void setLivingAnimations(EntityLivingBase par1EntityLivingBase, float par2, float par3, float par4)
    {
        ModelBase model = NPCRendererHelper.getPassModel(this.renderer);
        model.isChild = this.isChild;
        model.setLivingAnimations(this.entity, par2, par3, par4);
    }
}
