package noppes.npcs.client.gui;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.resources.IResource;
import net.minecraft.util.ResourceLocation;
import noppes.npcs.client.gui.util.GuiNpcButton;
import noppes.npcs.client.gui.util.GuiNpcTextField;
import noppes.npcs.client.gui.util.ITextfieldListener;
import noppes.npcs.client.gui.util.SubGuiInterface;
import org.lwjgl.opengl.GL11;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

public class SubGuiColorSelector extends SubGuiInterface implements ITextfieldListener
{
    private static final ResourceLocation resource = new ResourceLocation("customnpcs:textures/gui/color.png");
    private int colorX;
    private int colorY;
    private GuiNpcTextField textfield;
    public int color;

    public SubGuiColorSelector(int color)
    {
        this.xSize = 176;
        this.ySize = 222;
        this.color = color;
        this.setBackground("smallbg.png");
    }

    /**
     * Adds the buttons (and other controls) to the screen in question.
     */
    public void initGui()
    {
        super.initGui();
        this.colorX = this.guiLeft + 30;
        this.colorY = this.guiTop + 50;
        this.addTextField(this.textfield = new GuiNpcTextField(0, this, this.guiLeft + 53, this.guiTop + 20, 70, 20, this.getColor()));
        this.textfield.setTextColor(this.color);
        this.addButton(new GuiNpcButton(66, this.guiLeft + 112, this.guiTop + 198, 60, 20, "gui.done"));
    }

    public String getColor()
    {
        String str;

        for (str = Integer.toHexString(this.color); str.length() < 6; str = "0" + str)
        {
            ;
        }

        return str;
    }

    /**
     * Fired when a key is typed. This is the equivalent of KeyListener.keyTyped(KeyEvent e).
     */
    public void keyTyped(char character, int key)
    {
        String prev = this.textfield.getText();
        super.keyTyped(character, key);
        String newText = this.textfield.getText();

        if (!newText.equals(prev))
        {
            try
            {
                this.color = Integer.parseInt(this.textfield.getText(), 16);
                this.textfield.setTextColor(this.color);
            }
            catch (NumberFormatException var6)
            {
                this.textfield.setText(prev);
            }
        }
    }

    protected void actionPerformed(GuiButton guiButton)
    {
        super.actionPerformed(guiButton);

        if (guiButton.id == 66)
        {
            this.close();
        }
    }

    public void close()
    {
        super.close();
    }

    /**
     * Draws the screen and all the components in it.
     */
    public void drawScreen(int mouseX, int mouseY, float partialTick)
    {
        super.drawScreen(mouseX, mouseY, partialTick);
        this.mc.getTextureManager().bindTexture(resource);
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        this.drawTexturedModalRect(this.colorX, this.colorY, 0, 0, 120, 120);
    }

    /**
     * Called when the mouse is clicked.
     */
    public void mouseClicked(int mouseX, int mouseY, int mouseButton)
    {
        super.mouseClicked(mouseX, mouseY, mouseButton);

        if (mouseX >= this.colorX && mouseX <= this.colorX + 117 && mouseY >= this.colorY && mouseY <= this.colorY + 117)
        {
            InputStream stream = null;

            try
            {
                IResource iresource = this.mc.getResourceManager().getResource(resource);
                BufferedImage bufferedimage = ImageIO.read(stream = iresource.getInputStream());
                this.color = bufferedimage.getRGB((mouseX - this.guiLeft - 30) * 4, (mouseY - this.guiTop - 50) * 4) & 16777215;
                this.textfield.setTextColor(this.color);
                this.textfield.setText(this.getColor());
            }
            catch (IOException var15)
            {
                ;
            }
            finally
            {
                if (stream != null)
                {
                    try
                    {
                        stream.close();
                    }
                    catch (IOException var14)
                    {
                        ;
                    }
                }
            }
        }
    }

    public void unFocused(GuiNpcTextField textfield)
    {
        boolean color = false;
        int color1;

        try
        {
            color1 = Integer.parseInt(textfield.getText(), 16);
        }
        catch (NumberFormatException var4)
        {
            color1 = 0;
        }

        this.color = color1;
        textfield.setTextColor(color1);
    }
}
