package noppes.npcs.client.gui.questtypes;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.nbt.NBTTagCompound;
import noppes.npcs.client.Client;
import noppes.npcs.client.NoppesUtil;
import noppes.npcs.client.gui.GuiNPCDialogSelection;
import noppes.npcs.client.gui.util.GuiNpcButton;
import noppes.npcs.client.gui.util.GuiSelectionListener;
import noppes.npcs.client.gui.util.IGuiData;
import noppes.npcs.client.gui.util.SubGuiInterface;
import noppes.npcs.constants.EnumPacketServer;
import noppes.npcs.controllers.Quest;
import noppes.npcs.entity.EntityNPCInterface;

public class GuiNpcQuestDialogAfterComplete extends SubGuiInterface implements GuiSelectionListener, IGuiData
{
    private GuiScreen parent;
    private Quest quest;
    private String dialogName;
    private int selectedSlot;

    public GuiNpcQuestDialogAfterComplete(EntityNPCInterface npc, Quest q, GuiScreen parent)
    {
        this.npc = npc;
        this.parent = parent;
        this.title = "Quest Dialog Setup";
        this.quest = q;
        this.setBackground("menubg.png");
        this.xSize = 256;
        this.ySize = 216;
        this.closeOnEsc = true;
        Client.sendData(EnumPacketServer.QuestDialogAfterCompleteGetTitle, quest.dialogAfterQuestId != 0 ? quest.dialogAfterQuestId : -1);
    }

    /**
     * Adds the buttons (and other controls) to the screen in question.
     */
    public void initGui()
    {
        super.initGui();

        for (int i = 0; i < 1; ++i)
        {
            String title = "dialog.selectoption";

            if (dialogName != null)
            {
                title = dialogName;
            }

            this.addButton(new GuiNpcButton(i + 9, this.guiLeft + 10, 55 + i * 22, 20, 20, "X"));
            this.addButton(new GuiNpcButton(i + 3, this.guiLeft + 34, 55 + i * 22, 210, 20, title));
        }

        this.addButton(new GuiNpcButton(0, this.guiLeft + 150, this.guiTop + 190, 98, 20, "gui.back"));
    }

    protected void actionPerformed(GuiButton guiButton)
    {
        GuiNpcButton button = (GuiNpcButton) guiButton;

        if (button.id == 0)
        {
            this.close();
        }

        int slot;

        if (button.id >= 3 && button.id < 9)
        {
            this.selectedSlot = button.id - 3;
            slot = -1;

            if (this.quest.dialogAfterQuestId != 0)
            {
                slot = this.quest.dialogAfterQuestId;
            }

            GuiNPCDialogSelection gui = new GuiNPCDialogSelection(this.npc, this.parent, slot);
            gui.listener = this;
            NoppesUtil.openGUI(this.player, gui);
        }

        if (button.id >= 9 && button.id < 15)
        {
            slot = button.id - 9;
            this.quest.dialogAfterQuestId = 0;
            this.dialogName = null;
            this.save();
            this.initGui();
        }
    }

    public void save() {}

    public void selected(int id, String name)
    {
        this.dialogName = name;
        this.quest.dialogAfterQuestId = id;
    }

    public void setGuiData(NBTTagCompound compound)
    {
        if (compound.hasKey("1"))
        {
            this.dialogName = compound.getString("1");
        }

        this.initGui();
    }
}
