package noppes.npcs.client.gui.player;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.StatCollector;
import noppes.npcs.client.CustomNpcResourceListener;
import noppes.npcs.client.gui.util.GuiContainerNPCInterface;
import noppes.npcs.client.gui.util.GuiNpcButton;
import noppes.npcs.containers.ContainerCarpentryBench;
import noppes.npcs.controllers.RecipeController;
import noppes.npcs.entity.EntityNPCInterface;
import org.lwjgl.opengl.GL11;

public class GuiNpcCarpentryBench extends GuiContainerNPCInterface
{
    private final ResourceLocation resource = new ResourceLocation("customnpcs", "textures/gui/carpentry.png");
    private ContainerCarpentryBench container;
    private GuiNpcButton button;

    public GuiNpcCarpentryBench(ContainerCarpentryBench container)
    {
        super((EntityNPCInterface)null, container);
        this.container = container;
        this.title = "";
        this.allowUserInput = false;
        this.closeOnEsc = true;
        this.ySize = 180;
    }

    /**
     * Adds the buttons (and other controls) to the screen in question.
     */
    public void initGui()
    {
        super.initGui();
        this.addButton(this.button = new GuiNpcButton(0, this.guiLeft + 158, this.guiTop + 4, 12, 20, "..."));
    }

    public void buttonEvent(GuiButton guibutton)
    {
        this.displayGuiScreen(new GuiRecipes());
    }

    protected void drawGuiContainerBackgroundLayer(float f, int i, int j)
    {
        this.button.enabled = RecipeController.instance != null && !RecipeController.instance.anvilRecipes.isEmpty();
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        this.mc.renderEngine.bindTexture(this.resource);
        int l = (this.width - this.xSize) / 2;
        int i1 = (this.height - this.ySize) / 2;
        String title = StatCollector.translateToLocal("tile.npcCarpentyBench.name");

        if (this.container.getMetadata() >= 4)
        {
            title = StatCollector.translateToLocal("tile.anvil.name");
        }

        this.drawTexturedModalRect(l, i1, 0, 0, this.xSize, this.ySize);
        super.drawGuiContainerBackgroundLayer(f, i, j);
        this.fontRendererObj.drawString(title, this.guiLeft + 4, this.guiTop + 4, CustomNpcResourceListener.DefaultTextColor);
        this.fontRendererObj.drawString(StatCollector.translateToLocal("container.inventory"), this.guiLeft + 4, this.guiTop + 87, CustomNpcResourceListener.DefaultTextColor);
    }

    public void save() {}
}
