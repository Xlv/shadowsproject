package noppes.npcs.client.gui.model;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.renderer.entity.NPCRendererHelper;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.entity.RendererLivingEntity;
import net.minecraft.entity.EntityList;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagByte;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;
import noppes.npcs.client.Client;
import noppes.npcs.client.gui.util.*;
import noppes.npcs.constants.EnumPacketServer;
import noppes.npcs.controllers.PixelmonHelper;
import noppes.npcs.entity.EntityCustomNpc;
import noppes.npcs.entity.EntityFakeLiving;
import noppes.npcs.entity.EntityNPCInterface;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.*;

public class GuiCreationScreen extends GuiModelInterface implements ICustomScrollListener
{
    public HashMap < String, Class <? extends EntityLivingBase >> data = new HashMap();
    private List<String> list;
    private final String[] ignoredTags = new String[] {"CanBreakDoors", "Bred", "PlayerCreated", "Tame", "HasReproduced"};
    private GuiNpcButton prev;
    private GuiNpcButton next;
    private GuiScreen parent;
    private HashMap<Integer, String> mapped = new HashMap();

    public GuiCreationScreen(GuiScreen parent, EntityCustomNpc npc)
    {
        super(npc);
        this.parent = parent;
        Map mapping = EntityList.stringToClassMapping;
        Iterator var4 = mapping.keySet().iterator();

        while (var4.hasNext())
        {
            Object name = var4.next();
            Class c = (Class)mapping.get(name);

            try
            {
                if (!EntityCustomNpc.class.isAssignableFrom(c) && EntityLiving.class.isAssignableFrom(c) && c.getConstructor(new Class[] {World.class}) != null && !Modifier.isAbstract(c.getModifiers()) && RenderManager.instance.getEntityClassRenderObject(c) instanceof RendererLivingEntity)
                {
                    this.data.put(name.toString(), c.asSubclass(EntityLivingBase.class));
                }
            }
            catch (SecurityException var8)
            {
                var8.printStackTrace();
            }
            catch (NoSuchMethodException var9)
            {
                ;
            }
        }

        this.list = new ArrayList(this.data.keySet());
        Collections.sort(this.list, String.CASE_INSENSITIVE_ORDER);
    }

    /**
     * Adds the buttons (and other controls) to the screen in question.
     */
    public void initGui()
    {
        EntityLivingBase entity = this.playerdata.getEntity(this.npc);
        this.xOffset = entity == null ? 0 : 50;
        super.initGui();
        String title = "CustomNPC";

        if (entity != null)
        {
            title = (String)EntityList.classToStringMapping.get(this.playerdata.getEntityClass());
        }

        this.addButton(new GuiNpcButton(1, this.guiLeft + 140, this.guiTop, 100, 20, title));
        this.addButton(this.prev = new GuiNpcButton(0, this.guiLeft + 118, this.guiTop, 20, 20, "<"));
        this.addButton(this.next = new GuiNpcButton(2, this.guiLeft + 242, this.guiTop, 20, 20, ">"));
        this.prev.enabled = this.getCurrentEntityIndex() >= 0;
        this.next.enabled = this.getCurrentEntityIndex() < this.list.size() - 1;

        if (entity == null)
        {
            this.showPlayerButtons();
        }
        else if (PixelmonHelper.isPixelmon(entity))
        {
            this.showPixelmonMenu(entity);
        }
        else
        {
            this.showEntityButtons(entity);
        }
    }

    private void showPlayerButtons()
    {
        int y = this.guiTop;
        GuiNpcButton var10001;
        int var10004 = this.guiLeft + 4;
        y += 22;
        var10001 = new GuiNpcButton(8, var10004, y, 96, 20, "model.scale");
        this.addButton(var10001);
        var10004 = this.guiLeft + 50;
        y += 22;
        var10001 = new GuiNpcButton(4, var10004, y, 50, 20, "selectServer.edit");
        this.addButton(var10001);
        this.addLabel(new GuiNpcLabel(1, "Head", this.guiLeft, y + 5, 16777215));
        var10004 = this.guiLeft + 50;
        y += 22;
        var10001 = new GuiNpcButton(5, var10004, y, 50, 20, "selectServer.edit");
        this.addButton(var10001);
        this.addLabel(new GuiNpcLabel(2, "Body", this.guiLeft, y + 5, 16777215));
        var10004 = this.guiLeft + 50;
        y += 22;
        var10001 = new GuiNpcButton(6, var10004, y, 50, 20, "selectServer.edit");
        this.addButton(var10001);
        this.addLabel(new GuiNpcLabel(3, "Arms", this.guiLeft, y + 5, 16777215));
        var10004 = this.guiLeft + 50;
        y += 22;
        var10001 = new GuiNpcButton(7, var10004, y, 50, 20, "selectServer.edit");
        this.addButton(var10001);
        this.addLabel(new GuiNpcLabel(4, "Legs", this.guiLeft, y + 5, 16777215));
        this.addButton(new GuiNpcButton(44, this.guiLeft + 310, this.guiTop + 14, 80, 20, "Save Model"));
        this.addButton(new GuiNpcButton(45, this.guiLeft + 310, this.guiTop + 36, 80, 20, "Load Model"));
    }

    private void showPixelmonMenu(EntityLivingBase entity)
    {
        GuiCustomScroll scroll = new GuiCustomScroll(this, 0);
        scroll.setSize(120, 200);
        scroll.guiLeft = this.guiLeft;
        scroll.guiTop = this.guiTop + 20;
        this.addScroll(scroll);
        scroll.setList(PixelmonHelper.getPixelmonList());
        scroll.setSelected(PixelmonHelper.getName(entity));
        Minecraft.getMinecraft().thePlayer.sendChatMessage(PixelmonHelper.getName(entity));
    }

    private void showEntityButtons(EntityLivingBase entity)
    {
        this.mapped.clear();

        if (!(entity instanceof EntityNPCInterface))
        {
            int y = this.guiTop + 20;
            NBTTagCompound compound = this.getExtras(entity);
            Set keys = compound.func_150296_c();
            int i = 0;
            Iterator breed = keys.iterator();

            while (breed.hasNext())
            {
                String method = (String)breed.next();

                if (!this.isIgnored(method))
                {
                    NBTBase base = compound.getTag(method);

                    if (method.equals("Age"))
                    {
                        ++i;
                        this.addLabel(new GuiNpcLabel(0, "Child", this.guiLeft, y + 5 + i * 22, 16777215));
                        this.addButton(new GuiNpcButton(30, this.guiLeft + 80, y + i * 22, 50, 20, new String[] {"gui.no", "gui.yes"}, entity.isChild() ? 1 : 0));
                    }
                    else if (base.getId() == 1)
                    {
                        byte b = ((NBTTagByte)base).func_150290_f();

                        if (b == 0 || b == 1)
                        {
                            if (this.playerdata.extra.hasKey(method))
                            {
                                b = this.playerdata.extra.getByte(method);
                            }

                            ++i;
                            this.addLabel(new GuiNpcLabel(100 + i, method, this.guiLeft, y + 5 + i * 22, 16777215));
                            this.addButton(new GuiNpcButton(100 + i, this.guiLeft + 80, y + i * 22, 50, 20, new String[] {"gui.no", "gui.yes"}, b));
                            this.mapped.put(Integer.valueOf(i), method);
                        }
                    }
                }
            }

            if (EntityList.getEntityString(entity).equals("doggystyle.Dog"))
            {
                int var11 = 0;

                try
                {
                    Method var12 = entity.getClass().getMethod("getBreedID", new Class[0]);
                    var11 = ((Integer)var12.invoke(entity, new Object[0])).intValue();
                }
                catch (Exception var10)
                {
                    ;
                }

                ++i;
                this.addLabel(new GuiNpcLabel(201, "Breed", this.guiLeft, y + 5 + i * 22, 16777215));
                this.addButton(new GuiNpcButton(201, this.guiLeft + 80, y + i * 22, 50, 20, new String[] {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16"}, var11));
            }
        }
    }

    private boolean isIgnored(String tag)
    {
        String[] var2 = this.ignoredTags;
        int var3 = var2.length;

        for (int var4 = 0; var4 < var3; ++var4)
        {
            String s = var2[var4];

            if (s.equals(tag))
            {
                return true;
            }
        }

        return false;
    }

    private NBTTagCompound getExtras(EntityLivingBase entity)
    {
        NBTTagCompound fake = new NBTTagCompound();
        (new EntityFakeLiving(entity.worldObj)).writeEntityToNBT(fake);
        NBTTagCompound compound = new NBTTagCompound();

        try
        {
            entity.writeEntityToNBT(compound);
        }
        catch (Exception var7)
        {
            ;
        }

        Set keys = fake.func_150296_c();
        Iterator var5 = keys.iterator();

        while (var5.hasNext())
        {
            String name = (String)var5.next();
            compound.removeTag(name);
        }

        return compound;
    }

    private int getCurrentEntityIndex()
    {
        return this.list.indexOf(EntityList.classToStringMapping.get(this.playerdata.getEntityClass()));
    }

    protected void actionPerformed(GuiButton guiButton)
    {
        super.actionPerformed(guiButton);
        GuiNpcButton button = (GuiNpcButton) guiButton;
        int entity;

        if (button.id == 0)
        {
            entity = this.getCurrentEntityIndex();

            if (!this.prev.enabled)
            {
                return;
            }

            --entity;

            try
            {
                if (entity < 0)
                {
                    this.playerdata.setEntityClass((Class)null);
                    this.npc.display.texture = "customnpcs:textures/entity/humanmale/Steve.png";
                }
                else
                {
                    this.playerdata.setEntityClass((Class)this.data.get(this.list.get(entity)));
                    EntityLivingBase method = this.playerdata.getEntity(this.npc);

                    if (method != null)
                    {
                        RendererLivingEntity comp = (RendererLivingEntity)RenderManager.instance.getEntityRenderObject(method);
                        this.npc.display.texture = NPCRendererHelper.getTexture(comp, method);
                    }
                }

                this.npc.display.glowTexture = "";
                this.npc.textureLocation = null;
                this.npc.textureGlowLocation = null;
                this.npc.updateHitbox();
            }
            catch (Exception var7)
            {
                this.npc.display.texture = "customnpcs:textures/entity/humanmale/Steve.png";
            }

            this.initGui();
        }

        if (button.id == 2)
        {
            entity = this.getCurrentEntityIndex();

            if (!this.next.enabled)
            {
                return;
            }

            ++entity;
            this.playerdata.setEntityClass((Class)this.data.get(this.list.get(entity)));
            this.updateTexture();
            this.initGui();
        }

        if (button.id == 1)
        {
            this.mc.displayGuiScreen(new GuiEntitySelection(this, this.playerdata, this.npc));
        }

        if (button.id == 4)
        {
            this.mc.displayGuiScreen(new GuiModelHead(this, this.npc));
        }

        if (button.id == 5)
        {
            this.mc.displayGuiScreen(new GuiModelBody(this, this.npc));
        }

        if (button.id == 6)
        {
            this.mc.displayGuiScreen(new GuiModelArms(this, this.npc));
        }

        if (button.id == 7)
        {
            this.mc.displayGuiScreen(new GuiModelLegs(this, this.npc));
        }

        if (button.id == 8)
        {
            this.mc.displayGuiScreen(new GuiModelScale(this, this.playerdata, this.npc));
        }

        if (button.id == 30)
        {
            this.playerdata.extra.setInteger("Age", button.getValue() == 1 ? -24000 : 0);
            this.playerdata.clearEntity();
        }

        if (button.id == 44)
        {
            this.mc.displayGuiScreen(new GuiPresetSave(this, this.playerdata));
        }

        if (button.id == 45)
        {
            this.mc.displayGuiScreen(new GuiPresetSelection(this, this.playerdata));
        }

        if (button.id >= 100 && button.id < 200)
        {
            String var8 = (String)this.mapped.get(Integer.valueOf(button.id - 100));

            if (var8 != null)
            {
                this.playerdata.extra.setBoolean(var8, button.getValue() == 1);
                this.playerdata.clearEntity();
            }
        }

        if (button.id == 201)
        {
            try
            {
                EntityLivingBase var9 = this.playerdata.getEntity(this.npc);
                Method var10 = var9.getClass().getMethod("setBreedID", new Class[] {Integer.TYPE});
                var10.invoke(var9, new Object[] {Integer.valueOf(button.getValue())});
                NBTTagCompound var11 = new NBTTagCompound();
                var9.writeEntityToNBT(var11);
                this.playerdata.extra.setString("EntityData21", var11.getString("EntityData21"));
                this.playerdata.clearEntity();
                this.updateTexture();
            }
            catch (Exception var6)
            {
                ;
            }
        }
    }

    private void updateTexture()
    {
        try
        {
            EntityLivingBase ex = this.playerdata.getEntity(this.npc);

            if (ex != null)
            {
                RendererLivingEntity render = (RendererLivingEntity)RenderManager.instance.getEntityRenderObject(ex);
                this.npc.display.texture = NPCRendererHelper.getTexture(render, ex);
            }
            else
            {
                this.npc.display.texture = "customnpcs:textures/entity/humanmale/Steve.png";
            }

            this.npc.display.glowTexture = "";
            this.npc.textureLocation = null;
            this.npc.textureGlowLocation = null;
            this.npc.updateHitbox();
        }
        catch (Exception var3)
        {
            this.npc.display.texture = "customnpcs:textures/entity/humanmale/Steve.png";
        }
    }

    public void close()
    {
        Client.sendData(EnumPacketServer.ModelDataSave, new Object[] {this.playerdata.writeToNBT()});
        this.displayGuiScreen(this.parent);
    }

    public void customScrollClicked(int i, int j, int k, GuiCustomScroll scroll)
    {
        this.playerdata.clearEntity();
        this.playerdata.extra.setString("Name", scroll.getSelected());
        EntityLivingBase entity = this.playerdata.getEntity(this.npc);
        RendererLivingEntity render = (RendererLivingEntity)RenderManager.instance.getEntityRenderObject(entity);
        this.npc.display.texture = NPCRendererHelper.getTexture(render, entity);
        this.npc.textureLocation = null;
    }
}
