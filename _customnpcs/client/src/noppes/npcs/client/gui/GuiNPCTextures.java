package noppes.npcs.client.gui;

import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.entity.RenderManager;
import noppes.npcs.entity.EntityNPCInterface;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

public class GuiNPCTextures extends GuiNpcSelectionInterface
{
    public GuiNPCTextures(EntityNPCInterface npc, GuiScreen parent)
    {
        super(npc, parent, npc.display.texture);
        this.title = "Select Texture";
        this.parent = parent;
    }

    /**
     * Adds the buttons (and other controls) to the screen in question.
     */
    public void initGui()
    {
        super.initGui();
        int index = this.npc.display.texture.lastIndexOf("/");

        if (index > 0)
        {
            String asset = this.npc.display.texture.substring(index + 1);

            if (this.npc.display.texture.equals(this.assets.getAsset(asset)))
            {
                this.slot.selected = asset;
            }
        }
    }

    /**
     * Draws the screen and all the components in it.
     */
    public void drawScreen(int mouseX, int mouseY, float partialTick)
    {
        int l = this.width / 2 - 180;
        int i1 = this.height / 2 - 90;
        GL11.glEnable(GL12.GL_RESCALE_NORMAL);
        GL11.glEnable(GL11.GL_COLOR_MATERIAL);
        GL11.glPushMatrix();
        GL11.glTranslatef((float)(l + 33), (float)(i1 + 131), 50.0F);
        float f1 = 250.0F / (float)this.npc.display.modelSize;
        GL11.glScalef(-f1, f1, f1);
        GL11.glRotatef(180.0F, 0.0F, 0.0F, 1.0F);
        float f2 = this.npc.renderYawOffset;
        float f3 = this.npc.rotationYaw;
        float f4 = this.npc.rotationPitch;
        float f7 = this.npc.rotationYawHead;
        float f5 = (float)(l + 33) - (float) mouseX;
        float f6 = (float)(i1 + 131 - 50) - (float) mouseY;
        GL11.glRotatef(135.0F, 0.0F, 1.0F, 0.0F);
        RenderHelper.enableStandardItemLighting();
        GL11.glRotatef(-135.0F, 0.0F, 1.0F, 0.0F);
        GL11.glRotatef(-((float)Math.atan((double)(f6 / 40.0F))) * 20.0F, 1.0F, 0.0F, 0.0F);
        this.npc.renderYawOffset = (float)Math.atan((double)(f5 / 40.0F)) * 20.0F;
        this.npc.rotationYaw = (float)Math.atan((double)(f5 / 40.0F)) * 40.0F;
        this.npc.rotationPitch = -((float)Math.atan((double)(f6 / 40.0F))) * 20.0F;
        this.npc.rotationYawHead = this.npc.rotationYaw;
        this.npc.cloakUpdate();
        GL11.glTranslatef(0.0F, this.npc.yOffset, 0.0F);
        RenderManager.instance.playerViewY = 180.0F;
        RenderManager.instance.renderEntityWithPosYaw(this.npc, 0.0D, 0.0D, 0.0D, 0.0F, 1.0F);
        this.npc.renderYawOffset = f2;
        this.npc.rotationYaw = f3;
        this.npc.rotationPitch = f4;
        this.npc.rotationYawHead = f7;
        GL11.glPopMatrix();
        RenderHelper.disableStandardItemLighting();
        GL11.glDisable(GL12.GL_RESCALE_NORMAL);
        OpenGlHelper.setActiveTexture(OpenGlHelper.lightmapTexUnit);
        GL11.glDisable(GL11.GL_TEXTURE_2D);
        OpenGlHelper.setActiveTexture(OpenGlHelper.defaultTexUnit);
        super.drawScreen(mouseX, mouseY, partialTick);
    }

    public void elementClicked()
    {
        if (this.dataTextures.contains(this.slot.selected) && this.slot.selected != null)
        {
            this.npc.display.texture = this.assets.getAsset(this.slot.selected);
            this.npc.textureLocation = null;
        }
    }

    public void save() {}

    public String[] getExtension()
    {
        return new String[] {"png"};
    }
}
