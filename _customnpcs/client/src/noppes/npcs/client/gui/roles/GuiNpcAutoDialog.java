package noppes.npcs.client.gui.roles;

import net.minecraft.nbt.NBTTagCompound;
import noppes.npcs.client.Client;
import noppes.npcs.client.gui.util.GuiNPCInterface2;
import noppes.npcs.client.gui.util.GuiNpcLabel;
import noppes.npcs.client.gui.util.GuiNpcTextField;
import noppes.npcs.constants.EnumPacketServer;
import noppes.npcs.entity.EntityNPCInterface;
import noppes.npcs.roles.JobAutoDialog;

public class GuiNpcAutoDialog extends GuiNPCInterface2
{
    private JobAutoDialog job;

    public GuiNpcAutoDialog(EntityNPCInterface npc)
    {
        super(npc);
        this.job = (JobAutoDialog)npc.jobInterface;
    }

    /**
     * Adds the buttons (and other controls) to the screen in question.
     */
    public void initGui()
    {
        super.initGui();
        this.addLabel(new GuiNpcLabel(1, "Auto Dialog Range:", this.guiLeft + 60, this.guiTop + 110));
        this.addTextField(new GuiNpcTextField(1, this, this.fontRendererObj, this.guiLeft + 160, this.guiTop + 105, 40, 20, this.job.dialogRange + ""));
        this.getTextField(1).numbersOnly = true;
        this.getTextField(1).setMinMaxDefault(1, 64, 16);
    }

    public void elementClicked() {}

    public void save()
    {
        this.job.dialogRange = this.getTextField(1).getInteger();
        Client.sendData(EnumPacketServer.JobSave, this.job.writeToNBT(new NBTTagCompound()));
    }
}
