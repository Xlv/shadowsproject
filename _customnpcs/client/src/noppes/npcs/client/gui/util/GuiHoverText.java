package noppes.npcs.client.gui.util;

import net.minecraft.client.gui.GuiScreen;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

import java.util.ArrayList;

public class GuiHoverText extends GuiScreen
{
    private int x;
    private int y;
    public int id;
    protected static final ResourceLocation buttonTextures = new ResourceLocation("customnpcs:textures/gui/info.png");
    private String text;

    public GuiHoverText(int id, String text, int x, int y)
    {
        this.text = text;
        this.id = id;
        this.x = x;
        this.y = y;
    }

    /**
     * Draws the screen and all the components in it.
     */
    public void drawScreen(int mouseX, int mouseY, float partialTick)
    {
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        this.mc.getTextureManager().bindTexture(buttonTextures);
        this.drawTexturedModalRect(this.x, this.y, 0, 0, 12, 12);

        if (this.inArea(this.x, this.y, 12, 12, mouseX, mouseY))
        {
            ArrayList lines = new ArrayList();
            lines.add(this.text);
            this.drawHoveringText(lines, this.x + 8, this.y + 6, this.fontRendererObj);
            GL11.glDisable(GL11.GL_LIGHTING);
        }
    }

    public boolean inArea(int x, int y, int width, int height, int mouseX, int mouseY)
    {
        return mouseX >= x && mouseX <= x + width && mouseY >= y && mouseY <= y + height;
    }
}
