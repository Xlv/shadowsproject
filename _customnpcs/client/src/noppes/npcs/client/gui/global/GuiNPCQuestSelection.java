package noppes.npcs.client.gui.global;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import noppes.npcs.client.Client;
import noppes.npcs.client.NoppesUtil;
import noppes.npcs.client.gui.util.*;
import noppes.npcs.constants.EnumPacketServer;
import noppes.npcs.entity.EntityNPCInterface;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;

public class GuiNPCQuestSelection extends GuiNPCInterface implements IScrollData
{
    private GuiNPCStringSlot slot;
    private GuiScreen parent;
    private HashMap<String, Integer> data;
    private boolean selectCategory = true;
    public GuiSelectionListener listener;
    private int quest;

    public GuiNPCQuestSelection(EntityNPCInterface npc, GuiScreen parent, int quest)
    {
        super(npc);
        this.drawDefaultBackground = false;
        this.title = "Select Quest Category";
        this.parent = parent;
        this.data = new HashMap();
        this.quest = quest;

        if (quest >= 0)
        {
            Client.sendData(EnumPacketServer.QuestsGetFromQuest, new Object[] {Integer.valueOf(quest)});
            this.selectCategory = false;
            this.title = "Select Dialog";
        }
        else
        {
            Client.sendData(EnumPacketServer.QuestCategoriesGet, new Object[] {Integer.valueOf(quest)});
        }

        if (parent instanceof GuiSelectionListener)
        {
            this.listener = (GuiSelectionListener)parent;
        }
    }

    /**
     * Adds the buttons (and other controls) to the screen in question.
     */
    public void initGui()
    {
        super.initGui();
        Vector list = new Vector();
        this.slot = new GuiNPCStringSlot(list, this, false, 18);
        this.slot.registerScrollButtons(4, 5);
        this.addButton(new GuiNpcButton(2, this.width / 2 - 100, this.height - 41, 98, 20, "gui.back"));
        this.addButton(new GuiNpcButton(4, this.width / 2 + 2, this.height - 41, 98, 20, "mco.template.button.select"));
    }

    /**
     * Draws the screen and all the components in it.
     */
    public void drawScreen(int mouseX, int mouseY, float partialTick)
    {
        this.slot.drawScreen(mouseX, mouseY, partialTick);
        super.drawScreen(mouseX, mouseY, partialTick);
    }

    protected void actionPerformed(GuiButton guiButton)
    {
        int id = guiButton.id;

        if (id == 2)
        {
            if (this.selectCategory)
            {
                this.close();
                NoppesUtil.openGUI(this.player, this.parent);
            }
            else
            {
                this.title = "Select Dialog Category";
                this.selectCategory = true;
                Client.sendData(EnumPacketServer.QuestCategoriesGet, new Object[] {Integer.valueOf(this.quest)});
            }
        }

        if (id == 4)
        {
            if (this.slot.selected == null || this.slot.selected.isEmpty())
            {
                return;
            }

            this.doubleClicked();
        }
    }

    public String getSelected()
    {
        return this.slot.selected;
    }

    public void doubleClicked()
    {
        if (this.slot.selected != null && !this.slot.selected.isEmpty())
        {
            if (this.selectCategory)
            {
                this.selectCategory = false;
                this.title = "Select Quest";
                Client.sendData(EnumPacketServer.QuestsGet, new Object[] {this.data.get(this.slot.selected)});
            }
            else
            {
                this.quest = ((Integer)this.data.get(this.slot.selected)).intValue();
                this.close();
                NoppesUtil.openGUI(this.player, this.parent);
            }
        }
    }

    public void save()
    {
        if (this.quest >= 0 && this.listener != null)
        {
            this.listener.selected(this.quest, this.slot.selected);
        }
    }

    public void setData(Vector<String> list, HashMap<String, Integer> data)
    {
        this.data = data;
        this.slot.setList(list);

        if (this.quest >= 0)
        {
            Iterator var3 = data.keySet().iterator();

            while (var3.hasNext())
            {
                String name = (String)var3.next();

                if (((Integer)data.get(name)).intValue() == this.quest)
                {
                    this.slot.selected = name;
                }
            }
        }
    }

    public void setSelected(String selected) {}
}
