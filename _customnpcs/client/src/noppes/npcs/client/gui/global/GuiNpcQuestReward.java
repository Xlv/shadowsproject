package noppes.npcs.client.gui.global;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.util.ResourceLocation;
import noppes.npcs.client.NoppesUtil;
import noppes.npcs.client.gui.util.*;
import noppes.npcs.constants.EnumQuestExperienceType;
import noppes.npcs.containers.ContainerNpcQuestReward;
import noppes.npcs.controllers.Quest;
import noppes.npcs.entity.EntityNPCInterface;
import org.lwjgl.opengl.GL11;

public class GuiNpcQuestReward extends GuiContainerNPCInterface implements ITextfieldListener
{
    private Quest quest;
    private ResourceLocation resource;

    public GuiNpcQuestReward(EntityNPCInterface npc, ContainerNpcQuestReward container)
    {
        super(npc, container);
        this.quest = GuiNPCManageQuest.quest;
        this.resource = this.getResource("questreward.png");
    }

    /**
     * Adds the buttons (and other controls) to the screen in question.
     */
    public void initGui()
    {
        super.initGui();
        this.addLabel(new GuiNpcLabel(0, "quest.randomitem", this.guiLeft + 4, this.guiTop + 4));
        this.addButton(new GuiNpcButton(0, this.guiLeft + 4, this.guiTop + 14, 60, 20, new String[] {"gui.no", "gui.yes"}, this.quest.randomReward ? 1 : 0));
        this.addButton(new GuiNpcButton(5, this.guiLeft, this.guiTop + this.ySize, 98, 20, "gui.back"));

        this.addButton(new GuiNpcButton(6, this.guiLeft - 4 - 80, this.guiTop, 80, 20, new String[] {"Combat Type", "Research Type", "Survive Type"}, this.quest.experienceType == EnumQuestExperienceType.COMBAT ? 0 : this.quest.experienceType == EnumQuestExperienceType.RESEARCH ? 1 : 2));

        this.addLabel(new GuiNpcLabel(1, "quest.exp", this.guiLeft + 4, this.guiTop + 45));
        this.addTextField(new GuiNpcTextField(0, this, this.fontRendererObj, this.guiLeft + 4, this.guiTop + 55, 60, 20, this.quest.rewardExp + ""));
        this.getTextField(0).numbersOnly = true;
        this.getTextField(0).setMinMaxDefault(0, 99999, 0);

        int offsetY = 32;
        int y = guiTop + 20;
        this.addLabel(new GuiNpcLabel(2, "Combat", this.guiLeft - 4 - 60, y, 0xffffff));
        this.addTextField(new GuiNpcTextField(1, this, this.fontRendererObj, this.guiLeft - 4 - 60, y + 10, 60, 20, this.quest.combatReward + ""));
        this.getTextField(0).numbersOnly = true;
        this.getTextField(0).setMinMaxDefault(0, 99999, 0);
        y += offsetY;
        this.addLabel(new GuiNpcLabel(3, "Research", this.guiLeft - 4 - 60, y, 0xffffff));
        this.addTextField(new GuiNpcTextField(2, this, this.fontRendererObj, this.guiLeft - 4 - 60, y + 10, 60, 20, this.quest.researchReward + ""));
        this.getTextField(0).numbersOnly = true;
        this.getTextField(0).setMinMaxDefault(0, 99999, 0);
        y += offsetY;
        this.addLabel(new GuiNpcLabel(4, "Survive", this.guiLeft - 4 - 60, y, 0xffffff));
        this.addTextField(new GuiNpcTextField(3, this, this.fontRendererObj, this.guiLeft - 4 - 60, y + 10, 60, 20, this.quest.surviveReward + ""));
        this.getTextField(0).numbersOnly = true;
        this.getTextField(0).setMinMaxDefault(0, 99999, 0);
        y += offsetY;
        this.addLabel(new GuiNpcLabel(5, "Credits", this.guiLeft - 4 - 60, y, 0xffffff));
        this.addTextField(new GuiNpcTextField(4, this, this.fontRendererObj, this.guiLeft - 4 - 60, y + 10, 60, 20, this.quest.credits + ""));
        this.getTextField(0).numbersOnly = true;
        this.getTextField(0).setMinMaxDefault(0, 99999, 0);
        y += offsetY;
        this.addLabel(new GuiNpcLabel(6, "Platina", this.guiLeft - 4 - 60, y, 0xffffff));
        this.addTextField(new GuiNpcTextField(5, this, this.fontRendererObj, this.guiLeft - 4 - 60, y + 10, 60, 20, this.quest.platina + ""));
        this.getTextField(0).numbersOnly = true;
        this.getTextField(0).setMinMaxDefault(0, 99999, 0);
    }

    public void actionPerformed(GuiButton guiButton)
    {
        int id = guiButton.id;

        if (id == 5)
        {
            NoppesUtil.openGUI(this.player, GuiNPCManageQuest.Instance);
        }

        if (id == 0)
        {
            this.quest.randomReward = ((GuiNpcButton) guiButton).getValue() == 1;
        }

        if(id == 6) {
            if(this.quest.experienceType == EnumQuestExperienceType.COMBAT) {
                this.quest.experienceType = EnumQuestExperienceType.RESEARCH;
            } else if(this.quest.experienceType == EnumQuestExperienceType.RESEARCH) {
                this.quest.experienceType = EnumQuestExperienceType.SURVIVE;
            } else {
                this.quest.experienceType = EnumQuestExperienceType.COMBAT;
            }
        }
    }

    /**
     * "Called when the screen is unloaded. Used to disable keyboard repeat events."
     */
    public void onGuiClosed() {}

    protected void drawGuiContainerBackgroundLayer(float f, int i, int j)
    {
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        this.mc.renderEngine.bindTexture(this.resource);
        int l = (this.width - this.xSize) / 2;
        int i1 = (this.height - this.ySize) / 2;
        this.drawTexturedModalRect(l, i1, 0, 0, this.xSize, this.ySize);
        super.drawGuiContainerBackgroundLayer(f, i, j);
    }

    public void save() {}

    public void unFocused(GuiNpcTextField textfield)
    {
        if(textfield.id == 0) {
            this.quest.rewardExp = textfield.getInteger();
        } else if(textfield.id == 1) {
            this.quest.combatReward = textfield.getInteger();
        } else if(textfield.id == 2) {
            this.quest.researchReward = textfield.getInteger();
        } else if(textfield.id == 3) {
            this.quest.surviveReward = textfield.getInteger();
        } else if(textfield.id == 4) {
            this.quest.credits = textfield.getInteger();
        } else if(textfield.id == 5) {
            this.quest.platina = textfield.getInteger();
        }
    }
}
