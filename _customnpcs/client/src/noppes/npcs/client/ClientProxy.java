package noppes.npcs.client;

import cpw.mods.fml.client.registry.ClientRegistry;
import cpw.mods.fml.client.registry.RenderingRegistry;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.ObfuscationReflectionHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.particle.EntityFX;
import net.minecraft.client.particle.EntityFlameFX;
import net.minecraft.client.particle.EntitySmokeFX;
import net.minecraft.client.renderer.RenderGlobal;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.client.resources.IReloadableResourceManager;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.item.Item;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.ReportedException;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.client.MinecraftForgeClient;
import net.minecraftforge.common.MinecraftForge;
import noppes.npcs.*;
import noppes.npcs.blocks.tiles.*;
import noppes.npcs.client.controllers.ClientCloneController;
import noppes.npcs.client.controllers.MusicController;
import noppes.npcs.client.controllers.PresetController;
import noppes.npcs.client.fx.EntityElementalStaffFX;
import noppes.npcs.client.fx.EntityEnderFX;
import noppes.npcs.client.fx.EntityRainbowFX;
import noppes.npcs.client.gui.*;
import noppes.npcs.client.gui.global.*;
import noppes.npcs.client.gui.mainmenu.*;
import noppes.npcs.client.gui.player.*;
import noppes.npcs.client.gui.player.companion.GuiNpcCompanionInv;
import noppes.npcs.client.gui.player.companion.GuiNpcCompanionStats;
import noppes.npcs.client.gui.player.companion.GuiNpcCompanionTalents;
import noppes.npcs.client.gui.questtypes.GuiNpcQuestTypeItem;
import noppes.npcs.client.gui.roles.*;
import noppes.npcs.client.model.*;
import noppes.npcs.client.renderer.*;
import noppes.npcs.client.renderer.blocks.*;
import noppes.npcs.config.StringCache;
import noppes.npcs.constants.EnumGuiType;
import noppes.npcs.containers.*;
import noppes.npcs.entity.*;
import ru.xlv.core.util.obf.IgnoreObf;
import tconstruct.client.tabs.InventoryTabFactions;
import tconstruct.client.tabs.InventoryTabQuests;
import tconstruct.client.tabs.InventoryTabVanilla;
import tconstruct.client.tabs.TabRegistry;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

@IgnoreObf
public class ClientProxy extends CommonProxy
{
    public static KeyBinding QuestLog;
    public static ClientProxy.FontContainer Font;
    private ModelSkirtArmor model = new ModelSkirtArmor();

    public void load()
    {
        Font = new ClientProxy.FontContainer(CustomNpcs.FontType, CustomNpcs.FontSize);
        this.createFolders();
        CustomNpcs.Channel.register(new PacketHandlerClient());
        CustomNpcs.ChannelPlayer.register(new PacketHandlerPlayer());
        new MusicController();
        RenderingRegistry.registerEntityRenderingHandler(EntityNpcPony.class, new RenderNPCPony());
        RenderingRegistry.registerEntityRenderingHandler(EntityNpcCrystal.class, new RenderNpcCrystal(new ModelNpcCrystal(0.5F)));
        RenderingRegistry.registerEntityRenderingHandler(EntityNpcDragon.class, new RenderNpcDragon(new ModelNpcDragon(0.0F), 0.5F));
        RenderingRegistry.registerEntityRenderingHandler(EntityNpcSlime.class, new RenderNpcSlime(new ModelNpcSlime(16), new ModelNpcSlime(0), 0.25F));
        RenderingRegistry.registerEntityRenderingHandler(EntityProjectile.class, new RenderProjectile());
        RenderingRegistry.registerEntityRenderingHandler(EntityCustomNpc.class, new RenderCustomNpc());
        RenderingRegistry.registerEntityRenderingHandler(EntityNPCGolem.class, new RenderNPCHumanMale(new ModelNPCGolem(0.0F), new ModelNPCGolem(1.0F), new ModelNPCGolem(0.5F)));
        FMLCommonHandler.instance().bus().register(new ClientTickHandler());
        ClientRegistry.bindTileEntitySpecialRenderer(TileBlockAnvil.class, new BlockCarpentryBenchRenderer());
        ClientRegistry.bindTileEntitySpecialRenderer(TileMailbox.class, new BlockMailboxRenderer());
        RenderingRegistry.registerBlockHandler(new BlockBorderRenderer());

        if (!CustomNpcs.DisableExtraBlock)
        {
            ClientRegistry.bindTileEntitySpecialRenderer(TileBanner.class, new BlockBannerRenderer());
            ClientRegistry.bindTileEntitySpecialRenderer(TileWallBanner.class, new BlockWallBannerRenderer());
            ClientRegistry.bindTileEntitySpecialRenderer(TileTallLamp.class, new BlockTallLampRenderer());
            ClientRegistry.bindTileEntitySpecialRenderer(TileChair.class, new BlockChairRenderer());
            ClientRegistry.bindTileEntitySpecialRenderer(TileWeaponRack.class, new BlockWeaponRackRenderer());
            ClientRegistry.bindTileEntitySpecialRenderer(TileCrate.class, new BlockCrateRenderer());
            ClientRegistry.bindTileEntitySpecialRenderer(TileCouchWool.class, new BlockCouchWoolRenderer());
            ClientRegistry.bindTileEntitySpecialRenderer(TileCouchWood.class, new BlockCouchWoodRenderer());
            ClientRegistry.bindTileEntitySpecialRenderer(TileTable.class, new BlockTableRenderer());
            ClientRegistry.bindTileEntitySpecialRenderer(TileCandle.class, new BlockCandleRenderer());
            ClientRegistry.bindTileEntitySpecialRenderer(TileLamp.class, new BlockLampRenderer());
            ClientRegistry.bindTileEntitySpecialRenderer(TileStool.class, new BlockStoolRenderer());
            ClientRegistry.bindTileEntitySpecialRenderer(TileBigSign.class, new BlockBigSignRenderer());
            ClientRegistry.bindTileEntitySpecialRenderer(TileBarrel.class, new BlockBarrelRenderer());
            ClientRegistry.bindTileEntitySpecialRenderer(TileCampfire.class, new BlockCampfireRenderer());
            ClientRegistry.bindTileEntitySpecialRenderer(TileTombstone.class, new BlockTombstoneRenderer());
            ClientRegistry.bindTileEntitySpecialRenderer(TileShelf.class, new BlockShelfRenderer());
            ClientRegistry.bindTileEntitySpecialRenderer(TileSign.class, new BlockSignRenderer());
            ClientRegistry.bindTileEntitySpecialRenderer(TileBeam.class, new BlockBeamRenderer());
            ClientRegistry.bindTileEntitySpecialRenderer(TileBook.class, new BlockBookRenderer());
            ClientRegistry.bindTileEntitySpecialRenderer(TilePedestal.class, new BlockPedestalRenderer());
            RenderingRegistry.registerBlockHandler(new BlockBloodRenderer());
        }

        Minecraft mc = Minecraft.getMinecraft();
        QuestLog = new KeyBinding("Quest Log", 38, "key.categories.gameplay");
        ClientRegistry.registerKeyBinding(QuestLog);
        mc.gameSettings.loadOptions();
        new PresetController(CustomNpcs.Dir);

        //if (CustomNpcs.EnableUpdateChecker)
        //{
            //VersionChecker checker = new VersionChecker();
            //checker.start();
        //}

        ClientCloneController.Instance = new ClientCloneController();
        MinecraftForge.EVENT_BUS.register(new ClientEventHandler());

        if (CustomNpcs.InventoryGuiEnabled)
        {
            MinecraftForge.EVENT_BUS.register(new TabRegistry());

            if (TabRegistry.getTabList().size() < 2)
            {
                TabRegistry.registerTab(new InventoryTabVanilla());
            }

            TabRegistry.registerTab(new InventoryTabFactions());
            TabRegistry.registerTab(new InventoryTabQuests());
        }
    }

    private void createFolders()
    {
        File file = new File(CustomNpcs.Dir, "assets/customnpcs");

        if (!file.exists())
        {
            file.mkdirs();
        }

        File check = new File(file, "sounds");

        if (!check.exists())
        {
            check.mkdir();
        }

        File json = new File(file, "sounds.json");

        if (!json.exists())
        {
            try
            {
                json.createNewFile();
                BufferedWriter cache = new BufferedWriter(new FileWriter(json));
                cache.write("{\n\n}");
                cache.close();
            }
            catch (IOException var5)
            {
                ;
            }
        }

        check = new File(file, "textures");

        if (!check.exists())
        {
            check.mkdir();
        }

        File cache1 = new File(check, "cache");

        if (!cache1.exists())
        {
            cache1.mkdir();
        }

        ((IReloadableResourceManager)Minecraft.getMinecraft().getResourceManager()).registerReloadListener(new CustomNpcResourceListener());
    }

    public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z)
    {
        if (ID > EnumGuiType.values().length)
        {
            return null;
        }
        else
        {
            EnumGuiType gui = EnumGuiType.values()[ID];
            EntityNPCInterface npc = NoppesUtil.getLastNpc();
            Container container = this.getContainer(gui, player, x, y, z, npc);
            return this.getGui(npc, gui, container, x, y, z);
        }
    }

    private GuiScreen getGui(EntityNPCInterface npc, EnumGuiType gui, Container container, int x, int y, int z)
    {
        if (gui == EnumGuiType.MainMenuDisplay)
        {
            if (npc != null)
            {
                return new GuiNpcDisplay(npc);
            }

            Minecraft.getMinecraft().thePlayer.addChatMessage(new ChatComponentText("Unable to find npc"));
        }
        else
        {
            if (gui == EnumGuiType.MainMenuStats)
            {
                return new GuiNpcStats(npc);
            }

            if (gui == EnumGuiType.MainMenuInv)
            {
                return new GuiNPCInv(npc, (ContainerNPCInv)container);
            }

            if (gui == EnumGuiType.MainMenuAdvanced)
            {
                return new GuiNpcAdvanced(npc);
            }

            if (gui == EnumGuiType.QuestReward)
            {
                return new GuiNpcQuestReward(npc, (ContainerNpcQuestReward)container);
            }

            if (gui == EnumGuiType.QuestItem)
            {
                return new GuiNpcQuestTypeItem(npc, (ContainerNpcQuestTypeItem)container);
            }

            if (gui == EnumGuiType.MovingPath)
            {
                return new GuiNpcPather(npc);
            }

            if (gui == EnumGuiType.ManageFactions)
            {
                return new GuiNPCManageFactions(npc);
            }

            if (gui == EnumGuiType.ManageLinked)
            {
                return new GuiNPCManageLinkedNpc(npc);
            }

            if (gui == EnumGuiType.ManageTransport)
            {
                return new GuiNPCManageTransporters(npc);
            }

            if (gui == EnumGuiType.ManageRecipes)
            {
                return new GuiNpcManageRecipes(npc, (ContainerManageRecipes)container);
            }

            if (gui == EnumGuiType.ManageDialogs)
            {
                return new GuiNPCManageDialogs(npc);
            }

            if (gui == EnumGuiType.ManageQuests)
            {
                return new GuiNPCManageQuest(npc);
            }

            if (gui == EnumGuiType.ManageBanks)
            {
                return new GuiNPCManageBanks(npc, (ContainerManageBanks)container);
            }

            if (gui == EnumGuiType.MainMenuGlobal)
            {
                return new GuiNPCGlobalMainMenu(npc);
            }

            if (gui == EnumGuiType.MainMenuAI)
            {
                return new GuiNpcAI(npc);
            }

            if (gui == EnumGuiType.PlayerFollowerHire)
            {
                return new GuiNpcFollowerHire(npc, (ContainerNPCFollowerHire)container);
            }

            if (gui == EnumGuiType.PlayerFollower)
            {
                return new GuiNpcFollower(npc, (ContainerNPCFollower)container);
            }

            if (gui == EnumGuiType.PlayerTrader)
            {
                return new GuiNPCTrader(npc, (ContainerNPCTrader)container);
            }

            if (gui == EnumGuiType.PlayerBankSmall || gui == EnumGuiType.PlayerBankUnlock || gui == EnumGuiType.PlayerBankUprade || gui == EnumGuiType.PlayerBankLarge)
            {
                return new GuiNPCBankChest(npc, (ContainerNPCBankInterface)container);
            }

            if (gui == EnumGuiType.PlayerTransporter)
            {
                return new GuiTransportSelection(npc);
            }

            if (gui == EnumGuiType.Script)
            {
                return new GuiScript(npc);
            }

            if (gui == EnumGuiType.PlayerAnvil)
            {
                return new GuiNpcCarpentryBench((ContainerCarpentryBench)container);
            }

            if (gui == EnumGuiType.SetupFollower)
            {
                return new GuiNpcFollowerSetup(npc, (ContainerNPCFollowerSetup)container);
            }

            if (gui == EnumGuiType.SetupItemGiver)
            {
                return new GuiNpcItemGiver(npc, (ContainerNpcItemGiver)container);
            }

            if (gui == EnumGuiType.SetupTrader)
            {
                return new GuiNpcTraderSetup(npc, (ContainerNPCTraderSetup)container);
            }

            if (gui == EnumGuiType.SetupTransporter)
            {
                return new GuiNpcTransporter(npc);
            }

            if (gui == EnumGuiType.SetupBank)
            {
                return new GuiNpcBankSetup(npc);
            }

            if (gui == EnumGuiType.NpcRemote && Minecraft.getMinecraft().currentScreen == null)
            {
                return new GuiNpcRemoteEditor();
            }

            if (gui == EnumGuiType.PlayerMailman)
            {
                return new GuiMailmanWrite((ContainerMail)container, x == 1, y == 1);
            }

            if (gui == EnumGuiType.PlayerMailbox)
            {
                return new GuiMailbox();
            }

            if (gui == EnumGuiType.MerchantAdd)
            {
                return new GuiMerchantAdd();
            }

            if (gui == EnumGuiType.Crate)
            {
                return new GuiCrate((ContainerCrate)container);
            }

            if (gui == EnumGuiType.NpcDimensions)
            {
                return new GuiNpcDimension();
            }

            if (gui == EnumGuiType.Border)
            {
                return new GuiBorderBlock(x, y, z);
            }

            if (gui == EnumGuiType.BigSign)
            {
                return new GuiBigSign(x, y, z);
            }

            if (gui == EnumGuiType.RedstoneBlock)
            {
                return new GuiNpcRedstoneBlock(x, y, z);
            }

            if (gui == EnumGuiType.MobSpawner)
            {
                return new GuiNpcMobSpawner(x, y, z);
            }

            if (gui == EnumGuiType.MobSpawnerMounter)
            {
                return new GuiNpcMobSpawnerMounter(x, y, z);
            }

            if (gui == EnumGuiType.Waypoint)
            {
                return new GuiNpcWaypoint(x, y, z);
            }

            if (gui == EnumGuiType.Companion)
            {
                return new GuiNpcCompanionStats(npc);
            }

            if (gui == EnumGuiType.CompanionTalent)
            {
                return new GuiNpcCompanionTalents(npc);
            }

            if (gui == EnumGuiType.CompanionInv)
            {
                return new GuiNpcCompanionInv(npc, (ContainerNPCCompanion)container);
            }
        }

        return null;
    }

    public void openGui(int i, int j, int k, EnumGuiType gui, EntityPlayer player)
    {
        Minecraft minecraft = Minecraft.getMinecraft();

        if (minecraft.thePlayer == player)
        {
            GuiScreen guiscreen = this.getGui((EntityNPCInterface)null, gui, (Container)null, i, j, k);

            if (guiscreen != null)
            {
                minecraft.displayGuiScreen(guiscreen);
            }
        }
    }

    public void openGui(EntityNPCInterface npc, EnumGuiType gui)
    {
        this.openGui(npc, gui, 0, 0, 0);
    }

    public void openGui(EntityNPCInterface npc, EnumGuiType gui, int x, int y, int z)
    {
        Minecraft minecraft = Minecraft.getMinecraft();
        Container container = this.getContainer(gui, minecraft.thePlayer, x, y, z, npc);
        GuiScreen guiscreen = this.getGui(npc, gui, container, x, y, z);

        if (guiscreen != null)
        {
            minecraft.displayGuiScreen(guiscreen);
        }
    }

    public void openGui(EntityPlayer player, Object guiscreen)
    {
        Minecraft minecraft = Minecraft.getMinecraft();

        if (player.worldObj.isRemote && guiscreen instanceof GuiScreen)
        {
            if (guiscreen != null)
            {
                minecraft.displayGuiScreen((GuiScreen)guiscreen);
            }
        }
    }

    public void spawnParticle(EntityLivingBase player, String string, Object ... ob)
    {
        double height;
        double x;
        double y;
        double z;
        double f;

        if (string.equals("Spell"))
        {
            int data = ((Integer)ob[0]).intValue();
            int particles = ((Integer)ob[1]).intValue();

            for (int npc = 0; npc < particles; ++npc)
            {
                Random minecraft = player.worldObj.rand;
                height = (minecraft.nextDouble() - 0.5D) * (double)player.width;
                double rand = (double)player.getEyeHeight();
                x = (minecraft.nextDouble() - 0.5D) * (double)player.width;
                y = (minecraft.nextDouble() - 0.5D) * 2.0D;
                z = -minecraft.nextDouble();
                f = (minecraft.nextDouble() - 0.5D) * 2.0D;
                Minecraft.getMinecraft().effectRenderer.addEffect(new EntityElementalStaffFX(player, height, rand, x, y, z, f, data));
            }
        }
        else if (string.equals("ModelData"))
        {
            ModelData var24 = (ModelData)ob[0];
            ModelPartData var25 = (ModelPartData)ob[1];
            EntityCustomNpc var26 = (EntityCustomNpc)player;
            Minecraft var27 = Minecraft.getMinecraft();
            height = var26.getYOffset() + (double)var24.getBodyY();
            Random var28 = var26.getRNG();
            int i;

            if (var25.type == 0)
            {
                for (i = 0; i < 2; ++i)
                {
                    EntityEnderFX var29 = new EntityEnderFX(var26, (var28.nextDouble() - 0.5D) * (double)player.width, var28.nextDouble() * (double)player.height - height - 0.25D, (var28.nextDouble() - 0.5D) * (double)player.width, (var28.nextDouble() - 0.5D) * 2.0D, -var28.nextDouble(), (var28.nextDouble() - 0.5D) * 2.0D, var25);
                    var27.effectRenderer.addEffect(var29);
                }
            }
            else if (var25.type == 1)
            {
                for (i = 0; i < 2; ++i)
                {
                    x = player.posX + (var28.nextDouble() - 0.5D) * 0.9D;
                    y = player.posY + var28.nextDouble() * 1.9D - 0.25D - height;
                    z = player.posZ + (var28.nextDouble() - 0.5D) * 0.9D;
                    f = (var28.nextDouble() - 0.5D) * 2.0D;
                    double f1 = -var28.nextDouble();
                    double f2 = (var28.nextDouble() - 0.5D) * 2.0D;
                    var27.effectRenderer.addEffect(new EntityRainbowFX(player.worldObj, x, y, z, f, f1, f2));
                }
            }
        }
    }

    public ModelBiped getSkirtModel()
    {
        return this.model;
    }

    public boolean hasClient()
    {
        return true;
    }

    public EntityPlayer getPlayer()
    {
        return Minecraft.getMinecraft().thePlayer;
    }

    public void registerItem(Item item)
    {
        MinecraftForgeClient.registerItemRenderer(item, new NpcItemRenderer());
    }

    public static void bindTexture(ResourceLocation location)
    {
        try
        {
            if (location == null)
            {
                return;
            }

            TextureManager texturemanager = Minecraft.getMinecraft().getTextureManager();

            if (location != null)
            {
                texturemanager.bindTexture(location);
            }
        }
        catch (NullPointerException var2)
        {
            ;
        }
        catch (ReportedException var3)
        {
            ;
        }
    }

    public void spawnParticle(String particle, double x, double y, double z, double motionX, double motionY, double motionZ, float scale)
    {
        RenderGlobal render = Minecraft.getMinecraft().renderGlobal;
        EntityFX fx = render.doSpawnParticle(particle, x, y, z, motionX, motionY, motionZ);

        if (fx != null)
        {
            if (particle.equals("flame"))
            {
                ObfuscationReflectionHelper.setPrivateValue(EntityFlameFX.class, (EntityFlameFX)fx, Float.valueOf(scale), 0);
            }
            else if (particle.equals("smoke"))
            {
                ObfuscationReflectionHelper.setPrivateValue(EntitySmokeFX.class, (EntitySmokeFX)fx, Float.valueOf(scale), 0);
            }
        }
    }

    public static class FontContainer
    {
        private StringCache textFont = null;
        public boolean useCustomFont = true;

        private FontContainer() {}

        public FontContainer(String fontType, int fontSize)
        {
            this.textFont = new StringCache();
            this.textFont.setDefaultFont("Arial", fontSize, true);
            this.useCustomFont = !fontType.equalsIgnoreCase("minecraft");

            try
            {
                if (this.useCustomFont && !fontType.isEmpty() && !fontType.equalsIgnoreCase("default"))
                {
                    this.textFont.setDefaultFont(fontType, fontSize, true);
                }
                else
                {
                    this.textFont.setCustomFont(new ResourceLocation("customnpcs", "OpenSans.ttf"), fontSize, true);
                }
            }
            catch (Exception var4)
            {
                LogWriter.info("Failed loading font so using Arial");
            }
        }

        public int height()
        {
            return this.useCustomFont ? this.textFont.fontHeight : Minecraft.getMinecraft().fontRenderer.FONT_HEIGHT;
        }

        public int width(String text)
        {
            return this.useCustomFont ? this.textFont.getStringWidth(text) : Minecraft.getMinecraft().fontRenderer.getStringWidth(text);
        }

        public ClientProxy.FontContainer copy()
        {
            ClientProxy.FontContainer font = new ClientProxy.FontContainer();
            font.textFont = this.textFont;
            font.useCustomFont = this.useCustomFont;
            return font;
        }

        public void drawString(String text, int x, int y, int color)
        {
            if (this.useCustomFont)
            {
                this.textFont.renderString(text, x, y, color, true);
                this.textFont.renderString(text, x, y, color, false);
            }
            else
            {
                Minecraft.getMinecraft().fontRenderer.drawStringWithShadow(text, x, y, color);
            }
        }

        public String getName()
        {
            return !this.useCustomFont ? "Minecraft" : this.textFont.usedFont().getFontName();
        }
    }
}
