package noppes.npcs.controllers;

import net.minecraft.command.ICommandSender;
import net.minecraft.nbt.CompressedStreamTools;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.ChatComponentText;
import noppes.npcs.CustomNpcs;
import noppes.npcs.LogWriter;
import noppes.npcs.util.NBTJsonUtil;

import java.io.File;
import java.io.FileInputStream;
import java.util.*;

public class ServerCloneController
{
    public static ServerCloneController Instance;

    public ServerCloneController()
    {
        this.loadClones();
    }

    private void loadClones()
    {
        try
        {
            File e = new File(this.getDir(), "..");
            File file = new File(e, "clonednpcs.dat");
            System.out.println(file.getAbsolutePath());
            System.out.println(file.exists());

            if (file.exists())
            {
                Map clones = this.loadOldClones(file);
                file.delete();
                file = new File(e, "clonednpcs.dat_old");

                if (file.exists())
                {
                    file.delete();
                }

                Iterator var4 = clones.keySet().iterator();

                while (var4.hasNext())
                {
                    int tab = ((Integer)var4.next()).intValue();
                    Map map = (Map)clones.get(Integer.valueOf(tab));
                    Iterator var7 = map.keySet().iterator();

                    while (var7.hasNext())
                    {
                        String name = (String)var7.next();
                        this.saveClone(tab, name, (NBTTagCompound)map.get(name));
                    }
                }
            }
        }
        catch (Exception var9)
        {
            LogWriter.except(var9);
        }
    }

    public File getDir()
    {
        File dir = new File(CustomNpcs.getWorldSaveDirectory(), "clones");

        if (!dir.exists())
        {
            dir.mkdir();
        }

        return dir;
    }

    private Map<Integer, Map<String, NBTTagCompound>> loadOldClones(File file) throws Exception
    {
        HashMap clones = new HashMap();
        NBTTagCompound nbttagcompound1 = CompressedStreamTools.readCompressed(new FileInputStream(file));
        NBTTagList list = nbttagcompound1.getTagList("Data", 10);

        if (list == null)
        {
            return clones;
        }
        else
        {
            for (int i = 0; i < list.tagCount(); ++i)
            {
                NBTTagCompound compound = list.getCompoundTagAt(i);

                if (!compound.hasKey("ClonedTab"))
                {
                    compound.setInteger("ClonedTab", 1);
                }

                Object tab = (Map)clones.get(Integer.valueOf(compound.getInteger("ClonedTab")));

                if (tab == null)
                {
                    clones.put(Integer.valueOf(compound.getInteger("ClonedTab")), tab = new HashMap());
                }

                String name = compound.getString("ClonedName");

                for (int number = 1; ((Map)tab).containsKey(name); name = String.format("%s%s", new Object[] {compound.getString("ClonedName"), Integer.valueOf(number)}))
                {
                    ++number;
                }
                compound.removeTag("ClonedName");
                compound.removeTag("ClonedTab");
                compound.removeTag("ClonedDate");
                this.cleanTags(compound);
                ((Map)tab).put(name, compound);
            }

            return clones;
        }
    }

    public NBTTagCompound getCloneData(ICommandSender player, String name, int tab)
    {
        File file = new File(new File(this.getDir(), tab + ""), name + ".json");

        if (!file.exists())
        {
            if (player != null)
            {
                player.addChatMessage(new ChatComponentText("Could not find clone file"));
            }

            return null;
        }
        else
        {
            try
            {
                return NBTJsonUtil.LoadFile(file);
            }
            catch (Exception var6)
            {
                LogWriter.error("Error loading: " + file.getAbsolutePath(), var6);

                if (player != null)
                {
                    player.addChatMessage(new ChatComponentText(var6.getMessage()));
                }

                return null;
            }
        }
    }

    public void saveClone(int tab, String name, NBTTagCompound compound)
    {
        try
        {
            File e = new File(this.getDir(), tab + "");

            if (!e.exists())
            {
                e.mkdir();
            }

            String filename = name + ".json";
            File file = new File(e, filename + "_new");
            File file2 = new File(e, filename);
            NBTJsonUtil.SaveFile(file, compound);

            if (file2.exists())
            {
                file2.delete();
            }

            file.renameTo(file2);
        }
        catch (Exception var8)
        {
            LogWriter.except(var8);
        }
    }

    public List<String> getClones(int tab)
    {
        ArrayList list = new ArrayList();
        File dir = new File(this.getDir(), tab + "");

        if (dir.exists() && dir.isDirectory())
        {
            String[] var4 = dir.list();
            int var5 = var4.length;

            for (int var6 = 0; var6 < var5; ++var6)
            {
                String file = var4[var6];

                if (file.endsWith(".json"))
                {
                    list.add(file.substring(0, file.length() - 5));
                }
            }

            return list;
        }
        else
        {
            return list;
        }
    }

    public boolean removeClone(String name, int tab)
    {
        File file = new File(new File(this.getDir(), tab + ""), name + ".json");

        if (!file.exists())
        {
            return false;
        }
        else
        {
            file.delete();
            return true;
        }
    }

    public String addClone(NBTTagCompound nbttagcompound, String name, int tab)
    {
        this.cleanTags(nbttagcompound);
        this.saveClone(tab, name, nbttagcompound);
        return name;
    }

    public void cleanTags(NBTTagCompound nbttagcompound)
    {
        if (nbttagcompound.hasKey("ItemGiverId"))
        {
            nbttagcompound.setInteger("ItemGiverId", 0);
        }

        if (nbttagcompound.hasKey("TransporterId"))
        {
            nbttagcompound.setInteger("TransporterId", -1);
        }

        nbttagcompound.removeTag("StartPosNew");
        nbttagcompound.removeTag("StartPos");
        nbttagcompound.removeTag("MovingPathNew");
        nbttagcompound.removeTag("Pos");
        nbttagcompound.removeTag("Riding");

        if (!nbttagcompound.hasKey("ModRev"))
        {
            nbttagcompound.setInteger("ModRev", 1);
        }

        NBTTagCompound adv;

        if (nbttagcompound.hasKey("TransformRole"))
        {
            adv = nbttagcompound.getCompoundTag("TransformRole");
            adv.setInteger("TransporterId", -1);
            nbttagcompound.setTag("TransformRole", adv);
        }

        if (nbttagcompound.hasKey("TransformJob"))
        {
            adv = nbttagcompound.getCompoundTag("TransformJob");
            adv.setInteger("ItemGiverId", 0);
            nbttagcompound.setTag("TransformJob", adv);
        }

        if (nbttagcompound.hasKey("TransformAI"))
        {
            adv = nbttagcompound.getCompoundTag("TransformAI");
            adv.removeTag("StartPosNew");
            adv.removeTag("StartPos");
            adv.removeTag("MovingPathNew");
            nbttagcompound.setTag("TransformAI", adv);
        }
    }
}
