package noppes.npcs.entity.old;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;
import noppes.npcs.ModelData;
import noppes.npcs.entity.EntityCustomNpc;
import noppes.npcs.entity.EntityNPCInterface;

public class EntityNPCOrcMale extends EntityNPCInterface
{
    public EntityNPCOrcMale(World world)
    {
        super(world);
        this.scaleY = 1.0F;
        this.scaleX = this.scaleZ = 1.2F;
        this.display.texture = "customnpcs:textures/entity/orcmale/StrandedOrc.png";
    }

    /**
     * Called to update the entity's position/logic.
     */
    public void onUpdate()
    {
        this.isDead = true;

        if (!this.worldObj.isRemote)
        {
            NBTTagCompound compound = new NBTTagCompound();
            this.writeToNBT(compound);
            EntityCustomNpc npc = new EntityCustomNpc(this.worldObj);
            npc.readFromNBT(compound);
            ModelData data = npc.modelData;
            data.legs.setScale(1.2F, 1.05F);
            data.arms.setScale(1.2F, 1.05F);
            data.body.setScale(1.4F, 1.1F, 1.5F);
            data.head.setScale(1.2F, 1.1F);
            this.worldObj.spawnEntityInWorld(npc);
        }

        super.onUpdate();
    }
}
