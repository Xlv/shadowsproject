package ru.xlv.container.render;

import net.minecraft.client.renderer.entity.RendererLivingEntity;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import ru.krogenit.dds_loader.TextureDDS;
import ru.krogenit.dds_loader.TextureLoaderDDS;
import ru.krogenit.model_loader.Model;
import ru.krogenit.shaders.KrogenitShaders;
import ru.krogenit.shaders.pbr.IPBR;
import ru.xlv.container.XlvsContainerMod;
import ru.xlv.container.common.entity.EntityContainer;
import ru.xlv.core.common.item.rarity.EnumItemRarity;

import java.util.HashMap;
import java.util.Map;

public class RenderContainer extends RendererLivingEntity {

    private final Model containerModel = new Model(new ResourceLocation(XlvsContainerMod.MODID, "models/container.obj"));

    private final Map<EnumItemRarity, TextureDDS> TEXTURES = new HashMap<EnumItemRarity, TextureDDS>() {{
        for (EnumItemRarity value : EnumItemRarity.values()) {
            put(value, new TextureDDS(new ResourceLocation(XlvsContainerMod.MODID, "textures/models/container_" + value.name().toLowerCase() + ".dds")));
        }
    }};

    public RenderContainer() {
        super(null, 1);
    }

    private void render(EntityContainer entityContainer, double x, double y, double z) {
        IPBR shader = KrogenitShaders.getCurrentPBRShader(false);
        if(entityContainer.getRarity() != null) {
            TextureLoaderDDS.bindTexture(this.TEXTURES.get(entityContainer.getRarity()));
        }
        GL11.glPushMatrix();
        double d = entityContainer.getScale() == 1f ? 0d : Math.cos(entityContainer.getScale()) - .15d;
        GL11.glTranslated(x, y + .3d + d, z);
        GL11.glScalef(entityContainer.getScale() * 0.3f, entityContainer.getScale() * 0.3f, entityContainer.getScale() * 0.3f);
        GL11.glRotatef(entityContainer.getRotation(), 0f, 1f, 0f);
        containerModel.render(shader);
        KrogenitShaders.finishCurrentShader();
        GL11.glPopMatrix();
    }

    @Override
    public void doRender(Entity entity, double x, double y, double z, float rotationYaw, float tickTime) {
        render((EntityContainer) entity, x, y, z);
    }

    @Override
    public void doRenderPost(Entity entity, double x, double y, double z, float yaw, float tickTime) {

    }

    @Override
    protected ResourceLocation getEntityTexture(Entity entity) {
        return null;
    }

    @Override
    public void doRenderShadowAndFire(Entity e, double x, double y, double z, float yaw, float tickTime) {}
}
