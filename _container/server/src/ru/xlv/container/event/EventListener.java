package ru.xlv.container.event;

import cpw.mods.fml.common.eventhandler.EventPriority;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import ru.xlv.container.common.entity.EntityContainer;
import ru.xlv.container.entity.EntityContainerFactory;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.event.PlayerDeathEvent;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.mochar.network.matrix.PacketMatrixPlayerInventorySync;

public class EventListener {

    @SubscribeEvent(priority = EventPriority.LOWEST)
    public void event(PlayerDeathEvent event) {
        EntityContainer container = EntityContainerFactory.createContainer(event.getServerPlayer());
        event.getServerPlayer().getEntityPlayer().worldObj.spawnEntityInWorld(container);
        event.getServerPlayer().getEntityPlayer().inventory.clearInventory(null, -1);
    }

    //TODO: удалить
    @SubscribeEvent
    public void event(LivingDeathEvent event) {
        if(!event.isCanceled() && event.entityLiving instanceof EntityPlayer) {
            EntityPlayer player = (EntityPlayer) event.entityLiving;
            ServerPlayer serverPlayer = XlvsCore.INSTANCE.getPlayerManager().getPlayer(player);
            EntityContainer container = EntityContainerFactory.createContainer(serverPlayer);
            player.worldObj.spawnEntityInWorld(container);
            serverPlayer.getSelectedCharacter().getMatrixInventory().clear();
            XlvsCore.INSTANCE.getPacketHandler().sendPacketToPlayer(player, new PacketMatrixPlayerInventorySync(serverPlayer));
        }
    }
}
