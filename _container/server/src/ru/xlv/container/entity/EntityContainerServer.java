package ru.xlv.container.entity;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.World;
import ru.xlv.container.common.entity.EntityContainer;
import ru.xlv.container.network.PacketContainerSync;
import ru.xlv.container.network.PacketContainerSyncInteract;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.item.rarity.EnumItemRarity;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.core.util.Flex;

import javax.annotation.Nonnull;
import java.util.List;

public class EntityContainerServer extends EntityContainer {

    private static final long REMOVAL_TIME_MILLS = 300000L;
    private static final int UPDATE_RADIUS = 64;

//    private final List<ServerPlayer> playersInteractWith = new ArrayList<>();

    private long lastUpdateTimeMills;
    private long creationTimeMills = System.currentTimeMillis();

    public EntityContainerServer(World p_i1582_1_) {
        super(p_i1582_1_);
    }

    public void startInteract(ServerPlayer serverPlayer) {
//        synchronized (playersInteractWith) {
//            if (playersInteractWith.contains(serverPlayer)) return;
//            if (sendPacketSyncInv(serverPlayer)) {
//                playersInteractWith.add(serverPlayer);
//            }
//        }
        XlvsCore.INSTANCE.getPacketHandler().sendPacketToPlayer(serverPlayer.getEntityPlayer(), new PacketContainerSyncInteract(this));
    }

    public void endInteract(ServerPlayer serverPlayer) {
//        synchronized (playersInteractWith) {
//            playersInteractWith.remove(serverPlayer);
//        }
    }

//    public void takeItem(ServerPlayer serverPlayer, int x, int y) {
//        ItemStack itemStack = getMatrixInventory().getMatrixItem(x, y);
//        if (getMatrixInventory().removeItem(itemStack)) {
//            AddItemResult addItemResult = serverPlayer.getSelectedCharacter().getMatrixInventory().addItem(itemStack);
//            if(addItemResult != AddItemResult.SUCCESS) {
//                worldObj.spawnEntityInWorld(new EntityItem(worldObj, serverPlayer.getEntityPlayer().posX, serverPlayer.getEntityPlayer().posY, serverPlayer.getEntityPlayer().posZ, itemStack));
//            }
//        }
//        if(getMatrixInventory().isEmpty()) {
//            creationTimeMills = System.currentTimeMillis() - REMOVAL_TIME_MILLS + 5000L;
//        }
//    }

    @Override
    public void onEntityUpdate() {
        super.onEntityUpdate();
//        playersInteractWith.removeIf(serverPlayer -> !(serverPlayer.isOnline() && serverPlayer.getEntityPlayer().isEntityAlive() && serverPlayer.getEntityPlayer().getDistanceToEntity(this) < 4));
        setGrowing(creationTimeMills + 1000L > System.currentTimeMillis());
        if(System.currentTimeMillis() - lastUpdateTimeMills > 200L) {
            if(getMatrixInventory().isEmpty() || System.currentTimeMillis() - creationTimeMills > REMOVAL_TIME_MILLS) {
                setDead();
                return;
            }
            lastUpdateTimeMills = System.currentTimeMillis();
            List<?> list = worldObj.getEntitiesWithinAABB(EntityPlayer.class, AxisAlignedBB.getBoundingBox(
                    posX - UPDATE_RADIUS, posY - UPDATE_RADIUS, posZ - UPDATE_RADIUS,
                    posX + UPDATE_RADIUS, posY + UPDATE_RADIUS, posZ + UPDATE_RADIUS
            ));
            for (Object o : list) {
                if (o instanceof EntityPlayer && ((EntityPlayer) o).isEntityAlive()) {
                    XlvsCore.INSTANCE.getPacketHandler().sendPacketToPlayer((EntityPlayer) o, new PacketContainerSync(this));
                }
            }
        }
    }

//    private boolean sendPacketSyncInv(ServerPlayer serverPlayer) {
//        if(serverPlayer.isOnline() && serverPlayer.getEntityPlayer().isEntityAlive() && serverPlayer.getEntityPlayer().getDistanceToEntity(this) < 4) {
//            XlvsCore.INSTANCE.getPacketHandler().sendPacketToPlayer(serverPlayer.getEntityPlayer(), new PacketContainerSyncInv(this));
//            return true;
//        }
//        return false;
//    }

    @Override
    public void readEntityFromNBT(NBTTagCompound p_70037_1_) {
        super.readEntityFromNBT(p_70037_1_);
        if(p_70037_1_.hasKey("creationTimeMills"))
            this.creationTimeMills = p_70037_1_.getLong("creationTimeMills");
    }

    @Override
    public void writeEntityToNBT(NBTTagCompound p_70014_1_) {
        super.writeEntityToNBT(p_70014_1_);
        p_70014_1_.setLong("creationTimeMills", this.creationTimeMills);
    }

    @Override
    public void onMatrixInvItemRemoved(@Nonnull ItemStack itemStack) {
        calcRarity();
    }

    @Override
    public void onMatrixInvItemAdded(@Nonnull ItemStack itemStack) {
        calcRarity();
    }

    @Override
    public void onMatrixInvCleaned() {
        calcRarity();
    }

    private void calcRarity() {
        ItemStack uniqueElement = Flex.getUniqueElement(getMatrixInventory().getItems().values(), EnumItemRarity::getItemRarity, (enumItemRarity, enumItemRarity2) -> enumItemRarity.ordinal() < enumItemRarity2.ordinal());
        if (uniqueElement != null) {
            setRarity(EnumItemRarity.getItemRarity(uniqueElement));
        }
    }
}
