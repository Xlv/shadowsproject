package ru.xlv.guide.util;

import lombok.AllArgsConstructor;
import ru.xlv.core.common.util.config.Configurable;
import ru.xlv.core.common.util.config.IConfigGson;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class GuideConfig implements IConfigGson {

    @AllArgsConstructor
    public static class GuideConfigElement {
        @Configurable
        public final String article;
        @Configurable
        public final String text;
        @Configurable
        public final String imageURL;
    }

    @Configurable
    public final List<GuideConfigElement> elements = new ArrayList<>();

    private final String category;

    public GuideConfig(String category) {
        this.category = category;
    }

    @Override
    public File getConfigFile() {
        return new File("config/guide/" + category + ".json");
    }
}
