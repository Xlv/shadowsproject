package ru.xlv.guide.handle;

import ru.xlv.guide.common.Guide;

import java.util.ArrayList;
import java.util.List;

public class GuideManager {

    private final List<Guide> guideList = new ArrayList<>();

    public void sync(List<Guide> guides) {
        synchronized (guideList) {
            guideList.clear();
            guideList.addAll(guides);
        }
    }

    public synchronized List<Guide> getGuideList() {
        return guideList;
    }
}
