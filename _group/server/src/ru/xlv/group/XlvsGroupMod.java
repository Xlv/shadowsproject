package ru.xlv.group;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import lombok.Getter;
import ru.xlv.core.XlvsCore;
import ru.xlv.group.handle.GroupHandler;
import ru.xlv.group.network.*;
import ru.xlv.group.util.GroupLocalization;

import static ru.xlv.group.XlvsGroupMod.MODID;

@Mod(
        name = "XvlsGroupMod",
        modid = MODID,
        version = "1.0"
)
public class XlvsGroupMod {

    static final String MODID = "xlvsgroups";

    @Mod.Instance(MODID)
    public static XlvsGroupMod INSTANCE;

    @Getter
    private GroupHandler groupHandler;

    @Getter
    private final GroupLocalization localization = new GroupLocalization();

    @Mod.EventHandler
    public void event(FMLInitializationEvent event) {
        groupHandler = new GroupHandler();
        groupHandler.init();
        XlvsCore.INSTANCE.getPacketHandler().getPacketRegistry().register(MODID,
                new PacketGroupChangeLeader(),
                new PacketGroupInvite(),
                new PacketGroupInviteAccept(),
                new PacketGroupKick(),
                new PacketGroupSync(),
                new PacketGroupInviteNew()
        );
        localization.load();
    }
}
