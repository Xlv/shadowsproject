package ru.xlv.group.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.common.network.IPacketOutServer;

import java.io.IOException;

@NoArgsConstructor
@AllArgsConstructor
public class PacketGroupInviteNew implements IPacketOutServer {

    private String username;

    @Override
    public void write(EntityPlayer entityPlayer, ByteBufOutputStream bbos) throws IOException {
        bbos.writeUTF(username);
    }
}
