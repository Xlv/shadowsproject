package ru.krogenit.guns.item;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.World;
import org.lwjgl.input.Keyboard;

import java.util.List;

public class ItemAmmo extends ItemWithoutAnim {
    public int maxAmmo;

    public ItemAmmo(String name, int maxAmmo) {
        super(1, name);
        this.maxAmmo = maxAmmo;
        GameRegistry.registerItem(this, name);
    }

    @Override
    public boolean onItemUse(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, World par3World, int par4, int par5, int par6, int par7, float par8, float par9, float par10) {
        return false;
    }

    @Override
    public boolean onEntitySwing(EntityLivingBase e, ItemStack i) {
        return true;
    }

    public void addInformation(ItemStack itemStack, EntityPlayer p, List list, boolean flag) {
        if (!Keyboard.isKeyDown(Keyboard.KEY_LSHIFT)) {
            list.add("<Нажмите " + EnumChatFormatting.RED + "Shift" + EnumChatFormatting.GRAY + ">");
        } else {
            int curAmmo = getAmmo(itemStack);
            list.add("Патронов в магазине: " + curAmmo);
        }
    }

    public int getAmmo(ItemStack itemStack) {
        return maxAmmo - getInteger(itemStack, "ammo");
    }

    public void setAmmo(ItemStack itemStack, int ammo) {
        setInteger(itemStack, "ammo", maxAmmo - ammo);
    }
}
