package ru.krogenit.guns.item;

public enum EnumGunType {
    PISTOL, AUTO, SHOTGUN, AUTO_SHOTGUN, SNIPER_RIFLE;
}
