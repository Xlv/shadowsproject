package ru.krogenit.guns.network.packet;

import io.netty.buffer.ByteBufOutputStream;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import ru.krogenit.guns.item.ItemGun;
import ru.xlv.core.common.network.IPacketInOnServer;
import ru.xlv.core.common.network.IPacketOut;
import ru.xlv.core.common.util.ByteBufInputStream;

import java.io.IOException;

@NoArgsConstructor
@AllArgsConstructor
public class PacketShootServer implements IPacketInOnServer, IPacketOut {

    private float acc;

    @Override
    public void read(EntityPlayer p, ByteBufInputStream data) throws IOException {
        acc = data.readFloat();
        handleMessageOnServerSide(p);
    }

    @Override
    public void write(ByteBufOutputStream data) throws IOException {
        data.writeFloat(acc);
    }

    public void handleMessageOnServerSide(EntityPlayer p) {
        ItemStack item = p.getCurrentEquippedItem();
        if (item != null && item.getItem() instanceof ItemGun) {
            ItemGun weapon = (ItemGun) item.getItem();
            weapon.onItemLeftClick(item, p.worldObj, p, acc);
        }
    }
}