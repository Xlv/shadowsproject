package ru.krogenit.guns.network.packet;

import io.netty.buffer.ByteBufOutputStream;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import ru.xlv.core.common.network.IPacketIn;
import ru.xlv.core.common.network.IPacketOut;
import ru.xlv.core.common.util.ByteBufInputStream;

import java.io.IOException;

@NoArgsConstructor
@AllArgsConstructor
public class PacketFlashlightServer implements IPacketIn, IPacketOut {

    private String playerName;
    private boolean lowAmmo;

    @Override
    public void read(ByteBufInputStream data) throws IOException {
        playerName = data.readUTF();
        lowAmmo = data.readBoolean();
    }

    @Override
    public void write(ByteBufOutputStream data) throws IOException {
        data.writeUTF(playerName);
        data.writeBoolean(lowAmmo);
    }
}