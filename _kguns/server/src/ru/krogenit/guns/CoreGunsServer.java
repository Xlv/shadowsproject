package ru.krogenit.guns;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import lombok.Getter;
import ru.krogenit.guns.event.EventListener;
import ru.krogenit.guns.item.*;
import ru.krogenit.guns.network.PacketGunSync;
import ru.krogenit.guns.network.PacketMeleeSync;
import ru.krogenit.guns.network.packet.*;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.XlvsCoreCommon;
import ru.xlv.core.common.inventory.MatrixInventory;

import java.util.ArrayList;
import java.util.List;

@Mod(modid = CoreGunsCommon.MODID, name = "Guns Server", version = "1.0.0")
public class CoreGunsServer extends CoreGunsCommon {

    @Mod.Instance(CoreGunsCommon.MODID)
    public static CoreGunsServer instance;

    @Getter
    private final List<ItemGun> registeredGunList = new ArrayList<>();
    @Getter private final List<ItemMeleeWeapon> registeredMeleeList = new ArrayList<>();

    @Mod.EventHandler
    public void init(FMLInitializationEvent e) {
        XlvsCore.INSTANCE.getPacketHandler().getPacketRegistry().register(CoreGunsCommon.MODID,
                new PacketPlaySoundServer(),
                new PacketFlashlightServer(),
                new PacketBulletServer(),
                new PacketReloadServer(),
                new PacketModifiServer(),
                new PacketShootServer(),
                new PacketDamageServer(),
                new PacketMeleeSync()
        );

        ammo_incarn09 = new ItemAmmo("ammo_incarn09", 12);
        ammo_arkonista = new ItemAmmo("ammo_arkonista", 30);
        ammo_contributor = new ItemAmmo("ammo_contributor", 45);
        ammo_isaac = new ItemAmmo("ammo_isaac", 6);
        ammo_keeper = new ItemAmmo("ammo_keeper", 30);
        ammo_prowler = new ItemAmmo("ammo_prowler", 40);
        ammo_seeker = new ItemAmmo("ammo_seeker", 6);
        ammo_slash = new ItemAmmo("ammo_slash", 45);
        ammo_spitter = new ItemAmmo("ammo_spitter", 25);
        ammo_wasp = new ItemAmmo("ammo_wasp", 25);
        ammo_asr37 = new ItemAmmo("ammo_asr37", 35);
        ammo_barracuda = new ItemAmmo("ammo_barracuda", 6);
        ammo_bonecrusher = new ItemAmmo("ammo_bonecrusher", 4);
        ammo_devourer = new ItemAmmo("ammo_devourer", 8);
        ammo_hyperion = new ItemAmmo("ammo_hyperion", 1);
        ammo_osiris = new ItemAmmo("ammo_osiris", 10);
        ammo_radZinger = new ItemAmmo("ammo_radZinger", 30);
        ammo_ragnarok = new ItemAmmo("ammo_ragnarok", 120);
        ammo_vertmunt = new ItemAmmo("ammo_vertmunt", 8);

        registeredGunList.add(incarn09 = new ItemGunServer("incarn09", ammo_incarn09, 1).addSlotType(MatrixInventory.SlotType.ADD_WEAPON).addSlotType(MatrixInventory.SlotType.SECOND_WEAPON).addSlotType(MatrixInventory.SlotType.MAIN_WEAPON));
        registeredGunList.add(arkonista = new ItemGunServer("arkonista", ammo_arkonista, 1).addSlotType(MatrixInventory.SlotType.MAIN_WEAPON).addSlotType(MatrixInventory.SlotType.SECOND_WEAPON));
        registeredGunList.add(isaac = new ItemGunServer("isaac", ammo_isaac, 1).addSlotType(MatrixInventory.SlotType.ADD_WEAPON).addSlotType(MatrixInventory.SlotType.SECOND_WEAPON).addSlotType(MatrixInventory.SlotType.MAIN_WEAPON));
        registeredGunList.add(spitter = new ItemGunServer("spitter", ammo_spitter, 1).addSlotType(MatrixInventory.SlotType.SECOND_WEAPON).addSlotType(MatrixInventory.SlotType.MAIN_WEAPON));
        registeredGunList.add(keeper = new ItemGunServer("keeper", ammo_keeper, 1).addSlotType(MatrixInventory.SlotType.SECOND_WEAPON).addSlotType(MatrixInventory.SlotType.MAIN_WEAPON));
        registeredGunList.add(seeker = new ItemGunServer("seeker", ammo_seeker, 1).addSlotType(MatrixInventory.SlotType.ADD_WEAPON).addSlotType(MatrixInventory.SlotType.SECOND_WEAPON).addSlotType(MatrixInventory.SlotType.MAIN_WEAPON));
        registeredGunList.add(contributor = new ItemGunServer("contributor", ammo_contributor, 1).addSlotType(MatrixInventory.SlotType.SECOND_WEAPON).addSlotType(MatrixInventory.SlotType.MAIN_WEAPON));
        registeredGunList.add(prowler = new ItemGunServer("prowler", ammo_prowler, 1).addSlotType(MatrixInventory.SlotType.SECOND_WEAPON).addSlotType(MatrixInventory.SlotType.MAIN_WEAPON));
        registeredGunList.add(wasp = new ItemGunServer("wasp", ammo_wasp, 1).addSlotType(MatrixInventory.SlotType.MAIN_WEAPON).addSlotType(MatrixInventory.SlotType.SECOND_WEAPON));
        registeredGunList.add(slash = new ItemGunServer("slash", ammo_slash, 1).addSlotType(MatrixInventory.SlotType.MAIN_WEAPON).addSlotType(MatrixInventory.SlotType.SECOND_WEAPON));
        registeredGunList.add(asr37 = new ItemGunServer("asr37", ammo_asr37, 1).addSlotType(MatrixInventory.SlotType.MAIN_WEAPON).addSlotType(MatrixInventory.SlotType.SECOND_WEAPON));
        registeredGunList.add(barracuda = new ItemGunServer("barracuda", ammo_barracuda, 8).addSlotType(MatrixInventory.SlotType.ADD_WEAPON).addSlotType(MatrixInventory.SlotType.SECOND_WEAPON).addSlotType(MatrixInventory.SlotType.MAIN_WEAPON));
        registeredGunList.add(bonecrusher = new ItemGunServer("bonecrusher", ammo_bonecrusher, 8).addSlotType(MatrixInventory.SlotType.MAIN_WEAPON).addSlotType(MatrixInventory.SlotType.SECOND_WEAPON));
        registeredGunList.add(devourer = new ItemGunServer("devourer", ammo_devourer, 1).addSlotType(MatrixInventory.SlotType.MAIN_WEAPON).addSlotType(MatrixInventory.SlotType.SECOND_WEAPON));
        registeredGunList.add(hyperion = new ItemGunServer("hyperion", ammo_hyperion, 1).addSlotType(MatrixInventory.SlotType.MAIN_WEAPON).addSlotType(MatrixInventory.SlotType.SECOND_WEAPON));
        registeredGunList.add(osiris = new ItemGunServer("osiris", ammo_osiris, 1).addSlotType(MatrixInventory.SlotType.MAIN_WEAPON).addSlotType(MatrixInventory.SlotType.SECOND_WEAPON));
        registeredGunList.add(radZinger = new ItemGunServer("radZinger", ammo_radZinger, 1).addSlotType(MatrixInventory.SlotType.MAIN_WEAPON).addSlotType(MatrixInventory.SlotType.SECOND_WEAPON));
        registeredGunList.add(ragnarok = new ItemGunServer("ragnarok", ammo_ragnarok, 1).addSlotType(MatrixInventory.SlotType.MAIN_WEAPON).addSlotType(MatrixInventory.SlotType.SECOND_WEAPON));
        registeredGunList.add(vertmunt = new ItemGunServer("vertmunt", ammo_vertmunt, 8).addSlotType(MatrixInventory.SlotType.MAIN_WEAPON).addSlotType(MatrixInventory.SlotType.SECOND_WEAPON));
        registeredMeleeList.add(radzeg = new ItemMeleeWeaponServer("radzeg", 0).addSlotType(MatrixInventory.SlotType.MELEE_WEAPON));
        registeredMeleeList.add(azallet = new ItemMeleeWeaponServer("azallet", 0).addSlotType(MatrixInventory.SlotType.MELEE_WEAPON));
        registeredMeleeList.add(crasher = new ItemMeleeWeaponServer("crasher", 0).addSlotType(MatrixInventory.SlotType.MELEE_WEAPON));
        initMedicine();
        registerItems();

        XlvsCore.INSTANCE.getPacketHandler().getPacketRegistry().register(MODID, new PacketGunSync());
        XlvsCoreCommon.EVENT_BUS.register(new EventListener());
    }

    public void initMedicine() {
        firstAidKitBig = new ItemMedicineServer("firstaidkit_big", 100);
        firstAidKitSmall = new ItemMedicineServer("firstaidkit_small", 50);
        syringe = new ItemMedicineServer("syringe", 20);
    }
}