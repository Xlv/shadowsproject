package ru.krogenit.guns.render.ammo;

import lombok.Getter;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.IItemRenderer;
import ru.krogenit.dds_loader.TextureDDS;
import ru.krogenit.dds_loader.TextureLoaderDDS;
import ru.krogenit.guns.item.ammo.ItemAmmoClient;
import ru.krogenit.guns.render.*;
import ru.krogenit.model_loader.Model;
import ru.krogenit.shaders.KrogenitShaders;
import ru.krogenit.shaders.pbr.IPBR;
import ru.xlv.core.common.util.config.Configurable;
import ru.xlv.core.util.obf.IgnoreObf;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.lwjgl.opengl.GL11.*;

public class ItemGunAmmoRenderer extends ItemAnimationHelper implements IItemRenderer, ICustomizableRenderer {

    private static final String CONFIG_PATH_FORMAT = "config/renderer/ammo/%s_cfg.json";

    protected Model gunModel;
    protected ItemAmmoClient itemAmmo;
    @Configurable @IgnoreObf protected List<RenderObject> renderObjects = new ArrayList<>();
    @Getter protected Map<String, RenderObject> renderObjectsByName = new HashMap<>();

    private final TextureDDS diffuse;
    private TextureDDS normal;
    private TextureDDS specular;
    private TextureDDS gloss;
    private TextureDDS emission;
    private boolean hasSpecular;
    private boolean hasNormal;
    private boolean hasGloss;
    private boolean hasEmission;
    private float emissionPower;
    private final String modelPartName;

    public ItemGunAmmoRenderer(Model gunModel, ItemAmmoClient itemAmmo, String modelPartName, TextureDDS diffuse, TextureDDS normal, TextureDDS specular, TextureDDS gloss, TextureDDS emission, float emissionPower) {
        this.gunModel = gunModel;
        this.itemAmmo = itemAmmo;
        this.modelPartName = modelPartName;
        this.diffuse = diffuse;
        if(normal != null) {
            this.hasNormal = true;
            this.normal = normal;
        }
        if(specular != null) {
            this.hasSpecular = true;
            this.specular = specular;
        }
        if(gloss != null) {
            this.hasGloss = true;
            this.gloss = gloss;
        }
        if(emission != null) {
            this.hasEmission = true;
            this.emission = emission;
            this.emissionPower = emissionPower;
        }
    }

    @Override
    public void init() {
        renderObjects.add(new RenderObject(EnumRenderObjectType.FIRST_PERSON.getName()));
        renderObjects.add(new RenderObject(EnumRenderObjectType.EQUIPPED.getName()));
        renderObjects.add(new RenderObject(EnumRenderObjectType.ENTITY.getName()));
        renderObjects.add(new RenderObject(EnumRenderObjectType.INVENTORY.getName()));
        RenderObject ammo = new RenderObject("ammo");
        ammo.getPosition().x = 0.7f;
        ammo.getPosition().y = 0.6f;
        ammo.getPosition().z = 1f;
        ammo.getScale().x = 0.1f;
        ammo.getScale().y = 0.1f;
        ammo.getScale().z = 0.1f;
        renderObjects.add(ammo);
        RenderObject e = new RenderObject(EnumRenderObjectType.RIGHT_HAND.getName());
        e.getPosition().x = 0.5f;
        e.getPosition().y = 0.4f;
        e.getPosition().z = 0.4f;
        e.getRotation().x = 80f;
        e.getRotation().z = -20;
        renderObjects.add(e);
        load();
        for(RenderObject renderObject : renderObjects) {
            renderObjectsByName.put(renderObject.getName(), renderObject);
        }
    }

    private void renderAmmo() {
        IPBR shader = KrogenitShaders.getCurrentPBRShader(false);
        TextureLoaderDDS.bindTexture(diffuse);
        if(hasNormal) {
            TextureLoaderDDS.bindNormalMap(normal, shader);
        }
        if(hasSpecular) {
            TextureLoaderDDS.bindSpecularMap(specular, shader);
        }
        if(hasGloss) {
            TextureLoaderDDS.bindGlossMap(gloss, shader);
        }
        if(hasEmission) {
            TextureLoaderDDS.bindEmissionMap(emission, shader, emissionPower);
        }
        gunModel.renderPart(modelPartName, shader);
        if(hasNormal) shader.setNormalMapping(false);
        if(hasSpecular) shader.setSpecularMapping(false);
        if(hasGloss) shader.setGlossMapping(false);
        if(hasEmission) shader.setEmissionMapping(false);
        TextureLoaderDDS.unbind();
        KrogenitShaders.finishCurrentShader();
    }

    public void renderFirstPerson(ItemStack itemStack) {
        glRotatef(27f, 0, 0, 1);
        glRotatef(86f, 0, 1, 0);
        setupMatrix(EnumRenderObjectType.FIRST_PERSON.getName());
        renderHand();
        setupMatrix("ammo");
        renderAmmo();
    }

    public void renderEquipped(ItemStack itemStack) {
        setupMatrix(EnumRenderObjectType.EQUIPPED.getName());
        renderAmmo();
    }

    public void renderAsEntity(ItemStack itemStack) {
        setupMatrix(EnumRenderObjectType.ENTITY.getName());
        renderAmmo();
    }

    public void renderInventory(ItemStack itemStack) {
        setupMatrix(EnumRenderObjectType.INVENTORY.getName());
        renderAmmo();
    }

    private void setupMatrix(String name) {
        RenderObject renderObject = renderObjectsByName.get(name);
        Vector3fConfigurable position = renderObject.getPosition();
        Vector3fConfigurable rotation = renderObject.getRotation();
        Vector3fConfigurable scale = renderObject.getScale();
        glTranslatef(position.x , position.y, position.z);
        glScaled(scale.x, scale.y, scale.z);
        glRotatef(rotation.x, 1, 0, 0);
        glRotatef(rotation.z, 0, 0, 1);
        glRotatef(rotation.y, 0, 1, 0);
    }

    private void renderHand() {
        IPBR shader = KrogenitShaders.getCurrentPBRShader(true);
        glPushMatrix();
        setupMatrix(EnumRenderObjectType.RIGHT_HAND.getName());
        hand.renderRight();
        glPopMatrix();
        KrogenitShaders.finishCurrentShader();
    }

    @Override
    public Map<String, RenderObject> getRenderObjects() {
        return renderObjectsByName;
    }

    @Override
    public Map<String, List<AnimationNode>> getAnimations() {
        return null;
    }

    @Override
    public long getAnimationTime() {
        return 0;
    }

    @Override
    public boolean handleRenderType(ItemStack item, ItemRenderType type) {
        return true;
    }

    @Override
    public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack item, ItemRendererHelper helper) {
        return false;
    }

    @Override
    public void renderItem(ItemRenderType type, ItemStack itemStack, Object... data) {
        switch (type) {
            case EQUIPPED_FIRST_PERSON:
                renderFirstPerson(itemStack);
                return;
            case EQUIPPED:
                renderEquipped(itemStack);
                return;
            case INVENTORY:
                renderInventory(itemStack);
                return;
            case ENTITY:
                renderAsEntity(itemStack);
        }
    }

    @Override
    public void renderItemPost(ItemRenderType type, ItemStack item, Object... data) {

    }

    @Override
    public File getConfigFile() {
        return new File(String.format(CONFIG_PATH_FORMAT, itemAmmo.getUnlocalizedName()));
    }
}
