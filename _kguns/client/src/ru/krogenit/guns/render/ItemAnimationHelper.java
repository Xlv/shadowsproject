package ru.krogenit.guns.render;

import lombok.Getter;
import net.minecraft.client.Minecraft;
import org.lwjgl.util.vector.Vector3f;
import ru.krogenit.guns.model.Hand;

public abstract class ItemAnimationHelper {
    protected static final Minecraft mc = Minecraft.getMinecraft();
    public static final Hand hand = new Hand();

    protected final Vector3f[] anim = new Vector3f[15];
    protected final boolean[] isAnim = new boolean[15];
    protected static float animSpeed;
    @Getter protected long startAnimationTime;
    @Getter protected long totalAnimationTime;

    protected void playAnimation(int i, Vector3f anim, float sx, float sy, float sz, EnumAnimationType type) {
        boolean first = (i == 0 && !isAnim[i]);
        if(startAnimationTime == 0) {
            startAnimationTime = System.currentTimeMillis();
        }
        if (first || (!isAnim[i] && isAnim[i - 1])) {
            isAnim[i] = playAnimation(anim, sx, sy, sz, type);
        }
    }

    protected void playAnimation(int i, Vector3f[] anim, Vector3f[] speed, EnumAnimationType[] type) {
        if ((i == 0 && !isAnim[i]) || (!isAnim[i] && isAnim[i - 1])) {
            boolean ready = true;
            for (int j = 0; j < anim.length; j++)
                if (!playAnimation(anim[j], speed[j].x, speed[j].y, speed[j].z, type[j])) ready = false;
            isAnim[i] = ready;
        }
    }

    protected boolean playAnimation(Vector3f anim, float sx, float sy, float sz, EnumAnimationType type) {
        switch (type) {
            case SlowStart:
                if (sx != 0) sx = getStartSlowAnim(sx, anim.x);
                if (sy != 0) sy = getStartSlowAnim(sy, anim.y);
                if (sz != 0) sz = getStartSlowAnim(sz, anim.z);
                break;
            case SlowEnd:
                if (sx != 0) sx = getEndSlowAnim(sx, anim.x);
                if (sy != 0) sy = getEndSlowAnim(sy, anim.y);
                if (sz != 0) sz = getEndSlowAnim(sz, anim.z);
                break;
            case FastYOnX:
                sy = getFastOnAnim(sx, anim.x, sy, anim.y, false);
                break;
            case FastYOnZ:
                sy = getFastOnAnim(sz, anim.z, sy, anim.y, false);
                break;
            case FastXZOnY:
                sx = getFastOnAnim(sy, anim.y, sx, anim.x, false);
                sz = getFastOnAnim(sy, anim.y, sz, anim.z, false);
                break;
            case FastXOnYZ:
                sx = getFastOnAnim(sy, anim.y, sz, anim.z, sx, anim.x, false);
                break;
            case FastYZOnX:
                sy = getFastOnAnim(sx, anim.x, sy, anim.y, false);
                sz = getFastOnAnim(sx, anim.x, sz, anim.z, false);
                break;
            case FastYOnXZ:
                sy = getFastOnAnim(sx, anim.x, sz, anim.z, sy, anim.y, false);
                break;
            case YOnXZ:
                sy = getFastOnAnim(sx, anim.x, sz, anim.z, sy, anim.y, true);
                break;
            case XZOnY:
                sx = getFastOnAnim(sy, anim.y, sx, anim.x, true);
                sz = getFastOnAnim(sy, anim.y, sz, anim.z, true);
                break;
            case SlowStartAndEnd:
                if (sx != 0) sx = getSlowAnim(sx, anim.x);
                if (sy != 0) sy = getSlowAnim(sy, anim.y);
                if (sz != 0) sz = getSlowAnim(sz, anim.z);
        }
        anim.x += sx * animSpeed;
        anim.y += sy * animSpeed;
        anim.z += sz * animSpeed;
        if (sx >= 0) {
            if (anim.x >= 1) anim.x = 1;
            if (anim.y >= 1) anim.y = 1;
            if (anim.z >= 1) anim.z = 1;
            return anim.x == 1 && anim.y == 1 && anim.z == 1;
        } else {
            if (anim.x <= 0) anim.x = 0;
            if (anim.y <= 0) anim.y = 0;
            if (anim.z <= 0) anim.z = 0;
            return anim.x == 0 && anim.y == 0 && anim.z == 0;
        }
    }

    private float getFastOnAnim(float ons, float onanim, float fasts, float fastanim, boolean flag) {
        if (ons > 0) {
            if (onanim == 1) {
                if (fasts > 0) {
                    if (flag) return fasts;
                    fasts = (fastanim) / 5f;
                    if (fasts < 0.005f) fasts = 0.005f;
                } else {
                    if (flag) return fasts;
                    fasts = -(1 - fastanim) / 5f;
                    if (fasts > -0.005f) fasts = -0.005f;
                }
            } else {
                if (flag) return 0f;
                else return fasts / 20f;
            }
        } else {
            if (onanim == 0) {
                if (fasts > 0) {
                    if (flag) return fasts;
                    fasts = (fastanim) / 5f;
                    if (fasts < 0.005f) fasts = 0.005f;
                } else {
                    if (flag) return fasts;
                    fasts = -(1 - fastanim) / 5f;
                    if (fasts > -0.005f) fasts = -0.005f;
                }
            } else {
                if (flag) return 0f;
                else return fasts / 20f;
            }
        }
        return fasts;
    }

    private float getFastOnAnim(float ons, float onanim, float ons2, float onanim2, float fasts, float fastanim, boolean flag) {
        float slowSpeed = 3f;
        if (ons > 0) {
            if (ons2 > 0) {
                if (onanim == 1 && onanim2 == 1) {
                    if (fasts > 0) {
                        if (flag) return fasts;
                        fasts = (fastanim) / 5f;
                        if (fasts < 0.005f) fasts = 0.005f;
                    } else {
                        if (flag) return fasts;
                        fasts = -(1 - fastanim) / 5f;
                        if (fasts > -0.005f) fasts = -0.005f;
                    }
                }
                if (flag) return 0f;
                else return fasts / slowSpeed;
            } else {
                if (onanim == 1 && onanim2 == 0) {
                    if (fasts > 0) {
                        if (flag) return fasts;
                        fasts = (fastanim) / 5f;
                        if (fasts < 0.005f) fasts = 0.005f;
                    } else {
                        if (flag) return fasts;
                        fasts = -(1 - fastanim) / 5f;
                        if (fasts > -0.005f) fasts = -0.005f;
                    }
                }
                if (flag) return 0f;
                else return fasts / slowSpeed;
            }
        } else {
            if (ons2 > 0) {
                if (onanim == 0 && onanim2 == 1) {
                    if (fasts > 0) {
                        if (flag) return fasts;
                        fasts = (fastanim) / 5f;
                        if (fasts < 0.005f) fasts = 0.005f;
                    } else {
                        if (flag) return fasts;
                        fasts = -(1 - fastanim) / 5f;
                        if (fasts > -0.005f) fasts = -0.005f;
                    }
                }
                if (flag) return 0f;
                else return fasts / slowSpeed;
            } else {
                if (onanim == 0 && onanim2 == 0) {
                    if (fasts > 0) {
                        if (flag) return fasts;
                        fasts = (fastanim) / 5f;
                        if (fasts < 0.005f) fasts = 0.005f;
                    } else {
                        if (flag) return fasts;
                        fasts = -(1 - fastanim) / 5f;
                        if (fasts > -0.005f) fasts = -0.005f;
                    }
                }
                if (flag) return 0f;
                else return fasts / slowSpeed;
            }
        }
    }

    private float getSlowAnim(float s, float anim) {
        float min = s / 80f;
        if (s > 0) {
            if(anim > 0.5f) {
                anim -= 0.5f;
                anim *= 2f;
                s *= (1f - anim);
            } else {
                s *= (anim / 0.5f);
            }

            if (s < min) s = min;
        } else {
            if(anim > 0.5f) {
                anim -= 0.5f;
                anim *= 2f;
                s *= (1f - anim);
            } else {
                s *= (anim / 0.5f);
            }

            if (s > min) s = min;
        }
        return s;
    }

    private float getStartSlowAnim(float s, float anim) {
        float min = s / 80f;
        if (s > 0) {
            if(anim < 0.25f) {
                s *= (anim / 0.25f);
            }

            if (s < min) s = min;
        } else {
            if(anim > 0.75f) {
                anim -= 0.75f;
                anim *= 4f;
                s *= (1f - anim);
            }

            if (s > min) s = min;
        }
        return s;
    }

    private float getEndSlowAnim(float s, float anim) {
        float min = s / 80f;
        if (s > 0) {
            if(anim > 0.75f) {
                anim -= 0.75f;
                anim *= 4f;
                s *= (1f - anim);
            }

            if (s < min) s = min;
        } else {
            if(anim < 0.25f) {
                s *= (anim / 0.25f);
            }

            if (s > min) s = min;
        }
        return s;
    }

}
