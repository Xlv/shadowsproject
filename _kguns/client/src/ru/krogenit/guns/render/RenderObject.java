package ru.krogenit.guns.render;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ru.xlv.core.common.util.config.Configurable;
import ru.xlv.core.common.util.config.IConfigGson;
import ru.xlv.core.util.obf.IgnoreObf;

import java.io.File;

@RequiredArgsConstructor
@Getter
@Configurable
public class RenderObject implements IConfigGson {
    @IgnoreObf private final String name;
    @IgnoreObf private Vector3fConfigurable position = new Vector3fConfigurable(0,0,0);
    @IgnoreObf private Vector3fConfigurable rotation = new Vector3fConfigurable(0,0,0);
    @IgnoreObf private Vector3fConfigurable scale = new Vector3fConfigurable(1,1,1);

    @Override
    public File getConfigFile() {
        return null;
    }
}
