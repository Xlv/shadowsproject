package ru.krogenit.guns.render.entity;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import ru.krogenit.dds_loader.TextureLoaderDDS;
import ru.krogenit.guns.CoreGunsCommon;
import ru.krogenit.guns.entity.EntityBulletClient;
import ru.krogenit.shaders.KrogenitShaders;
import ru.krogenit.shaders.pbr.IPBR;
import ru.krogenit.utils.AnimationHelper;
import ru.krogenit.utils.Utils;

import static org.lwjgl.opengl.GL11.*;

@SideOnly(Side.CLIENT)
public class RenderEntityBullet extends Render {
    private static final ResourceLocation texture = new ResourceLocation(CoreGunsCommon.MODID + ":textures/shoot.png");
    private static final Minecraft mc = Minecraft.getMinecraft();

    public RenderEntityBullet() {
        postRender = true;
    }

    public void doRender(Entity entity, double x, double y, double z, float rotationYaw, float tickTime) {

    }

    @Override
    public void doRenderPost(Entity entity, double x, double y, double z, float rotationYaw, float tickTime) {
        EntityBulletClient bullet = (EntityBulletClient) entity;

        if (!KrogenitShaders.lightPass) {
            float animSpeed = AnimationHelper.getAnimationSpeed();
            bullet.renderLifeTime += animSpeed * 1.5f;
            float distance = bullet.getDistanceToEntity(mc.thePlayer);
            if(distance < 15f)
                distance /= 15f;
            else distance = 1f;
            if (bullet.renderLifeTime > 2.6f) {
//                bullet.color += 0.75f * animSpeed;
//                if (bullet.color > 3) {
//                    bullet.color = 3;
//                }
                glPushMatrix();
                glTranslatef((float) x, (float) y, (float) z);
                IPBR shader = KrogenitShaders.getCurrentPBRShader(false);
                shader.setEmissionMapping(true);
                shader.setEmissionPower(2f);
                shader.setLightMapping(false);
                glDisable(GL_CULL_FACE);
                glEnable(GL_BLEND);
                glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                glDepthMask(false);
                glAlphaFunc(GL_GREATER, 0.1F);
                GL11.glRotatef(bullet.prevRotationYaw + (bullet.rotationYaw - bullet.prevRotationYaw) * tickTime - 90.0F, 0.0F, 1.0F, 0.0F);
                GL11.glRotatef(bullet.prevRotationPitch + (bullet.rotationPitch - bullet.prevRotationPitch) * tickTime, 0.0F, 0.0F, 1.0F);
                float scale = 0.004f;
                glScalef(scale, scale, scale);
                Utils.bindTexture(texture);
                GL13.glActiveTexture(GL13.GL_TEXTURE4);
                Utils.bindTexture(texture);
                glColor4f(3f, 3f, 3f, 1f * distance);
                Tessellator t = Tessellator.instance;
                float sizeX = 100f;
                float sizeY = 20f;
                t.startDrawingQuads();
                t.addVertexWithUV(-sizeX, sizeY, 1, 0, (1.0F));
                t.addVertexWithUV(sizeX, sizeY, 1, 1, (1.0F));
                t.addVertexWithUV(sizeX, -sizeY, 1, 1, (0.0F));
                t.addVertexWithUV(-sizeX, -sizeY, 1, 0, (0.0F));
                t.draw();
                glRotatef(90, 1, 0, 0);
                t.startDrawingQuads();
                t.addVertexWithUV(-sizeX, sizeY, 1, 0, (1.0F));
                t.addVertexWithUV(sizeX, sizeY, 1, 1, (1.0F));
                t.addVertexWithUV(sizeX,  -sizeY, 1, 1, (0.0F));
                t.addVertexWithUV(-sizeX,  -sizeY, 1, 0, (0.0F));
                t.draw();
                glRotatef(90, 0, 1, 0);
                glScalef(0.05f, 1.0f, 1.0f);
                sizeY = 40f;
                t.startDrawingQuads();
                t.addVertexWithUV(-sizeX, sizeY, 1, 0, (1.0F));
                t.addVertexWithUV(sizeX, sizeY, 1, 1, (1.0F));
                t.addVertexWithUV(sizeX, -sizeY, 1, 1, (0.0F));
                t.addVertexWithUV(-sizeX, -sizeY, 1, 0, (0.0F));
                t.draw();

                glDepthMask(true);
                glDisable(GL_BLEND);
                glAlphaFunc(GL_GREATER, 0.1F);
                glEnable(GL_CULL_FACE);
                glPopMatrix();
                shader.setEmissionMapping(false);
                shader.setLightMapping(true);
                TextureLoaderDDS.unbind();
                KrogenitShaders.finishCurrentShader();
            }
        }
    }

    /**
     * Returns the location of an entity's texture. Doesn't seem to be called unless you call Render.bindEntityTexture.
     */
    public ResourceLocation getEntityTexture(Entity entity) {
        return TextureMap.locationItemsTexture;
    }
}
