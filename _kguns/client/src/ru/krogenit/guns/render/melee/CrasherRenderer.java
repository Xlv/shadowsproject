package ru.krogenit.guns.render.melee;

import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import ru.krogenit.dds_loader.TextureDDS;
import ru.krogenit.dds_loader.TextureLoaderDDS;
import ru.krogenit.guns.CoreGunsCommon;
import ru.krogenit.guns.item.ItemMeleeWeaponClient;
import ru.krogenit.guns.render.AbstractMeleeWeaponRenderer;
import ru.krogenit.model_loader.Model;
import ru.krogenit.shaders.KrogenitShaders;
import ru.krogenit.shaders.pbr.IPBR;

import static org.lwjgl.opengl.GL11.glScalef;
import static org.lwjgl.opengl.GL13.GL_TEXTURE0;
import static org.lwjgl.opengl.GL13.glActiveTexture;

public class CrasherRenderer extends AbstractMeleeWeaponRenderer {

    private final TextureDDS diffuseBase = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/Crasher-Base_D.dds"));
    private final TextureDDS normalBase = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/Crasher-Base_N.dds"));
    private final TextureDDS specularBase = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/Crasher-Base_S.dds"));
    private final TextureDDS glossBase = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/Crasher-Base_G.dds"));

    private final TextureDDS diffuseDefMat = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/Crasher-DefMat_D.dds"));
    private final TextureDDS normalDefMat = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/Crasher-DefMat_N.dds"));
    private final TextureDDS specularDefMat = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/Crasher-DefMat_S.dds"));
    private final TextureDDS glossDefMat = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/Crasher-DefMat_G.dds"));

    private final TextureDDS emission = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/Crasher-Emission_D.dds"));

    public CrasherRenderer(ItemMeleeWeaponClient itemMelee) {
        super(new Model(new ResourceLocation(CoreGunsCommon.MODID, "models/weapons/Crasher.obj")), itemMelee);
    }

    @Override
    public void renderInModifyGui(ItemStack itemStack) {
        float scale = 0.15f;
        glScalef(scale, scale, scale);
        renderWeapon();
    }

    @Override
    protected void renderWeapon() {
        IPBR shader = KrogenitShaders.getCurrentPBRShader(false);
        TextureLoaderDDS.bindTexture(diffuseBase);
        TextureLoaderDDS.bindNormalMap(normalBase, shader);
        TextureLoaderDDS.bindSpecularMap(specularBase, shader);
        TextureLoaderDDS.bindGlossMap(glossBase, shader);
        weaponModel.renderPart("Base", shader);
        glActiveTexture(GL_TEXTURE0);
        TextureLoaderDDS.bindTexture(diffuseDefMat);
        TextureLoaderDDS.bindNormalMap(normalDefMat, shader);
        TextureLoaderDDS.bindSpecularMap(specularDefMat, shader);
        TextureLoaderDDS.bindGlossMap(glossDefMat, shader);
        weaponModel.renderPart("Mat1", shader);
        shader.setUseTexture(false);
        TextureLoaderDDS.bindEmissionMap(emission, shader, 5f);
        weaponModel.renderPart("Emission", shader);
        shader.setEmissionMapping(false);
        shader.setUseTexture(true);
        shader.setNormalMapping(false);
        shader.setSpecularMapping(false);
        shader.setGlossMapping(false);
        TextureLoaderDDS.unbind();
        KrogenitShaders.finishCurrentShader();
    }
}
