package ru.krogenit.guns.render.guns;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector3f;
import ru.krogenit.dds_loader.TextureDDS;
import ru.krogenit.dds_loader.TextureLoaderDDS;
import ru.krogenit.guns.CoreGunsCommon;
import ru.krogenit.guns.item.ItemGun;
import ru.krogenit.guns.render.AbstractItemGunRenderer;
import ru.krogenit.model_loader.Model;
import ru.krogenit.shaders.KrogenitShaders;
import ru.krogenit.shaders.pbr.IPBR;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.GL_TEXTURE0;
import static org.lwjgl.opengl.GL13.glActiveTexture;

public class VertmuntRenderer extends AbstractItemGunRenderer {

    private final Model scope = new Model(new ResourceLocation(CoreGunsCommon.MODID, "models/attachments/VertMunt_Scope.obj"));

    private final TextureDDS resourceLocationStatefulDiffuse;
    private final TextureDDS resourceLocationStatefulNormal;
    private final TextureDDS resourceLocationStatefulSpecular;
    private final TextureDDS resourceLocationStatefulGlassMap;
    private final TextureDDS resourceLocationStatefulCollimatorDiffuseEmission;

    public VertmuntRenderer(ItemGun gun) {
        super(new Model(new ResourceLocation(CoreGunsCommon.MODID, "models/weapons/VertMunt.obj")), gun);
        resourceLocationStatefulDiffuse = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/VertMunt_D.dds"));
        resourceLocationStatefulNormal = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/VertMunt_N.dds"));
        resourceLocationStatefulSpecular = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/VertMunt_S.dds"));
        resourceLocationStatefulGlassMap = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/VertMunt_G.dds"));
        resourceLocationStatefulCollimatorDiffuseEmission = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/VertMunt_Collimator.dds"));
        postRender = true;
    }

    @Override
    public void renderInModifyGui(ItemStack itemStack) {
        float scale = 0.15f;
        glScalef(scale, scale, scale);
        renderGun(ItemRenderType.INVENTORY);
        renderComponents(itemStack, ItemRenderType.INVENTORY);
    }

    @Override
    protected void renderGun(ItemRenderType type) {
        IPBR shader = KrogenitShaders.getCurrentPBRShader(false);
        TextureLoaderDDS.bindTexture(resourceLocationStatefulDiffuse);
        TextureLoaderDDS.bindNormalMap(resourceLocationStatefulNormal, shader);
        TextureLoaderDDS.bindSpecularMap(resourceLocationStatefulSpecular, shader);
        TextureLoaderDDS.bindGlossMap(resourceLocationStatefulGlassMap, shader);
        glActiveTexture(GL_TEXTURE0);
        gunModel.renderPart("VertMunt", shader);
        gunModel.renderPart("Zatvor", shader);
        scope.renderPart("VertMunt_Scope", shader);
        if (!KrogenitShaders.lightPass && !KrogenitShaders.gBufferPass && type != ItemRenderType.EQUIPPED) {
            if(!KrogenitShaders.interfacePass) {
                glEnable(GL_BLEND);
                GL11.glAlphaFunc(GL_GREATER, 0.0001f);
            }
            TextureLoaderDDS.bindTexture(resourceLocationStatefulCollimatorDiffuseEmission);
            TextureLoaderDDS.bindEmissionMap(resourceLocationStatefulCollimatorDiffuseEmission, shader, 1f);
            scope.renderPart("Collimator", shader);
            if(!KrogenitShaders.interfacePass) {
                glDisable(GL_BLEND);
                GL11.glAlphaFunc(GL_GREATER, 0.5f);
            }
            shader.setEmissionMapping(false);
        }
        shader.setNormalMapping(false);
        shader.setSpecularMapping(false);
        shader.setGlossMapping(false);
        glActiveTexture(GL_TEXTURE0);
        KrogenitShaders.finishCurrentShader();
    }

    @Override
    protected void renderGunPost(ItemRenderType type) {
        IPBR shader = KrogenitShaders.getCurrentPBRShader(false);

        if (!KrogenitShaders.lightPass) {
            glEnable(GL_BLEND);
            GL11.glAlphaFunc(GL_GREATER, 0.0001f);
            TextureLoaderDDS.bindTexture(resourceLocationStatefulCollimatorDiffuseEmission);
            TextureLoaderDDS.bindEmissionMap(resourceLocationStatefulCollimatorDiffuseEmission, shader, 1f);
            scope.renderPart("Collimator", shader);
            glDisable(GL_BLEND);
            GL11.glAlphaFunc(GL_GREATER, 0.5f);
            shader.setEmissionMapping(false);
        }

        TextureLoaderDDS.unbind();
        KrogenitShaders.finishCurrentShader();
    }

    @Override
    public void renderComponents(ItemStack item, ItemRenderType type) {

    }

    @Override
    public void createParticles(ItemStack item, ItemRenderType type, EntityPlayer p) {
        boolean shouldSpawnSmokde = true;

        Vector3f smokeVec;
        Vector3f sleeveSpawn;
        if (type == ItemRenderType.EQUIPPED || mc.gameSettings.thirdPersonView > 0) {
            smokeVec = new Vector3f(-0.3f+0.02f + (aim.x / 4f), -0.1f-0.08f, 0.4f + 0.08f);
            sleeveSpawn = new Vector3f(1.2f + 0.5f, aim.x/10f, -0.6f + aim.x/1.7f + 0.45f);
        }
        else {
            smokeVec = new Vector3f(-0.3f + (aim.x / 4f), -0.1f, 0.4f);
            sleeveSpawn = new Vector3f(1.2f, aim.x/10f, -0.6f + aim.x/1.7f);
        }

        super.createBasePaticles(item, type, sleeveSpawn, smokeVec, true, shouldSpawnSmokde, 10, p);
    }

    @Override
    public void updatePostShootAnimation() {
        isPlayPostShootAnim = false;
    }

    @Override
    public boolean isNeedPreAnimation() {
        return false;
    }
}
