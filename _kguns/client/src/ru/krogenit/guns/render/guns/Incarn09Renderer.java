package ru.krogenit.guns.render.guns;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.util.vector.Vector3f;
import ru.krogenit.dds_loader.TextureDDS;
import ru.krogenit.dds_loader.TextureLoaderDDS;
import ru.krogenit.guns.CoreGunsCommon;
import ru.krogenit.guns.item.ItemGun;
import ru.krogenit.guns.render.AbstractItemGunRenderer;
import ru.krogenit.model_loader.Model;
import ru.krogenit.shaders.KrogenitShaders;
import ru.krogenit.shaders.pbr.IPBR;

import static org.lwjgl.opengl.GL11.glTranslatef;
import static org.lwjgl.opengl.GL13.GL_TEXTURE0;
import static org.lwjgl.opengl.GL13.glActiveTexture;

public class Incarn09Renderer extends AbstractItemGunRenderer {

    private final TextureDDS resourceLocationStatefulLowerGunDiffuse;
    private final TextureDDS resourceLocationStatefulLowerGunNormal;
    private final TextureDDS resourceLocationStatefulLowerGunSpecular;
    private final TextureDDS resourceLocationStatefulLowerGunGlassMap;
    private final TextureDDS resourceLocationStatefulLowerGunEmission;
    private final TextureDDS resourceLocationStatefulUpperGunDiffuse;
    private final TextureDDS resourceLocationStatefulUpperGunNormal;
    private final TextureDDS resourceLocationStatefulUpperGunSpecular;
    private final TextureDDS resourceLocationStatefulUpperGunGlassMap;
    private final TextureDDS resourceLocationStatefulUpperGunEmission;

    public Incarn09Renderer(ItemGun gun) {
        super(new Model(new ResourceLocation(CoreGunsCommon.MODID, "models/" + gun.getName() + "/" + gun.getName() + ".obj")), gun);
        resourceLocationStatefulLowerGunDiffuse = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/incarn09/model_LowerGun_Diffuse.dds"));
        resourceLocationStatefulLowerGunNormal = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/incarn09/model_LowerGun_Normal.dds"));
        resourceLocationStatefulLowerGunSpecular = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/incarn09/model_LowerGun_Specular.dds"));
        resourceLocationStatefulLowerGunGlassMap = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/incarn09/model_LowerGun_Glossiness.dds"));
        resourceLocationStatefulLowerGunEmission = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/incarn09/LowerGun_emissive.dds"));
        resourceLocationStatefulUpperGunDiffuse = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/incarn09/model_UpperGun_Diffuse.dds"));
        resourceLocationStatefulUpperGunNormal = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/incarn09/model_UpperGun_Normal.dds"));
        resourceLocationStatefulUpperGunSpecular = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/incarn09/model_UpperGun_Specular.dds"));
        resourceLocationStatefulUpperGunGlassMap = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/incarn09/model_UpperGun_Glossiness.dds"));
        resourceLocationStatefulUpperGunEmission = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/incarn09/UpperGun_emissive.dds"));
    }

    @Override
    public void renderInModifyGui(ItemStack itemStack) {
        renderGun(ItemRenderType.INVENTORY);
        renderComponents(itemStack, ItemRenderType.INVENTORY);
    }

    @Override
    public void renderComponents(ItemStack item, ItemRenderType type) {

    }

    @Override
    protected void renderGun(ItemRenderType type) {
        IPBR shader = KrogenitShaders.getCurrentPBRShader(false);
        TextureLoaderDDS.bindTexture(resourceLocationStatefulLowerGunDiffuse);
        TextureLoaderDDS.bindNormalMap(resourceLocationStatefulLowerGunNormal, shader);
        TextureLoaderDDS.bindSpecularMap(resourceLocationStatefulLowerGunSpecular, shader);
        TextureLoaderDDS.bindGlossMap(resourceLocationStatefulLowerGunGlassMap, shader);
        TextureLoaderDDS.bindEmissionMap(resourceLocationStatefulLowerGunEmission, shader, 10f);
        gunModel.renderPart("Pistol1", shader);
        glActiveTexture(GL_TEXTURE0);
        TextureLoaderDDS.bindTexture(resourceLocationStatefulUpperGunDiffuse);
        TextureLoaderDDS.bindNormalMap(resourceLocationStatefulUpperGunNormal, shader);
        TextureLoaderDDS.bindSpecularMap(resourceLocationStatefulUpperGunSpecular, shader);
        TextureLoaderDDS.bindGlossMap(resourceLocationStatefulUpperGunGlassMap, shader);
        TextureLoaderDDS.bindEmissionMap(resourceLocationStatefulUpperGunEmission, shader, 10f);
        gunModel.renderPart("Pistol2", shader);
        gunModel.renderPart("UnderBarrel_low_meshId10_name", shader);
        glTranslatef(0, 0, -zatvor / 3f);
        gunModel.renderPart("Zatvor_mat2", shader);
        shader.setNormalMapping(false);
        shader.setSpecularMapping(false);
        shader.setGlossMapping(false);
        shader.setEmissionMapping(false);
        glActiveTexture(GL_TEXTURE0);
        KrogenitShaders.finishCurrentShader();
    }

    @Override
    public void createParticles(ItemStack item, ItemRenderType type, EntityPlayer p) {
        boolean shouldSpawnSmokde = true;

        Vector3f smokeVec;
        Vector3f sleeveSpawn;
        if (type == ItemRenderType.EQUIPPED || mc.gameSettings.thirdPersonView > 0) {
            smokeVec = new Vector3f(-0.3f+0.02f + (aim.x / 4f), -0.1f-0.08f, 0.4f + 0.08f);
            sleeveSpawn = new Vector3f(1.2f + 0.5f, aim.x/10f, -0.6f + aim.x/1.7f + 0.45f);
        }
        else {
            smokeVec = new Vector3f(-0.3f + (aim.x / 4f), -0.1f, 0.4f);
            sleeveSpawn = new Vector3f(1.2f, aim.x/10f, -0.6f + aim.x/1.7f);
        }

        super.createBasePaticles(item, type, sleeveSpawn, smokeVec, true, shouldSpawnSmokde, 10, p);
    }

    @Override
    public void updatePostShootAnimation() {
        isPlayPostShootAnim = false;
    }

    @Override
    public boolean isNeedPreAnimation() {
        return false;
    }
}