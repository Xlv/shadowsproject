package ru.krogenit.guns.render;

import lombok.Getter;
import lombok.Setter;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import ru.krogenit.guns.item.ItemGun;
import ru.krogenit.model_loader.Model;
import ru.krogenit.shaders.KrogenitShaders;
import ru.krogenit.shaders.pbr.IPBR;
import ru.krogenit.utils.AnimationHelper;
import ru.xlv.core.common.util.config.Configurable;
import ru.xlv.core.util.obf.IgnoreObf;

import static org.lwjgl.opengl.GL11.*;

public abstract class AbstractItemGunRenderer extends GunAnimationHelper {

    @Getter protected Model gunModel;
    protected boolean postRender;

    public AbstractItemGunRenderer(Model gunModel, ItemGun itemGun) {
        super(itemGun);
        this.gunModel = gunModel;
    }

    @Override
    public void init() {
        super.init();
        if(inHudPosition == null) inHudPosition = new Vector3fConfigurable(23, 7, -700);
        if(inHudRotation == null) inHudRotation = new Vector3fConfigurable(0f, 80, -176);
        if(inHudScale == null) inHudScale = new Vector3fConfigurable(5, 5, 5);
        if(flashRotationVector == null) flashRotationVector = new Vector3fConfigurable(0,0,0);
    }

    @Configurable(comment = "gunPosition") @IgnoreObf
    @Getter protected final Vector3fConfigurable gunPosition = new Vector3fConfigurable(0.0f, 0f, 1.75f);
    @Configurable(comment = "gunRotation") @IgnoreObf
    @Getter protected final Vector3fConfigurable gunRotation = new Vector3fConfigurable(0.0f, 0f, 0f);
    @Configurable(comment = "gunScale") @IgnoreObf
    @Getter protected final Vector3fConfigurable gunScale = new Vector3fConfigurable(1f, 1f, 1f);

    @Configurable(comment = "maxKnockAnimation") @IgnoreObf
    @Getter protected final Vector3fConfigurable maxKnockAnimation = new Vector3fConfigurable(0.333f, 0f, 0f);
    @Configurable(comment = "maxKnockRotation") @IgnoreObf
    @Getter protected final Vector3fConfigurable maxKnockRotation = new Vector3fConfigurable(1.5f, 0f, 0f);

    protected void renderFirstPerson(ItemStack itemStack) {
        prepareFirstPerson();

        glPushMatrix();
        {
            glTranslatef(0, 0, -knockback.x * maxKnockAnimation.x);

            glPushMatrix();
            {
                glTranslatef(gunPosition.x + getXWeights(2), gunPosition.y + getYWeights(2), gunPosition.z + getZWeights(2));
                glRotatef(gunRotation.x + getXWeights(3), 1, 0, 0);
                glRotatef(gunRotation.z + getZWeights(3), 0, 0, 1);
                glRotatef(gunRotation.y + getYWeights(3), 0, 1, 0);
                glPushMatrix();
                glScalef(gunScale.x, gunScale.y, gunScale.z);
                renderGun(ItemRenderType.EQUIPPED_FIRST_PERSON);
                glPopMatrix();
                renderFlash(itemStack);
            }
            glPopMatrix();

            renderLeftHand();
            renderRightHand();
        }
        glPopMatrix();
    }

    @Configurable(comment = "allPosition") @IgnoreObf
    @Getter protected final Vector3fConfigurable allPosition = new Vector3fConfigurable(0.0f, 0f, 0f);
    @Configurable(comment = "allRotation") @IgnoreObf
    @Getter protected final Vector3fConfigurable allRotation = new Vector3fConfigurable(0.0f, 0f, 0f);
    @Configurable(comment = "allScale") @IgnoreObf
    @Getter protected final Vector3fConfigurable allScale = new Vector3fConfigurable(1f, 1f, 1f);
    @Configurable(comment = "sprintingTranslate") @IgnoreObf
    @Getter protected final Vector3fConfigurable sprintingTranslate = new Vector3fConfigurable(-0.15f, 0.3f, -0.2f);
    @Configurable(comment = "sprintingRotation") @IgnoreObf
    @Getter protected final Vector3fConfigurable sprintingRotation = new Vector3fConfigurable(1f, 15f, -20f);

    @Configurable(comment = "aimPosition") @IgnoreObf
    @Getter protected final Vector3fConfigurable aimPosition = new Vector3fConfigurable(0.995f, 0.375f, 0.03f);
    @Configurable(comment = "aimRotation") @IgnoreObf
    @Getter protected final Vector3fConfigurable aimRotation = new Vector3fConfigurable(-0.5f, 3f, -0.6f);

    protected void prepareFirstPerson() {
        glRotatef(27f, 0, 0, 1);
        glRotatef(86f, 0, 1, 0);

        glTranslatef(allPosition.x + getXWeights(0), allPosition.y + getYWeights(0), allPosition.z + getZWeights(0));
        glScalef(allScale.x, allScale.y, allScale.z);
        glRotatef(allRotation.x - weaponUpByNearBlock*60f + aim.x * aimRotation.x - knockback.x * maxKnockRotation.x +  + sprintingAnim.x*sprintingRotation.x + getXWeights(1), 1, 0, 0);
        glRotatef(allRotation.z + aim.x * aimRotation.z + sprintingAnim.x*sprintingRotation.z + getZWeights(1), 0, 0, 1);
        glRotatef(allRotation.y + sprintingAnim.x*sprintingRotation.y + modifiAnim.x*90f + aim.x * aimRotation.y + getYWeights(1), 0, 1, 0);
        glTranslatef(
                aim.x * aimPosition.x + sprintingAnim.x*sprintingTranslate.x + modifiAnim.x*-1.5f,
                -weaponUpByNearBlock/2f + aim.x * aimPosition.y + sprintingAnim.y*sprintingTranslate.y + modifiAnim.y*0.4f,
                -weaponUpByNearBlock/2f + aim.x * aimPosition.z + sprintingAnim.z*sprintingTranslate.z + modifiAnim.z*-0.7f -knockback.x * maxKnockAnimation.x
        );
    }

    @Configurable(comment = "equippedPosition") @IgnoreObf
    @Getter protected final Vector3fConfigurable equippedPosition = new Vector3fConfigurable(0f, 0f, 0f);
    @Configurable(comment = "equippedRotation") @IgnoreObf
    @Getter protected final Vector3fConfigurable equippedRotation = new Vector3fConfigurable(0f, 0f, 0f);
    @Configurable(comment = "equippedScale") @IgnoreObf
    @Getter protected final Vector3fConfigurable equippedScale = new Vector3fConfigurable(1f, 1f, 1f);

    private void setupEquippedMatrix(OtherPlayerRenderInfo info) {
        glRotatef(17, 1, 0, 0);
        glRotatef(10, 0, 0, 1);
        glRotatef(18, 0, 1, 0);
        glTranslatef(equippedPosition.x, equippedPosition.y, equippedPosition.z);
        glRotatef(equippedRotation.x, 1, 0, 0);
        glRotatef(equippedRotation.z, 0, 0, 1);
        glRotatef(equippedRotation.y, 0, 1, 0);
        glTranslatef(0, 0, -info.knockback / 20f);
        glScalef(equippedScale.x, equippedScale.y, equippedScale.z);
    }

    protected void renderEquipped(ItemStack itemStack, OtherPlayerRenderInfo info) {
        setupEquippedMatrix(info);
        renderGun(ItemRenderType.EQUIPPED);
        renderComponents(itemStack, ItemRenderType.EQUIPPED);
    }

    protected void renderEquippedPost(ItemStack itemStack, OtherPlayerRenderInfo info) {
        if(postRender) {
            setupEquippedMatrix(info);
            renderGunPost(ItemRenderType.EQUIPPED);
        }

        renderFlash3rd(itemStack, info);
    }

    @Configurable(comment = "inventoryPosition") @IgnoreObf
    @Getter protected final Vector3fConfigurable inventoryPosition = new Vector3fConfigurable(6.3f, 7f, 0f);
    @Configurable(comment = "inventoryRotation") @IgnoreObf
    @Getter protected final Vector3fConfigurable inventoryRotation = new Vector3fConfigurable(0f, -90f, -220f);
    @Configurable(comment = "inventoryScale") @IgnoreObf
    @Getter protected final Vector3fConfigurable inventoryScale = new Vector3fConfigurable(1f, 1f, 1f);

    protected void renderInventory(ItemStack itemStack) {
        glTranslatef(inventoryPosition.x, inventoryPosition.y, inventoryPosition.z);
        glRotatef(inventoryRotation.x, 1, 0, 0);
        glRotatef(inventoryRotation.z, 0, 0, 1);
        glRotatef(inventoryRotation.y, 0, 1, 0);
        glScalef(-inventoryScale.x, inventoryScale.y, inventoryScale.z);
        renderGun(ItemRenderType.INVENTORY);
        renderComponents(itemStack, ItemRenderType.INVENTORY);
    }

    @Configurable(comment = "entityPosition") @IgnoreObf
    @Getter protected final Vector3fConfigurable entityPosition = new Vector3fConfigurable(0f, 0f, 0f);
    @Configurable(comment = "entityRotation") @IgnoreObf
    @Getter protected final Vector3fConfigurable entityRotation = new Vector3fConfigurable(0f, 0f, 0f);
    @Configurable(comment = "entityScale") @IgnoreObf
    @Getter protected final Vector3fConfigurable entityScale = new Vector3fConfigurable(1f, 1f, 1f);

    private void setupEntityMatrix() {
        glTranslatef(entityPosition.x, entityPosition.y, entityPosition.z);
        glRotatef(entityRotation.x, 1, 0, 0);
        glRotatef(entityRotation.z, 0, 0, 1);
        glRotatef(entityRotation.y, 0, 1, 0);
        glScalef(entityScale.x, entityScale.y, entityScale.z);
    }

    protected void renderAsEntity(ItemStack itemStack) {
        setupEntityMatrix();
        renderGun(ItemRenderType.ENTITY);
        renderComponents(itemStack, ItemRenderType.INVENTORY);
    }

    protected void renderAsEntityPost(ItemStack itemStack) {
        if(postRender) {
            setupEntityMatrix();
            renderGunPost(ItemRenderType.ENTITY);
        }
    }

    public abstract void renderInModifyGui(ItemStack itemStack);

    @Configurable(comment = "flash3rdPosition") @IgnoreObf
    @Getter protected final Vector3fConfigurable flash3rdPosition = new Vector3fConfigurable(0f, 0f, 0f);
    @Configurable(comment = "flash3rdRotation") @IgnoreObf
    @Getter protected final Vector3fConfigurable flash3rdRotation = new Vector3fConfigurable(0f, 0f, 0f);
    @Configurable(comment = "flash3rdScale") @IgnoreObf
    @Getter protected final Vector3fConfigurable flash3rdScale = new Vector3fConfigurable(1f, 1f, 1f);
    @Getter @Setter protected boolean showFlash;

    protected void renderFlash3rd(ItemStack itemStack, OtherPlayerRenderInfo info) {
        glPushMatrix();
        glTranslatef(flash3rdPosition.x, flash3rdPosition.y, flash3rdPosition.z);
        glScalef(flash3rdScale.x, flash3rdScale.y, flash3rdScale.z);
        glRotatef(flash3rdRotation.x, 1, 0, 0);
        glRotatef(flash3rdRotation.z, 0, 0, 1);
        glRotatef(flash3rdRotation.y, 0, 1, 0);
        renderFlash(showFlash ? 0.7f : info.knockback, info.flashRotation, itemStack);
        glPopMatrix();
    }

    @Configurable @IgnoreObf
    @Getter protected final Vector3fConfigurable flashPosition = new Vector3fConfigurable(-0.0f, 0.25f, 1.3f);
    @Configurable @IgnoreObf
    @Getter protected Vector3fConfigurable flashRotationVector = new Vector3fConfigurable(-0.0f, 0f, 0f);
    @Configurable @IgnoreObf
    @Getter protected final Vector3fConfigurable flashScale = new Vector3fConfigurable(0.1f, 0.1f, 1.0f);

    protected void renderFlash(ItemStack itemStack) {
        glPushMatrix();
        glTranslatef(flashPosition.x, flashPosition.y, flashPosition.z);
        glScalef(flashScale.x, flashScale.y, flashScale.z);
        glRotatef(flashRotationVector.x, 1, 0, 0);
        glRotatef(flashRotationVector.z, 0, 0, 1);
        glRotatef(flashRotationVector.y, 0, 1, 0);
        renderFlash(showFlash ? 0.7f : flashAnimation, flashRotation, itemStack);
        glPopMatrix();
    }

    @Configurable @IgnoreObf
    @Getter protected final Vector3fConfigurable leftHandPosition = new Vector3fConfigurable(0.3f, -0.2f, 1.3f);
    @Configurable @IgnoreObf
    @Getter protected final Vector3fConfigurable leftHandRotation = new Vector3fConfigurable(90F, 0f, 25);
    @Configurable @IgnoreObf
    @Getter protected final Vector3fConfigurable leftHandScale = new Vector3fConfigurable(1.5f, 2.5f, 1.5f);

    protected void renderLeftHand() {
        IPBR shader = KrogenitShaders.getCurrentPBRShader(true);
        glPushMatrix();
        glTranslatef(leftHandPosition.x + getXWeights(4), leftHandPosition.y + getYWeights(4), leftHandPosition.z + getZWeights(4));
        glScaled(leftHandScale.x, leftHandScale.y, leftHandScale.z);
        glRotatef(leftHandRotation.x + getXWeights(5), 1, 0, 0);
        glRotatef(leftHandRotation.z + getZWeights(5), 0, 0, 1);
        glRotatef(leftHandRotation.y + getYWeights(5), 0, 1, 0);
        hand.renderLeft();
        glPopMatrix();
        KrogenitShaders.finishCurrentShader();
    }

    @Configurable @IgnoreObf
    @Getter protected final Vector3fConfigurable rightHandPosition = new Vector3fConfigurable(-0.2f, -0.2f, 1f);
    @Configurable @IgnoreObf
    @Getter protected final Vector3fConfigurable rightHandRotation = new Vector3fConfigurable(80f, 0f, -5f);
    @Configurable @IgnoreObf
    @Getter protected final Vector3fConfigurable rightHandScale = new Vector3fConfigurable(1.5f, 2.5f, 1.5f);

    protected void renderRightHand() {
        IPBR shader = KrogenitShaders.getCurrentPBRShader(true);
        glPushMatrix();
        glTranslatef(rightHandPosition.x + getXWeights(6), rightHandPosition.y + getYWeights(6), rightHandPosition.z + getZWeights(6));
        glScaled(rightHandScale.x, rightHandScale.y, rightHandScale.z);
        glRotatef(rightHandRotation.x + getXWeights(7), 1, 0, 0);
        glRotatef(rightHandRotation.z + getZWeights(7), 0, 0, 1);
        glRotatef(rightHandRotation.y + getYWeights(7), 0, 1, 0);
        hand.renderRight();
        glPopMatrix();
        KrogenitShaders.finishCurrentShader();
    }

    protected abstract void renderGun(ItemRenderType type);
    protected void renderGunPost(ItemRenderType type) {}
    protected abstract void renderComponents(ItemStack itemStack, ItemRenderType type);

    private boolean shouldUpdateAnimation(ItemRenderType type, ItemStack item) {
        return mc.thePlayer.getCurrentEquippedItem() == item && (type == ItemRenderType.EQUIPPED_FIRST_PERSON ||
                (type == ItemRenderType.EQUIPPED && mc.currentScreen == null));
    }

    public void renderItem(ItemRenderType type, ItemStack itemStack, Object... data) {
        if(!KrogenitShaders.lightPass) {
            if (itemStack.getTagCompound() == null) itemStack.setTagCompound(new NBTTagCompound());
            animSpeed = AnimationHelper.getAnimationSpeed();

            if (shouldUpdateAnimation(type, itemStack)) {
                updateBase(itemStack, type);
                updateKnockback();
                updatePredAnimation(itemStack);
//				 playAnimation(aim, 0.05f, 1, 1, AnimationType.SlowEnd);
//				playAnimation(sprintingAnim, 0.05f, 0.05f,  0.05f, AnimationType.SlowEnd);
//                forcePlayAim = false;
                updateAim();
//				 isPlayViewAnim = true;

                setUnloadReloadSpeed();
                updateReloadAnimation(itemStack);
                updatePostShootAnimation();
            }
            if (type == ItemRenderType.EQUIPPED && data[1] instanceof EntityPlayer)
                updateOtherFlashLights((EntityPlayer) data[1]);
        }

        switch (type) {
            case EQUIPPED_FIRST_PERSON:
                renderFirstPerson(itemStack);
                return;
            case EQUIPPED:
                renderEquipped(itemStack, getOtherPlayerInfo(data[1]));
                return;
            case INVENTORY:
                renderInventory(itemStack);
                return;
            case ENTITY:
                renderAsEntity(itemStack);
        }
    }

    @Override
    public void renderItemPost(ItemRenderType type, ItemStack itemStack, Object... data) {
        if(!KrogenitShaders.lightPass) {
            switch (type) {
                case EQUIPPED:
                    renderEquippedPost(itemStack, getOtherPlayerInfo(data[1]));
                    break;
                case ENTITY:
                    renderAsEntityPost(itemStack);
                    break;
            }
        }
    }

    @Configurable @IgnoreObf
    @Getter protected Vector3fConfigurable inHudPosition = new Vector3fConfigurable(23f, 7f, -700f);
    @Configurable @IgnoreObf
    @Getter protected Vector3fConfigurable inHudRotation = new Vector3fConfigurable(0f, 80f, -176f);
    @Configurable @IgnoreObf
    @Getter protected Vector3fConfigurable inHudScale = new Vector3fConfigurable(5f, 5f, 5f);

    public void renderGunInHud(IPBR shader) {
        glPushMatrix();
        glTranslatef(inHudPosition.x, inHudPosition.y, inHudPosition.z);
        glScaled(inHudScale.x, inHudScale.y, inHudScale.z);
        glRotatef(inHudRotation.x, 1, 0, 0);
        glRotatef(inHudRotation.z, 0, 0, 1);
        glRotatef(inHudRotation.y, 0, 1, 0);
        renderGun(ItemRenderType.INVENTORY);
        glPopMatrix();
    }
}
