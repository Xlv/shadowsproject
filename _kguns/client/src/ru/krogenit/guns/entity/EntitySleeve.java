package ru.krogenit.guns.entity;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.projectile.EntityThrowable;
import net.minecraft.init.Blocks;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MathHelper;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;
import org.lwjgl.util.vector.Vector3f;

import java.util.List;

public class EntitySleeve extends EntityThrowable {

    public float xRotation = 0.0F;
    public float yRotation = 0.0F;
    public float zRotation = 0.0F;
    public float xRotationSpeed;
    public float yRotationSpeed;
    public float zRotationSpeed;
    protected boolean collided = false;
    protected double prevY;
    public boolean renderOnGround;

    int sleeveType;

    public EntitySleeve(World w, EntityLivingBase shooter, boolean isShooterPlayer, Vector3f addPos, int type, float addRotY) {
        super(w);
        this.sleeveType = type;

        this.setLocationAndAngles(shooter.posX, shooter.posY + (double) shooter.getEyeHeight() - (isShooterPlayer ? 0.3D : 0.1D), shooter.posZ, shooter.rotationYawHead + addRotY, shooter.rotationPitch);

        double lookX = -MathHelper.sin(this.rotationYaw / 180.0F * (float) Math.PI) * MathHelper.cos(this.rotationPitch / 180.0F * (float) Math.PI);
        double lookY = -MathHelper.sin(this.rotationPitch / 180.0F * (float) Math.PI);
        double lookZ = MathHelper.cos(this.rotationYaw / 180.0F * (float) Math.PI) * MathHelper.cos(this.rotationPitch / 180.0F * (float) Math.PI);
        double ySpeed = MathHelper.sin((this.rotationPitch + 90.0F) / 180.0F * (float) Math.PI);
        Vec3 look = Vec3.createVectorHelper(lookX, lookY, lookZ).normalize();
        this.posX += look.xCoord * 0.4D * addPos.x;
        this.posY += look.yCoord * 0.4D * addPos.x;
        this.posZ += look.zCoord * 0.4D * addPos.x;

        this.posX += MathHelper.cos(this.rotationYaw / 180.0F * (float) Math.PI) * addPos.z;
        this.posZ += MathHelper.sin(this.rotationYaw / 180.0F * (float) Math.PI) * addPos.z;
        this.posY += addPos.y;

        prevY = 0.3D;
        look.rotateAroundY((float) Math.toRadians(-90.0D));
        this.motionY = ySpeed * 0.25D;
        this.motionX = look.xCoord * 0.15D;
        this.motionZ = look.zCoord * 0.15D;
        this.motionY *= 1.0D + (Math.random() - 0.5D) / 2.0D;
        this.motionX *= 1.0D + (Math.random() - 0.5D) / 2.0D;
        this.motionZ *= 1.0D + (Math.random() - 0.5D) / 2.0D;
        this.posX += this.motionX / 2.0D;
        this.posZ += this.motionZ / 2.0D;
        this.xRotationSpeed = ((float) Math.random() - 0.5F) * 20.0F;
        this.yRotationSpeed = (float) Math.random() * 20.0F + 20.0F;
        this.zRotationSpeed = ((float) Math.random() - 0.5F) * 20.0F;
        this.setSize(0.05F, 0.05F);
        this.height = 0.05F;
    }

    /**
     * Gets the amount of gravity to apply to the thrown entity with each tick.
     */
    protected float getGravityVelocity() {
        return 0.07F;
    }

    /**
     * Called to update the entity's position/logic.
     */
    public void onUpdate() {
        this.lastTickPosX = this.posX;
        this.lastTickPosY = this.posY;
        this.lastTickPosZ = this.posZ;
        super.onEntityUpdate();

        if (this.throwableShake > 0) {
            --this.throwableShake;
        }

        if (this.inGround) {
            if (this.worldObj.getBlock(this.field_145788_c, this.field_145786_d, this.field_145787_e) == this.field_145785_f) {
                ++this.ticksInGround;

                if (this.ticksInGround == 1200) {
                    this.setDead();
                }

                return;
            }

            this.inGround = false;
            this.motionX *= this.rand.nextFloat() * 0.2F;
            this.motionY *= this.rand.nextFloat() * 0.2F;
            this.motionZ *= this.rand.nextFloat() * 0.2F;
            this.ticksInGround = 0;
            this.ticksInAir = 0;
        } else {
            ++this.ticksInAir;
        }

        Vec3 vec3 = Vec3.createVectorHelper(this.posX, this.posY, this.posZ);
        Vec3 vec31 = Vec3.createVectorHelper(this.posX + this.motionX, this.posY + this.motionY, this.posZ + this.motionZ);
        MovingObjectPosition movingobjectposition = this.worldObj.rayTraceBlocks(vec3, vec31);
        vec3 = Vec3.createVectorHelper(this.posX, this.posY, this.posZ);
        vec31 = Vec3.createVectorHelper(this.posX + this.motionX, this.posY + this.motionY, this.posZ + this.motionZ);

        if (movingobjectposition != null) {
            vec31 = Vec3.createVectorHelper(movingobjectposition.hitVec.xCoord, movingobjectposition.hitVec.yCoord, movingobjectposition.hitVec.zCoord);
        }

        if (!this.worldObj.isRemote) {
            Entity entity = null;
            List list = this.worldObj.getEntitiesWithinAABBExcludingEntity(this, this.boundingBox.addCoord(this.motionX, this.motionY, this.motionZ).expand(1.0D, 1.0D, 1.0D));
            double d0 = 0.0D;
            EntityLivingBase entitylivingbase = this.getThrower();

            for (Object o : list) {
                Entity entity1 = (Entity) o;

                if (entity1.canBeCollidedWith() && (entity1 != entitylivingbase || this.ticksInAir >= 5)) {
                    float f = 0.3F;
                    AxisAlignedBB axisalignedbb = entity1.boundingBox.expand((double) f, (double) f, (double) f);
                    MovingObjectPosition movingobjectposition1 = axisalignedbb.calculateIntercept(vec3, vec31);

                    if (movingobjectposition1 != null) {
                        double d1 = vec3.distanceTo(movingobjectposition1.hitVec);

                        if (d1 < d0 || d0 == 0.0D) {
                            entity = entity1;
                            d0 = d1;
                        }
                    }
                }
            }

            if (entity != null) {
                movingobjectposition = new MovingObjectPosition(entity);
            }
        }

        if (movingobjectposition != null) {
            if (movingobjectposition.typeOfHit == MovingObjectPosition.MovingObjectType.BLOCK && this.worldObj.getBlock(movingobjectposition.blockX, movingobjectposition.blockY, movingobjectposition.blockZ) == Blocks.portal) {
                this.setInPortal();
            } else {
                this.onImpact(movingobjectposition);
            }
        }

        this.posX += this.motionX;
        this.posY += this.motionY;
        this.posZ += this.motionZ;
        float f1 = MathHelper.sqrt_double(this.motionX * this.motionX + this.motionZ * this.motionZ);
        this.rotationYaw = (float) (Math.atan2(this.motionX, this.motionZ) * 180.0D / Math.PI);

        for (this.rotationPitch = (float) (Math.atan2(this.motionY, f1) * 180.0D / Math.PI); this.rotationPitch - this.prevRotationPitch < -180.0F; this.prevRotationPitch -= 360.0F) {
            ;
        }

        while (this.rotationPitch - this.prevRotationPitch >= 180.0F) {
            this.prevRotationPitch += 360.0F;
        }

        while (this.rotationYaw - this.prevRotationYaw < -180.0F) {
            this.prevRotationYaw -= 360.0F;
        }

        while (this.rotationYaw - this.prevRotationYaw >= 180.0F) {
            this.prevRotationYaw += 360.0F;
        }

        this.rotationPitch = this.prevRotationPitch + (this.rotationPitch - this.prevRotationPitch) * 0.2F;
        this.rotationYaw = this.prevRotationYaw + (this.rotationYaw - this.prevRotationYaw) * 0.2F;
        float f2 = 0.99F;
        float f3 = this.getGravityVelocity();

        if (this.isInWater()) {
            for (int i = 0; i < 4; ++i) {
                float f4 = 0.25F;
                this.worldObj.spawnParticle("bubble", this.posX - this.motionX * (double) f4, this.posY - this.motionY * (double) f4, this.posZ - this.motionZ * (double) f4, this.motionX, this.motionY, this.motionZ);
            }

            f2 = 0.8F;
        }
        if (prevY >= 0.1f) {
            this.motionX *= f2;
            this.motionY *= f2;
            this.motionZ *= f2;
            this.motionY -= f3;
        } else {
            motionX = motionY = motionZ = 0D;
            this.posY = (int) posY;
        }
        this.setPosition(this.posX, this.posY, this.posZ);

        float rotationFactor = prevY > 0.1f ? 0.999F : 0.92f;
        this.xRotationSpeed *= rotationFactor;
        this.yRotationSpeed *= rotationFactor;
        this.zRotationSpeed *= rotationFactor;
        this.xRotation = (this.xRotation + this.xRotationSpeed) % 360.0F;
        this.yRotation = (this.yRotation + this.yRotationSpeed) % 360.0F;
        this.zRotation = (this.zRotation + this.zRotationSpeed) % 360.0F;

        float speedRot = 50f;
        if (prevY <= 0.1f) {
            if (xRotation > 0) {
                xRotation -= speedRot;
                if (xRotation < 0) xRotation = 0;
            } else if (xRotation < 0) {
                xRotation += speedRot;
                if (xRotation > 0) xRotation = 0;
            }
            if (zRotation > 0) {
                zRotation -= speedRot;
                if (zRotation < 0) zRotation = 0;
            } else if (zRotation < 0) {
                zRotation += speedRot;
                if (zRotation > 0) zRotation = 0;
            }
        }

        if (this.ticksExisted > 250) {
            this.setDead();
        }
    }

    /**
     * Called when this EntityThrowable hits a block or entity.
     */
    protected void onImpact(MovingObjectPosition mop) {
        this.hitEntity(mop.entityHit);
    }

    protected void hitEntity(Entity entity) {
        this.motionX *= -prevY;
        this.motionY *= -prevY;
        this.motionZ *= -prevY;
        this.xRotationSpeed *= 0.5F;
        this.yRotationSpeed *= 0.5F;
        this.zRotationSpeed *= 0.5F;
        prevY -= 0.05f;
    }

    protected void pushOff(int blockX, int blockY, int blockZ, int side) {
        boolean setY = Math.abs(this.motionY) > 0.20000000298023224D || side == 0;

        if (side != 0 && side != 1) {
            if (side != 2 && side != 3) {
                this.motionX *= -0.5D;
                this.motionY *= 0.800000011920929D;
                this.motionZ *= 0.5D;
            } else {
                this.motionX *= 0.5D;
                this.motionY *= 0.800000011920929D;
                this.motionZ *= -0.5D;
            }
        } else if (setY) {
            this.motionX *= 0.800000011920929D;
            this.motionY *= -0.5D;
            this.motionZ *= 0.800000011920929D;
            this.renderOnGround = false;
        } else {
            this.motionX *= 0.800000011920929D;
            this.motionY = 0.0D;
            this.motionZ *= 0.800000011920929D;
            this.xRotationSpeed = 0.0F;
            this.zRotationSpeed = 0.0F;
            this.xRotation = 0.0F;
            this.zRotation = 0.0F;
            this.renderOnGround = true;
        }

        if (!setY) {
            this.xRotationSpeed *= 0.5F;
            this.yRotationSpeed *= 0.5F;
            this.zRotationSpeed *= 0.5F;
        } else {
            this.xRotationSpeed = ((float) Math.random() - 0.5F) * 10.0F;
            this.yRotationSpeed = (float) Math.random() * 10.0F + 10.0F;
            this.zRotationSpeed = ((float) Math.random() - 0.5F) * 10.0F;
        }

        this.calculateNewImpact();
    }

    protected void calculateNewImpact() {
        Vec3 vec3 = Vec3.createVectorHelper(this.posX, this.posY, this.posZ);
        Vec3 vec31 = Vec3.createVectorHelper(this.posX + this.motionX, this.posY + this.motionY, this.posZ + this.motionZ);
        MovingObjectPosition movingobjectposition = this.worldObj.rayTraceBlocks(vec3, vec31);
        vec3 = Vec3.createVectorHelper(this.posX, this.posY, this.posZ);
        vec31 = Vec3.createVectorHelper(this.posX + this.motionX, this.posY + this.motionY, this.posZ + this.motionZ);

        if (movingobjectposition != null) {
            vec31 = Vec3.createVectorHelper(movingobjectposition.hitVec.xCoord, movingobjectposition.hitVec.yCoord, movingobjectposition.hitVec.zCoord);
        }

        if (!this.worldObj.isRemote) {
            Entity entity = null;
            List list = this.worldObj.getEntitiesWithinAABBExcludingEntity(this, this.boundingBox.addCoord(this.motionX, this.motionY, this.motionZ).expand(1.0D, 1.0D, 1.0D));
            double d0 = 0.0D;
            EntityLivingBase entityliving = this.getThrower();

            for (Object o : list) {
                Entity entity1 = (Entity) o;

                if (entity1.canBeCollidedWith() && (entity1 != entityliving || this.ticksExisted >= 5)) {
                    float f = 0.3F;
                    AxisAlignedBB axisalignedbb = entity1.boundingBox.expand(f, f, f);
                    MovingObjectPosition movingobjectposition1 = axisalignedbb.calculateIntercept(vec3, vec31);

                    if (movingobjectposition1 != null) {
                        double d1 = vec3.distanceTo(movingobjectposition1.hitVec);

                        if (d1 < d0 || d0 == 0.0D) {
                            entity = entity1;
                            d0 = d1;
                        }
                    }
                }
            }

            if (entity != null) {
                movingobjectposition = new MovingObjectPosition(entity);
            }
        }

        if (movingobjectposition != null) {
            this.onImpact(movingobjectposition);
        }
    }

    @SideOnly(Side.CLIENT)
    public void setPositionAndRotation2(double par1, double par3, double par5, float par7, float par8, int par9) {
    }

    public int getSleeveType() {
        return sleeveType;
    }
}
