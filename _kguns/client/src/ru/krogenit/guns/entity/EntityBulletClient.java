package ru.krogenit.guns.entity;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MathHelper;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;
import org.lwjgl.util.vector.Vector4f;
import ru.krogenit.guns.network.packet.PacketDamageClient;
import ru.xlv.core.XlvsCore;

import java.util.List;

public class EntityBulletClient extends EntityBullet {

    public float renderLifeTime = 0;

    public EntityBulletClient(World par1World, EntityPlayer p, float damage, float accuracy, Vector4f multiply) {
        super(par1World, p, damage, accuracy, multiply);
    }

    public EntityBulletClient(World par1World, double par2, double par4, double par6) {
        super(par1World, par2, par4, par6);
    }

    public EntityBulletClient(World par1World) {
        super(par1World);
    }

    @SideOnly(Side.CLIENT)
    public boolean isInRangeToRenderDist(double par1) {
        double d1 = this.boundingBox.getAverageEdgeLength() * 4.0D;
        d1 *= 64.0D;
        return par1 < d1 * d1;
    }

    @SideOnly(Side.CLIENT)
    public void setVelocity(double par1, double par3, double par5) {
        this.motionX = par1;
        this.motionY = par3;
        this.motionZ = par5;

        if (this.prevRotationPitch == 0.0F && this.prevRotationYaw == 0.0F) {
            float f = MathHelper.sqrt_double(par1 * par1 + par5 * par5);
            this.prevRotationYaw = this.rotationYaw = (float) (Math.atan2(par1, par5) * 180.0D / Math.PI);
            this.prevRotationPitch = this.rotationPitch = (float) (Math.atan2(par3, f) * 180.0D / Math.PI);
        }
    }

    @Override
    public void onUpdate() {
        super.onUpdate();

        if (this.ticksInAir > 1000) {
            this.setDead();
        }

        if (this.prevRotationPitch == 0.0F && this.prevRotationYaw == 0.0F) {
            float var1 = MathHelper.sqrt_double(this.motionX * this.motionX + this.motionZ * this.motionZ);
            this.prevRotationYaw = this.rotationYaw = (float) (Math.atan2(this.motionX, this.motionZ) * 180.0D / Math.PI);
            this.prevRotationPitch = this.rotationPitch = (float) (Math.atan2(this.motionY, var1) * 180.0D / Math.PI);
        }

        if (this.posY <= 0 || this.posY >= 256) {
            this.setDead();
        }

        if (this.throwableShake > 0) {
            --this.throwableShake;
        }

        if (this.inGround) {
            this.inGround = false;
            this.motionX *= this.rand.nextFloat() * 0.2F;
            this.motionY *= this.rand.nextFloat() * 0.2F;
            this.motionZ *= this.rand.nextFloat() * 0.2F;
            this.ticksInAir = 0;
        } else {
            ++this.ticksInAir;
        }

        Vec3 var16 = Vec3.createVectorHelper(this.posX, this.posY, this.posZ);
        Vec3 var2 = Vec3.createVectorHelper(this.posX + this.motionX, this.posY + this.motionY, this.posZ + this.motionZ);
        bulletPas = true;
        MovingObjectPosition var3 = this.worldObj.rayTraceBlocks_do_do(var16, var2, false, true, false);
        bulletPas = false;
        var16 = Vec3.createVectorHelper(this.posX, this.posY, this.posZ);
        var2 = Vec3.createVectorHelper(this.posX + this.motionX, this.posY + this.motionY, this.posZ + this.motionZ);
        Vec3 hitVec = null;
        int hitPart = 0;

        if (var3 != null) {
            var2 = Vec3.createVectorHelper(var3.hitVec.xCoord, var3.hitVec.yCoord, var3.hitVec.zCoord);
        }

        Entity var4 = null;
        List<?> var5 = this.worldObj.getEntitiesWithinAABBExcludingEntity(this, this.boundingBox.addCoord(this.motionX, this.motionY, this.motionZ).expand(1.0D, 1.0D, 1.0D));
        double var6 = 0.0D;
        float var10;

        for (Object o : var5) {
            Entity entity = (Entity) o;

            if (entity != this.thrower) {
                if (entity.canBeCollidedWith()) {
                    var10 = 0.3F;
                    AxisAlignedBB var11 = entity.boundingBox.expand(var10, var10, var10);
                    MovingObjectPosition var12 = var11.calculateIntercept(var16, var2);
                    if (var12 != null) {
                        double var13 = var16.distanceTo(var12.hitVec);

                        if (var13 < var6 || var6 == 0.0D) {
                            var4 = entity;
                            var6 = var13;
                        }
                    }
                }
            }
        }

        if (var4 != null) {
            var3 = new MovingObjectPosition(var4);
        }

        if (var3 != null) {
            if (var3.entityHit != null) {
                float damage = this.damage;
                switch (hitPart) {
                    case 0:
                    case 2:
                        damage *= damageMultiply.x;
                        break;
                    case 4:
                    case 6:
                        damage *= damageMultiply.y;
                        break;
                    case 8:
                        damage *= damageMultiply.z;
                        break;
                    case 10:
                        damage *= damageMultiply.w;
                        break;
                }

                XlvsCore.INSTANCE.getPacketHandler().sendPacketToServer(new PacketDamageClient(var3.entityHit.getEntityId(), damage));

                if (hitVec != null) {
                    if (hitPart == 10) {
                        // headshot
                    }

                    this.worldObj.spawnParticle("reddust", hitVec.xCoord, hitVec.yCoord, hitVec.zCoord, 0.0D, 0.0D, 0.0D);
                    this.worldObj.spawnParticle("reddust", hitVec.xCoord, hitVec.yCoord, hitVec.zCoord, 0.0D, 0.0D, 0.0D);
                    this.worldObj.spawnParticle("reddust", hitVec.xCoord, hitVec.yCoord, hitVec.zCoord, 0.0D, 0.0D, 0.0D);
                }

                this.setDead();
            } else {
                for (int i = 0; i < 8; ++i)
                    this.worldObj.spawnParticle("smoke", var3.hitVec.xCoord, var3.hitVec.yCoord, var3.hitVec.zCoord, 0.0D, 0.0D, 0.0D);

                Block b1 = worldObj.getBlock(var3.blockX, var3.blockY, var3.blockZ);

                if (var3.hitInfo == null) {
                    worldObj.spawnEntityInWorld(new EntityHole(worldObj, var3));
                } else {
                    if (var3.hitInfo instanceof Block) {
                        Block b = (Block) var3.hitInfo;
                        if (b != Blocks.wooden_door && b != Blocks.iron_door) {
                            worldObj.spawnEntityInWorld(new EntityHole(worldObj, var3));
                        }
                    }
                }

                this.setDead();
            }
        }

        this.posX += this.motionX;
        this.posY += this.motionY;
        this.posZ += this.motionZ;
        float f1 = MathHelper.sqrt_double(this.motionX * this.motionX + this.motionZ * this.motionZ);
        this.rotationYaw = (float) (Math.atan2(this.motionX, this.motionZ) * 180.0D / Math.PI);

        for (this.rotationPitch = (float) (Math.atan2(this.motionY, f1) * 180.0D / Math.PI); this.rotationPitch - this.prevRotationPitch < -180.0F; this.prevRotationPitch -= 360.0F) {
            ;
        }

        while (this.rotationPitch - this.prevRotationPitch >= 180.0F) {
            this.prevRotationPitch += 360.0F;
        }

        while (this.rotationYaw - this.prevRotationYaw < -180.0F) {
            this.prevRotationYaw -= 360.0F;
        }

        while (this.rotationYaw - this.prevRotationYaw >= 180.0F) {
            this.prevRotationYaw += 360.0F;
        }

        this.rotationPitch = this.prevRotationPitch + (this.rotationPitch - this.prevRotationPitch) * 0.2F;
        this.rotationYaw = this.prevRotationYaw + (this.rotationYaw - this.prevRotationYaw) * 0.2F;
        float f2 = 0.99F;

        if (this.isInWater()) {
            for (int k = 0; k < 4; ++k) {
                float f4 = 0.25F;
                this.worldObj.spawnParticle("bubble", this.posX - this.motionX * (double) f4, this.posY - this.motionY * (double) f4, this.posZ - this.motionZ * (double) f4, this.motionX, this.motionY, this.motionZ);
            }

            f2 = 0.8F;
        }

        this.motionX *= f2;
        this.motionY *= f2;
        this.motionZ *= f2;
        this.setPosition(this.posX, this.posY, this.posZ);
    }

    @SideOnly(Side.CLIENT)
    public float getShadowSize() {
        return 0.0F;
    }
}
