package ru.krogenit.guns.item;

import net.minecraft.client.Minecraft;

public class SoundHelperGuns {
    
    private static Minecraft mc = Minecraft.getMinecraft();

    public static float getRandomPitch() {
        return 1.0f + ((mc.theWorld.rand.nextFloat() - 0.5f) / 4f);
    }
}
