package ru.krogenit.guns.item;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.IItemRenderer;
import net.minecraftforge.client.MinecraftForgeClient;
import ru.krogenit.guns.CoreGunsClient;
import ru.krogenit.guns.render.AbstractMeleeWeaponRenderer;

public class ItemMeleeWeaponClient extends ItemMeleeWeapon {
    public ItemMeleeWeaponClient(String name, float damage, int durability) {
        super(name, damage, durability);
        setCreativeTab(CoreGunsClient.tabGuns);
    }

    @SideOnly(Side.CLIENT)
    public boolean isFull3D() {
        return true;
    }

    @Override
    public void registerIcons(IIconRegister par1IconRegister) {

    }

    @Override
    public boolean onEntitySwing(EntityLivingBase entityLiving, ItemStack stack) {
        Item item = stack.getItem();
        if (item instanceof ItemMeleeWeaponClient) {
            AbstractMeleeWeaponRenderer itemRenderer = (AbstractMeleeWeaponRenderer) MinecraftForgeClient.getItemRenderer(stack, IItemRenderer.ItemRenderType.EQUIPPED_FIRST_PERSON);
            if(itemRenderer.canHit()) {
                itemRenderer.hit();
            }
        }

        return super.onEntitySwing(entityLiving, stack);
    }
}
