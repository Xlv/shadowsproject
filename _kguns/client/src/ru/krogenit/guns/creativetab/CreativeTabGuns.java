package ru.krogenit.guns.creativetab;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import ru.krogenit.guns.CoreGunsCommon;

public class CreativeTabGuns extends CreativeTabs {

    public CreativeTabGuns(int id, String lable) {
        super(id, lable);
    }

    @Override
    public Item getTabIconItem() {
        return CoreGunsCommon.incarn09;
    }
}
