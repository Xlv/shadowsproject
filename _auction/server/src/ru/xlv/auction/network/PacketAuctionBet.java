package ru.xlv.auction.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.auction.XlvsAuctionMod;
import ru.xlv.auction.handle.lot.result.AuctionBetResult;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.network.IPacketCallbackOnServer;
import ru.xlv.core.common.network.IPacketInOnServer;
import ru.xlv.core.common.network.IPacketOutServer;
import ru.xlv.core.common.network.PacketCallbackSender;
import ru.xlv.core.common.util.ByteBufInputStream;

import java.io.IOException;

@NoArgsConstructor
public class PacketAuctionBet implements IPacketCallbackOnServer {

    private AuctionBetResult auctionBetResult;

    @Override
    public void read(EntityPlayer entityPlayer, ByteBufInputStream bbis, PacketCallbackSender packetCallbackSender) throws IOException {
        int amount = bbis.readInt();
        String uuid = bbis.readUTF();
        XlvsAuctionMod.INSTANCE.getAuctionHandler().makeBet(entityPlayer.getCommandSenderName(), amount, uuid).thenAccept(auctionBetResult -> {
            this.auctionBetResult = auctionBetResult;
            packetCallbackSender.send();
        });
    }

    @Override
    public void write(EntityPlayer entityPlayer, ByteBufOutputStream bbos) throws IOException {
        bbos.writeUTF(auctionBetResult.getResponseMessage());
    }

    @Override
    public boolean handleCallback() {
        return true;
    }
}
