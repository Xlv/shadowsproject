package ru.xlv.auction.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.auction.XlvsAuctionMod;
import ru.xlv.auction.handle.lot.TermType;
import ru.xlv.auction.handle.lot.result.AuctionPlaceResult;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.network.IPacketCallbackOnServer;
import ru.xlv.core.common.network.IPacketInOnServer;
import ru.xlv.core.common.network.IPacketOutServer;
import ru.xlv.core.common.network.PacketCallbackSender;
import ru.xlv.core.common.util.ByteBufInputStream;

import java.io.IOException;

@NoArgsConstructor
public class PacketAuctionPlace implements IPacketCallbackOnServer {

    private AuctionPlaceResult auctionPlaceResult;

    @Override
    public void read(EntityPlayer entityPlayer, ByteBufInputStream bbis, PacketCallbackSender packetCallbackSender) throws IOException {
        int matrixX = bbis.readInt();
        int matrixY = bbis.readInt();
        int amount = bbis.readInt();
        int termType0 = bbis.readInt();
        if(termType0 < 0 || termType0 >= TermType.values().length) return;
        TermType termType = TermType.values()[termType0];
        XlvsAuctionMod.INSTANCE.getAuctionHandler().placeLot(entityPlayer.getCommandSenderName(), matrixX, matrixY, amount, termType).thenAccept(auctionPlaceResult -> {
            this.auctionPlaceResult = auctionPlaceResult;
            packetCallbackSender.send();
        });
    }

    @Override
    public void write(EntityPlayer entityPlayer, ByteBufOutputStream bbos) throws IOException {
        bbos.writeUTF(auctionPlaceResult.getResponseMessage());
    }

    @Override
    public boolean handleCallback() {
        return true;
    }
}
