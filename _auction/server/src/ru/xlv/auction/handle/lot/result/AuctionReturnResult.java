package ru.xlv.auction.handle.lot.result;

public enum AuctionReturnResult {

    SUCCESS,
    UNKNOWN_ERROR,
//    NO_INV_SPACE,
    PLAYER_NOT_FOUND

}
