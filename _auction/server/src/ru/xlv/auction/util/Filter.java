//package ru.xlv.auction.common.util;
//
//import ru.xlv.auction.handle.lot.AuctionLot;
//
//import java.util.Comparator;
//
//public enum Filter {
//
//    INEXPENSIVE(AuctionLocalization.SORT_INEXPENSIVE, Comparator.comparingInt(AuctionLot::getStartCost)),
//    EXPENSIVE(AuctionLocalization.SORT_EXPENSIVE, Comparator.comparingInt(AuctionLot::getStartCost).reversed()),
//    NEWEST(AuctionLocalization.SORT_NEWEST, Comparator.comparingLong(AuctionLot::getTimestamp).reversed()),
//    OLDEST(AuctionLocalization.SORT_OLDEST, Comparator.comparingLong(AuctionLot::getTimestamp)),
//    DEFAULT(null, (a, b) -> 0);
//
//    private final String displayName;
//    private final Comparator<AuctionLot> comparator;
//
//    Filter(String displayName, Comparator<AuctionLot> comparator) {
//        this.displayName = displayName;
//        this.comparator = comparator;
//    }
//
//    public String getDisplayName() {
//        return displayName;
//    }
//
//    public Comparator<AuctionLot> getComparator() {
//        return comparator;
//    }
//}
