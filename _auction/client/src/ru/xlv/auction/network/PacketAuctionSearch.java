package ru.xlv.auction.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.xlv.auction.common.lot.SimpleAuctionLot;
import ru.xlv.auction.common.util.AuctionUtils;
import ru.xlv.core.common.network.IPacketCallbackEffective;
import ru.xlv.core.common.network.IPacketIn;
import ru.xlv.core.common.network.IPacketOut;
import ru.xlv.core.common.util.ByteBufInputStream;

import javax.annotation.Nullable;
import java.io.IOException;
import java.util.List;

@NoArgsConstructor
public class PacketAuctionSearch implements IPacketCallbackEffective<PacketAuctionSearch.Result> {

    @Getter
    public static class Result {
        private boolean isSuccess;
        private List<SimpleAuctionLot> auctionLots;
    }

    private Result result;
    private String searchRequest;

    public PacketAuctionSearch(String searchRequest) {
        this.searchRequest = searchRequest;
    }

    @Override
    public void write(ByteBufOutputStream bbos) throws IOException {
        bbos.writeUTF(searchRequest);
    }

    @Override
    public void read(ByteBufInputStream bbis) throws IOException {
        boolean hasResult = bbis.readBoolean();
        result = new Result();
        result.isSuccess = hasResult;
        if(hasResult) {
            result.auctionLots = AuctionUtils.readAuctionLotList(bbis);
        }
    }

    @Nullable
    @Override
    public Result getResult() {
        return result;
    }
}
