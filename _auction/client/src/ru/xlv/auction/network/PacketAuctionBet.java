package ru.xlv.auction.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import ru.xlv.core.common.network.IPacketCallbackEffective;
import ru.xlv.core.common.network.IPacketIn;
import ru.xlv.core.common.network.IPacketOut;
import ru.xlv.core.common.util.ByteBufInputStream;

import javax.annotation.Nullable;
import java.io.IOException;

@NoArgsConstructor
public class PacketAuctionBet implements IPacketCallbackEffective<String> {

    private String uuid, responseMessage;
    private int amount;

    public PacketAuctionBet(String uuid, int amount) {
        this.uuid = uuid;
        this.amount = amount;
    }

    @Override
    public void write(ByteBufOutputStream bbos) throws IOException {
        bbos.writeInt(amount);
        bbos.writeUTF(uuid);
    }

    @Override
    public void read(ByteBufInputStream bbis) throws IOException {
        responseMessage = bbis.readUTF();
    }

    @Nullable
    @Override
    public String getResult() {
        return responseMessage;
    }
}
