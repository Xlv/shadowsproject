package ru.xlv.auction.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import ru.xlv.core.common.network.IPacketCallbackEffective;
import ru.xlv.core.common.util.ByteBufInputStream;

import javax.annotation.Nullable;
import java.io.IOException;

@NoArgsConstructor
public class PacketAuctionPlace implements IPacketCallbackEffective<String> {

    private String responseMessage;
    private int matrixX, matrixY, amount, termType;

    public PacketAuctionPlace(int matrixX, int matrixY, int amount, int termType) {
        this.matrixX = matrixX;
        this.matrixY = matrixY;
        this.amount = amount;
        this.termType = termType;
    }

    @Override
    public void write(ByteBufOutputStream bbos) throws IOException {
        bbos.writeInt(matrixX);
        bbos.writeInt(matrixY);
        bbos.writeInt(amount);
        bbos.writeInt(termType);
    }

    @Override
    public void read(ByteBufInputStream bbis) throws IOException {
        responseMessage = bbis.readUTF();
    }

    @Nullable
    @Override
    public String getResult() {
        return responseMessage;
    }
}
