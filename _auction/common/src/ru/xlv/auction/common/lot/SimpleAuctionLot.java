package ru.xlv.auction.common.lot;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import net.minecraft.item.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Getter
@ToString
public class SimpleAuctionLot {

    private UUID uuid;
    private String seller;
    private ItemStack itemStack;
    private int startCost;
    private long timestamp;
    @Setter
    private LotState lotState;

    private List<AuctionBet> bets = new ArrayList<>();

    public SimpleAuctionLot(String seller, ItemStack itemStack, int startCost, long timestamp) {
        this(UUID.randomUUID(), seller, itemStack, startCost, timestamp);
    }

    public SimpleAuctionLot(UUID uuid, String seller, ItemStack itemStack, int startCost, long timestamp) {
        this.seller = seller;
        this.itemStack = itemStack;
        this.startCost = startCost;
        this.timestamp = timestamp;
        this.uuid = uuid;
    }

    public boolean equals(SimpleAuctionLot auctionLot) {
        return auctionLot.uuid.equals(uuid);
    }
}
