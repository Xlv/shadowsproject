package ru.xlv.collideblocks;

import cpw.mods.fml.client.registry.ClientRegistry;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.InputEvent;
import net.minecraft.client.Minecraft;
import org.lwjgl.input.Keyboard;
import ru.xlv.collideblocks.common.tile.TileEntityBlockCollide;
import ru.xlv.collideblocks.render.TileEntityBlockCollideRender;

import static ru.xlv.collideblocks.XlvsCollideBlocksMod.MODID;

@Mod(
        name = MODID,
        modid = MODID,
        version = "1.0"
)
public class XlvsCollideBlocksMod {

    static final String MODID = "cldblocks";

    @Mod.EventHandler
    public void event(FMLInitializationEvent event) {
        ClientRegistry.bindTileEntitySpecialRenderer(TileEntityBlockCollide.class, new TileEntityBlockCollideRender());
        FMLCommonHandler.instance().bus().register(this);
    }

    @SubscribeEvent
    public void event(InputEvent.KeyInputEvent event) {
        if(Keyboard.isKeyDown(Keyboard.KEY_6) && Keyboard.getEventKeyState() && Minecraft.getMinecraft().thePlayer != null && Minecraft.getMinecraft().thePlayer.capabilities.isCreativeMode) {
            TileEntityBlockCollideRender.iDebugEnabled = !TileEntityBlockCollideRender.iDebugEnabled;
        }
    }
}
