package ru.krogenit.advancedblockmodels.block;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.material.Material;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IIcon;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import org.lwjgl.util.vector.Vector3f;
import ru.krogenit.advancedblockmodels.CoreAdvancedBlockModelsClient;
import ru.krogenit.advancedblockmodels.CoreAdvancedBlockModelsCommon;
import ru.krogenit.advancedblockmodels.PlacementHighlightHandler;
import ru.krogenit.advancedblockmodels.block.tileentity.TEBlockModel;
import ru.krogenit.advancedblockmodels.block.tileentity.TEBlockModelClient;
import ru.krogenit.advancedblockmodels.block.tileentity.TEModel;
import ru.krogenit.advancedblockmodels.block.tileentity.TEModelClient;
import ru.krogenit.advancedblockmodels.item.ItemBlockModelClient;
import ru.krogenit.advancedblockmodels.network.packet.PacketTEModelRemoveClient;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.block.IBlockNoFallDust;

public class BlockModelClient extends BlockModel implements IBlockNoFallDust {

	private static boolean isShowBlocks;

	public BlockModelClient(Material mat, boolean isBreakable, String name) {
		super(mat, isBreakable, name);
		setCreativeTab(BlocksModelsClient.tabBlocksModels);
	}

	@SideOnly(Side.CLIENT)
	@Override
	public final ItemStack getPickBlock(MovingObjectPosition target, World world, int x, int y, int z, EntityPlayer p) {
		MovingObjectPosition pos = p.rayTrace(Minecraft.getMinecraft().playerController.getBlockReachDistance(), 0);
		if (pos != null) {
			TileEntity tile = world.getTileEntity(pos.blockX, pos.blockY, pos.blockZ);
			if (tile instanceof TEBlockModelClient) {
				if (pos.hitInfo instanceof TEModelClient) {
					TEModelClient part = (TEModelClient) pos.hitInfo;
					if(PlacementHighlightHandler.modelPreview != null) {
						PlacementHighlightHandler.modelPreview.setScale(new Vector3f(part.getScale()));
						PlacementHighlightHandler.modelPreview.setRotation(new Vector3f(part.getRotation()));
					}
					return ItemBlockModelClient.getStackWithName(part.getRenderer().getModel().getPath());
				}
			}
		}

		return super.getPickBlock(target, world, x, y, z, p);
	}

	@Override
	public boolean removedByPlayer(World w, EntityPlayer p, int x, int y, int z) {
		TileEntity tile = w.getTileEntity(x, y, z);
		if (tile instanceof TEBlockModel) {
			return removeTEModelByPlayer(p);
		}

		return false;
	}

	@SideOnly(Side.CLIENT)
	private boolean removeTEModelByPlayer(EntityPlayer player) {
		MovingObjectPosition pos = player.rayTrace(Minecraft.getMinecraft().playerController.getBlockReachDistance(), 0);
		if (pos != null) {
			if (pos.hitInfo instanceof TEModel) {
				TEModel part = (TEModel) pos.hitInfo;
				TEBlockModel te = part.getTe();
				int i = te.getParts().indexOf(part);
				XlvsCore.INSTANCE.getPacketHandler().sendPacketToServer(new PacketTEModelRemoveClient(te.xCoord, te.yCoord, te.zCoord, i));
			}
		}
		return false;
	}

	@Override
	public IIcon getIcon(int par1, int par2) {
		return this.blockIcon;
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void registerBlockIcons(IIconRegister par1IconRegister) {
		this.blockIcon = par1IconRegister.registerIcon(CoreAdvancedBlockModelsCommon.MODID + ":empty_red");
	}

	@SideOnly(Side.CLIENT)
	@Override
	public int getRenderType() {
		return isShowBlocks ? 0 : CoreAdvancedBlockModelsClient.blockModelRenderID;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public boolean shouldSideBeRendered(IBlockAccess w, int x, int y, int z, int side) {
		return isShowBlocks;
	}

	@Override
	public TileEntity createNewTileEntity(World w, int meta) {
		return new TEBlockModelClient();
	}
}