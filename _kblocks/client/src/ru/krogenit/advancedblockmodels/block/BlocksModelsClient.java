package ru.krogenit.advancedblockmodels.block;

import cpw.mods.fml.common.registry.GameRegistry;
import lombok.Getter;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.tileentity.TileEntity;
import ru.krogenit.advancedblockmodels.block.tileentity.TEBlockModelClient;
import ru.krogenit.advancedblockmodels.creativetab.CreativeTabBlocksModels;
import ru.krogenit.advancedblockmodels.item.ItemBlockModelClient;
import ru.krogenit.advancedblockmodels.render.block.IBlockModelRenderer;
import ru.krogenit.advancedblockmodels.render.block.add.BlockArmorRender;
import ru.krogenit.advancedblockmodels.render.block.add.BlockHologramRenderer;
import ru.krogenit.advancedblockmodels.render.block.add.BlockPropsRenderer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class BlocksModelsClient extends BlockModels {
	@Getter
	public static class ModelPart {
		private final String model;
		private final String name;

		ModelPart(String model, String name) {
			this.model = model;
			this.name = name;
		}
	}

	public static ArrayList<ModelPart> names = new ArrayList<>();
	protected static Map<String, IBlockModelRenderer> renderersMap = new HashMap<>();
	private static final HashMap<String, Integer> modelNameMap = new HashMap<>();
	static CreativeTabs tabBlocksModels = new CreativeTabBlocksModels();

	protected static void registerNewModel(String name, IBlockModelRenderer renderModel) {
		int nameAdd = modelNameMap.getOrDefault(name, 0);
		nameAdd++;

		StringBuilder buffer = new StringBuilder(name);
		buffer.insert(name.length(), " " + nameAdd + "");
		modelNameMap.put(name, nameAdd);

		names.add(new ModelPart(renderModel.getModelName(), buffer.toString()));
		renderersMap.put(renderModel.getModelName(), renderModel);
	}

	public static void registerModelBlocks() {
		modelBlock = new BlockModelClient(Material.rock, false, "block_model");
		GameRegistry.registerBlock(modelBlock, ItemBlockModelClient.class, "block_model");

		BlocksModelsClient.registerNewModel("Броня Инженера", new BlockArmorRender("advancedblocks:models/armor/Engineer-Deco.obj","engineer", BlockModel.ModelRenderType.ADVANCED, true, 10f));
		BlocksModelsClient.registerNewModel("Военная Броня", new BlockArmorRender("advancedblocks:models/armor/Military-Deco.obj", "military", BlockModel.ModelRenderType.ADVANCED, false, 1f));
		BlocksModelsClient.registerNewModel("Броня Полицейского", new BlockArmorRender("advancedblocks:models/armor/Police-Deco.obj","police", BlockModel.ModelRenderType.ADVANCED, true, 2f));
		BlocksModelsClient.registerNewModel("Броня Предатор", new BlockArmorRender("advancedblocks:models/armor/Predator-Deco.obj","predator", BlockModel.ModelRenderType.ADVANCED, true, 2.5f));
		BlocksModelsClient.registerNewModel("Броня Ученого", new BlockArmorRender("advancedblocks:models/armor/Scientific-Deco.obj","scientific", BlockModel.ModelRenderType.ADVANCED, true, 2.5f));
		BlocksModelsClient.registerNewModel("Паучья Броня", new BlockArmorRender("advancedblocks:models/armor/Spider-Deco.obj","spider", BlockModel.ModelRenderType.ADVANCED, true, 2.5f));
		BlocksModelsClient.registerNewModel("Броня Владика", new BlockArmorRender("advancedblocks:models/armor/Vlad-Deco.obj","vlad", BlockModel.ModelRenderType.ADVANCED, true, 2.5f));
		BlocksModelsClient.registerNewModel("Светлая Броня", new BlockArmorRender("advancedblocks:models/armor/Light-Deco.obj", "light", BlockModel.ModelRenderType.ADVANCED, true, 2f));
		BlocksModelsClient.registerNewModel("Джагернаут Броня", new BlockArmorRender("advancedblocks:models/armor/Jager-Deco.obj","jager", BlockModel.ModelRenderType.ADVANCED, true, 2f));
		BlocksModelsClient.registerNewModel("Невидимая Броня", new BlockArmorRender("advancedblocks:models/armor/Invisible-Deco.obj","invisible", BlockModel.ModelRenderType.ADVANCED, true, 2f));
		BlocksModelsClient.registerNewModel("Броня Колонист", new BlockArmorRender("advancedblocks:models/armor/Colonist-Deco.obj","colonist", BlockModel.ModelRenderType.ADVANCED, false, 2f));
		BlocksModelsClient.registerNewModel("Броня Глаз", new BlockArmorRender("advancedblocks:models/armor/Eye-Deco.obj","eye", BlockModel.ModelRenderType.ADVANCED, true, 2f));
		BlocksModelsClient.registerNewModel("Тяжелая Броня", new BlockArmorRender("advancedblocks:models/armor/Heavy-Deco.obj","heavy", BlockModel.ModelRenderType.ADVANCED, true, 2f));
		BlocksModelsClient.registerNewModel("Броня Война", new BlockArmorRender("advancedblocks:models/armor/Warrior-Deco.obj","warrior", BlockModel.ModelRenderType.ADVANCED, true, 2f));
		BlocksModelsClient.registerNewModel("Броня Медика", new BlockArmorRender("advancedblocks:models/armor/Medic-Deco.obj", "medic", BlockModel.ModelRenderType.ADVANCED, true, 2f));

		BlocksModelsClient.registerNewModel("Бочка", new BlockPropsRenderer("advancedblocks:models/props/1/barrel/barrel.obj", "1/barrel", BlockModel.ModelRenderType.ADVANCED,false, 2f));
		BlocksModelsClient.registerNewModel("Ящик", new BlockPropsRenderer("advancedblocks:models/props/1/box/box.obj","1/box", BlockModel.ModelRenderType.ADVANCED,false, 2f));
		BlocksModelsClient.registerNewModel("Напольная лампа", new BlockPropsRenderer("advancedblocks:models/props/1/floor_lamp_on_stand/floor_lamp_on_stand.obj", "1/floor_lamp_on_stand", BlockModel.ModelRenderType.ADVANCED,true, 2f));
		BlocksModelsClient.registerNewModel("Топливный бак", new BlockPropsRenderer("advancedblocks:models/props/1/fuel_tank/fuel_tank.obj","1/fuel_tank", BlockModel.ModelRenderType.ADVANCED,false, 2f));
		BlocksModelsClient.registerNewModel("Урна для бедных", new BlockPropsRenderer("advancedblocks:models/props/1/litter_bin_for_the_poor/litter_bin_for_the_poor.obj", "1/litter_bin_for_the_poor", BlockModel.ModelRenderType.ADVANCED,false, 2f, false));
		BlocksModelsClient.registerNewModel("Сканнер", new BlockPropsRenderer("advancedblocks:models/props/1/scanner/scanner.obj","1/scanner", BlockModel.ModelRenderType.ADVANCED,true, 2f));
		BlocksModelsClient.registerNewModel("Космический терминал", new BlockPropsRenderer("advancedblocks:models/props/1/space_terminal/space_terminal.obj","1/space_terminal", BlockModel.ModelRenderType.ADVANCED,false, 2f));
		BlocksModelsClient.registerNewModel("Токсичная бочка", new BlockPropsRenderer("advancedblocks:models/props/1/toxic_tank/toxic_tank.obj", "1/toxic_tank", BlockModel.ModelRenderType.ADVANCED,false, 2f));
		BlocksModelsClient.registerNewModel("Урна", new BlockPropsRenderer("advancedblocks:models/props/1/urn/urn.obj","1/urn", BlockModel.ModelRenderType.ADVANCED,false, 2f));
		BlocksModelsClient.registerNewModel("Настенная аптечка", new BlockPropsRenderer("advancedblocks:models/props/1/wall-mounted_first-aid kit/wall-mounted_first-aid kit.obj","1/wall-mounted_first-aid kit", BlockModel.ModelRenderType.ADVANCED,false, 2f));

		BlocksModelsClient.registerNewModel("Ахуенный контейнер", new BlockPropsRenderer("advancedblocks:models/props/2/ahuenny_containers/ahuenny_containers1.obj","2/ahuenny_containers", BlockModel.ModelRenderType.ADVANCED,true, 2f));
		BlocksModelsClient.registerNewModel("Ахуенный контейнер", new BlockPropsRenderer("advancedblocks:models/props/2/ahuenny_containers/ahuenny_containers2.obj","2/ahuenny_containers", BlockModel.ModelRenderType.ADVANCED,true, 2f));
		BlocksModelsClient.registerNewModel("Ахуенный контейнер", new BlockPropsRenderer("advancedblocks:models/props/2/ahuenny_containers/ahuenny_containers3.obj","2/ahuenny_containers", BlockModel.ModelRenderType.ADVANCED,true, 2f));
		BlocksModelsClient.registerNewModel("Кресло", new BlockPropsRenderer("advancedblocks:models/props/2/armchair/armchair.obj","2/armchair", BlockModel.ModelRenderType.ADVANCED,false, 2f));
		BlocksModelsClient.registerNewModel("Ящик", new BlockPropsRenderer("advancedblocks:models/props/2/boxes/boxes1.obj","2/boxes", BlockModel.ModelRenderType.ADVANCED,true, 2f));
		BlocksModelsClient.registerNewModel("Ящик", new BlockPropsRenderer("advancedblocks:models/props/2/boxes/boxes2.obj","2/boxes", BlockModel.ModelRenderType.ADVANCED,true, 2f));
		BlocksModelsClient.registerNewModel("Контейнер", new BlockPropsRenderer("advancedblocks:models/props/2/containers/containers1.obj","2/containers", BlockModel.ModelRenderType.ADVANCED,true, 2f));
		BlocksModelsClient.registerNewModel("Контейнер", new BlockPropsRenderer("advancedblocks:models/props/2/containers/containers2.obj","2/containers", BlockModel.ModelRenderType.ADVANCED,true, 2f));
		BlocksModelsClient.registerNewModel("Контейнер", new BlockPropsRenderer("advancedblocks:models/props/2/containers/containers3.obj","2/containers", BlockModel.ModelRenderType.ADVANCED,true, 2f));
		BlocksModelsClient.registerNewModel("Контейнер", new BlockPropsRenderer("advancedblocks:models/props/2/containers/containers4.obj","2/containers", BlockModel.ModelRenderType.ADVANCED,true, 2f));
		BlocksModelsClient.registerNewModel("Угловой столик", new BlockPropsRenderer("advancedblocks:models/props/2/corner_table/corner_table.obj","2/corner_table", BlockModel.ModelRenderType.ADVANCED,false, 2f));
		BlocksModelsClient.registerNewModel("Криопод", new BlockPropsRenderer("advancedblocks:models/props/2/cryopod/cryopod.obj","2/cryopod", BlockModel.ModelRenderType.ADVANCED,false, 2f));
		BlocksModelsClient.registerNewModel("Энергетическая ячейка", new BlockPropsRenderer("advancedblocks:models/props/2/energy_cell/energy_cell.obj","2/energy_cell", BlockModel.ModelRenderType.ADVANCED,false, 2f));
		BlocksModelsClient.registerNewModel("Оружейный ящик", new BlockPropsRenderer("advancedblocks:models/props/2/gun_box/gun_box.obj","2/gun_box", BlockModel.ModelRenderType.ADVANCED,true, 2f));
		BlocksModelsClient.registerNewModel("Закрытый ящик", new BlockPropsRenderer("advancedblocks:models/props/2/locked_container/locked_container.obj","2/locked_container", BlockModel.ModelRenderType.ADVANCED,true, 2f));
		BlocksModelsClient.registerNewModel("Голографический стол", new BlockHologramRenderer("advancedblocks:models/props/2/terra_hologram_table/terra_hologram_table.obj","2/terra_hologram_table", BlockModel.ModelRenderType.ADVANCED));

		BlocksModelsClient.registerNewModel( "И другая капсула", new BlockPropsRenderer("advancedblocks:models/props/3/and_another_capsule/and_another_capsule.obj","3/and_another_capsule", BlockModel.ModelRenderType.ADVANCED,true, 2f));
		BlocksModelsClient.registerNewModel( "Бочка", new BlockPropsRenderer("advancedblocks:models/props/3/barrel/barrel.obj","3/barrel", BlockModel.ModelRenderType.ADVANCED,false, 2f));
		BlocksModelsClient.registerNewModel( "Ящик из СССР", new BlockPropsRenderer("advancedblocks:models/props/3/box_of_the_ussr/box_of_the_ussr.obj","3/box_of_the_ussr", BlockModel.ModelRenderType.ADVANCED,true, 2f));
		BlocksModelsClient.registerNewModel("Капсула для анабиоза", new BlockPropsRenderer("advancedblocks:models/props/3/capsule_for_suspended_animation/capsule_for_suspended_animation.obj","3/capsule_for_suspended_animation", BlockModel.ModelRenderType.ADVANCED,false, 2f));
		BlocksModelsClient.registerNewModel("Капсула для анабиоза открытая", new BlockPropsRenderer("advancedblocks:models/props/3/capsule_for_suspended_animation/capsule_for_suspended_animation_opened.obj", "3/capsule_for_suspended_animation", BlockModel.ModelRenderType.ADVANCED,false, 2f));
		BlocksModelsClient.registerNewModel( "Хуй пойми что", new BlockPropsRenderer("advancedblocks:models/props/3/cock_understand_that/cock_understand_that.obj","3/cock_understand_that", BlockModel.ModelRenderType.ADVANCED,false, 2f));
		BlocksModelsClient.registerNewModel( "Сверло", new BlockPropsRenderer("advancedblocks:models/props/3/drill/drill.obj","3/drill", BlockModel.ModelRenderType.ADVANCED,false, 2f));
		BlocksModelsClient.registerNewModel( "Генератор", new BlockPropsRenderer("advancedblocks:models/props/3/generator/generator.obj","3/generator", BlockModel.ModelRenderType.ADVANCED,false, 2f));
		BlocksModelsClient.registerNewModel( "Стиральная машина", new BlockPropsRenderer("advancedblocks:models/props/3/washing_machine/washing_machine.obj","3/washing_machine", BlockModel.ModelRenderType.ADVANCED,false, 2f));

		BlocksModelsClient.registerNewModel( "Бочка", new BlockPropsRenderer("barrel/barrel_1", BlockModel.ModelRenderType.ADVANCED,false, 2f, false, true));
		BlocksModelsClient.registerNewModel( "Бочка", new BlockPropsRenderer("barrel/barrel_2", BlockModel.ModelRenderType.ADVANCED,false, 2f, false, true));
		BlocksModelsClient.registerNewModel( "Бокс", new BlockPropsRenderer("box/box_2", BlockModel.ModelRenderType.ADVANCED,false, 2f, false, false));
		BlocksModelsClient.registerNewModel( "Бетон", new BlockPropsRenderer("concrete/concrete_1", BlockModel.ModelRenderType.ADVANCED,false, 2f, false, false));
		BlocksModelsClient.registerNewModel( "Конт", new BlockPropsRenderer("cont/cont_1", BlockModel.ModelRenderType.ADVANCED,false, 2f, false, true));
		BlocksModelsClient.registerNewModel( "Герма", new BlockPropsRenderer("germo/germo_1", BlockModel.ModelRenderType.ADVANCED,false, 2f, false, true));
		BlocksModelsClient.registerNewModel( "Трампет", new BlockPropsRenderer("trumpet/trumpet_2", BlockModel.ModelRenderType.ADVANCED,false, 2f, false, true));
		BlocksModelsClient.registerNewModel( "Колесо", new BlockPropsRenderer("wheel/wheel_1", BlockModel.ModelRenderType.ADVANCED,false, 2f, false, true));
		BlocksModelsClient.registerNewModel( "Провода", new BlockPropsRenderer("wires/wires_1", BlockModel.ModelRenderType.ADVANCED,false, 2f, false, true));

		BlocksModelsClient.registerNewModel( "Мешок", new BlockPropsRenderer("bags/bags_1", BlockModel.ModelRenderType.ADVANCED,false, false, 2f, false, false));
		BlocksModelsClient.registerNewModel( "Стул", new BlockPropsRenderer("chair/chair_2", BlockModel.ModelRenderType.ADVANCED,false, false, 2f, false, true));
		BlocksModelsClient.registerNewModel( "Напиток", new BlockPropsRenderer("drink/drink_1", BlockModel.ModelRenderType.ADVANCED,true, false, 2f, false, false));
		BlocksModelsClient.registerNewModel( "Клавиатура", new BlockPropsRenderer("keyboard/keyboard_1", BlockModel.ModelRenderType.ADVANCED,false, false, 2f, false, false));
		BlocksModelsClient.registerNewModel( "Ленин", new BlockPropsRenderer("lenin/lenin_1", BlockModel.ModelRenderType.ADVANCED,true, false, 2f, false, false));
		BlocksModelsClient.registerNewModel( "Мария", new BlockPropsRenderer("maria/maria_1", BlockModel.ModelRenderType.ADVANCED,true, false, 2f, false, false));
		BlocksModelsClient.registerNewModel( "Монитор", new BlockPropsRenderer("monitor/monitor_1", BlockModel.ModelRenderType.ADVANCED,true, false, 2f, false, false));
		BlocksModelsClient.registerNewModel( "Бумага", new BlockPropsRenderer("paper/paper_1", BlockModel.ModelRenderType.ADVANCED,true, false, 2f, false, false));
		BlocksModelsClient.registerNewModel( "Труба", new BlockPropsRenderer("pipe/pipe_1", BlockModel.ModelRenderType.ADVANCED,true, false, 2f, false, false));
		BlocksModelsClient.registerNewModel( "Шлак", new BlockPropsRenderer("shlako/shlako_1", BlockModel.ModelRenderType.ADVANCED,true, false, 2f, false, false));
		BlocksModelsClient.registerNewModel( "Квадратный канал", new BlockPropsRenderer("squareduct/squareduct_1", BlockModel.ModelRenderType.ADVANCED,true, false, 2f, false, true));
		BlocksModelsClient.registerNewModel( "Квадратный канал", new BlockPropsRenderer("squareduct/squareduct_2", BlockModel.ModelRenderType.ADVANCED,true, false, 2f, false, true));
		BlocksModelsClient.registerNewModel( "Мусор", new BlockPropsRenderer("trash/trash_1", BlockModel.ModelRenderType.ADVANCED,true, false, 2f, false, true));
		BlocksModelsClient.registerNewModel( "Телевизор", new BlockPropsRenderer("tv/tv_1", BlockModel.ModelRenderType.ADVANCED,true, false, 2f, false, true));

		BlocksModelsClient.registerNewModel( "Чертеж", new BlockPropsRenderer("furniture/blueprint_1", BlockModel.ModelRenderType.ADVANCED,false, false, 2f, false, false));
		BlocksModelsClient.registerNewModel( "Чертеж", new BlockPropsRenderer("furniture/blueprint_2", BlockModel.ModelRenderType.ADVANCED,false, false, 2f, false, false));
		BlocksModelsClient.registerNewModel( "Чертеж", new BlockPropsRenderer("furniture/blueprint_3", BlockModel.ModelRenderType.ADVANCED,false, false, 2f, false, false));
		BlocksModelsClient.registerNewModel( "Чертеж", new BlockPropsRenderer("furniture/blueprint_4", BlockModel.ModelRenderType.ADVANCED,false, false, 2f, false, false));
		BlocksModelsClient.registerNewModel( "Компьютерное кресло", new BlockPropsRenderer("furniture/computer_chair_1", BlockModel.ModelRenderType.ADVANCED,true, false, 2f, false, true));
		BlocksModelsClient.registerNewModel( "Офисное кресло", new BlockPropsRenderer("furniture/office_chair_1", BlockModel.ModelRenderType.ADVANCED,true, false, 2f, false, true));
		BlocksModelsClient.registerNewModel( "Стол", new BlockPropsRenderer("furniture/table_1", BlockModel.ModelRenderType.ADVANCED,true, false, 2f, false, true));
		BlocksModelsClient.registerNewModel( "Стол", new BlockPropsRenderer("furniture/table_2", BlockModel.ModelRenderType.ADVANCED,true, false, 2f, false, true));
		BlocksModelsClient.registerNewModel( "Стол", new BlockPropsRenderer("furniture/table_3", BlockModel.ModelRenderType.ADVANCED,true, false, 2f, false, false));

		System.out.println("Зарегистрировано " + renderersMap.size() + " моделей");
	}

	public static void registerTileEntities() {
		TileEntity.addMapping(TEBlockModelClient.class, "TEBlockModelClient");
	}

	public static IBlockModelRenderer getRenderer(String model) {
		return renderersMap.get(model);
	}
}
