package ru.krogenit.advancedblockmodels.block.tileentity;

import lombok.Getter;
import net.minecraft.util.AxisAlignedBB;
import org.lwjgl.util.vector.Vector3f;
import ru.krogenit.advancedblockmodels.block.BlockModel;
import ru.krogenit.advancedblockmodels.block.BlocksModelsClient;
import ru.krogenit.advancedblockmodels.render.block.IBlockModelRenderer;
import ru.krogenit.advancedblockmodels.render.block.ITypeRenderer;
import ru.krogenit.advancedblockmodels.render.block.TERenderModel;

import java.io.DataInput;
import java.io.IOException;
import java.util.ArrayList;

@Getter
public class TEModelClient extends TEModel {

    private ITypeRenderer render;
    private final IBlockModelRenderer renderer;
    private final TEBlockModelClient clientTe;
    private final Vector3f animation;

    TEModelClient(DataInput data, TEBlockModelClient te) throws IOException {
        BlockModel.ModelCollideType[] types = BlockModel.ModelCollideType.values();
        this.type = types[data.readByte()];
        this.modelName = data.readUTF();
        this.renderer = BlocksModelsClient.getRenderer(modelName);
        this.position = new Vector3f(data.readFloat(), data.readFloat(), data.readFloat());
        this.rotation = new Vector3f(data.readFloat(), data.readFloat(), data.readFloat());
        this.scale = new Vector3f(data.readFloat(), data.readFloat(), data.readFloat());
        this.te = te;
        this.clientTe = te;
        this.worldObj = te.getWorldObj();
        this.xCoord = te.xCoord;
        this.yCoord = te.yCoord;
        this.zCoord = te.zCoord;
        this.animation = new Vector3f();

        this.hasAABB = data.readBoolean();

        if(renderer == null) {
            System.out.println("Модель " + modelName + " не зарегистрирована");
            return;
        }

        this.render = TERenderModel.getRenderer(type);

        if (this.renderer.isModelsLoaded()) {
            if (hasAABB) {
                AxisAlignedBB aabb = renderer.getAABB(rotation, scale);
                this.aabb.minX = aabb.minX;
                this.aabb.minY = aabb.minY;
                this.aabb.minZ = aabb.minZ;
                this.aabb.maxX = aabb.maxX;
                this.aabb.maxY = aabb.maxY;
                this.aabb.maxZ = aabb.maxZ;
                aabbCalculated = true;
                te.placeHelpBlocks(worldObj, xCoord, yCoord, zCoord, xCoord + position.x, yCoord + position.y, zCoord + position.z, this, TEBlockModel.HelpPlaceType.LOAD);
            }
            clientTe.refreshRenderBoxAndDist(this);
        } else {
            synchronized (TEBlockModel.partsToLoad) {
                ArrayList<TEModel> list = TEBlockModel.partsToLoad.computeIfAbsent(modelName, k -> new ArrayList<>());
                list.add(this);
            }
        }
    }

    public AxisAlignedBB getBoundingBox(int x, int y, int z) {
        if(!aabbCalculated) {
            if(renderer.isModelsLoaded()) {
                AxisAlignedBB aabb = renderer.getRenderAABB(this);
                this.aabb.minX = aabb.minX;
                this.aabb.minY = aabb.minY;
                this.aabb.minZ = aabb.minZ;
                this.aabb.maxX = aabb.maxX;
                this.aabb.maxY = aabb.maxY;
                this.aabb.maxZ = aabb.maxZ;
                aabbCalculated = true;
            }
        }

        if(aabbCalculated) {
            double minx = x + aabb.minX + this.position.x;
            double miny = y + aabb.minY + this.position.y;
            double minz = z + aabb.minZ + this.position.z;
            double maxx = x + aabb.maxX + this.position.x;
            double maxy = y + aabb.maxY + this.position.y;
            double maxz = z + aabb.maxZ + this.position.z;
            return AxisAlignedBB.getBoundingBox(minx, miny, minz, maxx, maxy, maxz);
        }

        return null;
    }

    public AxisAlignedBB getBoundingBoxLocal() {
        if (!aabbCalculated && renderer.isModelsLoaded()) {
            AxisAlignedBB aabb = renderer.getRenderAABB(this);
            this.aabb.minX = aabb.minX;
            this.aabb.minY = aabb.minY;
            this.aabb.minZ = aabb.minZ;
            this.aabb.maxX = aabb.maxX;
            this.aabb.maxY = aabb.maxY;
            this.aabb.maxZ = aabb.maxZ;
            aabbCalculated = true;
        }

        if (aabbCalculated) {
            aabbHelper.minX = aabb.minX + this.position.x;
            aabbHelper.minY = aabb.minY + this.position.y;
            aabbHelper.minZ = aabb.minZ + this.position.z;
            aabbHelper.maxX = aabb.maxX + this.position.x;
            aabbHelper.maxY = aabb.maxY + this.position.y;
            aabbHelper.maxZ = aabb.maxZ + this.position.z;
            return aabbHelper;
        }

        return null;
    }

    public boolean checkModelLoad() {
        if (renderer.isModelsLoaded()) {
            if (hasAABB) {
                AxisAlignedBB aabb = renderer.getAABB(rotation, scale);
                this.aabb.minX = aabb.minX;
                this.aabb.minY = aabb.minY;
                this.aabb.minZ = aabb.minZ;
                this.aabb.maxX = aabb.maxX;
                this.aabb.maxY = aabb.maxY;
                this.aabb.maxZ = aabb.maxZ;
                aabbCalculated = true;
                te.placeHelpBlocks(worldObj, xCoord, yCoord, zCoord, xCoord + position.x, yCoord + position.y, zCoord + position.z, this, TEBlockModel.HelpPlaceType.LOAD);
            }
            clientTe.refreshRenderBoxAndDist(this);
            return true;
        } else {
            return false;
        }
    }

    public Vector3f getAnimation() {
        return animation;
    }
}
