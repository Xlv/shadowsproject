package ru.krogenit.advancedblockmodels.key;

import net.minecraft.client.Minecraft;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.util.ChatComponentText;
import ru.krogenit.advancedblockmodels.block.BlockModel.ModelCollideType;

public class KeyModelPlaceType extends IKey {
	private boolean keyDown = false;
	private boolean keyUp = true;
	public static ModelCollideType placeType = ModelCollideType.ADVANCED;

	KeyModelPlaceType(KeyBinding keyBindings) {
		super(keyBindings);
	}

	@Override
	public void keyDown() {
		Minecraft mc = Minecraft.getMinecraft();
		if (!keyDown && mc.currentScreen == null && mc.thePlayer.capabilities.isCreativeMode) {
			if (placeType == ModelCollideType.ADVANCED)
				placeType = ModelCollideType.ADVANCED_NO_COLLISION;
			else if (placeType == ModelCollideType.ADVANCED_NO_COLLISION)
				placeType = ModelCollideType.EMPTY;
			else if (placeType == ModelCollideType.EMPTY)
				placeType = ModelCollideType.STANDARD;
			else
				placeType = ModelCollideType.ADVANCED;
			mc.thePlayer.addChatMessage(
					new ChatComponentText("Режим установки моделей изменён на: " + placeType.toString()));
			keyDown = true;
			keyUp = false;
		}
	}

	@Override
	public void keyUp() {
		if (!keyUp) {
			keyDown = false;
			keyUp = true;
		}
	}
}
