package ru.krogenit.advancedblockmodels.render.block;

import ru.krogenit.advancedblockmodels.block.tileentity.TEModelClient;

public interface ITypeRenderer {
	void renderPost(TEModelClient part);
	void render(TEModelClient part);
	void renderUtils(TEModelClient part);
}
