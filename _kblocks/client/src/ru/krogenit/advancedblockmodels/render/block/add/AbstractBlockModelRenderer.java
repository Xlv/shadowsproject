package ru.krogenit.advancedblockmodels.render.block.add;

import net.minecraft.client.Minecraft;
import net.minecraft.util.AxisAlignedBB;
import org.lwjgl.util.vector.Vector3f;
import ru.krogenit.advancedblockmodels.block.BlockModel;
import ru.krogenit.advancedblockmodels.block.tileentity.TEModelClient;
import ru.krogenit.advancedblockmodels.render.block.IBlockModelRenderer;
import ru.krogenit.model_loader.Model;
import ru.krogenit.shaders.pbr.IPBR;

import static org.lwjgl.opengl.GL11.glRotatef;
import static org.lwjgl.opengl.GL11.glScalef;

public abstract class AbstractBlockModelRenderer implements IBlockModelRenderer {
	protected static final Minecraft mc = Minecraft.getMinecraft();
	private static final Vector3f ZERO_COORDS = new Vector3f(0, 0, 0);
	private static final Vector3f NORMAL_SCALING = new Vector3f(1, 1, 1);

	protected final Model model;
	protected final Vector3f translate;
	protected final Vector3f scale;
	protected final Vector3f invScale;
	protected final Vector3f invRotation;
	protected final BlockModel.ModelRenderType type;
	protected boolean postRender;

	public AbstractBlockModelRenderer(BlockModel.ModelRenderType type, Model model) {
		this.invScale = NORMAL_SCALING;
		this.translate = ZERO_COORDS;
		this.scale = NORMAL_SCALING;
		this.invRotation = ZERO_COORDS;
		this.type = type;
		this.model = model;
	}

	public abstract void renderPost(TEModelClient part);
	public abstract void render(TEModelClient part);

	@Override
	public void renderTileEntity(TEModelClient part) {
		render(part);
	}

	@Override
	public void renderTileEntityPost(TEModelClient part) {
		renderPost(part);
	}

	@Override
	public void renderPreview(IPBR shader) {
		model.render(shader);
	}

	@Override
	public void renderInventory() {
		float baseScale = model.getInvScale();
		glScalef(invScale.x * baseScale, invScale.y * baseScale, invScale.z * baseScale);
		glRotatef(invRotation.y, 0, 1, 0);
		this.render(null);
	}

	@Override
	public void renderAsEntity() {
		float baseScale = 0.25f * model.getInvScale();
		glScalef(invScale.x * baseScale, invScale.y * baseScale, invScale.z * baseScale);
		this.render(null);
	}

	@Override
	public void renderAsEntityPost() {
		float baseScale = 0.25f * model.getInvScale();
		glScalef(invScale.x * baseScale, invScale.y * baseScale, invScale.z * baseScale);
		this.render(null);
	}

	@Override
	public void render3rdPerson() {
		float baseScale = model.getInvScale();
		glScalef(invScale.x * baseScale, invScale.y * baseScale, invScale.z * baseScale);
		glRotatef(invRotation.y, 0, 1, 0);
		this.render(null);
	}

	@Override
	public void render3rdPersonPost() {
		float baseScale = model.getInvScale();
		glScalef(invScale.x * baseScale, invScale.y * baseScale, invScale.z * baseScale);
		glRotatef(invRotation.y, 0, 1, 0);
		this.render(null);
	}

	@Override
	public void renderFirstPerson() {
		float baseScale = model.getInvScale();
		glScalef(invScale.x * baseScale, invScale.y * baseScale, invScale.z * baseScale);
		glRotatef(invRotation.y, 0, 1, 0);
		this.render(null);
	}

	@Override
	public AxisAlignedBB getAABB(Vector3f rotation, Vector3f scale) {
		return model.getRotatedModelBox(rotation, scale);
	}

	@Override
	public AxisAlignedBB getRenderAABB(TEModelClient part) {
		AxisAlignedBB axisAlignedBB = part.getAabb();
		if (axisAlignedBB == null) return part.getRenderer().getAABB(part.getRotation(), part.getScale());
		return axisAlignedBB;
	}

	@Override
	public boolean isModelsLoaded() {
		return model.isLoaded();
	}

	@Override
	public String getModelName() {
		return model.getPath();
	}

	@Override
	public Model getModel() {
		return model;
	}

	@Override
	public BlockModel.ModelRenderType getType() {
		return type;
	}

	@Override
	public Vector3f getScale() {
		return scale;
	}

	@Override
	public Vector3f getTranslate() {
		return translate;
	}

	public void setPostRender() {
		this.postRender = true;
	}

	@Override
	public boolean isPostRender() {
		return postRender;
	}
}
