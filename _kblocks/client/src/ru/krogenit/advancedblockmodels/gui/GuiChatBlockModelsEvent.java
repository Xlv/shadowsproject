package ru.krogenit.advancedblockmodels.gui;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiChat;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;
import org.lwjgl.opengl.GL11;
import ru.krogenit.advancedblockmodels.PlacementHighlightHandler;
import ru.krogenit.advancedblockmodels.item.ItemBlockModel;
import ru.krogenit.advancedblockmodels.render.block.RenderMetroModelPreview;
import ru.krogenit.client.event.EventChatGui;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

public class GuiChatBlockModelsEvent {

    private static final Minecraft mc = Minecraft.getMinecraft();
    private final List<GuiTextField> listInputs = new ArrayList<>();
    private GuiTextField posX;
    private GuiTextField posY;
    private GuiTextField posZ;
    private GuiTextField rotX;
    private GuiTextField rotY;
    private GuiTextField rotZ;
    private GuiTextField scaleX;
    private GuiTextField scaleY;
    private GuiTextField scaleZ;

    @SubscribeEvent
    public void event(EventChatGui event) {
        GuiChat chat = event.getGuiChat();
        if(event.getType() == EventChatGui.EnumChatEventType.INIT) {
            if (mc.thePlayer.getCurrentEquippedItem() != null && mc.thePlayer.getCurrentEquippedItem().getItem() instanceof ItemBlockModel
                    && PlacementHighlightHandler.modelPreview != null) {
                chat.getInputField().setCanLoseFocus(true);
                int outX = 80;
                int outY = 20;
                int baseX = 10;
                int baseY = -140;
                this.posX = new GuiTextField(mc.fontRenderer, baseX, chat.height / 2f + baseY, 50, 20);
                baseY += 20;
                this.posY = new GuiTextField(mc.fontRenderer, baseX, chat.height / 2f + baseY, 50, 20);
                baseY += 20;
                this.posZ = new GuiTextField(mc.fontRenderer, baseX, chat.height / 2f + baseY, 50, 20);

                baseX += outX;
                baseY = -140;
                this.rotX = new GuiTextField(mc.fontRenderer, baseX, chat.height / 2f + baseY, 50, 20);
                baseY += outY;
                this.rotY = new GuiTextField(mc.fontRenderer, baseX, chat.height / 2f + baseY, 50, 20);
                baseY += outY;
                this.rotZ = new GuiTextField(mc.fontRenderer, baseX, chat.height / 2f + baseY, 50, 20);

                baseX += outX;
                baseY = -140;
                this.scaleX = new GuiTextField(mc.fontRenderer, baseX, chat.height / 2f + baseY, 50, 20);
                baseY += outY;
                this.scaleY = new GuiTextField(mc.fontRenderer, baseX, chat.height / 2f + baseY, 50, 20);
                baseY += outY;
                this.scaleZ = new GuiTextField(mc.fontRenderer, baseX, chat.height / 2f + baseY, 50, 20);

                NumberFormat formatter = new DecimalFormat("#0.00");

                String modelPosX = formatter.format(PlacementHighlightHandler.modelPreview.getAddPos().x);
                String modelPosY = formatter.format(PlacementHighlightHandler.modelPreview.getAddPos().y);
                String modelPosZ = formatter.format(PlacementHighlightHandler.modelPreview.getAddPos().z);
                this.posX.setText(modelPosX.replace(",", "."));
                this.posY.setText(modelPosY.replace(",", "."));
                this.posZ.setText(modelPosZ.replace(",", "."));

                modelPosX = formatter.format(PlacementHighlightHandler.modelPreview.getRotation().x);
                modelPosY = formatter.format(PlacementHighlightHandler.modelPreview.getRotation().y);
                modelPosZ = formatter.format(PlacementHighlightHandler.modelPreview.getRotation().z);
                this.rotX.setText(modelPosX.replace(",", "."));
                this.rotY.setText(modelPosY.replace(",", "."));
                this.rotZ.setText(modelPosZ.replace(",", "."));

                modelPosX = formatter.format(PlacementHighlightHandler.modelPreview.getScale().x);
                modelPosY = formatter.format(PlacementHighlightHandler.modelPreview.getScale().y);
                modelPosZ = formatter.format(PlacementHighlightHandler.modelPreview.getScale().z);
                this.scaleX.setText(modelPosX.replace(",", "."));
                this.scaleY.setText(modelPosY.replace(",", "."));
                this.scaleZ.setText(modelPosZ.replace(",", "."));

                listInputs.clear();
                listInputs.add(posX);
                listInputs.add(posY);
                listInputs.add(posZ);
                listInputs.add(rotX);
                listInputs.add(rotY);
                listInputs.add(rotZ);
                listInputs.add(scaleX);
                listInputs.add(scaleY);
                listInputs.add(scaleZ);
            }
        } else if(event.getType() == EventChatGui.EnumChatEventType.UPDATE) {
            if (mc.thePlayer.getCurrentEquippedItem() != null && mc.thePlayer.getCurrentEquippedItem().getItem() instanceof ItemBlockModel
                    && PlacementHighlightHandler.modelPreview != null)
                for (GuiTextField listInput : listInputs) listInput.updateCursorCounter();
        } else if(event.getType() == EventChatGui.EnumChatEventType.DRAW) {
            if (mc.thePlayer.getCurrentEquippedItem() != null && mc.thePlayer.getCurrentEquippedItem().getItem() instanceof ItemBlockModel
                    && PlacementHighlightHandler.modelPreview != null) {
                Tessellator t = Tessellator.instance;
                GL11.glPushMatrix();
                GL11.glEnable(GL11.GL_BLEND);
                GL11.glDepthMask(false);
                GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
                GL11.glAlphaFunc(GL11.GL_GREATER, 0.003921569F);
                GL11.glDisable(GL11.GL_TEXTURE_2D);
                GL11.glColor4f(0, 0, 0, 0.75f);
                t.startDrawingQuads();
                int baseY = -40;
                int x = 5;
                int y = chat.height / 2 - 140 + baseY;
                int xSize = 220;
                int ySize = 150 + baseY;
                t.addVertexWithUV(x, y + ySize, 1, 0, 1.0F);
                t.addVertexWithUV(x + xSize, y + ySize, 1, 1, 1.0F);
                t.addVertexWithUV(x + xSize, y, 1, 1, 0.0F);
                t.addVertexWithUV(x, y, 1, 0, 0.0F);
                t.draw();
                GL11.glDepthMask(true);
                GL11.glAlphaFunc(GL11.GL_GREATER, 0.1F);
                GL11.glEnable(GL11.GL_TEXTURE_2D);
                GL11.glDisable(GL11.GL_BLEND);
                GL11.glColor4f(1, 1, 1, 1f);
                GL11.glPopMatrix();
                mc.fontRenderer.drawString("Настройка модели", 75, chat.height / 2f - 130 + baseY, Integer.MAX_VALUE, false);
                mc.fontRenderer.drawString("Позиция", 16, chat.height / 2f - 116 + baseY, Integer.MAX_VALUE, false);
                mc.fontRenderer.drawString("Поворот", 96, chat.height / 2f - 116 + baseY, Integer.MAX_VALUE, false);
                mc.fontRenderer.drawString("Размер", 176, chat.height / 2f - 116 + baseY, Integer.MAX_VALUE, false);

                for (GuiTextField listInput : listInputs) listInput.drawTextBox();
            }
        } else if(event.getType() == EventChatGui.EnumChatEventType.KEY_TYPED) {
            int key = event.getKey();
            char character = event.getKeyCharacter();
            if (key != 1) {
                if (key != 28 && key != 156) {
                    if (key != 200 && key != 208 && key != 201 && key != 209) {
                        if (mc.thePlayer.getCurrentEquippedItem() != null && mc.thePlayer.getCurrentEquippedItem().getItem() instanceof ItemBlockModel
                                && PlacementHighlightHandler.modelPreview != null) {

                            for (GuiTextField listInput : listInputs) listInput.textboxKeyTyped(character, key);

                            try {
                                RenderMetroModelPreview preview = PlacementHighlightHandler.modelPreview;
                                preview.getAddPos().x = Float.parseFloat(this.posX.getText());
                                preview.getAddPos().y = Float.parseFloat(this.posY.getText());
                                preview.getAddPos().z = Float.parseFloat(this.posZ.getText());

                                preview.getRotation().x = Float.parseFloat(this.rotX.getText());
                                preview.getRotation().y = Float.parseFloat(this.rotY.getText());
                                preview.getRotation().z = Float.parseFloat(this.rotZ.getText());

                                preview.getScale().x = Float.parseFloat(this.scaleX.getText());
                                preview.getScale().y = Float.parseFloat(this.scaleY.getText());
                                preview.getScale().z = Float.parseFloat(this.scaleZ.getText());

                                if (preview.getScale().x > 2)
                                    preview.getScale().x = 2;
                                else if (preview.getScale().x < 0.25f)
                                    preview.getScale().x = 0.25f;
                                if (preview.getScale().y > 2)
                                    preview.getScale().y = 2;
                                else if (preview.getScale().y < 0.25f)
                                    preview.getScale().y = 0.25f;
                                if (preview.getScale().z > 2)
                                    preview.getScale().z = 2;
                                else if (preview.getScale().z < 0.25f)
                                    preview.getScale().z = 0.25f;
                            } catch (Exception e) {
                                mc.thePlayer.addChatMessage(new ChatComponentText(EnumChatFormatting.RED + "Ошибка формата данных"));
                            }
                        }
                    }
                }
            }
        } else if(event.getType() == EventChatGui.EnumChatEventType.MOUSE_CLICKED) {
            for (GuiTextField listInput : listInputs) listInput.mouseClicked(event.getMouseX(), event.getMouseY(), event.getMouseButton());
        }
    }

    public void updateModelInfo() {
        NumberFormat formatter = new DecimalFormat("#0.00");

        String modelPosX = formatter.format(PlacementHighlightHandler.modelPreview.getAddPos().x);
        String modelPosY = formatter.format(PlacementHighlightHandler.modelPreview.getAddPos().y);
        String modelPosZ = formatter.format(PlacementHighlightHandler.modelPreview.getAddPos().z);
        if (posX != null) {
            this.posX.setText(modelPosX.replace(",", "."));
            this.posY.setText(modelPosY.replace(",", "."));
            this.posZ.setText(modelPosZ.replace(",", "."));

            modelPosX = formatter.format(PlacementHighlightHandler.modelPreview.getRotation().x);
            modelPosY = formatter.format(PlacementHighlightHandler.modelPreview.getRotation().y);
            modelPosZ = formatter.format(PlacementHighlightHandler.modelPreview.getRotation().z);
            this.rotX.setText(modelPosX.replace(",", "."));
            this.rotY.setText(modelPosY.replace(",", "."));
            this.rotZ.setText(modelPosZ.replace(",", "."));

            modelPosX = formatter.format(PlacementHighlightHandler.modelPreview.getScale().x);
            modelPosY = formatter.format(PlacementHighlightHandler.modelPreview.getScale().y);
            modelPosZ = formatter.format(PlacementHighlightHandler.modelPreview.getScale().z);
            this.scaleX.setText(modelPosX.replace(",", "."));
            this.scaleY.setText(modelPosY.replace(",", "."));
            this.scaleZ.setText(modelPosZ.replace(",", "."));
        }
    }
}
