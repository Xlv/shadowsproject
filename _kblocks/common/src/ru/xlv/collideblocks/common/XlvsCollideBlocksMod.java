package ru.xlv.collideblocks.common;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import ru.xlv.collideblocks.common.block.BlockCollider;
import ru.xlv.collideblocks.common.item.ItemBlockCollide;
import ru.xlv.collideblocks.common.tile.TileEntityBlockCollide;

import static ru.xlv.collideblocks.common.XlvsCollideBlocksMod.MODID;

@Mod(
        name = MODID,
        modid = MODID,
        version = "1.0"
)
public class XlvsCollideBlocksMod {

    static final String MODID = "cldblockscmn";

    @Mod.Instance(MODID)
    public static XlvsCollideBlocksMod INSTANCE;

    public static final Block BLOCK_COLLIDE = new BlockCollider();
    public static final Item ITEM_COLLIDE_BLOCK = new ItemBlockCollide();

    @Mod.EventHandler
    public void event(FMLInitializationEvent event) {
        GameRegistry.registerBlock(BLOCK_COLLIDE, BLOCK_COLLIDE.getUnlocalizedName());
        GameRegistry.registerItem(ITEM_COLLIDE_BLOCK, ITEM_COLLIDE_BLOCK.getUnlocalizedName());
        GameRegistry.registerTileEntity(TileEntityBlockCollide.class, "TileEntityBlockCollide");
    }
}
