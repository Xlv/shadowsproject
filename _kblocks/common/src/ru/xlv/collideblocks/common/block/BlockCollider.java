package ru.xlv.collideblocks.common.block;

import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;
import ru.xlv.collideblocks.common.XlvsCollideBlocksMod;
import ru.xlv.collideblocks.common.tile.TileEntityBlockCollide;

public class BlockCollider extends BlockContainer {

	public TileEntityBlockCollide tile;

	public BlockCollider() {
		super(Material.grass);
		this.setBlockName("blockCollide");
		this.setCreativeTab(CreativeTabs.tabMisc);
	}

	public AxisAlignedBB getCollisionBoundingBoxFromPool(World w, int x, int y, int z) {
		TileEntity tileEntity = w.getTileEntity(x, y, z);
		if(tileEntity instanceof TileEntityBlockCollide) {
			TileEntityBlockCollide tile = (TileEntityBlockCollide) tileEntity;
			if (w.isRemote) {
//			if (!ClientEventsUI.isDebugging)
//				setBlockBounds(0, 0, 0, 0, 0, 0);
//			else
				setBlockBounds(0, 0, 0, 1, 1, 1);
			} else {
//			setBlockBounds(0, 0, 0, 0, 0, 0);
			}
			return AxisAlignedBB.getBoundingBox((double) x + tile.minX, (double) y + tile.minY, (double) z + tile.minZ, (double) x + tile.maxX, (double) y + tile.maxY, (double) z + tile.maxZ);
		}
		return AxisAlignedBB.getBoundingBox((double) x + this.minX, (double) y + this.minY, (double) z + this.minZ, (double) x + this.maxX, (double) y + this.maxY, (double) z + this.maxZ);
	}


	public ItemStack getPickBlock(MovingObjectPosition target, World w, int x, int y, int z, EntityPlayer player) {
		ItemStack is = new ItemStack(XlvsCollideBlocksMod.BLOCK_COLLIDE);

		NBTTagCompound tag = new NBTTagCompound();
		TileEntityBlockCollide tile = (TileEntityBlockCollide) w.getTileEntity(x, y, z);
		tile.safeWrite(tag);
		is.setTagCompound(tag);

		return is;
	}

	public boolean onBlockActivated(World w, int x, int y, int z, EntityPlayer p, int side, float sx, float sy, float sz) {
		if (!p.capabilities.isCreativeMode)
			return false;
		if (p.getCurrentEquippedItem() != null)
			return false;
		double mode = 1 / 16f;
		if (!p.isSneaking())
			mode = -mode;

		TileEntityBlockCollide tile = (TileEntityBlockCollide) w.getTileEntity(x, y, z);
		if (side == 0 && tile.minY + mode >= 0 && tile.minY + mode <= 1)
			tile.minY += mode;
		else if (side == 1 && tile.maxY + mode <= 1 && tile.maxY + mode >= 0)
			tile.maxY += mode;
		else if (side == 2 && tile.minZ + mode >= 0 && tile.minZ + mode <= 1)
			tile.minZ += mode;
		else if (side == 3 && tile.maxZ + mode <= 1 && tile.maxZ + mode >= 0)
			tile.maxZ += mode;
		else if (side == 4 && tile.minX + mode >= 0 && tile.minX + mode <= 1)
			tile.minX += mode;
		else if (side == 5 && tile.maxX + mode <= 1 && tile.maxX + mode >= 0)
			tile.maxX += mode;
		tile.markDirty();
		return false;
	}

	public boolean isNormalCube() {
		return false;
	}

	public boolean renderAsNormalBlock() {
		return false;
	}

	public int getRenderType() {
		return -1;
	}

	public boolean isOpaqueCube() {
		return false;
	}

	@Override
	public TileEntity createNewTileEntity(World w, int i) {
		if (tile != null)
			return tile;
		else
			return new TileEntityBlockCollide();
	}

	@Override
	public void registerBlockIcons(IIconRegister p_149651_1_) {

	}
}
