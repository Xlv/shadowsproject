package ru.krogenit.advancedblockmodels.block;

import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import ru.krogenit.advancedblockmodels.block.tileentity.TEBlockModel;
import ru.krogenit.advancedblockmodels.block.tileentity.TEModel;

import java.util.List;

public class BlockModel extends BlockContainer {

	public enum ModelRenderType {
		ADVANCED((byte) 0), STANDARD((byte) 1);

		private final byte type;

		ModelRenderType(byte type) {
			this.type = type;
		}

		public byte toByte() {
			return this.type;
		}
	}

	public enum ModelCollideType {
		ADVANCED((byte) 0), STANDARD((byte) 1), EMPTY((byte) 2),
		ADVANCED_NO_COLLISION((byte) 3);

		private final byte type;

		ModelCollideType(byte type) {
			this.type = type;
		}

		public byte toByte() {
			return this.type;
		}

		public String toString() {
			if (type == ADVANCED.toByte()) return "Расширенный";
			else if (type == STANDARD.toByte()) return "Стандартный";
			else if (type == EMPTY.toByte()) return "Проходимый";
			else return "Расширенный без проверки касания";
		}
	}

	public BlockModel(Material mat, boolean isBreakable, String name) {
		super(mat);
		setBlockName(name);
		if (!isBreakable) {
			setBlockUnbreakable();
		}
		if (mat == Material.wood) {
			setStepSound(Block.soundTypeWood);
		} else if (mat == Material.rock) {
			setStepSound(Block.soundTypeStone);
		} else if (mat == Material.grass) {
			setStepSound(Block.soundTypeGrass);
		} else if (mat == Material.snow) {
			setStepSound(Block.soundTypeSnow);
		} else if (mat == Material.sand) {
			setStepSound(Block.soundTypeSand);
		} else if (mat == Material.glass) {
			setStepSound(Block.soundTypeGlass);
		} else if (mat == Material.iron) {
			setStepSound(Block.soundTypeMetal);
		}
	}

	@Override
	public void addCollisionBoxesToList(World w, int x, int y, int z, AxisAlignedBB blockBounds, List list, Entity collidingEntity) {
		TileEntity tile = w.getTileEntity(x, y, z);
		if (tile instanceof TEBlockModel) {
			TEBlockModel te = (TEBlockModel) tile;
			addCollisionBoxesToListAdvanced(te, w, x, y, z, blockBounds, list, collidingEntity);
		}
	}

	private void addCollisionBoxesToListAdvanced(TEBlockModel te, World w, int x, int y, int z, AxisAlignedBB blockBounds, List list, Entity collidingEntity) {
			for (TEModel part : te.getChildParts()) {
				if(part == null) continue;
				ModelCollideType type = part.getType();
				if (type == ModelCollideType.ADVANCED) {
					TEBlockModel te1 = part.getTe();
					AxisAlignedBB axisalignedbb1 = part.getBoundingBoxForCollision(te1.xCoord, te1.yCoord, te1.zCoord);
					if (axisalignedbb1 != null && blockBounds.intersectsWith(axisalignedbb1)) {
						list.add(axisalignedbb1);
					}
				} else if (type == ModelCollideType.STANDARD) {
					AxisAlignedBB axisalignedbb1 = this.getCollisionBoundingBoxFromPool(w, x, y, z);

					if (axisalignedbb1 != null && blockBounds.intersectsWith(axisalignedbb1)) {
						list.add(axisalignedbb1);
					}
				}
			}
			for (TEModel part : te.getParts()) {
				ModelCollideType type = part.getType();
				if (type == ModelCollideType.ADVANCED) {
					AxisAlignedBB axisalignedbb1 = part.getBoundingBoxForCollision(x, y, z);
					if (axisalignedbb1 != null && blockBounds.intersectsWith(axisalignedbb1)) {
						list.add(axisalignedbb1);
					}
				} else if (type == ModelCollideType.STANDARD) {
					AxisAlignedBB axisalignedbb1 = this.getCollisionBoundingBoxFromPool(w, x, y, z);

					if (axisalignedbb1 != null && blockBounds.intersectsWith(axisalignedbb1)) {
						list.add(axisalignedbb1);
					}
				}
			}
	}

	@Override
	public AxisAlignedBB getSelectedBoundingBoxFromPool(World par1World, int par2, int par3, int par4) {
		return AxisAlignedBB.getBoundingBox((double) par2 + this.minX, (double) par3 + this.minY, (double) par4 + this.minZ, (double) par2 + this.maxX, (double) par3 + this.maxY, (double) par4 + this.maxZ);
	}

	@Override
	public AxisAlignedBB getCollisionBoundingBoxFromPool(World w, int x, int y, int z) {
		return super.getCollisionBoundingBoxFromPool(w, x, y, z);
	}

	@Override
	public final MovingObjectPosition collisionRayTrace(World w, int x, int y, int z, Vec3 src, Vec3 dst) {
		TileEntity tile = w.getTileEntity(x, y, z);
		if (tile instanceof TEBlockModel) {
			TEBlockModel te = (TEBlockModel) tile;
			return collisionRayTraceAdvanced(te, w, x, y, z, src, dst);
		}
		return null;
	}

	private MovingObjectPosition collisionRayTraceAdvanced(TEBlockModel te, World w, int x, int y, int z, Vec3 src, Vec3 dst) {
		TEModel part = null;
		Vec3 src1 = src.addVector(-x, -y, -z);
		Vec3 dst1 = dst.addVector(-x, -y, -z);
		double best = dst1.squareDistanceTo(src1) + 1;
		MovingObjectPosition hitInfo = null;
		List<TEModel> parts = te.getParts();
		for (TEModel p : parts) {
			ModelCollideType type = p.getType();
			if (type == ModelCollideType.ADVANCED) {
				AxisAlignedBB aabb = p.getBoundingBoxLocal();
				if (aabb != null) {
					MovingObjectPosition rt = aabb.calculateIntercept(src1, dst1);
					if (rt != null) {
						double rtdist = rt.hitVec.squareDistanceTo(src1);
						if (rtdist < best) {
							hitInfo = rt;
							best = rtdist;
							part = p;
						}
					}
				}
			} else if (type == ModelCollideType.STANDARD || type == ModelCollideType.EMPTY) {
				AxisAlignedBB aabb = super.getCollisionBoundingBoxFromPool(w, x, y, z);
				if (aabb != null) {
					AxisAlignedBB aabb1 = AxisAlignedBB.getBoundingBox(aabb.minX - x, aabb.minY - y, aabb.minZ - z, aabb.maxX - x, aabb.maxY - y, aabb.maxZ - z);
					MovingObjectPosition rt = aabb1.calculateIntercept(src1, dst1);
					if (rt != null) {
						double rtdist = rt.hitVec.squareDistanceTo(src1);
						if (rtdist < best) {
							hitInfo = rt;
							best = rtdist;
							part = p;
						}
					}
				}
			}
		}

		for (TEModel p : te.getChildParts()) {
			if(p == null) continue;
			ModelCollideType type = p.getType();
			if (type == ModelCollideType.ADVANCED) {
				TEBlockModel te1 = p.getTe();
				AxisAlignedBB aabb = p.getBoundingBox(te1.xCoord - x, te1.yCoord - y, te1.zCoord - z);
				if (aabb != null) {
					MovingObjectPosition rt = aabb.calculateIntercept(src1, dst1);
					if (rt != null) {
						double rtdist = rt.hitVec.squareDistanceTo(src1);
						if (rtdist < best) {
							hitInfo = rt;
							best = rtdist;
							part = p;
						}
					}
				}
			} else if (type == ModelCollideType.STANDARD || type == ModelCollideType.EMPTY) {
				TEBlockModel te1 = p.getTe();
				AxisAlignedBB aabb = super.getCollisionBoundingBoxFromPool(w, te1.xCoord - x, te1.yCoord - y, te1.zCoord - z);
				if (aabb != null) {
					MovingObjectPosition rt = aabb.calculateIntercept(src1, dst1);
					if (rt != null) {
						double rtdist = rt.hitVec.squareDistanceTo(src1);
						if (rtdist < best) {
							hitInfo = rt;
							best = rtdist;
							part = p;
						}
					}
				}
			}
		}

//		MovingObjectPosition rt = super.collisionRayTrace(w, x, y, z, src, dst);
//		if (rt != null) {
//			double rtdist = rt.hitVec.squareDistanceTo(src);
//			if (rtdist < best) {
//				return rt;
//			}
//		}

		if (hitInfo == null) return null;

		MovingObjectPosition pos = new MovingObjectPosition(x, y, z, hitInfo.sideHit, hitInfo.hitVec.addVector(x, y, z));
		pos.hitInfo = part;
		return pos;
	}

	@Override
	public boolean removedByPlayer(World w, EntityPlayer p, int x, int y, int z) {
		return false;
	}

	public static boolean isTEHaveObject(TEBlockModel te) {
		return te.getChildParts().size() > 0;
	}
	
	@Override
	public boolean isReplaceable(IBlockAccess world, int x, int y, int z) {
		TileEntity tile = world.getTileEntity(x, y, z);
		if(tile instanceof TEBlockModel) {
			TEBlockModel te = (TEBlockModel) tile;
			return te.getParts().size() <= 0 && te.getChildParts().size() <= 0;
		}
		
		return true;
	}

	@Override
	public TileEntity createNewTileEntity(World p_149915_1_, int p_149915_2_) {
		return new TEBlockModel();
	}

	@Override
	public boolean isOpaqueCube() {
		return false;
	}

	@Override
	public boolean renderAsNormalBlock() {
		return false;
	}
}