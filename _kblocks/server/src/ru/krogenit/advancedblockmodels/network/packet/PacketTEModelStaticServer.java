package ru.krogenit.advancedblockmodels.network.packet;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import org.lwjgl.util.vector.Vector3f;
import ru.krogenit.advancedblockmodels.item.ItemBlockModelServer;
import ru.xlv.core.common.network.IPacketInOnServer;
import ru.xlv.core.common.util.ByteBufInputStream;

import java.io.IOException;

public class PacketTEModelStaticServer implements IPacketInOnServer {

    private int x, y, z, side;
    private float hitX, hitY, hitZ;
    private Vector3f scale;

    @Override
    public void read(EntityPlayer p, ByteBufInputStream data) throws IOException {
        x = data.readInt();
        y = data.readInt();
        z = data.readInt();
        side = data.readInt();

        hitX = data.readFloat();
        hitY = data.readFloat();
        hitZ = data.readFloat();

        scale = new Vector3f(data.readFloat(), data.readFloat(), data.readFloat());
        handleMessageOnServerSide(p);
    }

    public void handleMessageOnServerSide(EntityPlayer p) {
        World w = p.worldObj;

        ItemStack item = p.getCurrentEquippedItem();
        if (item != null && item.getItem() instanceof ItemBlockModelServer) {
            ItemBlockModelServer modelBlock = (ItemBlockModelServer) item.getItem();
            modelBlock.standartPlaceBlock(item, p, w, x, y, z, side, hitX,
                    hitY, hitZ, scale);
        }
    }
}
