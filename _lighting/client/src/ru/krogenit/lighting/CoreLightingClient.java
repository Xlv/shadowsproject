package ru.krogenit.lighting;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import net.minecraftforge.common.MinecraftForge;
import ru.krogenit.client.TickHandlerCore;
import ru.krogenit.lighting.item.ItemLightPlacerClient;
import ru.krogenit.lighting.network.*;

@Mod(modid = CoreLightingCommon.MODID, name = "Lighting Manager", version = "1.0.0")
@SuppressWarnings("unused")
public class CoreLightingClient {

    @Instance(CoreLightingCommon.MODID)
    public static CoreLightingClient instance;

    @EventHandler
    public void event(FMLPreInitializationEvent event) {
        PacketDispatcherLighting.registerMessage(PacketUpdateLightHandlerClient.class, PacketUpdateLight.class, Side.CLIENT);
        PacketDispatcherLighting.registerMessage(PacketUpdateBlockLightHandlerClient.class, PacketUpdateBlockLight.class, Side.CLIENT);
        MinecraftForge.EVENT_BUS.register(new GuiLightingEvents());
        FMLCommonHandler.instance().bus().register(new TickHandlerCore());
        FMLCommonHandler.instance().bus().register(new ClientTickHandlerLighting());
        MinecraftForge.EVENT_BUS.register(new RenderWorldLastLightingEvent());
    }

    @EventHandler
    public void event(FMLInitializationEvent e) {
        LightManager.registerLights();
        registerItem();
    }

    private void registerItem() {
        CoreLightingCommon.itemLightPlacer = new ItemLightPlacerClient();
        GameRegistry.registerItem(CoreLightingCommon.itemLightPlacer, "itemLightPlacer");
    }
}
