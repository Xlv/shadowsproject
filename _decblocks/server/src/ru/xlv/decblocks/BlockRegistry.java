package ru.xlv.decblocks;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import ru.xlv.decblocks.block.BlockBase;
import ru.xlv.decblocks.block.BlockSlab;
import ru.xlv.decblocks.block.BlockWall;

import java.lang.reflect.Field;

public class BlockRegistry {

    public static final Block block = new BlockBase("sp_block", Material.rock);
    public static final Block block1 = new BlockBase("sp_block1", Material.rock);
    public static final Block block2 = new BlockBase("sp_block2", Material.rock);
    public static final Block block3 = new BlockBase("sp_block3", Material.rock);
    public static final Block block4 = new BlockBase("sp_block4", Material.rock);
    public static final Block block5 = new BlockBase("sp_block5", Material.rock);
    public static final Block block6 = new BlockBase("sp_block6", Material.rock);
    public static final Block block7 = new BlockBase("sp_block7", Material.rock);
    public static final Block block8 = new BlockBase("sp_block8", Material.rock);
    public static final Block block9 = new BlockBase("sp_block9", Material.rock);
    public static final Block block0 = new BlockBase("sp_block0", Material.rock);

    public static final Block blockSlab = new BlockSlab("sp_slab", false, block, null);
    public static final Block blockSlab1 = new BlockSlab("sp_slab1", false, block1, null);
    public static final Block blockSlab2 = new BlockSlab("sp_slab2", false, block2, null);
    public static final Block blockSlab3 = new BlockSlab("sp_slab3", false, block3, null);
    public static final Block blockSlab4 = new BlockSlab("sp_slab4", false, block4, null);
    public static final Block blockSlab5 = new BlockSlab("sp_slab5", false, block5, null);
    public static final Block blockSlab6 = new BlockSlab("sp_slab6", false, block6, null);
    public static final Block blockSlab7 = new BlockSlab("sp_slab7", false, block7, null);
    public static final Block blockSlab8 = new BlockSlab("sp_slab8", false, block8, null);
    public static final Block blockSlab9 = new BlockSlab("sp_slab9", false, block9, null);
    public static final Block blockSlab0 = new BlockSlab("sp_slab0", false, block0, null);

    public static final Block doubleBlockSlab = new BlockSlab("double_sp_slab", true, block, (BlockSlab) blockSlab);
    public static final Block doubleBlockSlab1 = new BlockSlab("double_sp_slab1", true, block1, (BlockSlab) blockSlab1);
    public static final Block doubleBlockSlab2 = new BlockSlab("double_sp_slab2", true, block2, (BlockSlab) blockSlab2);
    public static final Block doubleBlockSlab3 = new BlockSlab("double_sp_slab3", true, block3, (BlockSlab) blockSlab3);
    public static final Block doubleBlockSlab4 = new BlockSlab("double_sp_slab4", true, block4, (BlockSlab) blockSlab4);
    public static final Block doubleBlockSlab5 = new BlockSlab("double_sp_slab5", true, block5, (BlockSlab) blockSlab5);
    public static final Block doubleBlockSlab6 = new BlockSlab("double_sp_slab6", true, block6, (BlockSlab) blockSlab6);
    public static final Block doubleBlockSlab7 = new BlockSlab("double_sp_slab7", true, block7, (BlockSlab) blockSlab7);
    public static final Block doubleBlockSlab8 = new BlockSlab("double_sp_slab8", true, block8, (BlockSlab) blockSlab8);
    public static final Block doubleBlockSlab9 = new BlockSlab("double_sp_slab9", true, block9, (BlockSlab) blockSlab9);
    public static final Block doubleBlockSlab0 = new BlockSlab("double_sp_slab0", true, block0, (BlockSlab) blockSlab0);

    public static final Block wall = new BlockWall("sp_wall", block);
    public static final Block wall1 = new BlockWall("sp_wall1", block1);
    public static final Block wall2 = new BlockWall("sp_wall2", block2);
    public static final Block wall3 = new BlockWall("sp_wall3", block3);
    public static final Block wall4 = new BlockWall("sp_wall4", block4);
    public static final Block wall5 = new BlockWall("sp_wall5", block5);
    public static final Block wall6 = new BlockWall("sp_wall6", block6);
    public static final Block wall7 = new BlockWall("sp_wall7", block7);
    public static final Block wall8 = new BlockWall("sp_wall8", block8);
    public static final Block wall9 = new BlockWall("sp_wall9", block9);
    public static final Block wall0 = new BlockWall("sp_wall0", block0);

    public static void init() {
        System.out.println("[XlvsDecBlocks] registering blocks...");
        for (Field field : BlockRegistry.class.getFields()) {
            try {
                registerBlock((Block) field.get(null));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    private static void registerBlock(Block block) {
        GameRegistry.registerBlock(block, block.getUnlocalizedName());
    }
}
