package ru.xlv.training.common.trainer;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ru.xlv.core.common.util.WorldPosition;

@Getter
@RequiredArgsConstructor
public class MarkerTrainer implements ITrainer {

    private final WorldPosition worldPosition;
    private final double radius;
    private final String description;

    @Override
    public ITrainer clone() {
        return new MarkerTrainer(new WorldPosition(worldPosition.getDimension(), worldPosition.getX(), worldPosition.getY(), worldPosition.getZ()), radius, description);
    }
}
