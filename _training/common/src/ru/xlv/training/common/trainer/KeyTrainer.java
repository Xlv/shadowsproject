package ru.xlv.training.common.trainer;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class KeyTrainer implements ITrainer {

    private final int keyCode;
    private final String description;

    @Override
    public ITrainer clone() {
        return new KeyTrainer(keyCode, description);
    }
}
