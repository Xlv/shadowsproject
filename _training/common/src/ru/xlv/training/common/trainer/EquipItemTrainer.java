package ru.xlv.training.common.trainer;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.minecraft.item.Item;

@Getter
@RequiredArgsConstructor
public class EquipItemTrainer implements ITrainer {

    private final Item item;
    private final String description;

    @Override
    public ITrainer clone() {
        return new EquipItemTrainer(item, description);
    }
}
