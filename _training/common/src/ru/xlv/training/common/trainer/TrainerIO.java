package ru.xlv.training.common.trainer;

import cpw.mods.fml.common.network.ByteBufUtils;
import io.netty.buffer.ByteBufOutputStream;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.core.common.util.WorldPosition;

import java.io.IOException;

public class TrainerIO {

    public static void write(ITrainer trainer, ByteBufOutputStream byteBufOutputStream) throws IOException {
        if(trainer != null) {
            byteBufOutputStream.writeUTF(trainer.getDescription());
        }
        if(trainer == null) {
            byteBufOutputStream.writeUTF("");
            byteBufOutputStream.writeInt(-1);
        } else if(trainer instanceof MarkerTrainer) {
            byteBufOutputStream.writeByte(0);
            byteBufOutputStream.writeInt(((MarkerTrainer) trainer).getWorldPosition().getDimension());
            byteBufOutputStream.writeDouble(((MarkerTrainer) trainer).getWorldPosition().getX());
            byteBufOutputStream.writeDouble(((MarkerTrainer) trainer).getWorldPosition().getY());
            byteBufOutputStream.writeDouble(((MarkerTrainer) trainer).getWorldPosition().getZ());
            byteBufOutputStream.writeDouble(((MarkerTrainer) trainer).getRadius());
        } else if(trainer instanceof EquipItemTrainer) {
            byteBufOutputStream.writeByte(1);
            byteBufOutputStream.writeInt(Item.getIdFromItem(((EquipItemTrainer) trainer).getItem()));
        } else if(trainer instanceof ItemStackTrainer) {
            byteBufOutputStream.writeByte(2);
            ByteBufUtils.writeItemStack(byteBufOutputStream.buffer(), ((ItemStackTrainer) trainer).getItemStack());
        } else if(trainer instanceof KeyTrainer) {
            byteBufOutputStream.writeByte(3);
            byteBufOutputStream.writeInt(((KeyTrainer) trainer).getKeyCode());
        } else if(trainer instanceof NpcInteractTrainer) {
            byteBufOutputStream.writeByte(4);
            byteBufOutputStream.writeInt(((NpcInteractTrainer) trainer).getX());
            byteBufOutputStream.writeInt(((NpcInteractTrainer) trainer).getY());
            byteBufOutputStream.writeInt(((NpcInteractTrainer) trainer).getZ());
            byteBufOutputStream.writeUTF(((NpcInteractTrainer) trainer).getNpcName());
        } else if(trainer instanceof SkillLearnTrainer) {
            byteBufOutputStream.writeByte(5);
            byteBufOutputStream.writeInt(((SkillLearnTrainer) trainer).getSkillId());
        } else if(trainer instanceof SkillUseTrainer) {
            byteBufOutputStream.writeByte(6);
            byteBufOutputStream.writeInt(((SkillUseTrainer) trainer).getSkillId());
        } else if(trainer instanceof SkillSelectTrainer) {
            byteBufOutputStream.writeByte(7);
            byteBufOutputStream.writeInt(((SkillSelectTrainer) trainer).getSkillId());
        }
    }

    public static ITrainer read(ByteBufInputStream byteBufInputStream) throws IOException {
        String description = byteBufInputStream.readUTF();
        switch (byteBufInputStream.readByte()) {
            case 0: {
                int dim = byteBufInputStream.readInt();
                double x = byteBufInputStream.readDouble();
                double y = byteBufInputStream.readDouble();
                double z = byteBufInputStream.readDouble();
                double radius = byteBufInputStream.readDouble();
                return new MarkerTrainer(new WorldPosition(dim, x, y, z), radius, description);
            }
            case 1:
                Item item = Item.getItemById(byteBufInputStream.readInt());
                return new EquipItemTrainer(item, description);
            case 2:
                ItemStack itemStack = ByteBufUtils.readItemStack(byteBufInputStream.getBuffer());
                return new ItemStackTrainer(itemStack, description);
            case 3:
                int keyCode = byteBufInputStream.readInt();
                return new KeyTrainer(keyCode, description);
            case 4:
                String entityName = byteBufInputStream.readUTF();
                int x = byteBufInputStream.readInt();
                int y = byteBufInputStream.readInt();
                int z = byteBufInputStream.readInt();
                return new NpcInteractTrainer(entityName, x, y, z, description);
            case 5: {
                int skillId = byteBufInputStream.readInt();
                return new SkillLearnTrainer(skillId, description);
            }
            case 6: {
                int skillId = byteBufInputStream.readInt();
                return new SkillUseTrainer(skillId, description);
            }
            case 7: {
                int skillId = byteBufInputStream.readInt();
                return new SkillSelectTrainer(skillId, description);
            }
        }
        return null;
    }
}
