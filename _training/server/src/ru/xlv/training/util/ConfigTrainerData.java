package ru.xlv.training.util;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ru.xlv.core.common.util.config.Configurable;

@Getter
@RequiredArgsConstructor
@Configurable
public class ConfigTrainerData {

    public enum TrainerType {
        ITEM_USE, KEY_PRESS, MARKER, EQUIP_ITEM, INTERACT_NPC, SHOOT, SKILL_USE, SKILL_LEARN, SKILL_SELECT
    }

    private final TrainerType trainerType;
    private final Object[] params;
    private final String description;
}
