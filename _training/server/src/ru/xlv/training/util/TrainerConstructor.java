package ru.xlv.training.util;

import ru.xlv.core.common.util.WorldPosition;
import ru.xlv.core.item.ItemStackFactory;
import ru.xlv.mochar.util.Utils;
import ru.xlv.training.common.trainer.*;

public class TrainerConstructor {

    private int counter;

    public ITrainer construct(ConfigTrainerData configTrainerData) {
        String description = configTrainerData.getDescription();
        switch (configTrainerData.getTrainerType()) {
            case MARKER: {
                int dim = getNextParam(configTrainerData, int.class);
                double x = getNextParam(configTrainerData, double.class);
                double y = getNextParam(configTrainerData, double.class);
                double z = getNextParam(configTrainerData, double.class);
                double radius = getNextParam(configTrainerData, double.class);
                return new MarkerTrainer(new WorldPosition(dim, x, y, z), radius, description);
            }
            case ITEM_USE: {
                String unlocalizedName = getNextParam(configTrainerData, String.class);
                int amount = getNextParam(configTrainerData, int.class);
                return new ItemStackTrainer(ItemStackFactory.create(unlocalizedName, amount), description);
            }
            case KEY_PRESS: {
                int keyCode = getNextParam(configTrainerData, int.class);
                return new KeyTrainer(keyCode, description);
            }
            case EQUIP_ITEM: {
                String unlocalizedName = getNextParam(configTrainerData, String.class);
                return new EquipItemTrainer(Utils.getItemByUnlocalizedName(unlocalizedName), description);
            }
            case SKILL_USE: {
                int skillId = getNextParam(configTrainerData, int.class);
                return new SkillUseTrainer(skillId, description);
            }
            case SKILL_LEARN: {
                int skillId = getNextParam(configTrainerData, int.class);
                return new SkillLearnTrainer(skillId, description);
            }
            case SKILL_SELECT: {
                int skillId = getNextParam(configTrainerData, int.class);
                return new SkillSelectTrainer(skillId, description);
            }
            case INTERACT_NPC: {
                String entityName = getNextParam(configTrainerData, String.class);
                int x = getNextParam(configTrainerData, int.class);
                int y = getNextParam(configTrainerData, int.class);
                int z = getNextParam(configTrainerData, int.class);
                return new NpcInteractTrainer(entityName, x, y, z, description);
            }
            case SHOOT: {

            }
        }
        return null;
    }

    private <T> T getNextParam(ConfigTrainerData configTrainerData, Class<T> tClass) {
        return tClass.cast(configTrainerData.getParams()[counter++]);
    }
}
