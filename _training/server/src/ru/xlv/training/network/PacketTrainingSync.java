package ru.xlv.training.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.common.network.IPacketOutServer;
import ru.xlv.training.common.trainer.ITrainer;
import ru.xlv.training.common.trainer.TrainerIO;

import java.io.IOException;

@NoArgsConstructor
@AllArgsConstructor
public class PacketTrainingSync implements IPacketOutServer {

    private ITrainer trainer;

    @Override
    public void write(EntityPlayer entityPlayer, ByteBufOutputStream bbos) throws IOException {
        TrainerIO.write(trainer, bbos);
    }
}
