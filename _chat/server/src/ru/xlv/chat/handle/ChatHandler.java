package ru.xlv.chat.handle;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;
import ru.xlv.chat.common.ChatChannelType;
import ru.xlv.chat.network.PacketChatMessage;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.network.IPacketOutServer;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.core.util.flex.FlexPlayerList;
import ru.xlv.friend.XlvsFriendMod;
import ru.xlv.group.XlvsGroupMod;
import ru.xlv.group.handle.GroupServer;

public class ChatHandler {

    public void handleMessage(EntityPlayer entityPlayer, String message, ChatChannelType chatChannelType) {
        if(message.startsWith("/")) {
            MinecraftServer.getServer().getCommandManager().executeCommand(entityPlayer, message);
            return;
        }
        ServerPlayer serverPlayer = XlvsCore.INSTANCE.getPlayerManager().getPlayer(entityPlayer);
        if (serverPlayer != null) {
            ChatComponentText chatComponentText = new ChatComponentText(chatChannelType.formatMessage(entityPlayer.getDisplayName(), message));
            if(chatChannelType == ChatChannelType.GROUP || chatChannelType == ChatChannelType.FRIEND) {
                chatComponentText.getChatStyle().setColor(EnumChatFormatting.GREEN);
            }
            String compressedMessage = ChatComponentText.Serializer.func_150696_a(chatComponentText);
            IPacketOutServer packet = new PacketChatMessage(compressedMessage, chatChannelType.ordinal());
            switch (chatChannelType) {
                case GROUP:
                    GroupServer playerGroup = XlvsGroupMod.INSTANCE.getGroupHandler().getPlayerGroup(serverPlayer);
                    if (playerGroup != null) {
                        playerGroup.getPlayers().forEach(serverPlayer1 -> {
                            if(serverPlayer1 == serverPlayer) return;
                            XlvsCore.INSTANCE.getPacketHandler().sendPacketToPlayer(serverPlayer1.getEntityPlayer(), packet);
                        });
                    } else {
                        XlvsCore.INSTANCE.getPacketHandler().sendPacketToPlayer(serverPlayer.getEntityPlayer(), packet);
                    }
                    break;
                case FRIEND:
                    XlvsFriendMod.INSTANCE.getFriendHandler().getAllFriendNames(entityPlayer.getCommandSenderName())
                            .stream()
                            .map(XlvsCore.INSTANCE.getPlayerManager()::getPlayer)
                            .forEach(serverPlayer1 -> XlvsCore.INSTANCE.getPacketHandler().sendPacketToPlayer(serverPlayer1.getEntityPlayer(), packet));
                    XlvsCore.INSTANCE.getPacketHandler().sendPacketToPlayer(serverPlayer.getEntityPlayer(), packet);
                    break;
                case TRADE:
                case LOCATION:
                    FlexPlayerList.actual()
                            .onlineOnly()
                            .sendPacket(packet)
                            .filter(serverPlayer1 -> serverPlayer1.getEntityPlayer().capabilities.isCreativeMode)
                            .forEach(serverPlayer1 -> serverPlayer1.getEntityPlayer().addChatMessage(chatComponentText));
                    break;
                case AROUND:
                    FlexPlayerList.actual()
                            .onlineOnly()
                            .around(entityPlayer, 32D)
                            .sendPacket(packet);
            }
        }
    }
}
