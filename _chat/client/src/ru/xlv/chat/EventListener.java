package ru.xlv.chat;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiChat;
import net.minecraftforge.client.event.GuiOpenEvent;
import net.minecraftforge.client.event.MouseEvent;
import org.lwjgl.input.Keyboard;
import ru.xlv.chat.gui.GuiChannelChat;

public class EventListener {

    @SubscribeEvent
    @SuppressWarnings("unused")
    public void event(GuiOpenEvent event) {
        if(event.gui != null && event.gui.getClass() == GuiChat.class && !Minecraft.getMinecraft().thePlayer.capabilities.isCreativeMode) {
            event.gui = new GuiChannelChat(((GuiChat) event.gui).defaultInputFieldText);
        }
    }

    @SubscribeEvent
    @SuppressWarnings("unused")
    public void event(MouseEvent event) {
        Minecraft mc = Minecraft.getMinecraft();
        if(mc.currentScreen instanceof GuiChannelChat) {
            int dwheel = event.dwheel;
            if (dwheel != 0) {
                if (dwheel > 1) {
                    dwheel = 1;
                }

                if (dwheel < -1) {
                    dwheel = -1;
                }

                if (!Keyboard.isKeyDown(Keyboard.KEY_LSHIFT)) {
                    dwheel *= 7;
                }
                XlvsChatMod.INSTANCE.getChannelChatManager().getChat(GuiChannelChat.chatType).scroll(dwheel);
            }
        }
    }
}
