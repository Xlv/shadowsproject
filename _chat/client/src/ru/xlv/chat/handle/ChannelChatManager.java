package ru.xlv.chat.handle;

import net.minecraft.client.Minecraft;
import net.minecraft.util.IChatComponent;
import ru.xlv.chat.common.ChatChannelType;
import ru.xlv.chat.gui.GuiChannelNewChat;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class ChannelChatManager {

    private final Map<ChatChannelType, GuiChannelNewChat> chats = Collections.synchronizedMap(new HashMap<>());

    public ChannelChatManager() {
        for (ChatChannelType value : ChatChannelType.values()) {
            chats.put(value, new GuiChannelNewChat(Minecraft.getMinecraft()));
        }
    }

    public void printMessage(IChatComponent chatComponent, ChatChannelType chatChannelType) {
        chats.get(chatChannelType).printChatMessage(chatComponent);
    }

    public GuiChannelNewChat getChat(ChatChannelType chatChannelType) {
        return chats.get(chatChannelType);
    }
}
