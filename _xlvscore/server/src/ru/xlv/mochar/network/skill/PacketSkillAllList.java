package ru.xlv.mochar.network.skill;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.common.network.IPacketOutServer;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.mochar.player.character.skill.Skill;
import ru.xlv.mochar.player.character.skill.SkillHandler;

import java.io.IOException;
import java.util.List;

@NoArgsConstructor
public class PacketSkillAllList implements IPacketOutServer {

    private ServerPlayer serverPlayer;

    public PacketSkillAllList(ServerPlayer serverPlayer) {
        this.serverPlayer = serverPlayer;
    }

    @Override
    public void write(EntityPlayer entityPlayer, ByteBufOutputStream bbos) throws IOException {
        List<Skill> skills = SkillHandler.getAllCharacterSkills(serverPlayer.getSelectedCharacter().getCharacterType());
        bbos.writeInt(skills.size());
        for (Skill skill : skills) {
            writeComposable(bbos, skill);
        }
    }
}
