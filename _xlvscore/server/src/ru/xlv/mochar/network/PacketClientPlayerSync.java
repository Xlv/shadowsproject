package ru.xlv.mochar.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import ru.xlv.core.common.inventory.MatrixInventory;
import ru.xlv.core.common.network.IPacketOutServer;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.mochar.player.character.Character;
import ru.xlv.mochar.util.Utils;

import java.io.IOException;

@NoArgsConstructor
@AllArgsConstructor
public class PacketClientPlayerSync implements IPacketOutServer {

    private ServerPlayer serverPlayer;

    @Override
    public void write(EntityPlayer entityPlayer, ByteBufOutputStream bbos) throws IOException {
        writeClientPlayer(serverPlayer, bbos);
    }

    public static void writeClientPlayer(ServerPlayer serverPlayer, ByteBufOutputStream byteBufOutputStream) throws IOException {
        if(serverPlayer == null) return;
        byteBufOutputStream.writeUTF(serverPlayer.getPlayerName());
        Character selectedCharacter = serverPlayer.getSelectedCharacter();
        byteBufOutputStream.writeInt(serverPlayer.getSelectedCharacterIndex());
        if(selectedCharacter != null && selectedCharacter.getSkillManager() != null && selectedCharacter.getSkillManager().getSkillListProvider() != null && selectedCharacter.getSkillManager().getSkillListProvider().getLearnedSkillIds() != null) {
            byteBufOutputStream.writeInt(selectedCharacter.getSkillManager().getSkillListProvider().getLearnedSkillIds().size());
        }
        byteBufOutputStream.writeLong(Utils.getPeriod(serverPlayer.getLastOnlineTimeMills(), System.currentTimeMillis()));
        int id = -1;
        if(serverPlayer.getEntityPlayer() != null) {
            ItemStack stackInSlot = serverPlayer.getEntityPlayer().inventory.getStackInSlot(MatrixInventory.SlotType.BRACERS.getAssociatedSlotIndex());
            if (stackInSlot != null) {
                id = Item.getIdFromItem(stackInSlot.getItem());
            }
        }
        byteBufOutputStream.writeInt(id);
        byteBufOutputStream.writeBoolean(serverPlayer.isOnline());
    }
}
