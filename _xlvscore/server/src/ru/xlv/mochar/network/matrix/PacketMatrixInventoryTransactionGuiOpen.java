package ru.xlv.mochar.network.matrix;

import io.netty.buffer.ByteBufOutputStream;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.common.network.IPacketOutServer;

import java.io.IOException;

@NoArgsConstructor
@AllArgsConstructor
public class PacketMatrixInventoryTransactionGuiOpen implements IPacketOutServer {

    private int x, y, z;

    @Override
    public void write(EntityPlayer entityPlayer, ByteBufOutputStream bbos) throws IOException {}
}
