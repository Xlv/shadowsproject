package ru.xlv.mochar.network.matrix;

import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.network.IPacketInOnServer;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.core.network.ControllablePacket;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.mochar.inventory.MatrixInventoryTransaction;

import java.io.IOException;

@ControllablePacket(period = 200L)
@NoArgsConstructor
public class PacketMatrixInventoryDropItem implements IPacketInOnServer {

    @Override
    public void read(EntityPlayer entityPlayer, ByteBufInputStream bbis) throws IOException {
        int x = bbis.readInt();
        int y = bbis.readInt();
        ServerPlayer serverPlayer = XlvsCore.INSTANCE.getPlayerManager().getPlayer(entityPlayer);
        if (serverPlayer != null) {
            MatrixInventoryTransaction matrixInventoryTransaction = new MatrixInventoryTransaction(serverPlayer.getSelectedCharacter());
            matrixInventoryTransaction.dropItem(entityPlayer, x, y);
            XlvsCore.INSTANCE.getPacketHandler().sendPacketToPlayer(entityPlayer, new PacketMatrixPlayerInventorySync(serverPlayer));
        }
    }
}
