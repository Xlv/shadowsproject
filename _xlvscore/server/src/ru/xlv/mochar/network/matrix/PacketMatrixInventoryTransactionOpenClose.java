package ru.xlv.mochar.network.matrix;

import lombok.NoArgsConstructor;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.inventory.IMatrixInventoryProvider;
import ru.xlv.core.common.network.IPacketInOnServer;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.mochar.inventory.MatrixInventoryController;

import java.io.IOException;

@NoArgsConstructor
public class PacketMatrixInventoryTransactionOpenClose implements IPacketInOnServer {
    @Override
    public void read(EntityPlayer entityPlayer, ByteBufInputStream bbis) throws IOException {
        int entityId = bbis.readInt();
        int x = bbis.readInt();
        int y = bbis.readInt();
        int z = bbis.readInt();
        boolean open = bbis.readBoolean();
        if (open) {
            IMatrixInventoryProvider matrixInventoryProvider = null;
            if (entityId != -1) {
                Entity entityByID = entityPlayer.worldObj.getEntityByID(entityId);
                if (entityByID instanceof IMatrixInventoryProvider) {
                    matrixInventoryProvider = (IMatrixInventoryProvider) entityByID;
                }
            } else {
                TileEntity tileEntity = entityPlayer.worldObj.getTileEntity(x, y, z);
                if (tileEntity instanceof IMatrixInventoryProvider) {
                    matrixInventoryProvider = (IMatrixInventoryProvider) tileEntity;
                }
            }
            if (matrixInventoryProvider != null) {
                if (MatrixInventoryController.INSTANCE.openMatrixInventory(entityPlayer, matrixInventoryProvider)) {
                    XlvsCore.INSTANCE.getPacketHandler().sendPacketToPlayer(entityPlayer, new PacketMatrixInventoryTransactionSync(matrixInventoryProvider.getMatrixInventory()));
                }
            }
        } else {
            MatrixInventoryController.INSTANCE.closeCurrentMatrixInventory(entityPlayer);
            XlvsCore.INSTANCE.getPacketHandler().sendPacketToPlayer(entityPlayer, new PacketMatrixInventoryTransactionSync(null));
        }
    }
}
