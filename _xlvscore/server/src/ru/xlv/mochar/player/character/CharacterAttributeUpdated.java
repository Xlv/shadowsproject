package ru.xlv.mochar.player.character;

import ru.xlv.core.common.player.character.CharacterAttribute;
import ru.xlv.core.common.player.character.CharacterAttributeType;
import ru.xlv.core.player.ServerPlayer;

public class CharacterAttributeUpdated extends CharacterAttribute {

    private final long period;
    private long timer;

    public CharacterAttributeUpdated(CharacterAttributeType type, long period) {
        super(type);
        this.period = period;
    }

    public CharacterAttributeUpdated(CharacterAttributeType type, double value, long period) {
        super(type, value);
        this.period = period;
    }

    /**
     * Calls once per tick
     * */
    public void update(ServerPlayer player) {
        if(System.currentTimeMillis() - timer >= period) {
            timer = System.currentTimeMillis();
            updatePeriodically(player);
        }
    }

    /**
     * Calls once per period
     * */
    public void updatePeriodically(ServerPlayer player) {}
}
