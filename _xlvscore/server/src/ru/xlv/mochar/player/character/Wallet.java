package ru.xlv.mochar.player.character;

import lombok.Getter;
import lombok.Setter;
import net.minecraft.nbt.NBTTagCompound;
import ru.xlv.core.common.database.DatabaseValue;
import ru.xlv.core.common.storage.ISavableNBT;
import ru.xlv.core.common.storage.NBTLoader;

@Setter
@Getter
public class Wallet implements ISavableNBT {

    @DatabaseValue
    private int credits;

    public void addCredits(int amount) {
        int a = Math.abs(amount);
        if(credits < Integer.MAX_VALUE - a) {
            credits += a;
        }
    }

    public boolean consumeCredits(int amount) {
        int a = Math.abs(amount);
        if(credits - a >= 0) {
            credits -= a;
            return true;
        }
        return false;
    }

    @Override
    public void writeToNBT(NBTTagCompound nbtTagCompound, NBTLoader nbtLoader) {
        nbtTagCompound.setInteger("credits", credits);
    }

    @Override
    public void readFromNBT(NBTTagCompound nbtTagCompound, NBTLoader nbtLoader) {
        credits = nbtTagCompound.getInteger("credits");
    }
}
