package ru.xlv.mochar.player.character.skill;

import lombok.RequiredArgsConstructor;
import lombok.ToString;
import ru.xlv.core.common.player.character.CharacterType;
import ru.xlv.core.common.player.character.ExperienceType;
import ru.xlv.core.common.util.Logger;
import ru.xlv.core.common.util.config.ConfigComment;
import ru.xlv.core.common.util.config.Configurable;
import ru.xlv.core.common.util.config.IConfigGson;
import ru.xlv.mochar.player.character.skill.cost.ISkillCost;
import ru.xlv.mochar.player.character.skill.cost.SkillCostItem;
import ru.xlv.mochar.player.character.skill.cost.SkillCostMana;
import ru.xlv.mochar.util.Utils;

import java.io.File;
import java.util.*;

@ToString
@RequiredArgsConstructor
public class SkillRegistry {

    @SuppressWarnings({"unused", "FieldMayBeFinal"})
    @ToString
    @RequiredArgsConstructor
    public static class SkillTypeModel implements IConfigGson {
        @Configurable
        @ConfigComment("Тип персонажа. Варианты: MEDIC, ASSASSIN, TANK")
        private CharacterType characterType;
        @Configurable
        @ConfigComment("Уникальное значение.")
        private int skillId;
        @Configurable
        @ConfigComment("Скиллы с одним и тем-же familyId не могут использоваться вместе.")
        private int familyId;
        @Configurable
        private String name;
        @Configurable
        private String description;
        @Configurable
        @ConfigComment("Устанавливать ru.xlv.core.player.character.skill.SkillEmptyPassive, если не знаете, что это")
        private String implClassPath = "ru.xlv.core.player.character.skill.SkillEmptyPassive";
        @Configurable
        @ConfigComment("Период действия эффекта скилла.")
        private float activationPeriod;
        @Configurable
        private float cooldown;
        @Configurable
        @ConfigComment("Любые тэги.")
        private final List<String> tags = new ArrayList<>();
        @Configurable
        @ConfigComment("Ид родительских скиллов.")
        private int[] parentIds = new int[0];
        @Configurable
        @ConfigComment({"Тип стоимости скилла. Варианты: MANA(количество); ITEM(уник имя,количество)", "Параметры через запятую, указанные в скобках в комменте к типу выше."})
        private final List<SkillCostModel> useCosts = new ArrayList<>();
        @Configurable
        @ConfigComment({"Тип правила изучения скилла. Варианты: EXP(тип опыта[BATTLE,EXPLORE,SURVIVE],количество), ITEM(уник. имя, количество), ANOTHER_LEARNED_SKILL(ид скиллов через запятую)", "Параметры через запятую, указанные в скобках в комменте к типу выше."})
        private final List<SkillLearnRuleModel> learnRules = new ArrayList<>();
        @Configurable
        private final Map<String, Object> customParams = new HashMap<>();
        @Configurable
        @ConfigComment("Название текстуры скилла.")
        private String textureName;
        @Configurable
        @ConfigComment("Позиция X иконки скилла в древе.")
        private float iconX;
        @Configurable
        @ConfigComment("Позиция Y иконки скилла в древе.")
        private float iconY;

        private transient final File file;

        @Override
        public File getConfigFile() {
            return file;
        }
    }

    @ToString
    @Configurable
    @RequiredArgsConstructor
    public static class SkillCostModel {
        private final SkillCostModelType type;
        private final String params;
    }

    @ToString
    @Configurable
    @RequiredArgsConstructor
    public static class SkillLearnRuleModel {
        private final SkillLearnRuleModelType type;
        private final String params;
    }

    public enum SkillCostModelType {
        MANA, ITEM
    }

    public enum SkillLearnRuleModelType {
        EXP, ITEM, ANOTHER_LEARNED_SKILL
    }

    public static final List<SkillType> REGISTRY = Collections.synchronizedList(new ArrayList<>());
    private static final Logger LOGGER = new Logger(SkillRegistry.class.getSimpleName());

    public static void load() {
        load0("config/xlvscore/skill");
    }

    public static void load0(String path) {
        List<SkillTypeModel> list = new ArrayList<>();
        File dir = new File(path);
        int i = 0;
        loadRecursively(dir, list);
        label:
        for (SkillTypeModel skillTypeModel : list) {
            List<ISkillCost> useCosts = new ArrayList<>();
            for (SkillCostModel useCost : skillTypeModel.useCosts) {
                try {
                    String[] ss = useCost.params.split(",");
                    switch (useCost.type) {
                        case ITEM:
                            useCosts.add(new SkillCostItem(Utils.getItemByUnlocalizedName(ss[0]), Integer.parseInt(ss[1])));
                            break;
                        case MANA:
                            useCosts.add(new SkillCostMana(Integer.parseInt(ss[0])));
                    }
                } catch (Exception e) {
                    LOGGER.error(skillTypeModel.toString());
                    e.printStackTrace();
                    continue label;
                }
            }
            List<SkillLearnRules.SkillLearnRule> learnRules = new ArrayList<>();
            for (SkillLearnRuleModel learnRule : skillTypeModel.learnRules) {
                try {
                    String[] ss = learnRule.params.split(",");
                    switch (learnRule.type) {
                        case EXP:
                            learnRules.add(new SkillLearnRules.SkillLearnRuleExp(ExperienceType.valueOf(ss[0]), Double.parseDouble(ss[1])));
                            break;
                        case ITEM:
                            learnRules.add(new SkillLearnRules.SkillLearnRuleItem(Utils.getItemByUnlocalizedName(ss[0]), Integer.parseInt(ss[1])));
                            break;
                        case ANOTHER_LEARNED_SKILL:
                            learnRules.add(new SkillLearnRules.SkillLearnRuleAnother(Utils.integerArrayToIntArray(Arrays.stream(ss).map(Integer::parseInt).toArray(Integer[]::new))));
                    }
                } catch (Exception e) {
                    LOGGER.error(skillTypeModel.toString());
                    e.printStackTrace();
                    continue label;
                }
            }
            SkillType skillType = new SkillType(
                    skillTypeModel.skillId,
                    skillTypeModel.parentIds,
                    skillTypeModel.familyId,
                    skillTypeModel.name,
                    skillTypeModel.description,
                    skillTypeModel.characterType,
                    skillTypeModel.implClassPath,
                    skillTypeModel.iconX,
                    skillTypeModel.iconY,
                    skillTypeModel.textureName,
                    ((long) skillTypeModel.activationPeriod * 1000),
                    ((long) skillTypeModel.cooldown * 1000),
                    useCosts.toArray(new ISkillCost[0]),
                    learnRules.toArray(new SkillLearnRules.SkillLearnRule[0]),
                    skillTypeModel.tags.toArray(new String[0]),
                    skillTypeModel.customParams
            );
//            LOGGER.info(skillType.toString());
            REGISTRY.add(skillType);
            i++;
        }
        REGISTRY.forEach(skillType -> REGISTRY.forEach(skillType1 -> {
            if(skillType != skillType1 && skillType.getSkillId() == skillType1.getSkillId()) {
                throw new RuntimeException("Duplicate skill ids: " + skillType.getName() + " = " + skillType1.getName());
            }
        }));
        boolean[] bb = new boolean[] {false};
        REGISTRY.forEach(skillType1 -> skillType1.getParentIds().forEach(value -> {
            if(value != -1) {
                for (SkillType type : REGISTRY) {
                    if (type.getSkillId() == value) {
                        return true;
                    }
                }
                System.err.println("The parent with the id: " + value + " of the skill " + skillType1 + " not found");
                bb[0] = true;
            }
            return true;
        }));
        if(bb[0]) {
            throw new RuntimeException("An error has occurred during loading skills");
        }
        LOGGER.info(i + " skills registered.");
    }

    private static void loadRecursively(File dir, List<SkillTypeModel> list) {
        File[] files = dir.listFiles();
        if (files != null) {
            for (File file : files) {
                if (file.isDirectory()) {
                    loadRecursively(file, list);
                } else {
                    SkillTypeModel skillTypeModel = new SkillTypeModel(file);
                    try {
                        skillTypeModel.loadExceptionally();
                    } catch (Exception e) {
                        e.printStackTrace();
                        continue;
                    }
                    skillTypeModel.learnRules.remove(null);
                    skillTypeModel.useCosts.remove(null);
                    list.add(skillTypeModel);
                }
            }
        }
    }

//    public static void main(String[] args) {
//        SkillRegistry.load0("run/config/xlvscore/skill");
//        REGISTRY.forEach(SkillFactory::createSkill);
////        REGISTRY.forEach(skillType -> System.out.println(skillType));
//    }
//    public static void main(String[] args) {
//        SkillTypeModel skillTypeModel = new SkillTypeModel(new File( "active.json"));
//        skillTypeModel.implClassPath = "ru.xlv.core.player.character.skill.SkillEmptyActive";
//        skillTypeModel.useCosts.add(new SkillCostModel(SkillCostModelType.MANA, "11"));
//        skillTypeModel.learnRules.add(new SkillLearnRuleModel(SkillLearnRuleModelType.EXP, "BATTLE,123"));
//        skillTypeModel.learnRules.add(new SkillLearnRuleModel(SkillLearnRuleModelType.ITEM, "iron_sword,12"));
//        skillTypeModel.parentIds = new int[2];
//        skillTypeModel.parentIds[0] = 123;
//        skillTypeModel.parentIds[1] = 228;
//        skillTypeModel.tags.add("tererarer");
//        skillTypeModel.tags.add("asdacxzcwf");
//        skillTypeModel.customParams.put("a", 123);
//        skillTypeModel.customParams.put("b", 30D);
//        skillTypeModel.customParams.put("c", "asdeewq123");
//        skillTypeModel.name = "Test";
//        skillTypeModel.description = "Testasd a da sd a ad \n asdasdasd";
//        skillTypeModel.familyId = 14;
//        skillTypeModel.textureName = "texture_name";
//        skillTypeModel.activationPeriod = 100;
//        skillTypeModel.cooldown = 50;
//        skillTypeModel.characterType = CharacterType.MEDIC;
//        skillTypeModel.save();
//        skillTypeModel.load();
//        skillTypeModel = new SkillTypeModel(new File( "passive.json"));
//        skillTypeModel.useCosts.add(new SkillCostModel(SkillCostModelType.MANA, "11"));
//        skillTypeModel.learnRules.add(new SkillLearnRuleModel(SkillLearnRuleModelType.EXP, "BATTLE,123"));
//        skillTypeModel.learnRules.add(new SkillLearnRuleModel(SkillLearnRuleModelType.ITEM, "iron_sword,12"));
//        skillTypeModel.parentIds = new int[2];
//        skillTypeModel.parentIds[0] = 123;
//        skillTypeModel.parentIds[1] = 228;
//        skillTypeModel.tags.add("tererarer");
//        skillTypeModel.tags.add("asdacxzcwf");
//        skillTypeModel.customParams.put("a", 123);
//        skillTypeModel.customParams.put("b", 30D);
//        skillTypeModel.customParams.put("c", "asdeewq123");
//        skillTypeModel.name = "Test";
//        skillTypeModel.description = "Testasd a da sd a ad \n asdasdasd";
//        skillTypeModel.familyId = 14;
//        skillTypeModel.textureName = "texture_name";
//        skillTypeModel.characterType = CharacterType.MEDIC;
//        skillTypeModel.save();
//        skillTypeModel.load();
//    }
}
