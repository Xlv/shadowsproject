package ru.xlv.mochar.player.character;

import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.*;
import net.minecraft.potion.PotionEffect;
import ru.xlv.core.common.player.character.CharacterAttribute;
import ru.xlv.core.common.player.character.CharacterType;
import ru.xlv.core.common.util.CommonUtils;
import ru.xlv.core.player.ServerPlayer;

import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

//todo привести в порядок
public class CharacterLoader {

    private final String SAVE_DIR = "config/characters/";

    public void save(EntityPlayer entityPlayer, ServerPlayer serverPlayer) {
//        PlayerCharacterData playerCharacterData = serverPlayer.getData(PlayerCharacterData.class);
//        if(playerCharacterData == null || playerCharacterData.getSelectedCharacter() == null) return;
//        File file = new File(SAVE_DIR + entityPlayer.getUniqueID().toString() + "/" + playerCharacterData.getSelectedCharacter().getCharacterType().name());
//        try {
//            if (!file.exists()) {
//                file.getParentFile().mkdirs();
//                file.createNewFile();
//            }
//
//            System.out.println("Saving character " + playerCharacterData.getSelectedCharacter().getCharacterType().name());
//            NBTTagCompound nbtTagCompound = new NBTTagCompound();
//            saveCharacters(entityPlayer, nbtTagCompound);
//            CompressedStreamTools.writeCompressed(nbtTagCompound, new FileOutputStream(file));
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        File file = new File(SAVE_DIR + entityPlayer.getUniqueID().toString() + "/" + serverPlayer.getSelectedCharacter().getCharacterType().name());
        try {
            if (!file.exists()) {
                file.getParentFile().mkdirs();
                file.createNewFile();
            }

            System.out.println("Saving character " + serverPlayer.getSelectedCharacter().getCharacterType().name());
            NBTTagCompound nbtTagCompound = new NBTTagCompound();
            saveEntityPlayerToNBT(entityPlayer, nbtTagCompound);
            CompressedStreamTools.writeCompressed(nbtTagCompound, new FileOutputStream(file));
            saveCharacter(entityPlayer, serverPlayer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void load(EntityPlayer entityPlayer, ServerPlayer serverPlayer) {
//        PlayerCharacterData playerCharacterData = serverPlayer.getData(PlayerCharacterData.class);
//        if(playerCharacterData == null || playerCharacterData.getSelectedCharacter() == null) return;
//        File file = new File(SAVE_DIR + entityPlayer.getUniqueID().toString() + "/" + playerCharacterData.getSelectedCharacter().getCharacterType().name());
//        try {
//            if(!file.exists()) {
//                System.out.println("Server has no character data");
//                for (CharacterType value : CharacterType.values()) {
//                    NBTTagCompound nbtTagCompound = new NBTTagCompound();
//                    saveCharacters(entityPlayer, nbtTagCompound);
//                    file = new File(SAVE_DIR + entityPlayer.getUniqueID().toString() + "/" + value.name());
//                    CompressedStreamTools.writeCompressed(nbtTagCompound, new FileOutputStream(file));
//                }
//                return;
//            }
//
//            NBTTagCompound nbtTagCompound = CompressedStreamTools.readCompressed(new FileInputStream(file));
//            if(nbtTagCompound != null) {
//                System.out.println("Loading character " + playerCharacterData.getSelectedCharacter().getCharacterType().name());
//                System.out.println(String.format("%s %s", entityPlayer.posX, entityPlayer.posZ));
//                loadCharacters(entityPlayer, nbtTagCompound);
//                entityPlayer.readFromNBT(nbtTagCompound);
//                System.out.println(String.format("%s %s", entityPlayer.posX, entityPlayer.posZ));
//            }
//            System.out.println("" + serverPlayer.getData(PlayerCharacterData.class).getSelectedCharacterIndex());
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        File file = new File(SAVE_DIR + entityPlayer.getUniqueID().toString() + "/" + serverPlayer.getSelectedCharacter().getCharacterType().name());
        try {
            if(!file.exists()) {
                System.out.println("Server has no character data");
                for (CharacterType value : CharacterType.values()) {
                    NBTTagCompound nbtTagCompound = new NBTTagCompound();
                    saveEntityPlayerToNBT(entityPlayer, nbtTagCompound);
                    file = new File(SAVE_DIR + entityPlayer.getUniqueID().toString() + "/" + value.name());
                    file.getParentFile().mkdirs();
                    file.createNewFile();
                    CompressedStreamTools.writeCompressed(nbtTagCompound, new FileOutputStream(file));
                }
                return;
            }

            NBTTagCompound nbtTagCompound = CompressedStreamTools.readCompressed(new FileInputStream(file));
            if(nbtTagCompound != null) {
                System.out.println("Loading character " + serverPlayer.getSelectedCharacter().getCharacterType().name());
                System.out.println(String.format("%s %s", entityPlayer.posX, entityPlayer.posZ));
                loadEntityPlayerFromNBT(entityPlayer, nbtTagCompound);
                entityPlayer.readFromNBT(nbtTagCompound);
                System.out.println(String.format("%s %s", entityPlayer.posX, entityPlayer.posZ));
            }
            loadCharacter(entityPlayer, serverPlayer);
            System.out.println("" + serverPlayer.getSelectedCharacterIndex());
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void saveCharacter(EntityPlayer entityPlayer, ServerPlayer serverPlayer) throws IOException {
        File file = new File(SAVE_DIR + entityPlayer.getUniqueID().toString() + "/" + serverPlayer.getSelectedCharacter().getCharacterType().name() + "ext");
        if (!file.exists()) {
            file.getParentFile().mkdirs();
            file.createNewFile();
        }
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(file));
        ArrayList<CharacterAttribute> list = new ArrayList<>(serverPlayer.getSelectedCharacter().getCharacterAttributes());
        objectOutputStream.writeObject(list);
        objectOutputStream.close();
    }

    private void loadCharacter(EntityPlayer entityPlayer, ServerPlayer serverPlayer) throws IOException, ClassNotFoundException {
        File file = new File(SAVE_DIR + entityPlayer.getUniqueID().toString() + "/" + serverPlayer.getSelectedCharacter().getCharacterType().name() + "ext");
        if(!file.exists()) {
            return;
        }
        ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(file));
        ArrayList<CharacterAttribute> list = (ArrayList<CharacterAttribute>) objectInputStream.readObject();
        if(list != null) {
            List<CharacterAttribute> list1 = new ArrayList<>();
            Iterator<CharacterAttribute> iterator = serverPlayer.getSelectedCharacter().getCharacterAttributes().iterator();
            while(iterator.hasNext()) {
                CharacterAttribute attribute = iterator.next();
                for (CharacterAttribute characterAttribute : list) {
                    if(characterAttribute.getType() == attribute.getType()) {
                        iterator.remove();
                        list1.add(characterAttribute);
                    }
                }
            }
            serverPlayer.getSelectedCharacter().getCharacterAttributes().addAll(list1);
        }
        objectInputStream.close();
    }

    public void loadAllInDir() {
//        File dir = new File(SAVE_DIR);
//        if(dir.exists() && dir.isDirectory()) {
//            File[] files = dir.listFiles();
//            if(files != null) {
//                for (File file : files) {
//                    ServerPlayer serverPlayer;
//                    if((serverPlayer = XlvsCore.INSTANCE.getPlayerManager().getPlayerByName(file.getName())) == null) {
//                        synchronized (XlvsCore.INSTANCE.getPlayerManager().getPlayerList()) {
//                            XlvsCore.INSTANCE.getPlayerManager().getPlayerList().add(serverPlayer = new ServerPlayer(file.getName()));
//                        }
//                    }
//                    load(serverPlayer);
//                }
//            }
//        }
    }

    public void loadEntityPlayerFromNBT(EntityPlayer entityPlayer, NBTTagCompound nbtTagCompound) {
        //Entity
        NBTTagList nbttaglist = nbtTagCompound.getTagList("Pos", 6);
        NBTTagList nbttaglist1 = nbtTagCompound.getTagList("Motion", 6);
        NBTTagList nbttaglist2 = nbtTagCompound.getTagList("Rotation", 5);
        entityPlayer.motionX = nbttaglist1.func_150309_d(0);
        entityPlayer.motionY = nbttaglist1.func_150309_d(1);
        entityPlayer.motionZ = nbttaglist1.func_150309_d(2);
        if (Math.abs(entityPlayer.motionX) > 10.0D) {
            entityPlayer.motionX = 0.0D;
        }
        if (Math.abs(entityPlayer.motionY) > 10.0D) {
            entityPlayer.motionY = 0.0D;
        }
        if (Math.abs(entityPlayer.motionZ) > 10.0D) {
            entityPlayer.motionZ = 0.0D;
        }
        entityPlayer.prevPosX = entityPlayer.lastTickPosX = entityPlayer.posX = nbttaglist.func_150309_d(0);
        entityPlayer.prevPosY = entityPlayer.lastTickPosY = entityPlayer.posY = nbttaglist.func_150309_d(1);
        entityPlayer.prevPosZ = entityPlayer.lastTickPosZ = entityPlayer.posZ = nbttaglist.func_150309_d(2);
        entityPlayer.prevRotationYaw = entityPlayer.rotationYaw = nbttaglist2.func_150308_e(0);
        entityPlayer.prevRotationPitch = entityPlayer.rotationPitch = nbttaglist2.func_150308_e(1);
        entityPlayer.fallDistance = nbtTagCompound.getFloat("FallDistance");
        entityPlayer.setAir(nbtTagCompound.getShort("Air"));
        entityPlayer.onGround = nbtTagCompound.getBoolean("OnGround");
        entityPlayer.dimension = nbtTagCompound.getInteger("Dimension");
        entityPlayer.timeUntilPortal = nbtTagCompound.getInteger("PortalCooldown");
        //Living
        entityPlayer.setAbsorptionAmount(nbtTagCompound.getFloat("AbsorptionAmount"));
        if (nbtTagCompound.hasKey("Attributes", 9) && entityPlayer.worldObj != null && !entityPlayer.worldObj.isRemote) {
            SharedMonsterAttributes.func_151475_a(entityPlayer.getAttributeMap(), nbtTagCompound.getTagList("Attributes", 10));
        }
        if (nbtTagCompound.hasKey("ActiveEffects", 9)) {
            nbttaglist = nbtTagCompound.getTagList("ActiveEffects", 10);
            for (int i = 0; i < nbttaglist.tagCount(); ++i) {
                NBTTagCompound nbttagcompound1 = nbttaglist.getCompoundTagAt(i);
                PotionEffect potioneffect = PotionEffect.readCustomPotionEffectFromNBT(nbttagcompound1);
                if (potioneffect != null) {
//                    entityPlayer.addPotionEffect(potioneffect);
                    //todo potions are not loading correctly
//                    entityPlayer.activePotionsMap.put(Integer.valueOf(potioneffect.getPotionID()), potioneffect);
                }
            }
        }
        if (nbtTagCompound.hasKey("HealF", 99)) {
            entityPlayer.setHealth(nbtTagCompound.getFloat("HealF"));
        } else {
            NBTBase nbtbase = nbtTagCompound.getTag("Health");
            if (nbtbase == null) {
                entityPlayer.setHealth(entityPlayer.getMaxHealth());
            } else if (nbtbase.getId() == 5) {
                entityPlayer.setHealth(((NBTTagFloat) nbtbase).func_150288_h());
            } else if (nbtbase.getId() == 2) {
                entityPlayer.setHealth((float) ((NBTTagShort) nbtbase).func_150289_e());
            }
        }
        entityPlayer.hurtTime = nbtTagCompound.getShort("HurtTime");
        entityPlayer.deathTime = nbtTagCompound.getShort("DeathTime");
        entityPlayer.attackTime = nbtTagCompound.getShort("AttackTime");
        //Player
        nbttaglist = nbtTagCompound.getTagList("Inventory", 10);
        entityPlayer.inventory.readFromNBT(nbttaglist);
        entityPlayer.inventory.currentItem = nbtTagCompound.getInteger("SelectedItemSlot");
        entityPlayer.experience = nbtTagCompound.getFloat("XpP");
        entityPlayer.experienceLevel = nbtTagCompound.getInteger("XpLevel");
        entityPlayer.experienceTotal = nbtTagCompound.getInteger("XpTotal");
        entityPlayer.setScore(nbtTagCompound.getInteger("Score"));
//            if (nbtTagCompound.hasKey("SpawnX", 99) && nbtTagCompound.hasKey("SpawnY", 99) && nbtTagCompound.hasKey("SpawnZ", 99)) {
//                entityPlayer.spawnChunk = new ChunkCoordinates(nbtTagCompound.getInteger("SpawnX"), nbtTagCompound.getInteger("SpawnY"), nbtTagCompound.getInteger("SpawnZ"));
//                entityPlayer.spawnForced = nbtTagCompound.getBoolean("SpawnForced");
//            }
//            NBTTagList spawnlist = null;
//            spawnlist = nbtTagCompound.getTagList("Spawns", 10);
//            for (int i = 0; i < spawnlist.tagCount(); i++) {
//                NBTTagCompound spawndata = (NBTTagCompound) spawnlist.getCompoundTagAt(i);
//                int spawndim = spawndata.getInteger("Dim");
//                entityPlayer.spawnChunkMap.put(spawndim, new ChunkCoordinates(spawndata.getInteger("SpawnX"), spawndata.getInteger("SpawnY"), spawndata.getInteger("SpawnZ")));
//                entityPlayer.spawnForcedMap.put(spawndim, spawndata.getBoolean("SpawnForced"));
//            }
        entityPlayer.getFoodStats().readNBT(nbtTagCompound);
        entityPlayer.capabilities.readCapabilitiesFromNBT(nbtTagCompound);
        if (nbtTagCompound.hasKey("EnderItems", 9)) {
            nbttaglist1 = nbtTagCompound.getTagList("EnderItems", 10);
            entityPlayer.getInventoryEnderChest().loadInventoryFromNBT(nbttaglist1);
        }

        //Custom
        {
//            NBTLoader nbtLoader = new NBTLoader(nbtTagCompound);
//            nbtLoader.readFromNBT(serverPlayer.getSelectedCharacter());

//            if (nbtTagCompound.hasKey("learnedSkills")) {
//                for (CharacterType value : CharacterType.values()) {
//                    System.out.println("iter");
//                    int[] skills = nbtTagCompound.getCompoundTag("learnedSkills").getIntArray(value.name());
//                    if (skills != null) {
//                        System.out.println(value.name());
//                        SkillHandler.setLoadedSkills(serverPlayer, value, skills);
//                    }
//                }
//            }
//            if(nbtTagCompound.hasKey("matrixInventory")) {
//                serverPlayer.getSelectedCharacter().getMatrixInventory().readFromNBT(nbtTagCompound.getCompoundTag("matrixInventory"));
//            }
//            if(nbtTagCompound.hasKey("char_experience")) {
//                serverPlayer.getSelectedCharacter().getExperienceHandler().readFromNBT(nbtTagCompound.getCompoundTag("char_experience"));
//            }
        }
    }

    public void saveEntityPlayerToNBT(EntityPlayer entityPlayer, NBTTagCompound nbtTagCompound) {
        //Entity
        {
            nbtTagCompound.setTag("Pos", CommonUtils.newDoubleNBTList(entityPlayer.posX, entityPlayer.posY + (double) entityPlayer.ySize, entityPlayer.posZ));
            nbtTagCompound.setTag("Motion", CommonUtils.newDoubleNBTList(entityPlayer.motionX, entityPlayer.motionY, entityPlayer.motionZ));
            nbtTagCompound.setTag("Rotation", CommonUtils.newFloatNBTList(entityPlayer.rotationYaw, entityPlayer.rotationPitch));
            nbtTagCompound.setFloat("FallDistance", entityPlayer.fallDistance);
            nbtTagCompound.setShort("Air", (short) entityPlayer.getAir());
            nbtTagCompound.setBoolean("OnGround", entityPlayer.onGround);
            nbtTagCompound.setInteger("Dimension", entityPlayer.dimension);
            nbtTagCompound.setInteger("PortalCooldown", entityPlayer.timeUntilPortal);
        }
        //Living
        {
            nbtTagCompound.setFloat("HealF", entityPlayer.getHealth());
            nbtTagCompound.setShort("Health", (short) ((int) Math.ceil((double) entityPlayer.getHealth())));
            nbtTagCompound.setShort("HurtTime", (short) entityPlayer.hurtTime);
            nbtTagCompound.setShort("DeathTime", (short) entityPlayer.deathTime);
            nbtTagCompound.setShort("AttackTime", (short) entityPlayer.attackTime);
            nbtTagCompound.setFloat("AbsorptionAmount", entityPlayer.getAbsorptionAmount());
            ItemStack[] aitemstack = entityPlayer.getLastActiveItems();
            int i = aitemstack.length;
            int j;
            ItemStack itemstack;
            for (j = 0; j < i; ++j) {
                itemstack = aitemstack[j];
                if (itemstack != null) {
                    entityPlayer.getAttributeMap().removeAttributeModifiers(itemstack.getAttributeModifiers());
                }
            }
            nbtTagCompound.setTag("Attributes", SharedMonsterAttributes.writeBaseAttributeMapToNBT(entityPlayer.getAttributeMap()));
            aitemstack = entityPlayer.getLastActiveItems();
            i = aitemstack.length;
            for (j = 0; j < i; ++j) {
                itemstack = aitemstack[j];
                if (itemstack != null) {
                    entityPlayer.getAttributeMap().applyAttributeModifiers(itemstack.getAttributeModifiers());
                }
            }
            if (!entityPlayer.getActivePotionEffects().isEmpty()) {
                NBTTagList nbttaglist = new NBTTagList();
                for (Object o : entityPlayer.getActivePotionEffects()) {
                    PotionEffect potioneffect = (PotionEffect) o;
                    nbttaglist.appendTag(potioneffect.writeCustomPotionEffectToNBT(new NBTTagCompound()));
                }
                nbtTagCompound.setTag("ActiveEffects", nbttaglist);
            }
        }
        //Player
        {
            nbtTagCompound.setTag("Inventory", entityPlayer.inventory.writeToNBT(new NBTTagList()));
            nbtTagCompound.setInteger("SelectedItemSlot", entityPlayer.inventory.currentItem);
            nbtTagCompound.setFloat("XpP", entityPlayer.experience);
            nbtTagCompound.setInteger("XpLevel", entityPlayer.experienceLevel);
            nbtTagCompound.setInteger("XpTotal", entityPlayer.experienceTotal);
            nbtTagCompound.setInteger("Score", entityPlayer.getScore());

//            if (entityPlayer.spawnChunk != null)
//            {
//                nbtTagCompound.setInteger("SpawnX", entityPlayer.spawnChunk.posX);
//                nbtTagCompound.setInteger("SpawnY", entityPlayer.spawnChunk.posY);
//                nbtTagCompound.setInteger("SpawnZ", entityPlayer.spawnChunk.posZ);
//                nbtTagCompound.setBoolean("SpawnForced", entityPlayer.spawnForced);
//            }

//            NBTTagList spawnlist = new NBTTagList();
//            for (Entry<Integer, ChunkCoordinates> entry : entityPlayer.spawnChunkMap.entrySet())
//            {
//                ChunkCoordinates spawn = entry.getValue();
//                if (spawn == null) continue;
//                Boolean forced = spawnForcedMap.get(entry.getKey());
//                if (forced == null) forced = false;
//                NBTTagCompound spawndata = new NBTTagCompound();
//                spawndata.setInteger("Dim", entry.getKey());
//                spawndata.setInteger("SpawnX", spawn.posX);
//                spawndata.setInteger("SpawnY", spawn.posY);
//                spawndata.setInteger("SpawnZ", spawn.posZ);
//                spawndata.setBoolean("SpawnForced", forced);
//                spawnlist.appendTag(spawndata);
//            }
//            nbtTagCompound.setTag("Spawns", spawnlist);

            entityPlayer.getFoodStats().writeNBT(nbtTagCompound);
            entityPlayer.capabilities.writeCapabilitiesToNBT(nbtTagCompound);
            nbtTagCompound.setTag("EnderItems", entityPlayer.getInventoryEnderChest().saveInventoryToNBT());
        }
        //Custom
        {
//            NBTLoader nbtLoader = new NBTLoader(nbtTagCompound);
//            if(serverPlayer.getSelectedCharacter() != null) {
//                nbtLoader.writeToNBT(serverPlayer.getSelectedCharacter());
//                if(!serverPlayer.getSelectedCharacter().getSkillHandler().getLearnedSkills().isEmpty()) {
//                    Integer[] skills = serverPlayer.getSelectedCharacter().getSkillHandler().getLearnedSkills()
//                            .stream()
//                            .map(skill -> skill.getSkillType().getId())
//                            .toArray(Integer[]::new);
//                    NBTTagCompound tag;
//                    if (!nbtTagCompound.hasKey("learnedSkills")) {
//                        tag = new NBTTagCompound();
//                    } else {
//                        tag = (NBTTagCompound) nbtTagCompound.getTag("learnedSkills");
//                    }
//                    for (CharacterType value : CharacterType.values()) {
//                        tag.setIntArray(value.name(), CommonUtils.newIntArray(skills));
//                    }
//                    nbtTagCompound.setTag("learnedSkills", tag);
//                }
//                NBTTagCompound tag = nbtTagCompound.getCompoundTag("matrixInventory");
//                if(tag == null) tag = new NBTTagCompound();
//                serverPlayer.getSelectedCharacter().getMatrixInventory().writeToNBT(tag);
//                nbtTagCompound.setTag("matrixInventory", tag);
//                tag = nbtTagCompound.getCompoundTag("char_experience");
//                if(tag == null) tag = new NBTTagCompound();
//                serverPlayer.getSelectedCharacter().getExperienceHandler().writeToNBT(tag);
//                nbtTagCompound.setTag("char_experience", tag);
//            }
        }
    }
}
