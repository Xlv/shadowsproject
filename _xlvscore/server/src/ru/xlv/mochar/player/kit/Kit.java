package ru.xlv.mochar.player.kit;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.minecraft.item.ItemStack;

import java.util.ArrayList;
import java.util.List;

@Getter
@RequiredArgsConstructor
public class Kit {

    private final List<ItemStack> items = new ArrayList<>();
}
