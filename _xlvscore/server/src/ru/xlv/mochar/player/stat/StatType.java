package ru.xlv.mochar.player.stat;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.player.stat.StatValueType;

@Getter
@RequiredArgsConstructor
public enum StatType {
    TOTAL_ONLINE_TIME_SECONDS(StatValueType.INT, XlvsCore.INSTANCE.getStatLocalization().getTotalOnlineTimeSecondsName()),
    TOTAL_ACHIEVEMENT_AMOUNT(StatValueType.INT, XlvsCore.INSTANCE.getStatLocalization().getTotalAchievementAmountName()),
    TOTAL_LEVEL_AMOUNT(StatValueType.INT, XlvsCore.INSTANCE.getStatLocalization().getTotalLevelAmountName()),//?
    SPEED_RECORD(StatValueType.DOUBLE, XlvsCore.INSTANCE.getStatLocalization().getSpeedRecordName()),
    TOTAL_DRUG_USED(StatValueType.INT, XlvsCore.INSTANCE.getStatLocalization().getTotalDrugUsedName()),//?
    TOTAL_STIMULANT_USED(StatValueType.INT, XlvsCore.INSTANCE.getStatLocalization().getTotalStimulantUsedName()),//?
    TOTAL_MEDIC_USED(StatValueType.INT, XlvsCore.INSTANCE.getStatLocalization().getTotalMedicUsedName()),//?
    TOTAL_GROUPED_TIME_SECONDS(StatValueType.INT, XlvsCore.INSTANCE.getStatLocalization().getTotalGroupedTimeSecondsName()),//?
    TOTAL_SKILL_LEARNED(StatValueType.INT, XlvsCore.INSTANCE.getStatLocalization().getTotalSkillLearnedName()),//?
    TOTAL_LOCATION_DISCOVERED(StatValueType.INT, XlvsCore.INSTANCE.getStatLocalization().getTotalLocationDiscoveredName()),
    TOTAL_SHOOTS(StatValueType.INT, XlvsCore.INSTANCE.getStatLocalization().getTotalShootsName()),
    TOTAL_HEADSHOTS(StatValueType.INT, XlvsCore.INSTANCE.getStatLocalization().getTotalHeadshotsName()),//?
    TOTAL_GRENADES_THROWN(StatValueType.INT, XlvsCore.INSTANCE.getStatLocalization().getTotalGrenadesThrownName()),//?
    TOTAL_SKILL_USED(StatValueType.INT, XlvsCore.INSTANCE.getStatLocalization().getTotalSkillUsedName()),
    TOTAL_PLAYERS_KILLED(StatValueType.INT, XlvsCore.INSTANCE.getStatLocalization().getTotalPlayersKilledName()),
    TOTAL_KILL_SERIES_AMOUNT(StatValueType.INT, XlvsCore.INSTANCE.getStatLocalization().getTotalKillSeriesAmountName()),
    TOTAL_NPCS_KILLED(StatValueType.INT, XlvsCore.INSTANCE.getStatLocalization().getTotalNpcsKilledName()),//?
    TOTAL_DEATHS(StatValueType.INT, XlvsCore.INSTANCE.getStatLocalization().getTotalDeathsName()),
    TOTAL_DEATHS_FROM_PLAYER(StatValueType.INT, XlvsCore.INSTANCE.getStatLocalization().getTotalDeathsFromPlayerName()),
    TOTAL_DEATHS_FROM_NPC(StatValueType.INT, XlvsCore.INSTANCE.getStatLocalization().getTotalDeathsFromNpcName()),//?
    TOTAL_DEATHS_FROM_FALL(StatValueType.INT, XlvsCore.INSTANCE.getStatLocalization().getTotalDeathsFromFallName()),
    TOTAL_DEATHS_FROM_SUFFOCATION(StatValueType.INT, XlvsCore.INSTANCE.getStatLocalization().getTotalDeathsFromSuffocationName()),//?
    TOTAL_SUICIDES(StatValueType.INT, XlvsCore.INSTANCE.getStatLocalization().getTotalSuicidesName()),//?
    TOTAL_CREDIT_RECEIVED(StatValueType.INT, XlvsCore.INSTANCE.getStatLocalization().getTotalCreditReceivedName()),
    TOTAL_CREDIT_FROM_QUESTS_RECEIVED(StatValueType.INT, XlvsCore.INSTANCE.getStatLocalization().getTotalCreditFromQuestsReceivedName()),//???
    TOTAL_CREDIT_FROM_NPC_KILLS_RECEIVED(StatValueType.INT, XlvsCore.INSTANCE.getStatLocalization().getTotalCreditFromNpcKillsReceivedName()),//???
    TOTAL_CREDIT_FROM_TRADING_RECEIVED(StatValueType.INT, XlvsCore.INSTANCE.getStatLocalization().getTotalCreditFromTradingReceivedName()),//???
    TOTAL_CREDIT_FROM_AUCTION_RECEIVED(StatValueType.INT, XlvsCore.INSTANCE.getStatLocalization().getTotalCreditFromAuctionReceivedName()),
    TOTAL_CREDIT_ON_AUCTION_SPENT(StatValueType.INT, XlvsCore.INSTANCE.getStatLocalization().getTotalCreditOnAuctionSpentName()),
    TOTAL_CREDIT_ON_TRADERS_SPENT(StatValueType.INT, XlvsCore.INSTANCE.getStatLocalization().getTotalCreditOnTradersSpentName()),//???
    TOTAL_CREDIT_ON_TRADING_SPENT(StatValueType.INT, XlvsCore.INSTANCE.getStatLocalization().getTotalCreditOnTradingSpentName()),//???
    TOTAL_TRADES_AMOUNT(StatValueType.INT, XlvsCore.INSTANCE.getStatLocalization().getTotalTradesAmountName()),//???
    TOTAL_ITEMS_PURCHASED_FROM_NPCS(StatValueType.INT, XlvsCore.INSTANCE.getStatLocalization().getTotalItemsPurchasedFromNpcsName()),//???
    TOTAL_ITEMS_PURCHASED_FROM_AUCTION(StatValueType.INT, XlvsCore.INSTANCE.getStatLocalization().getTotalItemsPurchasedFromAuctionName()),
    TOTAL_ITEMS_SALES_TO_NPCS(StatValueType.INT, XlvsCore.INSTANCE.getStatLocalization().getTotalItemsSalesToNpcsName()),//???
    TOTAL_ITEMS_SALES_ON_AUCTION(StatValueType.INT, XlvsCore.INSTANCE.getStatLocalization().getTotalItemsSalesOnAuctionName()),
    TOTAL_ITEMS_CREATED(StatValueType.INT, XlvsCore.INSTANCE.getStatLocalization().getTotalItemsCreatedName()),//???
    TOTAL_POST_SENT(StatValueType.INT, XlvsCore.INSTANCE.getStatLocalization().getTotalPostSentName()),
    TOTAL_FRIEND_ADDED(StatValueType.INT, XlvsCore.INSTANCE.getStatLocalization().getTotalFriendAddedName()),
    TOTAL_BLOCKED_PLAYERS(StatValueType.INT, XlvsCore.INSTANCE.getStatLocalization().getTotalBlockedPlayersName()),//???
    TOTAL_TRADES_MADE(StatValueType.INT, XlvsCore.INSTANCE.getStatLocalization().getTotalTradesMadeName()),//???
    TOTAL_REVENGES(StatValueType.INT, XlvsCore.INSTANCE.getStatLocalization().getTotalRevengesName()),
    TOTAL_QUEST_COMPLETED_COMBAT(StatValueType.INT, XlvsCore.INSTANCE.getStatLocalization().getTotalQuestCompletedCombatName()),
    TOTAL_QUEST_COMPLETED_EXPLORE(StatValueType.INT, XlvsCore.INSTANCE.getStatLocalization().getTotalQuestCompletedExploreName()),
    TOTAL_QUEST_COMPLETED_SURVIVE(StatValueType.INT, XlvsCore.INSTANCE.getStatLocalization().getTotalQuestCompletedSurviveName());

    private final StatValueType valueType;
    private final String displayName;
}
