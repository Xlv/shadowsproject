package ru.xlv.mochar.asm;

import cpw.mods.fml.relauncher.IFMLLoadingPlugin;
import ua.agravaine.hooklib.minecraft.HookLoader;

@IFMLLoadingPlugin.MCVersion("1.7.10")
public class XlvsHookLoader extends HookLoader {

    @Override
    protected void registerHooks() {
        registerHookContainer("ru.xlv.mochar.asm.XlvsHooks");
    }
}
