package ru.xlv.mochar.inventory;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import ru.xlv.core.common.inventory.IMatrixInventoryProvider;
import ru.xlv.core.common.inventory.MatrixInventory;
import ru.xlv.core.common.inventory.MatrixInventoryFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class MatrixInventoryController {

    public static final MatrixInventoryController INSTANCE = new MatrixInventoryController();

    private final Map<String, IMatrixInventoryProvider> activeOpenedInventories = Collections.synchronizedMap(new HashMap<>());

    private MatrixInventoryController() {}

    public void openMatrixInventory(@Nonnull EntityPlayer entityPlayer, @Nonnull IInventory inventory, int x, int y, int z, boolean force) {
        IMatrixInventoryProvider fakeMatrixInventoryProvider = new StaticFakeMatrixInventoryProvider(x, y, z, inventory, entityPlayer.openContainer);
        if(force) {
            closeOldAndOpenMatrixInventory(entityPlayer, fakeMatrixInventoryProvider);
        } else {
            openMatrixInventory(entityPlayer, fakeMatrixInventoryProvider);
        }
    }

    public <T extends TileEntity & IInventory> void openMatrixInventory(@Nonnull EntityPlayer entityPlayer, @Nonnull T tileEntity, boolean force) {
        IMatrixInventoryProvider fakeMatrixInventoryProvider = new TileEntityFakeMatrixInventoryProvider<>(tileEntity);
        if(force) {
            closeOldAndOpenMatrixInventory(entityPlayer, fakeMatrixInventoryProvider);
        } else {
            openMatrixInventory(entityPlayer, fakeMatrixInventoryProvider);
        }
    }

    public void openMatrixInventory(@Nonnull EntityPlayer entityPlayer, @Nonnull IInventory inventory, boolean force) {
        IMatrixInventoryProvider fakeMatrixInventoryProvider = new GhostFakeMatrixInventoryProvider(inventory);
        if(force) {
            closeOldAndOpenMatrixInventory(entityPlayer, fakeMatrixInventoryProvider);
        } else {
            openMatrixInventory(entityPlayer, fakeMatrixInventoryProvider);
        }
    }

    public boolean openMatrixInventory(@Nonnull EntityPlayer entityPlayer, @Nonnull IMatrixInventoryProvider matrixInventoryProvider) {
        return openMatrixInventory(entityPlayer, matrixInventoryProvider, false);
    }

    public boolean openMatrixInventory(@Nonnull EntityPlayer entityPlayer, @Nonnull IMatrixInventoryProvider matrixInventoryProvider, boolean force) {
        if (force) {
            closeOldAndOpenMatrixInventory(entityPlayer, matrixInventoryProvider);
            return true;
        }
        synchronized (activeOpenedInventories) {
            if (activeOpenedInventories.get(entityPlayer.getCommandSenderName()) != null) {
                return false;
            }
        }
        if(matrixInventoryProvider.canInteractWith(entityPlayer)) {
            if (matrixInventoryProvider.getMatrixInventory() != null) {
                synchronized (activeOpenedInventories) {
                    activeOpenedInventories.put(entityPlayer.getCommandSenderName(), matrixInventoryProvider);
                    return true;
                }
            }
        }
        return false;
    }

    public void closeOldAndOpenMatrixInventory(@Nonnull EntityPlayer entityPlayer, @Nonnull IMatrixInventoryProvider matrixInventoryProvider) {
        if(matrixInventoryProvider.canInteractWith(entityPlayer)) {
            if (matrixInventoryProvider.getMatrixInventory() != null) {
                synchronized (activeOpenedInventories) {
                    if (activeOpenedInventories.get(entityPlayer.getCommandSenderName()) != null) {
                        activeOpenedInventories.remove(entityPlayer.getCommandSenderName());
                    }
                    activeOpenedInventories.put(entityPlayer.getCommandSenderName(), matrixInventoryProvider);
                }
            }
        }
    }

    public void closeStaticMatrixInventory(@Nonnull EntityPlayer entityPlayer) {
        synchronized (activeOpenedInventories) {
            IMatrixInventoryProvider matrixInventoryProvider = activeOpenedInventories.get(entityPlayer.getCommandSenderName());
            if(matrixInventoryProvider instanceof StaticFakeMatrixInventoryProvider && ((StaticFakeMatrixInventoryProvider) matrixInventoryProvider).container != entityPlayer.openContainer) {
                activeOpenedInventories.remove(entityPlayer.getCommandSenderName());
            }
        }
    }

    public void closeCurrentMatrixInventory(@Nonnull EntityPlayer entityPlayer) {
        synchronized (activeOpenedInventories) {
            activeOpenedInventories.remove(entityPlayer.getCommandSenderName());
        }
    }

    @Nullable
    public MatrixInventory getTransactionMatrixInventory(@Nonnull EntityPlayer entityPlayer) {
        synchronized (activeOpenedInventories) {
            IMatrixInventoryProvider matrixInventoryProvider = activeOpenedInventories.get(entityPlayer.getCommandSenderName());
            return matrixInventoryProvider == null ? null : matrixInventoryProvider.getMatrixInventory();
        }
    }

    @Nullable
    public IMatrixInventoryProvider getTransactionMatrixInventoryProvider(@Nonnull EntityPlayer entityPlayer) {
        synchronized (activeOpenedInventories) {
            return activeOpenedInventories.get(entityPlayer.getCommandSenderName());
        }
    }

    @Getter
    private static class StaticFakeMatrixInventoryProvider extends FakeMatrixInventoryProvider {

        private final int x, y, z;
        private final IInventory inventory;
        private final Container container;

        public StaticFakeMatrixInventoryProvider(int x, int y, int z, IInventory inventory, Container container) {
            super(MatrixInventoryFactory.create(inventory));
            this.x = x;
            this.y = y;
            this.z = z;
            this.inventory = inventory;
            this.container = container;
        }

        @Override
        public boolean canInteractWith(@Nonnull EntityPlayer entityPlayer) {
            return entityPlayer.getDistance(x, y, z) < 4;
        }

        @Override
        public void onMatrixInvItemRemoved(@Nonnull ItemStack itemStack) {
            for (int i = 0; i < inventory.getSizeInventory(); i++) {
                ItemStack stackInSlot = inventory.getStackInSlot(i);
                if (stackInSlot != null && ItemStack.areItemStacksEqual(itemStack, stackInSlot)) {
                    inventory.setInventorySlotContents(i, null);
                    break;
                }
            }
        }

        @Override
        public void onMatrixInvItemAdded(@Nonnull ItemStack itemStack) {
            for (int i = 0; i < inventory.getSizeInventory(); i++) {
                ItemStack stackInSlot = inventory.getStackInSlot(i);
                if (stackInSlot == null) {
                    inventory.setInventorySlotContents(i, itemStack);
                    break;
                }
            }
//            getMatrixInventory().removeItem(itemStack);
        }
    }

    @Getter
    private static class TileEntityFakeMatrixInventoryProvider<T extends TileEntity & IInventory> extends FakeMatrixInventoryProvider {

        private final T tileEntity;

        public TileEntityFakeMatrixInventoryProvider(T tileEntity) {
            super(MatrixInventoryFactory.create(tileEntity));
            this.tileEntity = tileEntity;
        }

        @Override
        public boolean canInteractWith(@Nonnull EntityPlayer entityPlayer) {
            return entityPlayer.getDistance(tileEntity.xCoord, tileEntity.yCoord, tileEntity.zCoord) < 4;
        }

        @Override
        public void onMatrixInvItemRemoved(@Nonnull ItemStack itemStack) {
            for (int i = 0; i < tileEntity.getSizeInventory(); i++) {
                ItemStack stackInSlot = tileEntity.getStackInSlot(i);
                if (stackInSlot != null && ItemStack.areItemStacksEqual(itemStack, stackInSlot)) {
                    tileEntity.setInventorySlotContents(i, null);
                    break;
                }
            }
        }

        @Override
        public void onMatrixInvItemAdded(@Nonnull ItemStack itemStack) {
            for (int i = 0; i < tileEntity.getSizeInventory(); i++) {
                ItemStack stackInSlot = tileEntity.getStackInSlot(i);
                if (stackInSlot == null) {
                    tileEntity.setInventorySlotContents(i, itemStack);
                    break;
                }
            }
//            getMatrixInventory().removeItem(itemStack);
        }
    }

    @Getter
    private static class GhostFakeMatrixInventoryProvider extends FakeMatrixInventoryProvider {

        private final IInventory iInventory;

        public GhostFakeMatrixInventoryProvider(IInventory iInventory) {
            super(MatrixInventoryFactory.create(iInventory));
            this.iInventory = iInventory;
        }

        @Override
        public void onMatrixInvItemRemoved(@Nonnull ItemStack itemStack) {
            for (int i = 0; i < iInventory.getSizeInventory(); i++) {
                ItemStack stackInSlot = iInventory.getStackInSlot(i);
                if (stackInSlot != null && ItemStack.areItemStacksEqual(itemStack, stackInSlot)) {
                    iInventory.setInventorySlotContents(i, null);
                    break;
                }
            }
        }

        @Override
        public void onMatrixInvItemAdded(@Nonnull ItemStack itemStack) {
            for (int i = 0; i < iInventory.getSizeInventory(); i++) {
                ItemStack stackInSlot = iInventory.getStackInSlot(i);
                if (stackInSlot == null) {
                    iInventory.setInventorySlotContents(i, itemStack);
                    break;
                }
            }
//            getMatrixInventory().removeItem(itemStack);
        }

        @Override
        public boolean canInteractWith(@Nonnull EntityPlayer entityPlayer) {
            return true;
        }
    }

    @Getter
    @RequiredArgsConstructor
    private abstract static class FakeMatrixInventoryProvider implements IMatrixInventoryProvider {

        private final MatrixInventory matrixInventory;

        @Override
        public boolean canInteractWith(@Nonnull EntityPlayer entityPlayer) {
            return true;
        }

        @Override
        public boolean isAddingItemNotAllowed(@Nonnull ItemStack itemStack) {
            return false;
        }
    }
}
