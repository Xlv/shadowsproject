package ru.xlv.mochar.location;

import lombok.Getter;
import ru.xlv.core.common.util.config.Configurable;
import ru.xlv.core.common.util.config.IConfigGson;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@Getter
public class LocationConfig implements IConfigGson {

    @Getter
    @Configurable
    public static class LocationElement {
        private final String name = null;
        private final int x0 = 0;
        private final int x1 = 0;
        private final int z0 = 0;
        private final int z1 = 0;
        private final boolean allowPvp = false;
    }

    @Configurable
    private final List<LocationElement> locations = new ArrayList<>();

    @Override
    public File getConfigFile() {
        return new File("config/location/config.json");
    }
}
