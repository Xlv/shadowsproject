package ru.xlv.mochar.location;

import lombok.Getter;
import lombok.ToString;
import ru.xlv.core.common.util.config.Configurable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@ToString
@Getter
public class Location implements Serializable {

    @Configurable
    private String name;
    @Configurable
    private final List<LocationFlag> locationFlags = new ArrayList<>();
    @Configurable
    private final int x0, z0, x1, z1;

    public Location(int x0, int z0, int x1, int z1, String name) {
        this(x0, z0, x1, z1);
        this.name = name;
    }

    public Location(int x0, int z0, int x1, int z1) {
        this.x0 = Math.min(x0, x1);
        this.x1 = Math.max(x0, x1);
        this.z0 = Math.min(z0, z1);
        this.z1 = Math.max(z0, z1);
    }

    public boolean isInside(int x, int y) {
        return x >= x0 && x <= x1 && y >= z0 && y <= z1;
    }

    public void addFlag(LocationFlag locationFlag) {
        locationFlags.add(locationFlag);
    }
}
