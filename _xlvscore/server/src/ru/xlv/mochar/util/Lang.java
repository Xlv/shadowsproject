package ru.xlv.mochar.util;

public class Lang {

    public static final String SKILL_ALREADY_ACTIVE_MESSAGE = "Способность уже активна.";
    public static final String SKILL_RECHARGING_MESSAGE = "Способность еще перезаряжается.";
    public static final String EXP_ADD_MESSAGE = "Вы получили {0} {1} опыта.";
    public static final String EXP_CONSUME_MESSAGE = "Вы потратили {0} {1} опыта.";
    public static final String AREA_ENTER_MESSAGE = "Вы вошли в зону: {0}";
}
