package ru.xlv.core.command;

import cpw.mods.fml.common.event.FMLServerStartingEvent;
import lombok.SneakyThrows;
import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import org.reflections.Reflections;
import org.reflections.scanners.MethodAnnotationsScanner;
import ru.xlv.mochar.util.Utils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Set;

public class CommandScanner {

    @SneakyThrows
    public static void scanAndRegister(FMLServerStartingEvent event) {
        Set<Method> scan = scan();
        for (Method method : scan) {
            Command annotation = method.getAnnotation(Command.class);
            event.registerServerCommand(new CommandBase() {
                @Override
                public String getCommandName() {
                    return annotation.value().equals("") ? method.getName() : annotation.value();
                }

                @Override
                public String getCommandUsage(ICommandSender p_71518_1_) {
                    return null;
                }

                @Override
                public void processCommand(ICommandSender p_71515_1_, String[] p_71515_2_) {
                    if(!(p_71515_1_ instanceof EntityPlayer)) return;
                    EntityPlayer entityPlayer = (EntityPlayer) p_71515_1_;
                    if (annotation.isOpCommand() && !Utils.isPlayerOp(entityPlayer)) {
                        return;
                    }
                    try {
                        method.invoke(null, entityPlayer, p_71515_2_);
                    } catch (IllegalAccessException | InvocationTargetException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    private static Set<Method> scan() {
        Reflections reflections = new Reflections("ru", new MethodAnnotationsScanner());
        return reflections.getMethodsAnnotatedWith(Command.class);
    }
}
