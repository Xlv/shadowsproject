package ru.xlv.core;

import com.google.common.collect.Queues;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListenableFutureTask;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.*;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.TickEvent;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import ru.xlv.core.achievement.AchievementHandler;
import ru.xlv.core.achievement.AchievementLocalization;
import ru.xlv.core.command.CommandScanner;
import ru.xlv.core.common.XlvsCoreCommon;
import ru.xlv.core.common.block.BlockRegistry;
import ru.xlv.core.common.item.ItemRegistry;
import ru.xlv.core.common.network.PacketRegistry;
import ru.xlv.core.common.util.CommonUtils;
import ru.xlv.core.common.util.StepSoundRegistry;
import ru.xlv.core.common.util.SyncResultHandler;
import ru.xlv.core.database.DatabaseManager;
import ru.xlv.core.event.DamageEventListener;
import ru.xlv.core.event.EventListener;
import ru.xlv.core.event.OpenContainerEventListener;
import ru.xlv.core.event.OpenDoorEventListener;
import ru.xlv.core.module.ServerModule;
import ru.xlv.core.network.*;
import ru.xlv.core.player.PlayerManagerServer;
import ru.xlv.core.schedule.ScheduledAnnotationExecutor;
import ru.xlv.core.service.INotificationService;
import ru.xlv.core.util.CoreLocalization;
import ru.xlv.core.util.SkillLocalization;
import ru.xlv.mochar.network.*;
import ru.xlv.mochar.network.matrix.*;
import ru.xlv.mochar.network.skill.*;
import ru.xlv.mochar.player.character.skill.SkillConfig;
import ru.xlv.mochar.player.character.skill.SkillHandler;
import ru.xlv.mochar.player.character.skill.SkillRegistry;
import ru.xlv.mochar.player.stat.StatLocalization;

import javax.annotation.Nullable;
import java.util.Queue;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;

@Mod(
        name = "XlvsCore",
        modid = XlvsCore.MODID,
        version = "1.0"
)
@Getter
public class XlvsCore {

    public static final String MODID = "xlvscore";

    @Mod.Instance(MODID)
    public static XlvsCore INSTANCE;

    @Getter(AccessLevel.NONE)
    private final Queue<ListenableFutureTask<?>> queue = Queues.newArrayDeque();
    @Getter(AccessLevel.NONE)
    private final Thread mainThread = Thread.currentThread();

    private final ScheduledAnnotationExecutor scheduledAnnotationExecutor = new ScheduledAnnotationExecutor();

    private BlockRegistry blockRegistry;
    private PacketHandlerServer packetHandler;
    private PlayerManagerServer playerManager;
    private DatabaseManager databaseManager;
    private final AchievementHandler achievementHandler = new AchievementHandler();
    private final AchievementLocalization achievementLocalization = new AchievementLocalization();
    private final StatLocalization statLocalization = new StatLocalization();
    private final CoreLocalization localization = new CoreLocalization();
    private final SkillLocalization skillLocalization = new SkillLocalization();
    private final SkillConfig skillConfig = new SkillConfig();

    /**
     * Сервис предоставляет возможность отправлять уведомления игрокам
     * */
    @Setter
    @Nullable
    private INotificationService notificationService;

    @Mod.EventHandler
    @SuppressWarnings("unused")
    public void event(FMLPreInitializationEvent event) {
        XlvsCoreCommon.scanForModules(ServerModule.class, this);
        databaseManager = new DatabaseManager();
        databaseManager.init();

        ItemRegistry.register();
        blockRegistry = new BlockRegistry();

        PacketRegistry packetRegistry = new PacketRegistry();
        packetRegistry.register(MODID,
                new PacketSkillList(),
                new PacketSkillUse(),
                new PacketSkillSelect(),
                new PacketEnterLocation(),
                new PacketMatrixPlayerInventorySync(),
                new PacketMatrixInventoryItemMove(),
                new PacketMatrixInventorySpecItemMove(),
                new PacketMatrixInventoryTransactionSync(),
                new PacketMatrixInventoryDropItem(),
                new PacketClientPlayerSync(),
                new PacketStatSync(),
                new PacketAchievementList(),
                new PacketAchievementNew(),
                new PacketItemPickUp(),
                new PacketMatrixInventorySpecSpecItemMove(),
                new PacketPlayerMainSync(),
                new PacketSkillLearn(),
                new PacketSkillAllList(),
                new PacketSkillBuildCreate(),
                new PacketSkillBuildRemove(),
                new PacketSkillBuildSync(),
                new PacketSkillBuildChange(),
                new PacketItemBaseSync(),
                new PacketPlayerAttributesGet(),
                new PacketMatrixInventoryTransactionOpenClose(),
                new PacketMatrixInventoryTransactionItemMove(),
                new PacketMatrixInventoryTransactionTakeAll(),
                new PacketPlayerDeathEvent(),
                new PacketMatrixInventoryTransactionItemMultiMove(),
                new PacketItemUse(),
                new PacketPlaySound(),
                new PacketSkillSync(),
                new PacketMatrixInventoryTransactionGuiOpen(),
                new PacketClientPlayerGet(),
                new PacketDoorOpen(),
                new PacketSysTimeGet()
        );
        packetHandler = new PacketHandlerServer(packetRegistry);
        SyncResultHandler.setMainThreadExecutor(this::runUsingMainThread);

        playerManager = new PlayerManagerServer();
        SkillRegistry.load();
        SkillHandler.init();

        localization.load();
        achievementLocalization.load();
        statLocalization.load();
        skillLocalization.load();
        skillConfig.load();

        FMLCommonHandler.instance().bus().register(this);
        XlvsCoreCommon.MODULE_MANAGER.preInit();
    }

    @Mod.EventHandler
    @SuppressWarnings("unused")
    public void event(FMLInitializationEvent event) {
        CommonUtils.registerEvents(new ru.xlv.mochar.event.EventListener());
        CommonUtils.registerEvents(new DamageEventListener());
        CommonUtils.registerEvents(new OpenContainerEventListener());
        CommonUtils.registerEvents(new OpenDoorEventListener());
        EventListener target = new EventListener();
        XlvsCoreCommon.EVENT_BUS.register(target);
        CommonUtils.registerEvents(target);
        StepSoundRegistry.initAndGet();
        XlvsCoreCommon.MODULE_MANAGER.init();
    }

    @Mod.EventHandler
    @SuppressWarnings("unused")
    public void event(FMLPostInitializationEvent event) {
        playerManager.init();
        scheduledAnnotationExecutor.init();
        XlvsCoreCommon.MODULE_MANAGER.postInit();
        getPacketHandler().getPacketRegistry().applyRegistration();
    }

    @Mod.EventHandler
    @SuppressWarnings("unused")
    public void event(FMLServerStoppingEvent event) {
        scheduledAnnotationExecutor.shutdown();
        databaseManager.shutdown();
    }

    @Mod.EventHandler
    @SuppressWarnings("unused")
    public void event(FMLServerStartedEvent event) {
        XlvsCoreCommon.MODULE_MANAGER.constructed();
    }

    @SuppressWarnings("unused")
    @Mod.EventHandler
    public void event(FMLServerStartingEvent event) {
        CommandScanner.scanAndRegister(event);
    }

    @SubscribeEvent
    @SuppressWarnings("unused")
    public void event(TickEvent.ServerTickEvent event) {
        synchronized (queue) {
            while (!queue.isEmpty()) {
                queue.poll().run();
            }
        }
    }

    /**
     * позволяет выполнить задачу в основном потоке игры.
     * @return фьючер, который позволяет отслеживать состояние задачи.
     * */
    public <T> ListenableFuture<T> runUsingMainThread(Callable<T> p_152343_1_) {
        if (Thread.currentThread() != mainThread) {
            ListenableFutureTask<T> listenableFutureTask = ListenableFutureTask.create(p_152343_1_);
            synchronized (queue) {
                queue.add(listenableFutureTask);
                return listenableFutureTask;
            }
        } else {
            try {
                //noinspection UnstableApiUsage
                return Futures.immediateFuture(p_152343_1_.call());
            } catch (Exception exception) {
                //noinspection UnstableApiUsage
                return Futures.immediateFailedCheckedFuture(exception);
            }
        }
    }

    /**
     * позволяет выполнить задачу в основном потоке игры.
     * @return фьючер, который позволяет отслеживать состояние задачи.
     * */
    @SuppressWarnings("all")
    public ListenableFuture<Object> runUsingMainThread(Runnable p_152344_1_) {
        return runUsingMainThread(Executors.callable(p_152344_1_));
    }
}
