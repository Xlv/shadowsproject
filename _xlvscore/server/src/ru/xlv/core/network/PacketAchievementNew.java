package ru.xlv.core.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.achievement.Achievement;
import ru.xlv.core.achievement.AchievementUtils;
import ru.xlv.core.common.network.IPacketOutServer;

import java.io.IOException;

@NoArgsConstructor
public class PacketAchievementNew implements IPacketOutServer {

    private Achievement achievement;

    public PacketAchievementNew(Achievement achievement) {
        this.achievement = achievement;
    }

    @Override
    public void write(EntityPlayer entityPlayer, ByteBufOutputStream bbos) throws IOException {
        AchievementUtils.writeAchievement(achievement, bbos);
    }
}
