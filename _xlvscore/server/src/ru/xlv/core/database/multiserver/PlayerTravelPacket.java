package ru.xlv.core.database.multiserver;

import io.netty.buffer.ByteBufOutputStream;
import lombok.Getter;
import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.common.network.IPacketInOnServer;
import ru.xlv.core.common.network.IPacketOutServer;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.core.common.util.ServerPosition;
import ru.xlv.core.util.JsonUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collections;
import java.util.List;

@Getter
@NoArgsConstructor
public class PlayerTravelPacket implements IPacketOutServer, IPacketInOnServer {

    private ServerPosition from, to;
    private final List<Object> metadata = new ArrayList<>();

    public PlayerTravelPacket(ServerPosition from, ServerPosition to, Object... metadata) {
        this.from = from;
        this.to = to;
        Collections.addAll(this.metadata, metadata);
    }

    @Override
    public void read(EntityPlayer entityPlayer, ByteBufInputStream bbis) throws IOException {
        from = new ServerPosition(bbis.readUTF(), bbis.readInt(), bbis.readDouble(), bbis.readDouble(), bbis.readDouble());
        to = new ServerPosition(bbis.readUTF(), bbis.readInt(), bbis.readDouble(), bbis.readDouble(), bbis.readDouble());
        int c = bbis.readInt();
        try {
            for (int i = 0; i < c; i++) {
                Class<?> aClass = Class.forName(bbis.readUTF());
                metadata.add(JsonUtils.SIMPLE_GSON.fromJson(new String(Base64.getDecoder().decode(bbis.readUTF())), aClass));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void write(EntityPlayer entityPlayer, ByteBufOutputStream bbos) throws IOException {
        bbos.writeUTF(from.getServerAddress());
        bbos.writeInt(from.getDimension());
        bbos.writeDouble(from.getX());
        bbos.writeDouble(from.getY());
        bbos.writeDouble(from.getZ());
        bbos.writeUTF(to.getServerAddress());
        bbos.writeInt(to.getDimension());
        bbos.writeDouble(to.getX());
        bbos.writeDouble(to.getY());
        bbos.writeDouble(to.getZ());
        bbos.writeInt(metadata.size());
        for (Object object : metadata) {
            bbos.writeUTF(object.getClass().getName());
            bbos.writeUTF(Base64.getEncoder().encodeToString(JsonUtils.SIMPLE_GSON.toJson(object).getBytes()));
        }
    }
}
