package ru.xlv.core.database.mariadb;

import lombok.SneakyThrows;
import ru.xlv.core.common.util.BiHolder;
import ru.xlv.core.database.AbstractProviderDB;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.sql.*;

public class MariaDBProvider extends AbstractProviderDB<ConfigMariaDB> {

    private Connection connection;

    public MariaDBProvider(ConfigMariaDB config) {
        super(config);
    }

    @SneakyThrows
    @Override
    public void init() {
        connection = DriverManager.getConnection(
                String.format("jdbc:mariadb://%s:%s/%s", config.getHost(), config.getPort(), config.getDatabase()),
                config.getUsername(),
                config.getPassword()
        );
    }

    @SneakyThrows
    @Override
    public void shutdown() {
        if(connection != null) {
            connection.close();
        }
    }

    @Nullable
    public BiHolder<Statement, ResultSet> doRequestAsync(@Nonnull String sql) {
        return doRequestAsync(sql, 0);
    }

    @SneakyThrows
    private BiHolder<Statement, ResultSet> doRequestAsync(@Nonnull String sql, int deep) {
        if(deep < 3) {
            if (connection.isClosed()) {
                init();
            }
            Statement statement = connection.createStatement();
            try {
                ResultSet resultSet = statement.executeQuery(sql);
                return new BiHolder<>(statement, resultSet);
            } catch (SQLNonTransientConnectionException e) {
                doRequestAsync(sql, deep + 1);
            }
        }
        return null;
    }
}
