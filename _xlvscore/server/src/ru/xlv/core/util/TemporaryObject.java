package ru.xlv.core.util;

import lombok.Getter;
import lombok.Setter;

public class TemporaryObject<T> {

    @Setter
    @Getter
    private T object;
    @Setter
    private long removalPeriod;
    private long creationTimeMills;

    public TemporaryObject(T object, long removalPeriod) {
        this.object = object;
        this.removalPeriod = removalPeriod;
        this.creationTimeMills = System.currentTimeMillis();
    }

    public boolean shouldBeRemoved() {
        return System.currentTimeMillis() - creationTimeMills >= removalPeriod;
    }

    public void updateTimer() {
        creationTimeMills = System.currentTimeMillis();
    }
}
