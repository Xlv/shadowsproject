package ru.xlv.core.util;

import com.google.gson.*;
import com.google.gson.internal.JsonReaderInternalAccess;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import gnu.trove.list.TIntList;
import gnu.trove.list.array.TIntArrayList;
import gnu.trove.map.TIntIntMap;
import gnu.trove.map.hash.TIntIntHashMap;
import lombok.SneakyThrows;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.JsonToNBT;
import net.minecraft.nbt.NBTException;
import net.minecraft.nbt.NBTTagCompound;
import ru.xlv.core.common.database.DatabaseAdaptedBy;
import ru.xlv.core.common.database.DatabaseValue;
import ru.xlv.core.item.ItemStackFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

public class JsonUtils {

    public static final TypeAdapter<ItemStack> ITEM_STACK_TYPE_ADAPTER = new TypeAdapter<ItemStack>() {
        @Override
        public void write(JsonWriter out, ItemStack value) throws IOException {
            if(value == null) {
                return;
            }
            out.beginArray();
            Item item = value.getItem();
            if(item != null) {
                out.value(Item.itemRegistry.getNameForObject(item));
                out.value(value.stackSize);
                out.value(value.getItemDamage());
                if (value.getTagCompound() != null) {
                    out.value(JsonUtils.nbtToJson(value.getTagCompound()));
                } else {
                    out.value("");
                }
            } else {
//                out.value("null");
//                out.value(1);
//                out.value(0);
//                out.value("");
                throw new IOException("Cannot save an itemStack " + value);
            }
            out.endArray();
        }

        @Override
        public ItemStack read(JsonReader in) {
            try {
                in.beginArray();
                String unlocalizedName = in.nextString();
                int amount = in.nextInt();
                int damage = in.nextInt();
                String jsonNbt = in.nextString();
                ItemStack itemStack = ItemStackFactory.create(unlocalizedName, amount, damage, jsonNbt);
                if(itemStack == null) {
                    throw new IOException("item with unlocalized name " + unlocalizedName + " was not found.");
                }
                in.endArray();
                return itemStack;
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    };


    public static final TypeAdapter<TIntList> TINT_LIST_TYPE_ADAPTER = new TypeAdapter<TIntList>() {
        @Override
        public TIntList read(JsonReader in) throws IOException {
            if (in.peek() == JsonToken.NULL) {
                in.nextNull();
                return null;
            }
            TIntList tIntList = new TIntArrayList();
            in.beginArray();
            while (in.hasNext()) {
                int i = in.nextInt();
                tIntList.add(i);
            }
            in.endArray();
            return tIntList;
        }

        @Override
        public void write(JsonWriter out, TIntList tIntList) throws IOException {
            if (tIntList == null) {
                out.nullValue();
                return;
            }
            out.beginArray();
            for (int i = 0; i < tIntList.size(); i++) {
                out.value(tIntList.get(i));
            }
            out.endArray();
        }
    };

    public static final
    TypeAdapter<TIntIntMap> TINTINT_MAP_TYPE_ADAPTER = new TypeAdapter<TIntIntMap>() {
        @Override
        public void write(JsonWriter out, TIntIntMap value) throws IOException {
            if (value == null) {
                out.nullValue();
                return;
            }
            out.beginObject();
            for (int key : value.keys()) {
                out.name(String.valueOf(key));
                out.value(value.get(key));
            }
            out.endObject();
        }

        @Override
        public TIntIntMap read(JsonReader in) throws IOException {
            JsonToken peek = in.peek();
            if (peek == JsonToken.NULL) {
                in.nextNull();
                return null;
            }
            TIntIntMap map = new TIntIntHashMap();
            if (peek == JsonToken.BEGIN_ARRAY) {
                in.beginArray();
                while (in.hasNext()) {
                    in.beginArray(); // entry array
                    int key = in.nextInt();
                    int value = in.nextInt();
                    int replaced = map.put(key, value);
                    if (replaced != 0) {//?
                        throw new JsonSyntaxException("duplicate key: " + key);
                    }
                    in.endArray();
                }
                in.endArray();
            } else {
                in.beginObject();
                while (in.hasNext()) {
                    JsonReaderInternalAccess.INSTANCE.promoteNameToValue(in);
                    int key = in.nextInt();
                    int value = in.nextInt();
                    int replaced = map.put(key, value);
                    if (replaced != 0) {//?
                        throw new JsonSyntaxException("duplicate key: " + key);
                    }
                }
                in.endObject();
            }
            return map;
        }
    };

    public static final Gson SIMPLE_GSON = new GsonBuilder()
            .registerTypeAdapter(ItemStack.class, ITEM_STACK_TYPE_ADAPTER)
            .setPrettyPrinting()
            .create();

    public static final Gson DB_GSON;

    static {
        DB_GSON = new GsonBuilder()
                    .registerTypeAdapter(TIntList.class, TINT_LIST_TYPE_ADAPTER)
                    .registerTypeAdapter(TIntIntMap.class, TINTINT_MAP_TYPE_ADAPTER)
                    .registerTypeAdapter(ItemStack.class, ITEM_STACK_TYPE_ADAPTER)
                    .registerTypeAdapterFactory(new TypeAdapterFactory() {
                        private final Map<Class<?>, Object> fieldTypeAdapters = new HashMap<>();

                        @SneakyThrows
                        @Override
                        @SuppressWarnings("unchecked")
                        public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> type) {
                            DatabaseAdaptedBy annotation = type.getRawType().getAnnotation(DatabaseAdaptedBy.class);
                            if(annotation != null) {
//                                System.out.println(type.getRawType());
                                return (TypeAdapter<T>) annotation.value().newInstance();
                            }
                            Object o = fieldTypeAdapters.get(type.getRawType());
                            if(o != null) {
                                return (TypeAdapter<T>) fieldTypeAdapters.remove(type.getRawType());
                            }
                            for (Field field : type.getRawType().getDeclaredFields()) {
                                annotation = field.getAnnotation(DatabaseAdaptedBy.class);
                                if(annotation != null) {
                                    o = fieldTypeAdapters.get(field.getType());
                                    if (o != null) {
                                        System.out.println("field serialize adapter for " + field + " already registered. This one will be ignored.");
                                    } else {
//                                        System.out.println("found serialize adapter for " + field.getType());
                                        fieldTypeAdapters.put(field.getType(), annotation.value().newInstance());
                                    }
                                }
                            }
                            return null;
                        }
                    })
                    .setExclusionStrategies(new ExclusionStrategy() {
                        @Override
                        public boolean shouldSkipField(FieldAttributes f) {
                            return f.getDeclaringClass().getAnnotation(DatabaseValue.class) == null && f.getAnnotation(DatabaseValue.class) == null;
                        }

                        @Override
                        public boolean shouldSkipClass(Class<?> clazz) {
                            return false;
                        }
                    })
                    .setPrettyPrinting()
                    .create();
    }

    @Nonnull
    public static String nbtToJson(@Nonnull NBTTagCompound nbtTagCompound) {
        return nbtTagCompound.toString();
    }

    //не конвертит адекватно массивы
    @Nullable
    public static NBTTagCompound jsonToNBT(@Nullable String json) {
        if(json != null) {
            try {
                return (NBTTagCompound) JsonToNBT.func_150315_a(json);
            } catch (NBTException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static String itemStackToJson(ItemStack itemStack) {
        String stringedNbtTagCompound = nbtToJson(itemStack.getTagCompound() != null ? itemStack.getTagCompound() : new NBTTagCompound());
        ItemStackModel itemStackModel = new ItemStackModel(
                Item.getIdFromItem(itemStack.getItem()),
                itemStack.stackSize,
                itemStack.getItemDamage(),
                stringedNbtTagCompound
        );
        return SIMPLE_GSON.toJson(itemStackModel);
    }

    public static ItemStack jsonToItemStack(String json) {
        ItemStackModel itemStackModel = SIMPLE_GSON.fromJson(json, ItemStackModel.class);
        ItemStack itemStack = new ItemStack(Item.getItemById(itemStackModel.getId()));
        itemStack.stackSize = itemStackModel.getCount();
        itemStack.setItemDamage(itemStackModel.getDamage());
        itemStack.setTagCompound(jsonToNBT(itemStackModel.getStringedNbtTagCompound()));
        return itemStack;
    }
}
