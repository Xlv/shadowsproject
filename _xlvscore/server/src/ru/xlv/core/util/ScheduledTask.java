package ru.xlv.core.util;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ScheduledTask {

    private final long period;
    private final Runnable runnable;
    private long lastTimeMills;

    public void update() {
        if(System.currentTimeMillis() >= lastTimeMills) {
            lastTimeMills = System.currentTimeMillis() + period;
            runnable.run();
        }
    }
}
