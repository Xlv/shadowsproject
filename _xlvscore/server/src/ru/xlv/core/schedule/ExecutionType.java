package ru.xlv.core.schedule;

public enum ExecutionType {
    SYNC, ASYNC
}
