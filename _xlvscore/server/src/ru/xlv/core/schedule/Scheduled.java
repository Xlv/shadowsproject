package ru.xlv.core.schedule;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * На этапе инициализации XlvsCore, все методы, помеченные этой аннотацией будут вызываться из
 * основного игрового цикла. По сути, это аналог метода подписанного на {@link cpw.mods.fml.common.gameevent.TickEvent.ServerTickEvent}.
 *
 * Методы должны быть публичными и статическими, чтобы рефлектор мог получить к ним доступ.
 * Методы могут иметь какие угодно параметры.
 * */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Scheduled {
    long period() default 0L;
    ExecutionType type() default ExecutionType.SYNC;
}
