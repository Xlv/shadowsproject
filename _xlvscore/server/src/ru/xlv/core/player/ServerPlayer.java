package ru.xlv.core.player;

import lombok.Getter;
import lombok.Setter;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.common.database.DatabaseValue;
import ru.xlv.core.common.player.Player;
import ru.xlv.core.common.player.character.CharacterType;
import ru.xlv.core.util.KeyValueStore;
import ru.xlv.core.util.ScheduledTask;
import ru.xlv.mochar.player.character.Character;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ServerPlayer extends Player {

    @Getter
    private EntityPlayer entityPlayer;
    @Setter
    @Getter
    @DatabaseValue
    private boolean isNewbie = true;
    @Setter
    @Getter
    @DatabaseValue
    private int selectedCharacterIndex;
    @Setter
    @Getter
    private double platinum;
    @Getter
    @DatabaseValue
    private final List<Character> characters = Collections.synchronizedList(new ArrayList<>());
    @DatabaseValue
    private final KeyValueStore keyValueStore = new KeyValueStore();

    private ScheduledTask updateBoostsScheduledTask;

    private boolean isInited;

    @Getter
    private int disableBlockMovingTimer;

    public ServerPlayer(String playerName) {
        super(playerName);
    }

    public void init(EntityPlayer entityPlayer) {
        this.entityPlayer = entityPlayer;
        this.playerName = entityPlayer.getCommandSenderName();
        if(!isInited) {
            isInited = true;
            updateBoostsScheduledTask = new ScheduledTask(200L, () -> {
                synchronized (characters) {
                    getSelectedCharacter().handleAttributeMods(this);
                }
            });
            if(characters.isEmpty()) {
                for (CharacterType value : CharacterType.values()) {
                    Character character = new Character();
                    character.init(this, value);
                    characters.add(character);
                }
            } else {
                for (CharacterType value : CharacterType.values()) {
                    for (Character character : characters) {
                        if(character.getCharacterType() == value) {
                            character.init(this, value);
                            break;
                        }
                    }
                }
            }
        }
    }

    /**
     * Calls 20 times per second if player is online
     * */
    public void update() {
        if(getEntityPlayer() != null) {
            updateBoostsScheduledTask.update();
            synchronized (characters) {
                getSelectedCharacter().update(this);
            }
            if (getEntityPlayer().getFoodStats() != null) {
                if (getEntityPlayer().getFoodStats().getFoodLevel() <= 10) {
                    getEntityPlayer().getFoodStats().addStats(5, 1);
                } else if (getEntityPlayer().getFoodStats().getFoodLevel() > 17) {
                    getEntityPlayer().getFoodStats().addStats(-1, 1);
                }
            }
        }
    }

    public void movePlayer(int dimension, double x, double y, double z) {
        if(isOnline()) {
            if(dimension != getEntityPlayer().dimension) {
                getEntityPlayer().travelToDimension(dimension);
            }
            getEntityPlayer().setPositionAndUpdate(x, y, z);
        }
    }

    public synchronized void setDisableBlockMovingTimer(int disableBlockMovingTimer) {
        this.disableBlockMovingTimer = disableBlockMovingTimer;
    }

    public synchronized KeyValueStore getKeyValueStore() {
        return keyValueStore;
    }

    public Character getSelectedCharacter() {
        if(selectedCharacterIndex < 0 || selectedCharacterIndex > 2 || characters.isEmpty()) return null;
        return characters.get(selectedCharacterIndex);
    }

    @Override
    public boolean isOnline() {
        return super.isOnline() && getEntityPlayer() != null;
    }
}
