package ru.xlv.core.player;

public interface IPlayerTracker {

    void onUpdate(ServerPlayer serverPlayer);
}
