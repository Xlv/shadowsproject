package ru.xlv.core.event;

import cpw.mods.fml.common.eventhandler.Cancelable;
import cpw.mods.fml.common.eventhandler.Event;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import net.minecraft.entity.EntityLivingBase;
import ru.xlv.core.player.ServerPlayer;

@Setter
@Getter
@AllArgsConstructor
@Cancelable
public class EntityDamagePlayerEvent extends Event {

    private final ServerPlayer serverPlayer;
    private final EntityLivingBase entityLivingBase;
    private float amount;

    @Cancelable
    public static class Pre extends EntityDamagePlayerEvent {
        public Pre(ServerPlayer serverPlayer, EntityLivingBase entityLivingBase, float amount) {
            super(serverPlayer, entityLivingBase, amount);
        }
    }

    @Cancelable
    public static class Post extends EntityDamagePlayerEvent {
        public Post(ServerPlayer serverPlayer, EntityLivingBase entityLivingBase, float amount) {
            super(serverPlayer, entityLivingBase, amount);
        }
    }
}
