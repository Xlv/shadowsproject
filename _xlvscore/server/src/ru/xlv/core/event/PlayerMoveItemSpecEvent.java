package ru.xlv.core.event;

import cpw.mods.fml.common.eventhandler.Cancelable;
import cpw.mods.fml.common.eventhandler.Event;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.minecraft.item.ItemStack;
import ru.xlv.core.common.inventory.MatrixInventory;
import ru.xlv.core.player.ServerPlayer;

@Getter
@RequiredArgsConstructor
@Cancelable
public class PlayerMoveItemSpecEvent extends Event {

    private final ServerPlayer serverPlayer;
    private final ItemStack itemStack;
    private final MatrixInventory.SlotType slotType;
    private final boolean fromSpec;
}
