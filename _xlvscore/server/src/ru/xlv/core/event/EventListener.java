package ru.xlv.core.event;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.DamageSource;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.achievement.AchievementType;
import ru.xlv.core.common.XlvsCoreCommon;
import ru.xlv.core.common.event.OnQuestCompleteEvent;
import ru.xlv.core.common.event.PlayerShootsWeaponEvent;
import ru.xlv.core.common.inventory.MatrixInventory;
import ru.xlv.core.common.item.ItemArmor;
import ru.xlv.core.common.item.ItemBase;
import ru.xlv.core.common.item.ItemRegistry;
import ru.xlv.core.common.player.character.ExperienceType;
import ru.xlv.core.common.player.stat.StatIntProvider;
import ru.xlv.core.network.PacketItemBaseSync;
import ru.xlv.core.network.PacketPlaySound;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.core.util.SoundType;
import ru.xlv.mochar.XlvsMainMod;
import ru.xlv.mochar.location.Location;
import ru.xlv.mochar.network.PacketPlayerDeathEvent;
import ru.xlv.mochar.player.character.skill.experience.ExperienceManager;
import ru.xlv.mochar.player.character.skill.result.SkillExecuteResult;
import ru.xlv.mochar.player.stat.StatManager;
import ru.xlv.mochar.player.stat.StatType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EventListener {

    private static Class<?> entityNpcClass;
    static {
        try {
            entityNpcClass = Class.forName("noppes.npcs.entity.EntityNPCInterface");
        } catch (ClassNotFoundException ignored) {}
    }

    private final KillController killController = new KillController();

    @SubscribeEvent
    public void event(PlayerEnterLocationEvent event) {
        for (String discoveredLocationName : event.getServerPlayer().getSelectedCharacter().getDiscoveredLocationNames()) {
            if(discoveredLocationName.equals(event.getLocation().getName())) {
                return;
            }
        }
        event.getServerPlayer().getSelectedCharacter().getDiscoveredLocationNames().add(event.getLocation().getName());
        StatManager statManager = event.getServerPlayer().getSelectedCharacter().getStatManager();
        statManager.getStatProvider(StatType.TOTAL_LOCATION_DISCOVERED).increment();
    }

    @SubscribeEvent
    public void event(LivingDeathEvent event) {
        Entity entityKiller = event.source.getSourceOfDamage();
        boolean isTargetPlayer = event.entityLiving instanceof EntityPlayer;
        boolean isKillerPlayer = entityKiller instanceof EntityPlayer;
        if(isKillerPlayer) {
            if (isTargetPlayer) {
                ServerPlayer target = XlvsCore.INSTANCE.getPlayerManager().getPlayer(event.entityLiving.getCommandSenderName());
                ServerPlayer killer = XlvsCore.INSTANCE.getPlayerManager().getPlayer(entityKiller.getCommandSenderName());
                XlvsCoreCommon.EVENT_BUS.post(new PlayerDeathEvent(target, event.source));
                XlvsCore.INSTANCE.getPacketHandler().sendPacketToPlayer((EntityPlayer) event.entityLiving, new PacketPlayerDeathEvent(entityKiller.getEntityId()));
                killController.onKill(killer, target);
                StatManager statManager = killer.getSelectedCharacter().getStatManager();
                statManager.getStatProvider(StatType.TOTAL_PLAYERS_KILLED).increment();
                statManager = target.getSelectedCharacter().getStatManager();
                statManager.getStatProvider(StatType.TOTAL_DEATHS_FROM_PLAYER).increment();
                statManager.getStatProvider(StatType.TOTAL_DEATHS).increment();
                XlvsCoreCommon.EVENT_BUS.post(new PlayerKilledEvent(target, killer));
            } else if(isEntityNpc(event.entityLiving)) {
                ServerPlayer killer = XlvsCore.INSTANCE.getPlayerManager().getPlayer(entityKiller.getCommandSenderName());
                StatManager statManager = killer.getSelectedCharacter().getStatManager();
                statManager.getStatProvider(StatType.TOTAL_NPCS_KILLED).increment();
            }
        } else if(isTargetPlayer) {
            ServerPlayer target = XlvsCore.INSTANCE.getPlayerManager().getPlayer(event.entityLiving.getCommandSenderName());
            XlvsCoreCommon.EVENT_BUS.post(new PlayerDeathEvent(target, event.source));
            XlvsCore.INSTANCE.getPacketHandler().sendPacketToPlayer((EntityPlayer) event.entityLiving, new PacketPlayerDeathEvent(entityKiller != null ? entityKiller.getEntityId() : -1));
            StatManager statManager = target.getSelectedCharacter().getStatManager();
            if(isEntityNpc(entityKiller)) {
                statManager.getStatProvider(StatType.TOTAL_DEATHS_FROM_NPC).increment();
                statManager.getStatProvider(StatType.TOTAL_DEATHS).increment();
            } else if(event.source == DamageSource.fall) {
                statManager.getStatProvider(StatType.TOTAL_DEATHS_FROM_FALL).increment();
            } else if(event.source == DamageSource.inFire || event.source == DamageSource.onFire) {
                XlvsCore.INSTANCE.getAchievementHandler().updateAchievement(target, AchievementType.DEAD_BY_FIRE);
            }
        }
        XlvsCore.INSTANCE.getPacketHandler().sendPacketToAllAround(event.entityLiving, 16, new PacketPlaySound(event.entityLiving, SoundType.PLAYER_DEATH));
    }

    private boolean isEntityNpc(Entity entity) {
        return entityNpcClass != null && entity != null && entity.getClass().isAssignableFrom(entityNpcClass);
    }

    @SubscribeEvent
    public void event(PlayerLearnedSkillEvent event) {
        StatManager statManager = event.getServerPlayer().getSelectedCharacter().getStatManager();
        statManager.getStatProvider(StatType.TOTAL_LEVEL_AMOUNT).increment();
        XlvsCore.INSTANCE.getAchievementHandler().updateAchievement(event.getServerPlayer(), AchievementType.LEVEL_UP);
    }

    @SubscribeEvent
    public void event(PlayerUsedSkillEvent event) {
        if(event.getSkillExecuteResult() == SkillExecuteResult.SUCCESS) {
            StatManager statManager = event.getServerPlayer().getSelectedCharacter().getStatManager();
            statManager.getStatProvider(StatType.TOTAL_SKILL_USED).increment();
        }
    }

    @SubscribeEvent
    public void event(PlayerCreditsReceivedEvent event) {
        StatManager statManager = event.getServerPlayer().getSelectedCharacter().getStatManager();
        ((StatIntProvider) statManager.getStatProvider(StatType.TOTAL_CREDIT_RECEIVED)).increment(event.getAmount());
    }

    @SubscribeEvent
    public void event(PlayerShootsWeaponEvent event) {
        ServerPlayer serverPlayer = XlvsCore.INSTANCE.getPlayerManager().getPlayer(event.getEntityPlayer());
        StatManager statManager = serverPlayer.getSelectedCharacter().getStatManager();
        statManager.getStatProvider(StatType.TOTAL_SHOOTS).increment();
    }

    @SubscribeEvent
    public void event(PlayerEquipItemEvent event) {
        if (event.getItemStack().getItem() instanceof ItemArmor) {
            ItemArmor.SetFamily setFamily = ((ItemArmor) event.getItemStack().getItem()).getSetFamily();
            if(setFamily != null) {
                if (isSetEquipped(event.getServerPlayer(), setFamily)) {
                    AchievementType achievementType;
                    switch (setFamily) {
                        case SCORPION:
                            achievementType = AchievementType.EQUIP_SCORPION;
                            break;
                        case PTN:
                            achievementType = AchievementType.EQUIP_PALADIN;
                            break;
                        case BKU_0992:
                            achievementType = AchievementType.EQUIP_BKU_0992;
                            break;
                        case AZUR_ES:
                            achievementType = AchievementType.EQUIP_RADIANT;
                            break;
                        case NIHIL:
                            achievementType = AchievementType.EQUIP_MISFORTUNE;
                            break;
                        case MURUS_KL:
                            achievementType = AchievementType.EQUIP_BANK;
                            break;
                        case ENIGMA_B:
                            achievementType = AchievementType.EQUIP_SILENCE;
                            break;
                        case CENSOR:
                            achievementType = AchievementType.EQUIP_CENSOR;
                            break;
                        case SPEC_RIOT:
                            achievementType = AchievementType.EQUIP_SPEC_RIOT;
                            break;
                        default:
                            return;
                    }
                    XlvsCore.INSTANCE.getAchievementHandler().updateAchievement(event.getServerPlayer(), achievementType);
                }
            }
        }
    }

    private boolean isSetEquipped(ServerPlayer serverPlayer, ItemArmor.SetFamily setFamily) {
        MatrixInventory.SlotType[] slotTypes = {
                MatrixInventory.SlotType.HEAD,
                MatrixInventory.SlotType.BODY,
                MatrixInventory.SlotType.BRACERS,
                MatrixInventory.SlotType.LEGS,
                MatrixInventory.SlotType.FEET
        };
        for (MatrixInventory.SlotType slotType : slotTypes) {
            ItemStack itemStack = serverPlayer.getEntityPlayer().inventory.getStackInSlot(slotType.getAssociatedSlotIndex());
            if(itemStack == null || !(itemStack.getItem() instanceof ItemArmor) || ((ItemArmor) itemStack.getItem()).getSetFamily() != setFamily) {
                return false;
            }
        }
        return true;
    }

    @SubscribeEvent
    public void event(OnQuestCompleteEvent event) {
        EntityPlayer player = event.getPlayer();
        ServerPlayer serverPlayer = XlvsCore.INSTANCE.getPlayerManager().getPlayer(player);
        if(serverPlayer != null && serverPlayer.isOnline()) {
            ExperienceManager experienceManager = serverPlayer.getSelectedCharacter().getExperienceManager();
            experienceManager.addExp(serverPlayer, ExperienceType.BATTLE, event.getCombatReward());
            experienceManager.addExp(serverPlayer, ExperienceType.EXPLORE, event.getResearchReward());
            experienceManager.addExp(serverPlayer, ExperienceType.SURVIVE, event.getSurviveReward());
            serverPlayer.getSelectedCharacter().getWallet().addCredits(event.getCredits());
            switch (event.getExperienceType()) {
                case 0: serverPlayer.getSelectedCharacter().getStatManager().getStatProvider(StatType.TOTAL_QUEST_COMPLETED_COMBAT).increment(); break;
                case 1: serverPlayer.getSelectedCharacter().getStatManager().getStatProvider(StatType.TOTAL_QUEST_COMPLETED_EXPLORE).increment(); break;
                case 2: serverPlayer.getSelectedCharacter().getStatManager().getStatProvider(StatType.TOTAL_QUEST_COMPLETED_SURVIVE).increment(); break;
            }
        }
    }

    @SubscribeEvent
    public void event(ServerPlayerLoginEvent event) {
        for (ItemBase itemBase : ItemRegistry.REGISTRY) {
            XlvsCore.INSTANCE.getPacketHandler().sendPacketToPlayer(event.getServerPlayer().getEntityPlayer(), new PacketItemBaseSync(itemBase));
        }
    }

    @Getter
    @RequiredArgsConstructor
    private static class PlayerKills {
        private final ServerPlayer serverPlayer;
        @Setter
        private ServerPlayer lastKiller;
        protected int count;
        protected long lastKillTimeMills;

        private final Map<String, Integer> locationKills = new HashMap<>();
    }

    private static class KillController {

        private static final long KILL_SERIES_COUNT_PERIOD = 15000L;

        private final List<PlayerKills> playerKillList = new ArrayList<>();

        protected void onKill(ServerPlayer killer, ServerPlayer target) {
            PlayerKills killerPlayerKills = getPlayerKills(killer);
            PlayerKills targetPlayerKills = getPlayerKills(target);
            if(target == killerPlayerKills.getLastKiller()) {
                XlvsCore.INSTANCE.getAchievementHandler().updateAchievement(killer, AchievementType.REVENGE);
            }
            targetPlayerKills.setLastKiller(killer);
            Location location = XlvsMainMod.INSTANCE.getLocationManager().getPlayerLocationIn(killer.getPlayerName());
            if (location != null) {
                int locationKills = killerPlayerKills.getLocationKills().getOrDefault(location.getName(), 0);
                killerPlayerKills.getLocationKills().put(location.getName(), locationKills + 1);
            }
            if(System.currentTimeMillis() - killerPlayerKills.lastKillTimeMills < KILL_SERIES_COUNT_PERIOD) {
                StatManager statManager = killer.getSelectedCharacter().getStatManager();
                statManager.getStatProvider(StatType.TOTAL_KILL_SERIES_AMOUNT).increment();
            }
            killerPlayerKills.lastKillTimeMills = System.currentTimeMillis();
        }

        private PlayerKills getPlayerKills(ServerPlayer serverPlayer) {
            PlayerKills playerKills = null;
            synchronized (playerKillList) {
                for (PlayerKills playerKills1 : playerKillList) {
                    if (playerKills1.getServerPlayer() == serverPlayer) {
                        playerKills = playerKills1;
                        break;
                    }
                }
            }
            if (playerKills == null) {
                synchronized (playerKillList) {
                    playerKillList.add(playerKills = new PlayerKills(serverPlayer));
                }
            }
            return playerKills;
        }
    }
}
