package ru.xlv.core.event;

import cpw.mods.fml.common.eventhandler.Cancelable;
import cpw.mods.fml.common.eventhandler.Event;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import ru.xlv.core.player.ServerPlayer;

@Setter
@Getter
@AllArgsConstructor
@Cancelable
public class PlayerConsumeManaEvent extends Event {

    private final ServerPlayer serverPlayer;
    private double amount;
}
