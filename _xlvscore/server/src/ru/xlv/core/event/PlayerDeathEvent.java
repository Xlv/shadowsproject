package ru.xlv.core.event;

import cpw.mods.fml.common.eventhandler.Event;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.minecraft.util.DamageSource;
import ru.xlv.core.player.ServerPlayer;

@Getter
@RequiredArgsConstructor
public class PlayerDeathEvent extends Event {

    private final ServerPlayer serverPlayer;
    private final DamageSource damageSource;
}
