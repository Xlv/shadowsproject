package ru.xlv.core.event;

import cpw.mods.fml.common.eventhandler.Event;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ru.xlv.core.player.ServerPlayer;

@RequiredArgsConstructor
@Getter
public class ServerPlayerLogoutEvent extends Event {

    private final ServerPlayer serverPlayer;
}
