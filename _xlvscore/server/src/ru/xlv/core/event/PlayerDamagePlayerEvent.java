package ru.xlv.core.event;

import cpw.mods.fml.common.eventhandler.Cancelable;
import cpw.mods.fml.common.eventhandler.Event;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import net.minecraft.util.DamageSource;
import ru.xlv.core.player.ServerPlayer;

@Setter
@Getter
@AllArgsConstructor
@Cancelable
public class PlayerDamagePlayerEvent extends Event {

    private final ServerPlayer attacker, target;
    private final DamageSource damageSource;
    private float amount;

    @Cancelable
    public static class Pre extends PlayerDamagePlayerEvent {
        public Pre(ServerPlayer attacker, ServerPlayer target, DamageSource damageSource, float amount) {
            super(attacker, target, damageSource, amount);
        }
    }

    @Cancelable
    public static class Post extends PlayerDamagePlayerEvent {
        public Post(ServerPlayer attacker, ServerPlayer target, DamageSource damageSource, float amount) {
            super(attacker, target, damageSource, amount);
        }
    }
}
