package ru.xlv.core.common.util;

import lombok.experimental.UtilityClass;
import net.minecraft.block.Block;
import ru.xlv.core.util.Flex;
import ru.xlv.core.util.SoundType;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

import static ru.xlv.core.util.SoundType.*;

@UtilityClass
public class StepSoundRegistry {

    private final List<StepSoundRegistryElement> elements = new ArrayList<>();

    public List<StepSoundRegistryElement> initAndGet() {
        elements.clear();
        setSound(getBlock("stone"), BLOCK_CONCRETE_STEP);
        setSound(getBlock("grass"), BLOCK_GRASS_STEP);
        setSound(getBlock("dirt"), BLOCK_DIRT_STEP);
        setSound(getBlock("cobblestone"), BLOCK_CONCRETE_STEP);
        setSound(getBlock("planks"), BLOCK_WOOD_STEP);
        setSound(getBlock("planks"), 1, BLOCK_GRASS_STEP);
        setSound(getBlock("planks"), 3, BLOCK_GRASS_STEP);
        setSound(getBlock("planks"), 4, BLOCK_METAL_STEP);
        setSound(getBlock("bedrock"), BLOCK_CONCRETE_STEP);
        setSound(getBlock("flowing_water"), BLOCK_WATER_STEP);
        setSound(getBlock("water"), BLOCK_WATER_STEP);
        setSound(getBlock("sand"), BLOCK_SAND_STEP);
        setSound(getBlock("gravel"), BLOCK_GRAVEL_STEP);
        setSound(getBlock("gold_ore"), BLOCK_CONCRETE_STEP);
        setSound(getBlock("iron_ore"), BLOCK_CONCRETE_STEP);
        setSound(getBlock("coal_ore"), BLOCK_CONCRETE_STEP);
        setSound(getBlock("log"), BLOCK_HARDWOOD_STEP);
        setSound(getBlock("leaves"), BLOCK_GRASS_STEP);
        setSound(getBlock("glass"), BLOCK_GLASS_STEP);
        setSound(getBlock("lapis_ore"), BLOCK_CONCRETE_STEP);
        setSound(getBlock("lapis_block"), BLOCK_CONCRETE_STEP);
        setSound(getBlock("sandstone"), BLOCK_CONCRETE_STEP);
        setSound(getBlock("wool"), BLOCK_CONCRETE_STEP);
        setSound(getBlock("wool"), 1, BLOCK_METAL_STEP);
        setSound(getBlock("wool"), 7, BLOCK_METAL_STEP);
        setSound(getBlock("wool"), 8, BLOCK_METAL_STEP);
        setSound(getBlock("gold_block"), BLOCK_CONCRETE_STEP);
        setSound(getBlock("iron_block"), BLOCK_CONCRETE_STEP);
        setSound(getBlock("double_stone_slab"), BLOCK_CONCRETE_STEP);
        setSound(getBlock("stone_slab"), BLOCK_CONCRETE_STEP);
        setSound(getBlock("brick_block"), BLOCK_CONCRETE_STEP);
        setSound(getBlock("bookshelf"), BLOCK_WOOD_STEP);
        setSound(getBlock("mossy_cobblestone"), BLOCK_CONCRETE_STEP);
        setSound(getBlock("obsidian"), BLOCK_CONCRETE_STEP);
        setSound(getBlock("oak_stairs"), BLOCK_WOOD_STEP);
        setSound(getBlock("chest"), BLOCK_WOOD_STEP);
        setSound(getBlock("diamond_ore"), BLOCK_CONCRETE_STEP);
        setSound(getBlock("diamond_block"), BLOCK_CONCRETE_STEP);
        setSound(getBlock("wooden_door"), BLOCK_WOOD_STEP);
        setSound(getBlock("stone_stairs"), BLOCK_CONCRETE_STEP);
        setSound(getBlock("iron_door"), BLOCK_METAL_STEP);
        setSound(getBlock("redstone_ore"), BLOCK_CONCRETE_STEP);
        setSound(getBlock("lit_redstone_ore"), BLOCK_CONCRETE_STEP);
        setSound(getBlock("snow_layer"), BLOCK_SNOW_STEP);
        setSound(getBlock("ice"), BLOCK_ICE_STEP);
        setSound(getBlock("snow"), BLOCK_SNOW_STEP);
        setSound(getBlock("clay"), BLOCK_METAL_STEP);
        setSound(getBlock("soul_sand"), BLOCK_MUD_STEP);
        setSound(getBlock("glowstone"), BLOCK_METAL_STEP);
        setSound(getBlock("stained_glass"), BLOCK_GLASS_STEP);
        setSound(getBlock("stonebrick"), BLOCK_CONCRETE_STEP);
        setSound(getBlock("iron_bars"), BLOCK_METAL_STEP);
        setSound(getBlock("glass_pane"), BLOCK_GLASS_STEP);
        setSound(getBlock("brick_stairs"), BLOCK_CONCRETE_STEP);
        setSound(getBlock("stone_brick_stairs"), BLOCK_CONCRETE_STEP);
        setSound(getBlock("mycelium"), BLOCK_GRASS_STEP);
        setSound(getBlock("nether_brick"), BLOCK_METAL_STEP);
        setSound(getBlock("nether_brick_fence"), BLOCK_METAL_STEP);
        setSound(getBlock("nether_brick_stairs"), BLOCK_METAL_STEP);
        setSound(getBlock("cauldron"), BLOCK_METAL_STEP);
        setSound(getBlock("redstone_lamp"), BLOCK_METAL_STEP);
        setSound(getBlock("double_wooden_slab"), BLOCK_WOOD_STEP);
        setSound(getBlock("double_wooden_slab"), 1, BLOCK_GRASS_STEP);
        setSound(getBlock("double_wooden_slab"), 3, BLOCK_GRASS_STEP);
        setSound(getBlock("double_wooden_slab"), 4, BLOCK_METAL_STEP);
        setSound(getBlock("wooden_slab"), BLOCK_WOOD_STEP);
        setSound(getBlock("wooden_slab"), 1, BLOCK_GRASS_STEP);
        setSound(getBlock("wooden_slab"), 3, BLOCK_GRASS_STEP);
        setSound(getBlock("wooden_slab"), 4, BLOCK_METAL_STEP);
        setSound(getBlock("sandstone_stairs"), BLOCK_CONCRETE_STEP);
        setSound(getBlock("emerald_ore"), BLOCK_CONCRETE_STEP);
        setSound(getBlock("emerald_block"), BLOCK_CONCRETE_STEP);
        setSound(getBlock("spruce_stairs"), BLOCK_GRASS_STEP);
        setSound(getBlock("birch_stairs"), BLOCK_WOOD_STEP);
        setSound(getBlock("jungle_stairs"), BLOCK_GRASS_STEP);
        setSound(getBlock("beacon"), BLOCK_METAL_STEP);
        setSound(getBlock("cobblestone_wall"), BLOCK_CONCRETE_STEP);
        setSound(getBlock("redstone_block"), BLOCK_CONCRETE_STEP);
        setSound(getBlock("quartz_ore"), BLOCK_CONCRETE_STEP);
        setSound(getBlock("quartz_block"), BLOCK_CONCRETE_STEP);
        setSound(getBlock("quartz_stairs"), BLOCK_CONCRETE_STEP);
        setSound(getBlock("stained_glass_pane"), BLOCK_GLASS_STEP);
        setSound(getBlock("leaves2"), BLOCK_GRASS_STEP);
        setSound(getBlock("log2"), BLOCK_HARDWOOD_STEP);
        setSound(getBlock("acacia_stairs"), BLOCK_METAL_STEP);
        setSound(getBlock("dark_oak_stairs"), BLOCK_WOOD_STEP);
        setSound(getBlock("hay_block"), BLOCK_GRASS_STEP);
        setSound(getBlock("carpet"), BLOCK_CONCRETE_STEP);
        setSound(getBlock("hardened_clay"), BLOCK_CONCRETE_STEP);
        setSound(getBlock("stained_hardened_clay"), BLOCK_CONCRETE_STEP);
        setSound(getBlock("clay"), BLOCK_CONCRETE_STEP);
        setSound(getBlock("coal_block"), BLOCK_CONCRETE_STEP);
        setSound(getBlock("packed_ice"), BLOCK_ICE_STEP);
        setSound(getBlock("end_stone"), BLOCK_METAL_STEP);
        return elements;
    }

    public void setSound(Block block, SoundType soundType) {
        setSound(block, 0, soundType);
    }

    public void setSound(Block block, int metadata, SoundType soundType) {
        elements.add(new StepSoundRegistryElement(block, metadata, soundType));
    }

    private Block getBlock(String name) {
        return Block.getBlockFromName(name);
    }

    @Nullable
    public SoundType getSoundType(Block block) {
        StepSoundRegistryElement collectionElement = Flex.getCollectionElement(elements, soundRegistryElement -> soundRegistryElement.getBlock() == block);
        return collectionElement == null ? null : collectionElement.getSoundType();
    }

    @Nullable
    public SoundType getSoundType(Block block, int metadata) {
        StepSoundRegistryElement collectionElement = Flex.getCollectionElement(elements, soundRegistryElement -> soundRegistryElement.getBlock() == block && soundRegistryElement.getMetadata() == metadata);
        return collectionElement == null ? null : collectionElement.getSoundType();
    }
}
