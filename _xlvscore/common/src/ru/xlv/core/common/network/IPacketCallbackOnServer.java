package ru.xlv.core.common.network;

import io.netty.buffer.ByteBufOutputStream;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.common.util.ByteBufInputStream;

import java.io.IOException;

public interface IPacketCallbackOnServer extends IPacketCallback {

    /**
     * @param packetCallbackSender использовать только если {@link IPacketCallbackOnServer#handleCallback()} == true, иначе будет отослано два пакета.
     * */
    void read(EntityPlayer entityPlayer, ByteBufInputStream bbis, PacketCallbackSender packetCallbackSender) throws IOException;
    void write(EntityPlayer entityPlayer, ByteBufOutputStream bbos) throws IOException;

    @Deprecated
    @Override
    default void read(ByteBufInputStream bbis) throws IOException {}

    @Deprecated
    @Override
    default void write(ByteBufOutputStream bbos) throws IOException {}

    default boolean handleCallback() {
        return false;
    }
}
