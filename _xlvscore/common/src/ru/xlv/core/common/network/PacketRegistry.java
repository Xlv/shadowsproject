package ru.xlv.core.common.network;

import gnu.trove.TCollections;
import gnu.trove.map.TIntObjectMap;
import gnu.trove.map.TObjectIntMap;
import gnu.trove.map.hash.TIntObjectHashMap;
import gnu.trove.map.hash.TObjectIntHashMap;
import lombok.RequiredArgsConstructor;
import ru.xlv.core.common.util.CommonUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PacketRegistry {

    @RequiredArgsConstructor
    private static class RegisterElement implements Comparable<RegisterElement> {
        private final String modid;
        private final List<IPacket> packets = new ArrayList<>();

        @Override
        public int compareTo(RegisterElement o) {
            return modid.compareTo(o.modid);
        }
    }

    private final TIntObjectMap<IPacket> REGISTRY = TCollections.synchronizedMap(new TIntObjectHashMap<>());
    private final TObjectIntMap<Class<? extends IPacket>> CLASS_REGISTRY = TCollections.synchronizedMap(new TObjectIntHashMap<>());

    private final List<RegisterElement> registerElements = new ArrayList<>();

    private int packetIDCounter;

    public void register(String modid, IPacket... packets) {
        for (IPacket packet : packets) {
            register(modid, packet);
        }
    }

    public void register(String modid, IPacket packet) {
        RegisterElement element = getRegisterElement(modid);
        if (element == null) {
            registerElements.add(element = new RegisterElement(modid));
        }
        element.packets.add(packet);
    }

    public void applyRegistration() {
        Collections.sort(registerElements);
        for (RegisterElement registerElement : registerElements) {
            for (IPacket packet : registerElement.packets) {
                register(packet);
            }
        }
        registerElements.clear();
    }

    private RegisterElement getRegisterElement(String modid) {
        for (RegisterElement registerElement : registerElements) {
            if(registerElement.modid.equals(modid)) {
                return registerElement;
            }
        }
        return null;
    }

    private void register(IPacket packet) {
        int pid = getPacketIDCounter();
        REGISTRY.put(pid, packet);
        CLASS_REGISTRY.put(packet.getClass(), pid);
        register0(packet, pid);
    }

    private void register0(IPacket packet, int id) {
        if(CommonUtils.isDebugEnabled()) {
            System.out.println("registered a " + packet.getClass().getName() + " with id " + id);
        }
    }

    private int getPacketIDCounter() {
        int packetId = packetIDCounter;
        packetIDCounter++;
        return packetId;
    }

    public TIntObjectMap<IPacket> getRegistry() {
        return REGISTRY;
    }

    public TObjectIntMap<Class<? extends IPacket>> getClassRegistry() {
        return CLASS_REGISTRY;
    }
}
