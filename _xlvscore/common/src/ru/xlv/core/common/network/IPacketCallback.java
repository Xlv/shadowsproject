package ru.xlv.core.common.network;

import io.netty.buffer.ByteBufOutputStream;
import ru.xlv.core.common.util.ByteBufInputStream;

import java.io.IOException;

/**
 * Отличается от всех пакетов тем, что дополнительно подписывается и заносится в кеш, пока не дождется ответа от сервера, подписанного тем же id.
 * Когда придет ответ от сервера, будет вызван {@link IPacketCallback#read(ByteBufInputStream)}.
 *
 * Важно понимать, что пакет может быть отправлен только от клиента к серверу и никак иначе. Серверный пакет тоже должен наследовать данный интерфейс.
 * */
public interface IPacketCallback extends IPacket {

    void write(ByteBufOutputStream bbos) throws IOException;
    void read(ByteBufInputStream bbis) throws IOException;
}
