package ru.xlv.core.common.item.quality;

import net.minecraft.init.Items;
import net.minecraft.item.Item;
import ru.xlv.core.common.item.ItemRegistry;
import ru.xlv.core.common.util.config.Configurable;
import ru.xlv.core.common.util.config.IConfig;
import ru.xlv.core.common.util.config.IConfigJson;

import javax.annotation.Nonnull;
import java.io.File;
import java.lang.reflect.Field;
import java.util.*;

/**
 * Needs to getting the quality of any item you want
 * */
@Deprecated
public class ItemQualityAssn implements IConfig, IConfigJson {

    public static final ItemQualityAssn INSTANCE = new ItemQualityAssn();

    /**
     * Temporal list used to convert qualities to string and back.
     *
     * Data example: "[type]/[item_name]/[quality_name]" -> "default/apple/high"
     * Types:
     *      default - for default mc items
     *      ss - for shadow project's items
     * */
    @Configurable
    private ArrayList<String> configContent = new ArrayList<>();

    private Map<Item, EnumItemQuality> qualityMap = Collections.synchronizedMap(new HashMap<>());

    private ItemQualityAssn() {
        load(this);
        for (String s : configContent) {
            String[] ss = s.split("/");
            Item item = null;
            Class<?> clazz = null;
            if(ss[0].equals("default")) {
                clazz = Items.class;
            } else if(ss[0].equals("ss")) {
                clazz = ItemRegistry.class;
            }
            if(clazz == null) continue;
            for (Field declaredField : clazz.getDeclaredFields()) {
                if(declaredField.getName().toLowerCase().equals(ss[1])) {
                    try {
                        item = (Item) declaredField.get(this);
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                    break;
                }
            }
            if(item == null) continue;
            qualityMap.put(item, EnumItemQuality.valueOf(ss[2].toUpperCase()));
        }
    }

    @Nonnull
    public EnumItemQuality getItemQuality(Item item) {
        if(item instanceof IQualityProvider) {
            return ((IQualityProvider) item).getItemQuality();
        } else if(qualityMap.get(item) != null) {
            return qualityMap.get(item);
        }
        return EnumItemQuality.BASIC;
    }

    @Override
    public File getConfigFile() {
        return new File("config/qualityAssn.json");
    }

    @Override
    public boolean createConfigIfNull() {
        return true;
    }

    @Override
    public boolean enablePrettySaving() {
        return true;
    }
}
