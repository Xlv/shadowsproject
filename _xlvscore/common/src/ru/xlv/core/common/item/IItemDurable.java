package ru.xlv.core.common.item;

public interface IItemDurable {

    double getMaxDurability();

    double getDurability();

    /**
     * Affect to attributes of character or not. It needs when we calc damage for example
     * */
    boolean affectAttributes();

    void setDurability(double value);

    default void calcDurabilityOnShootByGun() {
        double d = getDurability() - 1;
        if(d <= 0) d = 0;
        setDurability(d);
    }

    default void calcDurabilityOnTakeDamage(double inputDamage) {
        double d = getDurability() - inputDamage / 10;
        if(d <= 0) d = 0;
        setDurability(d);
    }

    default void resetDurability() {
        setDurability(getMaxDurability());
    }
}
