package ru.xlv.core.common.item;

import cpw.mods.fml.common.registry.GameRegistry;
import lombok.experimental.UtilityClass;
import ru.xlv.core.common.item.tag.EnumItemTag;
import ru.xlv.core.common.util.CommonUtils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@UtilityClass
@SuppressWarnings("unused")
public class ItemRegistry {

    public final List<ItemBase> REGISTRY = Collections.synchronizedList(new ArrayList<>());

    private final ItemBase.ItemBaseBuilder expBuilder = ItemBase.builder().removeAfterApplied(true);
    public final ItemBase ITEM_APPLIED = expBuilder
            .name("exp")
            .appliedInstantly(new AppliedInstantly(AppliedInstantly.Type.EXP, 50))
            .removeAfterApplied(true)
            .build();
    public final ItemBase ITEM_APPLIED1 = expBuilder
            .name("exp1")
            .appliedInstantly(new AppliedInstantly(AppliedInstantly.Type.EXP, 80))
            .removeAfterApplied(true)
            .build();
    public final ItemBase ITEM_APPLIED2 = expBuilder
            .name("exp2")
            .appliedInstantly(new AppliedInstantly(AppliedInstantly.Type.EXP, 120))
            .removeAfterApplied(true)
            .build();
    public final ItemBase ITEM_APPLIED3 = expBuilder
            .name("exp3")
            .appliedInstantly(new AppliedInstantly(AppliedInstantly.Type.EXP, 180))
            .removeAfterApplied(true)
            .build();
    public final ItemBase ITEM_APPLIED4 = expBuilder
            .name("exp4")
            .appliedInstantly(new AppliedInstantly(AppliedInstantly.Type.EXP, 250))
            .removeAfterApplied(true)
            .build();

    private final ItemBase.ItemBaseBuilder creditsBuilder = ItemBase.builder().removeAfterApplied(true);
    public final ItemBase ITEM_APPLIED5 = creditsBuilder
            .name("credits")
            .appliedInstantly(new AppliedInstantly(AppliedInstantly.Type.CREDITS, 200))
            .build();
    public final ItemBase ITEM_APPLIED6 = creditsBuilder
            .name("credits1")
            .appliedInstantly(new AppliedInstantly(AppliedInstantly.Type.CREDITS, 250))
            .build();
    public final ItemBase ITEM_APPLIED7 = creditsBuilder
            .name("credits2")
            .appliedInstantly(new AppliedInstantly(AppliedInstantly.Type.CREDITS, 400))
            .build();
    public final ItemBase ITEM_APPLIED8 = creditsBuilder
            .name("credits3")
            .appliedInstantly(new AppliedInstantly(AppliedInstantly.Type.CREDITS, 550))
            .build();
    public final ItemBase ITEM_APPLIED9 = creditsBuilder
            .name("credits4")
            .appliedInstantly(new AppliedInstantly(AppliedInstantly.Type.CREDITS, 800))
            .build();

    public final ItemBase ITEM_APPLIED10 = ItemBase.builder()
            .name("expCredits")
            .appliedInstantly(new AppliedInstantly(AppliedInstantly.Type.EXP, 250))
            .appliedInstantly(new AppliedInstantly(AppliedInstantly.Type.CREDITS, 700))
            .removeAfterApplied(true)
            .build();
    public final ItemBase ITEM_APPLIED11 = ItemBase.builder()
            .name("expCredits1")
            .appliedInstantly(new AppliedInstantly(AppliedInstantly.Type.EXP, 400))
            .appliedInstantly(new AppliedInstantly(AppliedInstantly.Type.CREDITS, 1000))
            .removeAfterApplied(true)
            .build();

    public void register() {
        registerItems(ItemRegistry.class);
    }

    public void addItemTags(ItemBase itemBase, String[] tags) {
        for (String s : tags) {
            if(s.length() > 0) {
                itemBase.getItemTags().add(EnumItemTag.valueOf(s.trim()));
            }
        }
    }

    public void registerItems(Class<?> targetClass) {
        for (Field field : targetClass.getFields()) {
            if(isAssignableFrom(field.getType(), ItemBase.class)) {
                try {
                    registerItem((ItemBase) field.get(null));
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private boolean isAssignableFrom(Class<?> target, Class<?> parent) {
        if(target == null) return false;
        else if(target == parent) return true;
        return isAssignableFrom(target.getSuperclass(), parent);
    }

    private void registerItem(ItemBase item) {
        if(CommonUtils.isServerSide()) {
            ConfigItemBase configItemBase = new ConfigItemBase(item.getName());
            configItemBase.load();
            item.setInvMatrixSize(configItemBase.getMatrixWidth(), configItemBase.getMatrixHeight());
            item.setItemRarity(configItemBase.getItemRarity());
            item.setItemQuality(configItemBase.getItemQuality());
            item.setDescription(configItemBase.getDescription());
            item.setSlogan(configItemBase.getSlogan());
            item.setDisplayName(configItemBase.getDisplayName());
            item.setSecondName(configItemBase.getSecondName());
            addItemTags(item, configItemBase.getTags().split(","));
        }
        item.init();
        GameRegistry.registerItem(item, item.getUnlocalizedName());
        REGISTRY.add(item);
    }
}
