package ru.xlv.core.common;

import cpw.mods.fml.common.eventhandler.EventBus;
import ru.xlv.core.common.module.Module;
import ru.xlv.core.common.module.ModuleManager;
import ru.xlv.core.common.module.ModuleScanner;

public class XlvsCoreCommon {

    /**
     * Шина для собственных событий.
     * */
    public static final EventBus EVENT_BUS = new EventBus();

    public static final ModuleManager MODULE_MANAGER = new ModuleManager();

    public static <T extends Module<?>> void scanForModules(Class<T> aClass, Object... params) {
        new ModuleScanner(MODULE_MANAGER).scan(aClass, params);
    }
}
