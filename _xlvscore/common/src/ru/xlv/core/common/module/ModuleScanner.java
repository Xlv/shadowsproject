package ru.xlv.core.common.module;

import lombok.RequiredArgsConstructor;
import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;

@RequiredArgsConstructor
public class ModuleScanner {//todo очень медленный

    private final ModuleManager moduleManager;

    public void scan(Class<? extends Module<?>> aClass, Object... params) {
        Reflections reflections = new Reflections("ru", new SubTypesScanner());
        reflections.getSubTypesOf(aClass).forEach(aClass1 -> moduleManager.register(aClass1, params));
    }
}
