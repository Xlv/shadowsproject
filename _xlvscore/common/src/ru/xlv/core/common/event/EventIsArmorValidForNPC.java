package ru.xlv.core.common.event;

import cpw.mods.fml.common.eventhandler.Event;
import lombok.AllArgsConstructor;
import lombok.Getter;
import net.minecraft.item.ItemStack;

@AllArgsConstructor
@Getter
public class EventIsArmorValidForNPC extends Event {

    private final ItemStack itemStack;
    private final int armorType;

    @Override
    public boolean isCancelable() {
        return true;
    }
}
