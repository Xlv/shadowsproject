package ru.xlv.core.common.player.character;

import ru.xlv.core.common.database.DatabaseValue;

import java.io.Serializable;

@DatabaseValue
public class CharacterAttribute implements Serializable {

    private final CharacterAttributeType type;
    private double value, maxValue = -1;

    public CharacterAttribute(CharacterAttributeType type) {
        this.type = type;
    }

    public CharacterAttribute(CharacterAttributeType type, double value) {
        this.type = type;
        this.value = value;
    }

    public CharacterAttribute(CharacterAttributeType type, double value, double maxValue) {
        this.type = type;
        this.value = value;
        this.maxValue = maxValue;
    }

    public CharacterAttributeType getType() {
        return type;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        if(maxValue != -1 && value > maxValue) {
            maxValue = value;
        }
        this.value = value >= 0 ? value : 0;
    }

    public void addValue(double value) {
        if(this.value != this.maxValue) {
            setValue(Math.min(this.value + value, maxValue));
        }
    }

    public void setMaxValue(double maxValue) {
        this.maxValue = maxValue;
    }

    public void resetValue() {
        this.value = type.getDefaultValue();
    }

    public boolean consume(double value) {
        if(this.value >= value) {
            setValue(this.value - value);
            return true;
        }
        return false;
    }

    public double getMaxValue() {
        return maxValue;
    }
}
