package ru.xlv.core.common.player.stat;

import ru.xlv.core.common.database.DatabaseAdaptedBy;

@DatabaseAdaptedBy(StatDatabaseAdapter.class)
public interface IStatProvider {
    void increment();

    Number getStatValue();
}


