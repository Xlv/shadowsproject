package ru.xlv.core.common.player.character;

import lombok.Getter;
import lombok.Setter;
import ru.xlv.core.common.database.DatabaseValue;

@Setter
@Getter
public class CharacterDebuff {

    @DatabaseValue
    private final CharacterDebuffType type;
    @DatabaseValue
    private long period;
    @DatabaseValue
    private long creationTimeMills;

    public CharacterDebuff(CharacterDebuffType type, long period) {
        this.type = type;
        this.period = period;
        creationTimeMills = System.currentTimeMillis();
    }

    public boolean shouldBeRemoved() {
        return System.currentTimeMillis() - creationTimeMills >= period;
    }
}
