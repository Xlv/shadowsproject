package ru.xlv.core.common.player.stat;

import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import ru.xlv.core.common.database.AbstractDatabaseAdapter;

import java.io.IOException;

public class StatDatabaseAdapter extends AbstractDatabaseAdapter<IStatProvider> {
    @Override
    public void write(JsonWriter out, IStatProvider value) throws IOException {
        out.beginArray();
        if(value instanceof StatIntProvider) {
            out.value(0);
            out.value(((StatIntProvider) value).getStatValue());
        } else if(value instanceof StatDoubleProvider) {
            out.value(1);
            out.value(((StatDoubleProvider) value).getStatValue());
        }
        out.endArray();
    }

    @Override
    public IStatProvider read(JsonReader in) throws IOException {
        IStatProvider statProvider;
        in.beginArray();
        switch (in.nextInt()) {
            case 0:
                statProvider = new StatIntProvider(in.nextInt());
                break;
            case 1:
                statProvider = new StatDoubleProvider(in.nextDouble());
                break;
            default: return null;
        }
        in.endArray();
        return statProvider;
    }
}
