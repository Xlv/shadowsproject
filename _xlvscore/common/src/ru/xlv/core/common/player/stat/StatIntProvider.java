package ru.xlv.core.common.player.stat;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Setter
@Getter
@AllArgsConstructor
public class StatIntProvider implements IStatProvider {

    private int value;

    public void increment(int i) {
        if(value < Integer.MAX_VALUE - i) {
            value += i;
        }
    }

    @Override
    public void increment() {
        increment(1);
    }

    @Override
    public Integer getStatValue() {
        return value;
    }
}
