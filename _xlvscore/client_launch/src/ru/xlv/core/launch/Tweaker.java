package ru.xlv.core.launch;

import net.minecraft.launchwrapper.ITweaker;
import net.minecraft.launchwrapper.LaunchClassLoader;
import ru.xlv.core.util.obf.IgnoreObf;

import java.io.File;
import java.util.List;

@IgnoreObf
public class Tweaker implements ITweaker {

//    public static Object DCD;

    private String[] args = new String[0];

//    private static String token, primaryKey, url, username;

    @Override
    public void acceptOptions(List<String> args, File gameDir, File assetsDir, String profile) {
//        if(CoreUtils.isStartedFromGradle()) {
//            return;
//        }
//        List<String> tempArgs = new ArrayList<>(args);
//        String token = null, primaryKey = null, url = null, username = null;
//        for (int i = 1; i < tempArgs.size(); i++) {
//            switch (tempArgs.get(i - 1)) {
//                case "--accessToken":
//                    token = tempArgs.get(i);
//                    break;
//                case "--username":
//                    username = tempArgs.get(i);
//                    break;
//                case "--pk":
//                    primaryKey = tempArgs.get(i);
//                    break;
//                case "--u":
//                    url = tempArgs.get(i);
//            }
//        }
//        if(token != null && primaryKey != null && url != null && username != null) {
//            Tweaker.token = token;
//            Tweaker.primaryKey = primaryKey;
//            Tweaker.url = url;
//            Tweaker.username = username;
//        }
    }

    @Override
    public void injectIntoClassLoader(LaunchClassLoader classLoader) {
//        try {
//            Class<?> aClass = classLoader.findClass("ru.xlv.core.util.CoreUtils");
//            for (Method declaredMethod : aClass.getDeclaredMethods()) {
//                if(declaredMethod.getTypeParameters().length == 4) {
//                    declaredMethod.invoke(null, token, primaryKey, url, username);
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException | IllegalAccessException | InvocationTargetException e) {
//            e.printStackTrace();
//        }
//        classLoader.registerTransformer("ru.xlv.core.common.asm.hook.HookClassTransformer");
    }

    @Override
    public String getLaunchTarget() {
        return "ru.xlv.core.launch.Main";
//        return "net.minecraft.client.main.Main";
    }

    @Override
    public String[] getLaunchArguments() {
        return args;
    }
}
