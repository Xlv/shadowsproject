package ru.xlv.core.event;

import cpw.mods.fml.common.eventhandler.EventPriority;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.client.audio.SoundEventAccessorComposite;
import net.minecraft.client.audio.SoundPoolEntry;
import net.minecraft.util.ResourceLocation;
import ru.xlv.core.common.XlvsCoreCommon;
import ru.xlv.core.common.util.CommonUtils;
import ru.xlv.core.common.util.StepSoundRegistry;
import ru.xlv.core.util.SoundType;

public class SoundEventListener {

    @SuppressWarnings("unused")
    @SubscribeEvent(priority = EventPriority.HIGHEST)
    public void event(PreloadableResourcesLoadPostEvent event) {
        StepSoundRegistry.initAndGet().forEach(stepSoundRegistryElement -> {
            loadSound(stepSoundRegistryElement.getSoundType());
            if (stepSoundRegistryElement.getMetadata() != 0) {
                stepSoundRegistryElement.getBlock().setCustomStepSound(stepSoundRegistryElement.getSoundType(), stepSoundRegistryElement.getMetadata());
            } else {
                stepSoundRegistryElement.getBlock().setCustomStepSound(stepSoundRegistryElement.getSoundType());
            }
            if (CommonUtils.isDebugEnabled()) {
                System.out.println(stepSoundRegistryElement.getBlock() + ":" + stepSoundRegistryElement.getMetadata() + " loaded");
            }
        });
        SoundType[] preLoaded = new SoundType[]{
                SoundType.RAIN
        };
        for (SoundType soundType : preLoaded) {
            loadSound(soundType);
        }
        XlvsCoreCommon.EVENT_BUS.unregister(this);
    }

    private void loadSound(SoundType soundType) {
        SoundEventAccessorComposite sound = Minecraft.getMinecraft().getSoundHandler().getSound(new ResourceLocation(soundType.getSoundKey()));
        SoundPoolEntry soundPoolEntry = sound.func_148720_g();
        Minecraft.getMinecraft().getSoundHandler().sndManager.loadSound(
                PositionedSoundRecord.func_147674_a(new ResourceLocation(soundType.getSoundKey()), 1.0F),
                soundPoolEntry.getSoundPoolEntryLocation()
        );
    }
}
