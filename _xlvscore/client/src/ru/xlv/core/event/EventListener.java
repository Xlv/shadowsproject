package ru.xlv.core.event;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.*;
import net.minecraft.client.gui.inventory.GuiBeacon;
import net.minecraft.client.gui.inventory.GuiCrafting;
import net.minecraft.client.gui.inventory.GuiDispenser;
import net.minecraft.client.gui.inventory.GuiFurnace;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.MovingObjectPosition;
import net.minecraftforge.client.event.GuiOpenEvent;
import net.minecraftforge.client.event.MouseEvent;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import org.lwjgl.input.Keyboard;
import ru.krogenit.client.gui.api.GuiDrawUtils;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.item.ItemBase;
import ru.xlv.core.gui.GuiCharacterMainMenu;
import ru.xlv.customfont.FontContainer;
import ru.xlv.customfont.FontType;

public class EventListener {

    @SubscribeEvent
    public void event(MouseEvent event) {
        Minecraft mc = Minecraft.getMinecraft();
        if(mc.currentScreen instanceof GuiChat) {
            XlvsCore.INSTANCE.getGameOverlayManager().mouseClicked(event.x, event.y, event.button);
        }
    }

    @SubscribeEvent
    public void event(GuiOpenEvent event) {
        if(event.gui instanceof GuiMainMenu) {
            event.gui = new GuiCharacterMainMenu();
        } else if(event.gui instanceof GuiBeacon || event.gui instanceof GuiHopper || event.gui instanceof GuiDispenser || event.gui instanceof GuiCrafting
                || event.gui instanceof GuiFurnace || event.gui instanceof GuiEnchantment || event.gui instanceof GuiRepair) {
            event.setCanceled(true);
        }
    }

    @SuppressWarnings("unused")
    @SubscribeEvent
    public void blockSpeedBreak(PlayerEvent.BreakSpeed e) {
        if(!e.entityPlayer.capabilities.isCreativeMode) {
            e.setCanceled(true);
        }
    }

    @SubscribeEvent
    public void event(RenderGameOverlayEvent event) {
        if (event.type == RenderGameOverlayEvent.ElementType.ALL) {
            Minecraft mc = Minecraft.getMinecraft();
            MovingObjectPosition objectMouseOver = mc.objectMouseOver;
            if (objectMouseOver != null && objectMouseOver.typeOfHit == MovingObjectPosition.MovingObjectType.ENTITY && objectMouseOver.entityHit instanceof EntityItem) {
                EntityItem closeItem = (EntityItem) objectMouseOver.entityHit;
                ItemStack entityItem = closeItem.getEntityItem();
                Item item = entityItem.getItem();
                String text = null;
                if(item instanceof ItemBase) text = ((ItemBase) item).getDisplayName();
                if(text == null) text = entityItem.getDisplayName();
                text = text.toUpperCase();
                String text2 = Keyboard.getKeyName(Keyboard.KEY_F);
                String text3 = "ПОДОБРАТЬ";
                String text4 = "[ " + text2 + " ]";
                float x = event.resolution.getScaledWidth() / 2f;
                float y = event.resolution.getScaledHeight() / 2f + 4;
                float fs = 1.0f;
                FontContainer fontContainer = FontType.FUTURA_PT_MEDIUM.getFontContainer();
                GuiDrawUtils.drawStringNoScale(fontContainer, text3, x - fontContainer.width(text3) / 2f, y, fs, 0xFFFFFF);
                y+=10;
                GuiDrawUtils.drawStringNoScale(fontContainer, text, x - fontContainer.width(text) / 2f, y, fs, 0xFFFFFF);
                y+=10;
                GuiDrawUtils.drawStringNoScale(fontContainer, text4, x - fontContainer.width(text4) / 2f, y, fs, 0xFFFFFF);
            }
        }
    }
}
