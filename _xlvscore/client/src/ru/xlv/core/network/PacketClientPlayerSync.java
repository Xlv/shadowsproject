package ru.xlv.core.network;

import lombok.NoArgsConstructor;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import ru.xlv.core.common.network.IPacketIn;
import ru.xlv.core.common.player.character.CharacterType;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.core.player.ClientPlayer;
import ru.xlv.core.player.ClientPlayerManager;

import java.io.IOException;

@NoArgsConstructor
public class PacketClientPlayerSync implements IPacketIn {

    @Override
    public void read(ByteBufInputStream bbis) throws IOException {
        ClientPlayerManager.INSTANCE.syncPlayer(readClientPlayer(bbis));
    }

    public static ClientPlayer readClientPlayer(ByteBufInputStream byteBufInputStream) throws IOException {
        ClientPlayer clientPlayer = new ClientPlayer(byteBufInputStream.readUTF());
        clientPlayer.setCharacterType(CharacterType.values()[byteBufInputStream.readInt()]);
        clientPlayer.setLevel(byteBufInputStream.readInt());
        clientPlayer.setLastOnlinePeriodTimeMills(byteBufInputStream.readLong());
        int id = byteBufInputStream.readInt();
        if(id != -1) {
            clientPlayer.setCurrentBracers(new ItemStack(Item.getItemById(id)));
        } else {
            clientPlayer.setCurrentBracers(null);
        }
        clientPlayer.setOnline(byteBufInputStream.readBoolean());
        return clientPlayer;
    }
}
