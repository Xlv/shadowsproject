package ru.xlv.core.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import ru.xlv.core.common.network.IPacketCallbackEffective;
import ru.xlv.core.common.player.character.CharacterAttribute;
import ru.xlv.core.common.player.character.CharacterAttributeType;
import ru.xlv.core.common.util.ByteBufInputStream;

import javax.annotation.Nullable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public class PacketPlayerAttributesGet implements IPacketCallbackEffective<List<CharacterAttribute>> {

    private List<CharacterAttribute> result;

    private String username;

    public PacketPlayerAttributesGet(String username) {
        this.username = username;
    }

    @Override
    public void write(ByteBufOutputStream bbos) throws IOException {
        bbos.writeUTF(username);
    }

    @Override
    public void read(ByteBufInputStream bbis) throws IOException {
        result = new ArrayList<>();
        int c = bbis.readInt();
        for (int i = 0; i < c; i++) {
            CharacterAttributeType characterAttributeType = CharacterAttributeType.values()[bbis.readInt()];
            CharacterAttribute characterAttribute = new CharacterAttribute(characterAttributeType, bbis.readDouble(), bbis.readDouble());
            result.add(characterAttribute);
        }
    }

    @Nullable
    @Override
    public List<CharacterAttribute> getResult() {
        return result;
    }
}
