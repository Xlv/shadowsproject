package ru.xlv.core.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import ru.xlv.core.common.network.IPacketCallbackEffective;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.core.player.ClientPlayer;

import javax.annotation.Nullable;
import java.io.IOException;

@NoArgsConstructor
@AllArgsConstructor
public class PacketClientPlayerGet implements IPacketCallbackEffective<PacketClientPlayerGet.Result> {

    private final Result result = new Result();

    private String username;

    @Override
    public void write(ByteBufOutputStream bbos) throws IOException {
        bbos.writeUTF(username);
    }

    @Override
    public void read(ByteBufInputStream bbis) throws IOException {
        result.success = bbis.readBoolean();
        result.clientPlayer = PacketClientPlayerSync.readClientPlayer(bbis);
    }

    @Nullable
    @Override
    public Result getResult() {
        return result;
    }

    @Getter
    @RequiredArgsConstructor
    public static class Result {
        private boolean success;
        private ClientPlayer clientPlayer;
    }
}
