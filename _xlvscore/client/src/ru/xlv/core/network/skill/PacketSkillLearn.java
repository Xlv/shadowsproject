package ru.xlv.core.network.skill;

import io.netty.buffer.ByteBufOutputStream;
import lombok.Getter;
import lombok.NoArgsConstructor;
import ru.xlv.core.common.network.IPacketCallbackEffective;
import ru.xlv.core.common.util.ByteBufInputStream;

import javax.annotation.Nullable;
import java.io.IOException;

@NoArgsConstructor
public class PacketSkillLearn implements IPacketCallbackEffective<PacketSkillLearn.Result> {

    @Getter
    public static class Result {
        private boolean success;
        private String responseMessage;
    }

    private final Result result = new Result();
    private int skillId;

    public PacketSkillLearn(int skillId) {
        this.skillId = skillId;
    }

    @Override
    public void write(ByteBufOutputStream bbos) throws IOException {
        bbos.writeInt(skillId);
    }

    @Override
    public void read(ByteBufInputStream bbis) throws IOException {
        result.success = bbis.readBoolean();
        result.responseMessage = bbis.readUTF();
    }

    @Nullable
    @Override
    public Result getResult() {
        return result;
    }
}
