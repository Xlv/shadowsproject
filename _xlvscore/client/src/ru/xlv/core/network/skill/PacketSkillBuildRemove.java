package ru.xlv.core.network.skill;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import ru.xlv.core.common.network.IPacketOut;

import java.io.IOException;

@NoArgsConstructor
public class PacketSkillBuildRemove implements IPacketOut {

    private int buildIndex;

    public PacketSkillBuildRemove(int buildIndex) {
        this.buildIndex = buildIndex;
    }

    @Override
    public void write(ByteBufOutputStream bbos) throws IOException {
        bbos.writeInt(buildIndex);
    }
}
