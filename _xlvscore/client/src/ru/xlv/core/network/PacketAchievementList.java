package ru.xlv.core.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import ru.xlv.core.achievement.AchievementUtils;
import ru.xlv.core.achievement.Achievement;
import ru.xlv.core.common.network.IPacketCallbackEffective;
import ru.xlv.core.common.util.ByteBufInputStream;

import javax.annotation.Nullable;
import java.io.IOException;
import java.util.List;

@NoArgsConstructor
public class PacketAchievementList implements IPacketCallbackEffective<List<Achievement>> {

    private List<Achievement> achievements;
    private String username;

    public PacketAchievementList(String username) {
        this.username = username;
    }

    @Override
    public void write(ByteBufOutputStream bbos) throws IOException {
        bbos.writeUTF(username);
    }

    @Override
    public void read(ByteBufInputStream bbis) throws IOException {
        achievements = AchievementUtils.readAchievementList(bbis);
    }

    @Nullable
    @Override
    public List<Achievement> getResult() {
        return achievements;
    }
}
