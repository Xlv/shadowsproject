package ru.xlv.core.gui.button;

import net.minecraft.util.ResourceLocation;
import ru.krogenit.client.gui.api.GuiButtonAdvanced;
import ru.krogenit.client.gui.api.GuiDrawUtils;
import ru.krogenit.utils.AnimationHelper;
import ru.xlv.core.util.SoundType;
import ru.xlv.core.util.SoundUtils;
import ru.xlv.customfont.FontType;

public class TexturedButtonIcon extends GuiButtonAdvanced {

    private ResourceLocation iconTexture;

    private float iconX, iconY, iconSize;
    private float textOffsetX;
    private float blending;
    protected boolean hovered;

    public TexturedButtonIcon(int buttonId, float x, float y, float widthIn, float heightIn, String buttonText) {
        super(buttonId, x, y, widthIn, heightIn, buttonText);
    }

    public void setIconTexture(ResourceLocation iconTexture) {
        this.iconTexture = iconTexture;
    }

    public void setIconX(float iconX) {
        this.iconX = iconX;
    }

    public void setIconY(float iconY) {
        this.iconY = iconY;
    }

    public void setIconSize(float iconSize) {
        this.iconSize = iconSize;
    }

    public void setTextOffsetX(float textOffsetX) {
        this.textOffsetX = textOffsetX;
    }

    @Override
    public void drawButton(int mouseX, int mouseY) {
        if (this.visible) {
            boolean hovered = isHovered(mouseX, mouseY);
            if(hovered && this.hovered != hovered) {
                SoundUtils.playGuiSound(SoundType.BUTTON_HOVER);
            }
            this.hovered = hovered;
            if(hovered) {
                blending += AnimationHelper.getAnimationSpeed() * 0.1f;
                if(blending > 1) blending = 1;
            } else {
                blending -= AnimationHelper.getAnimationSpeed() * 0.1f;
                if(blending < 0) blending = 0f;
            }

            GuiDrawUtils.drawRect(texture, xPosition, yPosition, width, height, 1f, 1f, 1f, textureHover != null ? 1f - blending : 1.0f);

            if(textureHover != null) {
                GuiDrawUtils.drawRect(textureHover, xPosition, yPosition, width, height, 1f, 1f, 1f, blending);
            }

            if(iconTexture != null) {
                GuiDrawUtils.drawRect(iconTexture, xPosition + iconX, yPosition + iconY, iconSize, iconSize);
            }
            drawText();
        }
    }

    @Override
    protected void drawText() {
        int j = 0xffffff;
        float fs = 2.15f;
        float offsetY = height / 10f;
        GuiDrawUtils.drawCenteredStringNoXYScale(FontType.FUTURA_PT_DEMI, displayString, this.xPosition + this.width / 2.0f + textOffsetX,
                this.yPosition + this.height / 2f - offsetY, fs, j);
    }
}
