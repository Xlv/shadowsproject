package ru.xlv.core.gui.button;

import gnu.trove.map.TIntObjectMap;
import gnu.trove.map.hash.TIntObjectHashMap;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.texture.DynamicTexture;
import net.minecraft.client.renderer.texture.ITextureObject;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import ru.krogenit.client.gui.api.EnumStringRenderType;
import ru.krogenit.client.gui.api.GuiButtonAdvanced;
import ru.krogenit.client.gui.api.GuiDrawUtils;
import ru.krogenit.client.gui.api.ScaleGui;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.resource.preload.PreLoadableResource;
import ru.xlv.core.util.data.News;
import ru.xlv.customfont.FontType;
import ru.xlv.mochar.XlvsMainMod;

public class ButtonNewsElement extends GuiButtonAdvanced {

    private static final TIntObjectMap<ResourceLocation> TEXTURE_REGISTRY = new TIntObjectHashMap<>();
	
    @PreLoadableResource public static final ResourceLocation resLocNewsBottomLine = new ResourceLocation(XlvsMainMod.MODID, "textures/gui/news_bottom_line.png");
    private ResourceLocation texture;
    private News news;

    private float elementHeight;

    private boolean isExpanded;

    public ButtonNewsElement(int buttonId, float x, float y, float widthIn, float heightIn) {
        super(buttonId, x, y, widthIn, heightIn, "");
    }

    @Override
    public void drawButton(int mouseX, int mouseY) {
        if(getNews() != null && getNews().getArticle() != null && getNews().getText() != null) {
            GL11.glColor4f(1f, 1f, 1f, 1f);
            float y = yPosition;
            float textScale = 1.9f;
            float articleHeight = GuiDrawUtils.drawSplittedStringNoScale(FontType.FUTURA_PT_DEMI, getNews().getArticle(), xPosition, y - ScaleGui.get(8), textScale, width, isExpanded ? -1 : 45 * textScale, 0xffffff, EnumStringRenderType.DEFAULT);
            GL11.glColor4f(1, 1, 1, 1);
            y += articleHeight;
            float texHeight = 0;
            if(texture == null) {
                texture = TEXTURE_REGISTRY.get(id);
            }
            if (texture != null) {
                float texWidth = 0;
                float maxTexWidth = width;
                mc.getTextureManager().bindTexture(texture);
                ITextureObject textureObject = mc.renderEngine.getTexture(this.texture);
                if(textureObject instanceof DynamicTexture) {
                    DynamicTexture dynTexture = (DynamicTexture) textureObject;
                    texWidth = dynTexture.width;
                    texHeight = dynTexture.height;
                    if(texWidth > maxTexWidth) {
                        float diff = (texWidth) / maxTexWidth;
                        texWidth = maxTexWidth;
                        texHeight /= diff;
                    }
                }
                GuiDrawUtils.drawRect(xPosition, y, texWidth, texHeight);
            }

            y += texHeight + height / 20f;
            textScale = 1.14f;
            float textHeight = GuiDrawUtils.drawSplittedStringNoScale(FontType.ROBOTO_REGULAR, getNews().getText(), xPosition,
                    y, textScale, width, (isExpanded ? -1 : 42 * textScale), 0xffffff, EnumStringRenderType.DEFAULT);
            y += textHeight + height / 30f;
            float x = xPosition;
            float bottomWidth = width;
            float bottomHeight = height / 200f;
            GuiDrawUtils.drawRect(resLocNewsBottomLine, x, y, bottomWidth, bottomHeight);
            y += height / 14f;
            elementHeight = y - yPosition;
        }
        
        this.mouseDragged(mc, mouseX, mouseY);
    }

    public void drawButton(float x, float y, int mouseX, int mouseY) {
        xPosition = x;
        yPosition = y;
        drawButton(mouseX, mouseY);
    }

    @Override
    public boolean mousePressed(Minecraft mc, int mouseX, int mouseY) {
        return this.enabled && this.visible && mouseX >= this.xPosition && mouseY >= this.yPosition && mouseX < this.xPosition + this.width && mouseY < this.yPosition + getElementHeight();
    }

    public boolean isExpanded() {
        return isExpanded;
    }

    public void setExpanded(boolean expanded) {
        isExpanded = expanded;
    }

    public void setNews(News news) {
        this.news = news;
        XlvsCore.INSTANCE.getTextureLoader().loadTexture(news.getImageInputStream(), "jpg", "temp/button_news_" + id)
                .thenAcceptSync(resourceLocation -> TEXTURE_REGISTRY.put(id, resourceLocation));
    }

    public News getNews() {
        return news;
    }

    public float getElementHeight() {
        return elementHeight;
    }
}
