package ru.xlv.core.gui.overlay;

import net.minecraft.client.Minecraft;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import org.lwjgl.opengl.GL11;
import ru.xlv.core.gui.GameOverlayElement;

public class GameOverlayEnterArea extends GameOverlayElement {

    private static final float RENDER_PERIOD = 5000F;

    private String message;

    private float alpha = 1f;

    private long startTimeMills;

    @Override
    public void renderPost(RenderGameOverlayEvent.Post event) {
        if(message != null && System.currentTimeMillis() - startTimeMills < RENDER_PERIOD) {
            GL11.glColor4f(1, 1, 1, alpha);
            long l = System.currentTimeMillis() - startTimeMills;
            float ampl = 0.01F * event.partialTicks;
            if(l <= RENDER_PERIOD / 5 && alpha < 1F) {
                alpha += ampl;
            } else if(l >= RENDER_PERIOD - RENDER_PERIOD / 5 && alpha > 0F) {
                alpha -= ampl;
            }
            if(message != null) {
                Minecraft.getMinecraft().fontRenderer.drawString(message, event.resolution.getScaledWidth() / 2 - Minecraft.getMinecraft().fontRenderer.getStringWidth(message) / 2, 20, 0xffffff);
            }
            GL11.glColor4f(1, 1, 1, 1);
        } else {
            alpha = 1F;
        }
    }

    @Override
    public void mouseClicked(int mouseX, int mouseY, int mouseButton) {

    }

    public void setMessage(String message) {
        this.message = message;
        startTimeMills = System.currentTimeMillis();
    }
}
