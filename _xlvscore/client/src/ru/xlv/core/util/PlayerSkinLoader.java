package ru.xlv.core.util;

import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import net.minecraft.client.entity.AbstractClientPlayer;
import net.minecraft.util.ResourceLocation;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.FileNotFoundException;
import java.net.URL;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

@UtilityClass
public class PlayerSkinLoader {

    private final String LOCATION_FORMAT = "xlvscore:textures/skin/%s";
    private final String URL_FORMAT = "";

    private final ExecutorService EXECUTOR_SERVICE = Executors.newSingleThreadExecutor();
    private final Queue<FutureTask<BufferedImage>> DOWNLOADED_QUEUE = new LinkedList<>();
    private final Map<String, State> STATE_MAP = new HashMap<>();

    public ResourceLocation getLocationSkin(String username) {
        State state;
        synchronized (STATE_MAP) {
            state = STATE_MAP.get(username);
        }
        if(state != null) {
            switch (state) {
                case READY:
                    return new ResourceLocation(String.format(LOCATION_FORMAT, username));
                case DOWNLOADED:

                    synchronized (STATE_MAP) {
                        STATE_MAP.put(username, State.READY);
                    }
            }
        } else {
            EXECUTOR_SERVICE.submit(() -> {
//                DOWNLOADED_QUEUE.add(new FutureTask<>())
//                downloadImage(String.format(URL_FORMAT, username));
            });
        }
        return AbstractClientPlayer.locationStevePng;
    }

    @SneakyThrows
    private BufferedImage downloadImage(String url) {
        try {
            return ImageIO.read(new URL(url).openStream());
        } catch (FileNotFoundException ignored) {}
        return null;
    }

    private class Data {

    }

    private enum State {
        DOWNLOADING,
        DOWNLOADED,
        READY
    }
}
