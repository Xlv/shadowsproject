package ru.xlv.core.util;

import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.texture.SimpleTexture;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;
import net.minecraftforge.client.model.AdvancedModelLoader;
import net.minecraftforge.client.model.IModelCustom;
import ru.krogenit.dds_loader.TextureLoaderDDS;
import ru.xlv.core.model.ModelLoader;
import ru.xlv.core.renderer.texture.Texture;
import ru.xlv.core.util.obf.IgnoreObf;

import javax.annotation.Nullable;
import java.io.InputStream;
import java.net.URI;
import java.util.List;

//@IgnoreObf
//public class Utils {
//
//    /**
//     * loads a resource without decoding
//     * */
//    @IgnoreObf
//    @Nullable
//    public static InputStream getInputStream(ResourceLocation resourceLocation) {
//        try {
//            return getInputStream(resourceLocation, false);
//        } catch (IOException | NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
//            e.printStackTrace();
//        }
//        return null;
//    }
//
//    @IgnoreObf
//    public static InputStream getInputStream(ResourceLocation resourceLocation, boolean decryptBefore) throws IOException, NoSuchMethodException, InvocationTargetException, IllegalAccessException {
//        InputStream inputStream = Minecraft.getMinecraft().getResourceManager().getResource(resourceLocation).getInputStream();
//        if(decryptBefore) {
////            inputStream = (InputStream) Tweaker.DCD.getClass().getMethod("d", InputStream.class).invoke(Tweaker.DCD, inputStream);
//            try {
//                inputStream = getEncryptedInputStream0(inputStream);
//            } catch (NoSuchPaddingException | NoSuchAlgorithmException | InvalidKeyException e) {
//                e.printStackTrace();
//            }
//        }
//        return inputStream;
//    }
//
//    /**
//     * looking for a target file from root dir
//     * */
//    @IgnoreObf
//    public static InputStream getInputStream(String filePath, boolean isEncrypted) {
//        try {
//            File file = new File(filePath);
//            FileInputStream fileInputStream = new FileInputStream(file);
//            if(isEncrypted) {
//                return getEncryptedInputStream0(fileInputStream);
//            } else {
//                return fileInputStream;
//            }
//        } catch (FileNotFoundException | NoSuchPaddingException | NoSuchAlgorithmException | InvalidKeyException e) {
//            e.printStackTrace();
//        }
//        return null;
//    }
//
//    private static InputStream getEncryptedInputStream0(InputStream inputStream) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException {
//        try {
//            return (InputStream) Tweaker.DCD.getClass().getMethod("d", InputStream.class).invoke(Tweaker.DCD, inputStream);
//        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
//            e.printStackTrace();
//        }
//        return inputStream;
//    }
//
//    @IgnoreObf
//    public static InputStream getEncryptedInputStream(InputStream inputStream) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, IOException {
//        try(ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
//            byte[] b = new byte[1024];
//            int numberOfBytedRead;
//            InputStream is = getEncryptedInputStream0(inputStream);
//            while ((numberOfBytedRead = is.read(b)) >= 0) {
//                baos.write(b, 0, numberOfBytedRead);
//            }
//            return new ByteArrayInputStream(baos.toByteArray());
//        }
//    }
//
//    /**
//     * Loads an encrypted model
//     * */
//    @IgnoreObf
//    public static IModelCustom loadModel(ResourceLocation resourceLocation, boolean decryptBefore) {
//        if(decryptBefore) {
//            return ModelLoader.loadModel(resourceLocation);
//        }
//        return AdvancedModelLoader.loadModel(resourceLocation);
//    }
//
//    /**
//     * Uses for loading an encrypted resource
//     * */
//    @IgnoreObf
//    public static void bindTexture(ResourceLocation resourceLocation) {
//        bindTexture(resourceLocation, true);
//    }
//
//    @IgnoreObf
//    public static void bindTexture(ResourceLocation resourceLocation, boolean decryptBefore) {
//        if(decryptBefore) {
//            if(resourceLocation.getResourcePath().endsWith(".png")) {
//                if (Minecraft.getMinecraft().getTextureManager().getTexture(resourceLocation) == null) {
//                    Minecraft.getMinecraft().getTextureManager().loadTexture(resourceLocation, new Texture(resourceLocation));
//                }
//            } else if(resourceLocation.getResourcePath().endsWith(".dds")) {
//                TextureLoaderDDS.loadTexture(resourceLocation, new Texture(resourceLocation));
//            }
//        }
//        if(resourceLocation.getResourcePath().endsWith(".png")) {
//            Minecraft.getMinecraft().getTextureManager().bindTexture(resourceLocation);
//        } else if(resourceLocation.getResourcePath().endsWith(".dds")) {
//            TextureLoaderDDS.bindTexture(resourceLocation);
//        }
//    }
//}

@IgnoreObf
public class Utils {

//    public static Main main = new Main("zGJtHGNHkFD6zST5+HbQnA==".getBytes());
//
//    /**
//     * loads a resource without decoding
//     * */
//    @IgnoreObf
//    @Nullable
//    public static InputStream getInputStream(ResourceLocation resourceLocation) {
//        try {
//            return getInputStream(resourceLocation, false);
//        } catch (IOException | NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
//            e.printStackTrace();
//        }
//        return null;
//    }
//
//    @IgnoreObf
//    public static InputStream getInputStream(ResourceLocation resourceLocation, boolean decryptBefore) throws IOException, NoSuchMethodException, InvocationTargetException, IllegalAccessException {
//        InputStream inputStream = Minecraft.getMinecraft().getResourceManager().getResource(resourceLocation).getInputStream();
//        if(decryptBefore) {
////            inputStream = (InputStream) Tweaker.DCD.getClass().getMethod("d", InputStream.class).invoke(Tweaker.DCD, inputStream);
//            try {
//                inputStream = getEncryptedInputStream0(inputStream);
//            } catch (NoSuchPaddingException | NoSuchAlgorithmException | InvalidKeyException e) {
//                e.printStackTrace();
//            }
//        }
//        return inputStream;
//    }
//
//    /**
//     * looking for a target file from root dir
//     * */
//    @IgnoreObf
//    public static InputStream getInputStream(String filePath, boolean isEncrypted) {
//        try {
//            File file = new File(filePath);
//            FileInputStream fileInputStream = new FileInputStream(file);
//            if(isEncrypted) {
//                return getEncryptedInputStream0(fileInputStream);
//            } else {
//                return fileInputStream;
//            }
//        } catch (FileNotFoundException | NoSuchPaddingException | NoSuchAlgorithmException | InvalidKeyException e) {
//            e.printStackTrace();
//        }
//        return null;
//    }
//
//    private static InputStream getEncryptedInputStream0(InputStream inputStream) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException {
////        try {
////            Class.forName("GradleStart");
////            return inputStream;
////        } catch (ClassNotFoundException e) {
////            e.printStackTrace();
////        }
//        return main.d(inputStream);
//    }
//
//    @IgnoreObf
//    public static InputStream getEncryptedInputStream(InputStream inputStream) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, IOException {
//        try(ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
//            byte[] b = new byte[1024];
//            int numberOfBytedRead;
//            InputStream is = getEncryptedInputStream0(inputStream);
//            while ((numberOfBytedRead = is.read(b)) >= 0) {
//                baos.write(b, 0, numberOfBytedRead);
//            }
//            return new ByteArrayInputStream(baos.toByteArray());
//        }
//    }

    /**
     * Loads an encrypted model
     * */
    @IgnoreObf
    public static IModelCustom loadModel(ResourceLocation resourceLocation, boolean decryptBefore) {
        if(decryptBefore) {
            return ModelLoader.loadModel(resourceLocation);
        }
        return AdvancedModelLoader.loadModel(resourceLocation);
    }

    /**
     * Uses for loading an encrypted resource
     * */
    @IgnoreObf
    public static void bindTexture(ResourceLocation resourceLocation) {
        bindTexture(resourceLocation, true);
    }

    @IgnoreObf
    public static void bindTexture(ResourceLocation resourceLocation, boolean decryptBefore) {
        if(decryptBefore) {
            if(resourceLocation.getResourcePath().endsWith(".png")) {
                if (Minecraft.getMinecraft().getTextureManager().getTexture(resourceLocation) == null) {
                    Minecraft.getMinecraft().getTextureManager().loadTexture(resourceLocation, new Texture(resourceLocation));
                }
            } else if(resourceLocation.getResourcePath().endsWith(".dds")) {
                if(TextureLoaderDDS.getTexture(resourceLocation) == null) {
                    TextureLoaderDDS.loadTexture(resourceLocation, new SimpleTexture(resourceLocation));
                }
            }
        }
        if(resourceLocation.getResourcePath().endsWith(".png")) {
            Minecraft.getMinecraft().getTextureManager().bindTexture(resourceLocation);
        } else if(resourceLocation.getResourcePath().endsWith(".dds")) {
            TextureLoaderDDS.bindTexture(resourceLocation);
        }
    }

    private static void bindTex0(ResourceLocation resourceLocation) {
        if(resourceLocation.getResourcePath().endsWith("png")) {
            Minecraft.getMinecraft().getTextureManager().bindTexture(resourceLocation);
        } else if(resourceLocation.getResourcePath().endsWith("dds")) {
            TextureLoaderDDS.bindTexture(resourceLocation);
        }
    }

    public static InputStream getInputStreamFromZip(ResourceLocation loc) {
        return CoreUtils.getResourceInputStream(loc.getResourceDomain() + "/" + loc.getResourcePath());
    }

    public static void openBrowser(String url) {
        try {
            Class throwable = Class.forName("java.awt.Desktop");
            Object object = throwable.getMethod("getDesktop", new Class[0]).invoke((Object)null, new Object[0]);
            throwable.getMethod("browse", new Class[]{URI.class}).invoke(object, new Object[]{new URI(url)});
        } catch (Throwable var3) {
            var3.printStackTrace();
        }
    }

    @Nullable
    public static EntityLivingBase getClosestEntityLookedAt(double distance, float tick) {
        try {
            EntityLivingBase viewEntity = Minecraft.getMinecraft().renderViewEntity;
            distance = getDistanceToClosestSolidWall(viewEntity, tick, distance);
            EntityLivingBase result = null;
            double closest = distance;
            if (viewEntity == null) {
                viewEntity = Minecraft.getMinecraft().thePlayer;
            }
            if (viewEntity != null) {
                World worldObj = viewEntity.worldObj;
                MovingObjectPosition objectMouseOver = viewEntity.rayTrace(distance, 0.5F);
                Vec3 playerPosition = viewEntity.getPosition(tick);
                if (objectMouseOver != null) {
                    distance = getDistanceToClosestSolidWall(viewEntity, tick, distance);
                }

                Vec3 dirVec = viewEntity.getLookVec();
                Vec3 lookFarCoord = playerPosition.addVector(dirVec.xCoord * distance, dirVec.yCoord * distance, dirVec.zCoord * distance);
                List<?> targettedEntities = worldObj.getEntitiesWithinAABBExcludingEntity(viewEntity, AxisAlignedBB.getBoundingBox(
                        viewEntity.posX - (double)(viewEntity.width / 2.0F), viewEntity.posY, viewEntity.posZ - (double)(viewEntity.width / 2.0F),
                        viewEntity.posX + (double)(viewEntity.width / 2.0F), viewEntity.posY + (double) viewEntity.height,
                        viewEntity.posZ + (double)(viewEntity.width / 2.0F)).addCoord(dirVec.xCoord * distance, dirVec.yCoord * distance, dirVec.zCoord * distance));
                for (Object targettedEntity1 : targettedEntities) {
                    Entity targettedEntity = (Entity) targettedEntity1;
                    if (targettedEntity instanceof EntityLivingBase && !targettedEntity.isInvisible()) {
                        double precheck = (double) viewEntity.getDistanceToEntity(targettedEntity);
                        AxisAlignedBB aabb = AxisAlignedBB.getBoundingBox(targettedEntity.posX - (double) (targettedEntity.width / 2.0F),
                                targettedEntity.posY, targettedEntity.posZ - (double) (targettedEntity.width / 2.0F), targettedEntity.posX + (double) (targettedEntity.width / 2.0F),
                                targettedEntity.posY + (double) targettedEntity.height, targettedEntity.posZ + (double) (targettedEntity.width / 2.0F));
                        MovingObjectPosition mopElIntercept = aabb.calculateIntercept(playerPosition, lookFarCoord);
                        if (mopElIntercept != null && precheck < closest) {
                            result = (EntityLivingBase) targettedEntity;
                            closest = precheck;
                        }
                    }
                }
            }
            return result;
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            return null;
        }
    }

    private static double getDistanceToClosestSolidWall(EntityLivingBase viewEntity, float tick, double traceDistance) {
        return getClosestSolidWall(viewEntity, viewEntity.getPosition(tick), tick, traceDistance, 0, 0.0D);
    }

    private static double getClosestSolidWall(EntityLivingBase viewEntity, Vec3 startPosition, float tick, double traceDistance, int count, double offset) {
        if (count++ <= 20 && traceDistance - offset > 0.0D) {
            Vec3 vec31 = viewEntity.getLookVec();
            Vec3 vec32 = startPosition.addVector(vec31.xCoord * (traceDistance - offset), vec31.yCoord * (traceDistance - offset), vec31.zCoord * (traceDistance - offset));
            MovingObjectPosition objectMouseOver = viewEntity.worldObj.rayTraceBlocks(startPosition, vec32);
            if (objectMouseOver != null) {
                Block block = viewEntity.worldObj.getBlock(objectMouseOver.blockX, objectMouseOver.blockY, objectMouseOver.blockZ);
                if (block != null) {
                    if (block.getClass().getName().contains("BlockFrame")) {
                        return objectMouseOver.hitVec.distanceTo(viewEntity.getPosition(tick));
                    }

                    if (!block.getMaterial().isOpaque()) {
                        return getClosestSolidWall(viewEntity, objectMouseOver.hitVec.addVector(vec31.xCoord, vec31.yCoord, vec31.zCoord), tick, traceDistance, count, objectMouseOver.hitVec.distanceTo(startPosition));
                    }

                    return objectMouseOver.hitVec.distanceTo(viewEntity.getPosition(tick));
                }
            }
        }
        return traceDistance;
    }
}
