package ru.xlv.core.util;

import ru.xlv.core.api.CoreAPI;

import java.io.InputStream;

public class CoreUtils {

    public static boolean isDebugEnabled;
    private static final String BUILD_VERSION = "@BUILD_VERSION@";
    private static final String CONNECTION_REMOTE_GAME_SERVER_ADDRESS = "193.70.81.74:25567";
    private static String connectionGameServerAddress;
    private static boolean isStartedFromGradle;
    static {
        try {
            Class.forName("GradleStart");
            isStartedFromGradle = true;
            connectionGameServerAddress = "localhost";
            System.out.println("Client has been started from GradleStart");
        } catch (Throwable ignored) {
            isStartedFromGradle = false;
            connectionGameServerAddress = CONNECTION_REMOTE_GAME_SERVER_ADDRESS;
        }
    }

    public static boolean isStartedFromGradle() {
        return isStartedFromGradle;
    }

    public static String getConnectionGameServerAddress() {
        return connectionGameServerAddress;
    }

    /**
     * gets InputStream decrypting it before
     * */
    public static InputStream getResourceInputStream(String loc) {
        return CoreAPI.getResourceInputStream(loc, true);
    }
}
