package ru.xlv.core.util;

import java.util.HashMap;
import java.util.Map;

public class MapBuilder<K, V> {

    private Map<K, V> map;
    private K[] keys;
    private V[] values;

    public MapBuilder<K, V> keys(K[] keys) {
        this.keys = keys;
        return this;
    }

    public MapBuilder<K, V> values(V[] values) {
        this.values = values;
        return this;
    }

    public MapBuilder<K, V> impl(Map<K, V> map) {
        this.map = map;
        return this;
    }

    public Map<K, V> build() {
        Map<K, V> map = this.map == null ? new HashMap<>() : this.map;
        for (int i = 0; i < keys.length; i++) {
            map.put(keys[i], values[i]);
        }
        return map;
    }

    public static MapBuilder<?, ?> builder() {
        return new MapBuilder<>();
    }
}
