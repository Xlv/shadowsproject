package ru.xlv.shop.handle.item;

import com.google.gson.annotations.SerializedName;
import cpw.mods.fml.common.network.ByteBufUtils;
import io.netty.buffer.ByteBufOutputStream;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.ToString;
import net.minecraft.item.ItemStack;
import ru.xlv.core.common.util.config.Configurable;
import ru.xlv.core.item.ItemStackFactory;
import ru.xlv.shop.common.ShopItem;
import ru.xlv.shop.common.ShopItemCategory;
import ru.xlv.shop.common.ShopItemCost;
import ru.xlv.shop.common.ShopItemType;

import java.util.List;

@ToString(doNotUseGetters = true, callSuper = true)
@Getter
public class ShopItemStack extends ShopItem {

    @Configurable
    @SerializedName("unlocalizedName")
    private final String itemUnlocalizedName;
    @Configurable
    private final int amount;
    @Configurable
    private final int metadata;
    @Configurable
    private final String nbtString;

    @Getter(AccessLevel.NONE)
    private transient ItemStack itemStack;

    public ShopItemStack(ShopItemType shopItemType, int id, String name, String description, List<ShopItemCategory> categories, List<ShopItemCost> costs, String itemUnlocalizedName, int amount, int metadata, String nbtString) {
        super(shopItemType, id, name, description, categories, costs);
        this.itemUnlocalizedName = itemUnlocalizedName;
        this.amount = amount;
        this.metadata = metadata;
        this.nbtString = nbtString;
    }

    @Override
    public void writeDataToPacketPost(ByteBufOutputStream byteBufOutputStream) {
        ByteBufUtils.writeItemStack(byteBufOutputStream.buffer(), getItemStack());
    }

    public ItemStack getItemStack() {
        if (itemStack == null) {
            itemStack = ItemStackFactory.create(itemUnlocalizedName, amount, metadata, nbtString);
        }
        return itemStack;
    }
}
