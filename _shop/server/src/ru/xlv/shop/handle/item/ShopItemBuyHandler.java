package ru.xlv.shop.handle.item;

import ru.xlv.core.XlvsCore;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.shop.common.ShopItem;
import ru.xlv.shop.common.ShopItemCost;
import ru.xlv.shop.handle.result.ShopBuyResult;

import java.sql.SQLException;

public abstract class ShopItemBuyHandler<T extends ShopItem> {

    @SuppressWarnings("unchecked")
    public final ShopBuyResult checkConditions(ServerPlayer serverPlayer, ShopItem shopItem, ShopItemCost.Type type) {
        return checkConditions0(serverPlayer, (T) shopItem, type);
    }

    @SuppressWarnings("unchecked")
    public final ShopBuyResult consumeCosts(ServerPlayer serverPlayer, ShopItem shopItem, ShopItemCost.Type type) {
        return consumeCosts0(serverPlayer, (T) shopItem, type);
    }

    @SuppressWarnings("unchecked")
    public final boolean giveOut(ServerPlayer serverPlayer, ShopItem shopItem) {
        return giveOut0(serverPlayer, (T) shopItem);
    }

    protected ShopBuyResult checkConditions0(ServerPlayer serverPlayer, T t, ShopItemCost.Type type) {
        for (ShopItemCost cost : t.getCosts()) {
            if(cost.getType() == type) {
                switch (cost.getType()) {
                    case CREDITS:
                        if (serverPlayer.getSelectedCharacter().getWallet().getCredits() < cost.getAmount()) {
                            return ShopBuyResult.NOT_ENOUGH_MONEY;
                        }
                        break;
                    case PLATINUM:
                        if (serverPlayer.getPlatinum() < cost.getAmount()) {
                            return ShopBuyResult.NOT_ENOUGH_MONEY;
                        }
                }
                return ShopBuyResult.SUCCESS;
            }
        }
        return ShopBuyResult.UNSUPPORTED_COST;
    }

    protected ShopBuyResult consumeCosts0(ServerPlayer serverPlayer, T t, ShopItemCost.Type type) {
        for (ShopItemCost cost : t.getCosts()) {
            if(cost.getType() == type) {
                switch (cost.getType()) {
                    case CREDITS:
                        if (!serverPlayer.getSelectedCharacter().getWallet().consumeCredits(cost.getAmount())) {
                            return ShopBuyResult.NOT_ENOUGH_MONEY;
                        }
                        break;
                    case PLATINUM:
                        try {
                            if (!XlvsCore.INSTANCE.getPlayerManager().consumePlatinum(serverPlayer, cost.getAmount())) {
                                return ShopBuyResult.NOT_ENOUGH_MONEY;
                            }
                        } catch (SQLException throwables) {
                            throwables.printStackTrace();
                            return ShopBuyResult.UNKNOWN_ERROR;
                        }
                }
                return ShopBuyResult.SUCCESS;
            }
        }
        return ShopBuyResult.UNSUPPORTED_COST;
    }

    protected abstract boolean giveOut0(ServerPlayer serverPlayer, T t);
}
