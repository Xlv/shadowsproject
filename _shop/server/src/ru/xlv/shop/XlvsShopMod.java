package ru.xlv.shop;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import lombok.Getter;
import ru.xlv.core.XlvsCore;
import ru.xlv.shop.handle.ShopHandler;
import ru.xlv.shop.handle.ShopItemManager;
import ru.xlv.shop.handle.item.ShopItemStack;
import ru.xlv.shop.handle.item.ShopItemStackBuyHandler;
import ru.xlv.shop.network.PacketShopBuy;
import ru.xlv.shop.network.PacketShopSyncCategory;
import ru.xlv.shop.util.ShopConfig;
import ru.xlv.shop.util.ShopLocalization;

import static ru.xlv.shop.XlvsShopMod.MODID;

@Mod(
        name = MODID,
        modid = MODID,
        version = "1.0"
)
@Getter
public class XlvsShopMod {

    static final String MODID = "xlvsshop";

    @Mod.Instance(MODID)
    public static XlvsShopMod INSTANCE;

    private final ShopLocalization localization = new ShopLocalization();
    private final ShopConfig shopConfig = new ShopConfig();
    private ShopHandler shopHandler;

    @Mod.EventHandler
    public void event(FMLInitializationEvent event) {
        localization.load();
        shopConfig.load();
        ShopItemManager shopItemManager = new ShopItemManager(shopConfig.getShopItemList());
        shopHandler = new ShopHandler(shopItemManager);
        shopHandler.setShopItemBuyHandler(ShopItemStack.class, new ShopItemStackBuyHandler());
        XlvsCore.INSTANCE.getPacketHandler().getPacketRegistry().register(MODID,
                new PacketShopBuy(),
                new PacketShopSyncCategory()
        );
    }
}
