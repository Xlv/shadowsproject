package ru.xlv.navigator.handle;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.event.ServerPlayerLoginEvent;
import ru.xlv.navigator.network.PacketNavigatorSync;

public class EventListener {

    @SubscribeEvent
    public void event(ServerPlayerLoginEvent event) {
        XlvsCore.INSTANCE.getPacketHandler().sendPacketToPlayer(event.getServerPlayer().getEntityPlayer(), new PacketNavigatorSync());
    }
}
