package ru.xlv.navigator.common.portal;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.minecraft.util.AxisAlignedBB;

import java.util.UUID;

@Getter
@RequiredArgsConstructor
public class Portal {

    private final UUID uuid;
    private final AxisAlignedBB portalArea;
    private final PortalDestination portalDestination;
}
