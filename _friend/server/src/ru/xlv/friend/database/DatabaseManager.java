package ru.xlv.friend.database;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.changestream.ChangeStreamDocument;
import com.mongodb.client.model.changestream.FullDocument;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import org.bson.Document;
import ru.xlv.core.database.IDatabaseEventListener;
import ru.xlv.core.database.mongodb.ConfigMongoDB;
import ru.xlv.core.database.mongodb.MongoProvider;
import ru.xlv.core.util.IConverter;
import ru.xlv.friend.common.FriendRelation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

public class DatabaseManager {

    private static final String FRIEND_INITIATOR_KEY = "initiator";
    private static final String FRIEND_TARGET_KEY = "target";
    private static final String FRIEND_STATE_KEY = "state";

    private final IConverter<FriendRelation, Document> FRIEND_CONVERTER = new IConverter<FriendRelation, Document>() {
        @Override
        public Document convertTo(FriendRelation friendRelation) {
            return new Document()
                    .append(FRIEND_INITIATOR_KEY, friendRelation.getInitiator())
                    .append(FRIEND_TARGET_KEY, friendRelation.getTarget())
                    .append(FRIEND_STATE_KEY, friendRelation.getState().ordinal());
        }

        @Override
        public FriendRelation convertFrom(Document document) {
            FriendRelation friendRelation = new FriendRelation(document.getString(FRIEND_INITIATOR_KEY), document.getString(FRIEND_TARGET_KEY));
            friendRelation.setState(FriendRelation.State.values()[document.getInteger(FRIEND_STATE_KEY)]);
            return friendRelation;
        }
    };

    private ExecutorService executorService;

    private MongoProvider mongoProvider;
    private MongoCollection<Document> friendCollection;

    public void init(IDatabaseEventListener<FriendRelation> databaseEventListener) {
        executorService = Executors.newFixedThreadPool(16);
        mongoProvider = new MongoProvider(new ConfigMongoDB("config/friend/database.json"));
        mongoProvider.init();
        friendCollection = mongoProvider.getDatabase().getCollection("friend");
        executorService.submit(() -> friendCollection.watch(
                Collections.singletonList(
                        Aggregates.match(
                                Filters.in("operationType", Arrays.asList("insert", "update", "replace", "delete"))
                        )
                )
        ).fullDocument(FullDocument.UPDATE_LOOKUP).forEach((Consumer<? super ChangeStreamDocument<Document>>) documentChangeStreamDocument -> {
            switch (documentChangeStreamDocument.getOperationType()) {
                case UPDATE:
                    databaseEventListener.onUpdate(FRIEND_CONVERTER.convertFrom(documentChangeStreamDocument.getFullDocument()));
                    break;
                case REPLACE:
                case DELETE:
                    databaseEventListener.onDelete(FRIEND_CONVERTER.convertFrom(documentChangeStreamDocument.getFullDocument()));
                    break;
                case INSERT:
                    databaseEventListener.onInsert(FRIEND_CONVERTER.convertFrom(documentChangeStreamDocument.getFullDocument()));
            }
        }));
    }

    public void shutdown() {
        executorService.shutdown();
        mongoProvider.shutdown();
    }

    public CompletableFuture<Boolean> removeFriendRelation(FriendRelation friendRelation) {
        return CompletableFuture.supplyAsync(() -> {
            DeleteResult deleteResult = friendCollection.deleteOne(
                    Filters.and(
                            Filters.eq(FRIEND_INITIATOR_KEY, friendRelation.getInitiator()),
                            Filters.eq(FRIEND_TARGET_KEY, friendRelation.getTarget())
                    )
            );
            return deleteResult.getDeletedCount() > 0;
        }, executorService);
    }

    public CompletableFuture<List<FriendRelation>> getAllFriends() {
        return CompletableFuture.supplyAsync(() -> {
            List<FriendRelation> list = new ArrayList<>();
            friendCollection.find().forEach((Consumer<? super Document>) document -> list.add(FRIEND_CONVERTER.convertFrom(document)));
            return list;
        }, executorService);
    }

    public CompletableFuture<Boolean> updateFriendRelation(FriendRelation friendRelation) {
        return CompletableFuture.supplyAsync(() -> {
            Document document = FRIEND_CONVERTER.convertTo(friendRelation);
            UpdateResult updateResult = friendCollection.updateOne(
                    Filters.and(
                            Filters.eq(FRIEND_INITIATOR_KEY, friendRelation.getInitiator()),
                            Filters.eq(FRIEND_TARGET_KEY, friendRelation.getTarget())
                    ),
                    new Document("$set", document)
            );
            if(updateResult.getModifiedCount() == 0) {
                friendCollection.insertOne(document);
            }
            return true;
        }, executorService);
    }
}
