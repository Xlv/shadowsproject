package ru.xlv.friend.handle.result;

import ru.xlv.friend.XlvsFriendMod;

public enum FriendInviteResult {

    SUCCESSFULLY_INVITED(XlvsFriendMod.INSTANCE.getLocalization().responseFriendInviteSuccessMessage),
    SUCCESSFULLY_ACCEPTED(XlvsFriendMod.INSTANCE.getLocalization().responseFriendInviteAcceptedMessage),
    DATABASE_ERROR(XlvsFriendMod.INSTANCE.getLocalization().responseFriendInviteDatabaseErrorMessage),
    ALREADY_INVITED(XlvsFriendMod.INSTANCE.getLocalization().responseFriendInviteAlreadyInvitedMessage);

    private String responseMessage;

    FriendInviteResult(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getResponseMessage() {
        return responseMessage;
    }
}
