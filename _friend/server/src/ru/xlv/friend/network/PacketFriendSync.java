package ru.xlv.friend.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.network.IPacketCallbackOnServer;
import ru.xlv.core.common.network.PacketCallbackSender;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.core.network.RequestController;
import ru.xlv.friend.XlvsFriendMod;
import ru.xlv.friend.common.FriendIO;
import ru.xlv.friend.common.FriendRelation;

import java.io.IOException;
import java.util.List;

@NoArgsConstructor
public class PacketFriendSync implements IPacketCallbackOnServer {

    private static final RequestController<String> REQUEST_CONTROLLER = new RequestController.Periodic<>(1000L);

    private List<FriendRelation> relations;

    @Override
    public void read(EntityPlayer entityPlayer, ByteBufInputStream bbis, PacketCallbackSender packetCallbackSender) throws IOException {
        if(REQUEST_CONTROLLER.tryRequest(entityPlayer.getCommandSenderName())) {
            relations = XlvsFriendMod.INSTANCE.getFriendHandler().getAllRelations(entityPlayer.getCommandSenderName());
            packetCallbackSender.send();
        }
    }

    @Override
    public void write(EntityPlayer entityPlayer, ByteBufOutputStream bbos) throws IOException {
        bbos.writeBoolean(relations != null);
        if (relations != null) {
            FriendIO.writeFriendRelationList(relations, bbos);
        } else {
            bbos.writeUTF(XlvsCore.INSTANCE.getLocalization().responseTooManyRequests);
        }
    }

    @Override
    public boolean handleCallback() {
        return true;
    }
}
