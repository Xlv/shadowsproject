package ru.xlv.friend.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.common.network.IPacketOutServer;
import ru.xlv.friend.XlvsFriendMod;
import ru.xlv.friend.common.FriendIO;
import ru.xlv.friend.common.FriendRelation;

import java.io.IOException;
import java.util.List;

@NoArgsConstructor
public class PacketFriendSyncServer implements IPacketOutServer {
    @Override
    public void write(EntityPlayer entityPlayer, ByteBufOutputStream bbos) throws IOException {
        List<FriendRelation> allRelations = XlvsFriendMod.INSTANCE.getFriendHandler().getAllRelations(entityPlayer.getCommandSenderName());
        if (!allRelations.isEmpty()) {
            FriendIO.writeFriendRelationList(allRelations, bbos);
        }
    }
}
