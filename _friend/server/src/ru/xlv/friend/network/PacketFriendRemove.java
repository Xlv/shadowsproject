package ru.xlv.friend.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.network.IPacketCallbackOnServer;
import ru.xlv.core.common.network.IPacketInOnServer;
import ru.xlv.core.common.network.IPacketOutServer;
import ru.xlv.core.common.network.PacketCallbackSender;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.friend.XlvsFriendMod;
import ru.xlv.friend.handle.result.FriendRemoveResult;

import java.io.IOException;

@NoArgsConstructor
public class PacketFriendRemove implements IPacketCallbackOnServer {

    private FriendRemoveResult friendRemoveResult;

    @Override
    public void read(EntityPlayer entityPlayer, ByteBufInputStream bbis, PacketCallbackSender packetCallbackSender) throws IOException {
        String targetName = bbis.readUTF();
        XlvsFriendMod.INSTANCE.getFriendHandler().removeFriend(entityPlayer.getCommandSenderName(), targetName).thenAccept(friendRemoveResult -> {
            this.friendRemoveResult = friendRemoveResult;
            packetCallbackSender.send();
        });
    }

    @Override
    public void write(EntityPlayer entityPlayer, ByteBufOutputStream bbos) throws IOException {
        bbos.writeUTF(friendRemoveResult.getResponseMessage());
    }

    @Override
    public boolean handleCallback() {
        return true;
    }
}
