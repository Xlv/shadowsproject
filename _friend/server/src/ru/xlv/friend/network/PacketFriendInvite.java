package ru.xlv.friend.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.common.network.IPacketOutServer;
import ru.xlv.friend.common.FriendIO;
import ru.xlv.friend.common.FriendRelation;

import java.io.IOException;

@NoArgsConstructor
public class PacketFriendInvite implements IPacketOutServer {

    private FriendRelation friendRelation;

    public PacketFriendInvite(FriendRelation friendRelation) {
        this.friendRelation = friendRelation;
    }

    @Override
    public void write(EntityPlayer entityPlayer, ByteBufOutputStream bbos) throws IOException {
        FriendIO.writeFriendRelation(friendRelation, bbos);
    }
}
