package ru.xlv.friend;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStoppingEvent;
import lombok.Getter;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.XlvsCoreCommon;
import ru.xlv.friend.database.DatabaseEventListener;
import ru.xlv.friend.database.DatabaseManager;
import ru.xlv.friend.event.EventListener;
import ru.xlv.friend.handle.FriendHandler;
import ru.xlv.friend.network.*;
import ru.xlv.friend.util.FriendLocalization;

import static ru.xlv.friend.XlvsFriendMod.MODID;

@Mod(
        name = "XlvsFriendMod",
        modid = MODID,
        version = "1.0"
)
public class XlvsFriendMod {

    static final String MODID = "xlvsfriend";

    @Mod.Instance(MODID)
    public static XlvsFriendMod INSTANCE;

    @Getter
    private FriendHandler friendHandler;
    @Getter
    private DatabaseManager databaseManager;
    @Getter
    private final FriendLocalization localization = new FriendLocalization();

    @Mod.EventHandler
    public void event(FMLInitializationEvent event) {
        databaseManager = new DatabaseManager();
        databaseManager.init(new DatabaseEventListener());
        friendHandler = new FriendHandler();
        friendHandler.init();

        XlvsCore.INSTANCE.getPacketHandler().getPacketRegistry().register(MODID,
                new PacketFriendSync(),
                new PacketFriendInvite(),
                new PacketFriendInviteRequest(),
                new PacketFriendRemove(),
                new PacketFriendPlayerListGet(),
                new PacketFriendSyncServer()
        );
        localization.load();

        XlvsCoreCommon.EVENT_BUS.register(new EventListener());
    }

    @Mod.EventHandler
    public void event(FMLServerStoppingEvent event) {
        databaseManager.shutdown();
    }
}
