package ru.xlv.friend.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import ru.xlv.core.common.network.IPacketCallbackEffective;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.core.network.PacketClientPlayerSync;
import ru.xlv.core.player.ClientPlayer;

import javax.annotation.Nullable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
public class PacketFriendPlayerListGet implements IPacketCallbackEffective<List<ClientPlayer>> {

    private final List<ClientPlayer> result = new ArrayList<>();

    private FilterType filterType;
    private int offset;

    @Override
    public void write(ByteBufOutputStream bbos) throws IOException {
        bbos.writeInt(filterType.ordinal());
        bbos.writeInt(offset);
    }

    @Override
    public void read(ByteBufInputStream bbis) throws IOException {
        result.addAll(readList(bbis, () -> PacketClientPlayerSync.readClientPlayer(bbis)));
    }

    @Nullable
    @Override
    public List<ClientPlayer> getResult() {
        return result;
    }

    public enum FilterType {
        FRIENDS,
        OUTGOING_INVITE,
        INCOMING_INVITE
    }
}
