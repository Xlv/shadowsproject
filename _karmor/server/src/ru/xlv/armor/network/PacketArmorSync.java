package ru.xlv.armor.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.armor.CoreArmorCommon;
import ru.xlv.armor.util.ArmorUtil;
import ru.xlv.core.common.item.ItemArmor;
import ru.xlv.core.common.network.IPacketOutServer;

import java.io.IOException;

@NoArgsConstructor
public class PacketArmorSync implements IPacketOutServer {

    private ItemArmor itemArmor;

    public PacketArmorSync(ItemArmor itemArmor) {
        this.itemArmor = itemArmor;
    }

    @Override
    public void write(EntityPlayer entityPlayer, ByteBufOutputStream bbos) throws IOException {
        ArmorUtil.writeArmor(itemArmor, bbos);
    }
}
