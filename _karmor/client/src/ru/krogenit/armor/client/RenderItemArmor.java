package ru.krogenit.armor.client;

import net.minecraft.item.ItemStack;
import net.minecraftforge.client.IItemRenderer;
import org.lwjgl.opengl.GL11;
import ru.krogenit.armor.item.ItemArmorByPart;
import ru.xlv.core.common.inventory.MatrixInventory;

public class RenderItemArmor implements IItemRenderer {

	private final IArmorRenderer armorRenderer;

	public RenderItemArmor(IArmorRenderer armorRenderer) {
		this.armorRenderer = armorRenderer;
	}

	@Override
	public boolean handleRenderType(ItemStack var1, ItemRenderType type) {
		return true;
	}

	@Override
	public boolean shouldUseRenderHelper(ItemRenderType var1, ItemStack var2, ItemRendererHelper var3) {
		return false;
	}

	@Override
	public void renderItem(ItemRenderType type, ItemStack var2, Object... var3) {
		ItemArmorByPart armorItem = (ItemArmorByPart) var2.getItem();

		switch (type) {
			case EQUIPPED_FIRST_PERSON:
				GL11.glPushMatrix();
				GL11.glTranslatef(1.1F, -0.2F, -0.0F);
				GL11.glRotatef(-90.0F, 0.0F, 1.0F, 0.0F);
				GL11.glRotatef(5f, 0.0F, 0.0F, 1.0F);
				GL11.glRotatef(15.0F, 1.0F, 0.0F, 0.0F);
				renderByType(armorItem.getSlotTypes().get(0), type);
				GL11.glPopMatrix();
				return;
			case EQUIPPED:
				GL11.glPushMatrix();
				GL11.glRotatef(120.0F, 1.0F, 0.0F, 0.0F);
				GL11.glRotatef(180.0F, 0.0F, 1.0F, 0.0F);
				GL11.glRotatef(-160.0F, 0.0F, 0.0F, 1.0F);
				GL11.glTranslatef(0.3F, -0.9F, 0.2F);
				GL11.glScalef(1.2F, 1.2F, 1.2F);
				renderByType(armorItem.getSlotTypes().get(0), type);
				GL11.glPopMatrix();
				return;
			case INVENTORY:
				GL11.glTranslatef(8f, 20f, 0);
				GL11.glRotatef(-15f, 0, 1, 0);
				GL11.glRotatef(-10f, 1, 0, 0);
				GL11.glRotatef(180f, 0, 0, 1);
				float scale = 12f;
				GL11.glScalef(-scale, scale, scale);
				renderByType(armorItem.getSlotTypes().get(0), type);
				return;
			case ENTITY:
				GL11.glPushMatrix();
				GL11.glRotatef(90.0F, 1.0F, 0.0F, 0.0F);
				GL11.glTranslatef(-0.0F, -1.3F, 0.05F);
				GL11.glScalef(1.5F, 1.5F, 1.5F);
				renderByType(armorItem.getSlotTypes().get(0), type);
				GL11.glPopMatrix();
		}
	}

	@Override
	public void renderItemPost(ItemRenderType type, ItemStack item, Object... data) {

	}

	private void renderByType(MatrixInventory.SlotType slotType, ItemRenderType type) {
		switch (slotType) {
			case HEAD:
				armorRenderer.renderHeadInv(type);
				break;
			case BODY:
				armorRenderer.renderBodyInv(type);
				break;
			case BRACERS:
				armorRenderer.renderBracersInv(type);
				break;
			case LEGS:
				armorRenderer.renderLegsInv(type);
				break;
			case FEET:
				armorRenderer.renderBootsInv(type);
				break;
		}
	}
}
