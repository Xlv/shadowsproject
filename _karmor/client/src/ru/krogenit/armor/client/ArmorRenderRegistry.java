package ru.krogenit.armor.client;

public class ArmorRenderRegistry {

    public static ArmorRenderer armorEngineer = new ArmorRenderer("Engineer", true, 10f);
    public static ArmorRenderer armorLight = new ArmorRenderer("Light", true, 2f);
    public static ArmorRenderer armorMilitary = new ArmorRenderer("Military", false, 1f);
    public static ArmorRenderer armorPolice = new ArmorRenderer("Police", true, 2f);
    public static ArmorRenderer armorPredator = new ArmorRenderer("Predator", true, 2.5f);
    public static ArmorRenderer armorScientific = new ArmorRenderer("Scientific", true, 2.5f);
    public static ArmorRenderer armorSpider = new ArmorRenderer("Spider", true, 2.5f);
    public static ArmorRenderer armorStealth = new ArmorRenderer("Invisible", true, 2f);
    public static ArmorRenderer armorVlad = new ArmorRenderer("Vlad", true, 2.5f);
    public static ArmorRenderer armorJager = new ArmorRenderer("Jager", true, 2f);
    public static ArmorRenderer armorMedic = new ArmorRenderer("Medic", true, 2f);
    public static ArmorRenderer armorColonist = new ArmorRenderer("Colonist", false, 2f);
    public static ArmorRenderer armorEye = new ArmorRenderer("Eye", true, 2f);
    public static ArmorRenderer armorHeavy = new ArmorRenderer("Heavy", true, 2f);
    public static ArmorRenderer armorWarrior = new ArmorRenderer("Warrior", true, 2f);
}
