package ru.krogenit.armor.client;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import noppes.npcs.client.renderer.RenderNPCHumanMale;
import noppes.npcs.entity.EntityNPCInterface;
import ru.krogenit.armor.CoreArmorClient;
import ru.krogenit.armor.item.ItemArmorByPart;
import ru.krogenit.armor.item.ItemArmorByPartClient;
import ru.xlv.core.common.event.EventRenderArmorNPC;

import java.util.Collection;

public class EventRenderNPCArmor {

    @SubscribeEvent
    public void event(EventRenderArmorNPC e) {
        EntityLivingBase entityLivingBase = e.getEntityLivingBase();
        if(entityLivingBase instanceof EntityNPCInterface) {
            EntityNPCInterface npc = (EntityNPCInterface) entityLivingBase;
            Collection<ItemStack> values = npc.inventory.getArmor().values();
            for (ItemStack itemStack : values) {
                if(itemStack != null && itemStack.getItem() instanceof ItemArmorByPartClient) {
                    ItemArmorByPart itemArmorByPart = (ItemArmorByPart) itemStack.getItem();
                    IArmorRenderer render = CoreArmorClient.getModelForArmor(itemStack.getItem().getUnlocalizedName());
                    if (render != null) {
                        render.render(((RenderNPCHumanMale)e.getRender()).getModelBipedMain(), itemArmorByPart.getSlotTypes().get(0), entityLivingBase, itemStack);
                    }
                }
            }
        }
    }
}
