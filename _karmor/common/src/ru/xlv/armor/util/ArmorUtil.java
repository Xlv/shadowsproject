package ru.xlv.armor.util;

import io.netty.buffer.ByteBufOutputStream;
import ru.xlv.armor.CoreArmorCommon;
import ru.xlv.core.common.item.ItemArmor;
import ru.xlv.core.common.item.quality.EnumItemQuality;
import ru.xlv.core.common.item.rarity.EnumItemRarity;
import ru.xlv.core.common.item.tag.EnumItemTag;
import ru.xlv.core.common.player.character.CharacterAttributeMod;
import ru.xlv.core.common.player.character.CharacterAttributeType;
import ru.xlv.core.common.util.ByteBufInputStream;

import java.io.IOException;

public class ArmorUtil {

    public static <T extends ItemArmor> void writeArmor(T itemArmor, ByteBufOutputStream byteBufOutputStream) throws IOException {
        byteBufOutputStream.writeUTF(itemArmor.getUnlocalizedName());
        byteBufOutputStream.writeInt(itemArmor.getCharacterAttributeBoostMap().size());
        for (CharacterAttributeType characterAttributeType : itemArmor.getCharacterAttributeBoostMap().keySet()) {
            byteBufOutputStream.writeInt(characterAttributeType.ordinal());
            byteBufOutputStream.writeDouble(itemArmor.getCharacterAttributeBoostMap().get(characterAttributeType).getValueAdd());
        }
        byteBufOutputStream.writeInt(itemArmor.getInvMatrixWidth());
        byteBufOutputStream.writeInt(itemArmor.getInvMatrixHeight());
        byteBufOutputStream.writeInt(itemArmor.getMaxDamage());
        byteBufOutputStream.writeInt(itemArmor.getItemQuality().ordinal());
        byteBufOutputStream.writeInt(itemArmor.getItemRarity().ordinal());
        byteBufOutputStream.writeUTF(itemArmor.getDisplayName());
        byteBufOutputStream.writeUTF(itemArmor.getSecondName());
        byteBufOutputStream.writeUTF(itemArmor.getDescription());
        byteBufOutputStream.writeUTF(itemArmor.getSlogan());
        byteBufOutputStream.writeInt(itemArmor.getItemTags().size());
        for (EnumItemTag itemTag : itemArmor.getItemTags()) {
            byteBufOutputStream.writeInt(itemTag.ordinal());
        }
    }

    public static void readArmor(ByteBufInputStream byteBufInputStream) throws IOException {
        String unlocalizedName = byteBufInputStream.readUTF();
        for (ItemArmor itemArmor : CoreArmorCommon.registeredArmorList) {
            if(itemArmor.getUnlocalizedName().equals(unlocalizedName)) {
                itemArmor.getCharacterAttributeBoostMap().clear();
                itemArmor.getItemTags().clear();
                int c = byteBufInputStream.readInt();
                for (int i = 0; i < c; i++) {
                    CharacterAttributeType value = CharacterAttributeType.values()[byteBufInputStream.readInt()];
                    CharacterAttributeMod value1 = CharacterAttributeMod.builder().attributeType(value).valueAdd(byteBufInputStream.readDouble()).build();
                    itemArmor.getCharacterAttributeBoostMap().put(value, value1);
                }
                itemArmor.setInvMatrixSize(byteBufInputStream.readInt(), byteBufInputStream.readInt());
                itemArmor.setMaxDamage(byteBufInputStream.readInt());
                itemArmor.setItemQuality(EnumItemQuality.values()[byteBufInputStream.readInt()]);
                itemArmor.setItemRarity(EnumItemRarity.values()[byteBufInputStream.readInt()]);
                itemArmor.setDisplayName(byteBufInputStream.readUTF());
                itemArmor.setSecondName(byteBufInputStream.readUTF());
                itemArmor.setDescription(byteBufInputStream.readUTF());
                itemArmor.setSlogan(byteBufInputStream.readUTF());
                c = byteBufInputStream.readInt();
                for (int i = 0; i < c; i++) {
                    itemArmor.getItemTags().add(EnumItemTag.values()[byteBufInputStream.readInt()]);
                }
                break;
            }
        }
    }
}
