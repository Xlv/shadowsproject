// ==================================================================
// This file is part of Smart Moving.
//
// Smart Moving is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Smart Moving is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Smart Moving. If not, see <http://www.gnu.org/licenses/>.
// ==================================================================

package net.smart.moving;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Loader;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.ModContainer;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.TickEvent.ClientTickEvent;
import cpw.mods.fml.common.network.FMLNetworkEvent.ClientCustomPacketEvent;
import cpw.mods.fml.common.network.FMLNetworkEvent.ServerCustomPacketEvent;
import cpw.mods.fml.common.network.NetworkRegistry;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.NetHandlerPlayServer;
import net.smart.core.SmartCoreEventHandler;
import net.smart.moving.config.SmartMovingConfig;
import net.smart.moving.config.SmartMovingOptions;
import net.smart.utilities.Reflect;

import java.io.File;
import java.util.List;

@Mod(modid = "SmartMoving", name = "Smart Moving", version = "15.6", dependencies = "required-after:PlayerAPI@[1.3,)")
public class SmartMovingMod
{
	protected static String ModComVersion = "2.3.1";

	private final boolean isClient;

	private boolean hasRenderer = false;

	public SmartMovingMod()
	{
		isClient = FMLCommonHandler.instance().getSide().isClient();
	}

	@EventHandler
	@SuppressWarnings("unused")
	public void init(FMLInitializationEvent event)
	{
		NetworkRegistry.INSTANCE.newEventDrivenChannel(SmartMovingPacketStream.Id).register(this);
		SmartMovingServer.initialize(new File("."), FMLCommonHandler.instance().getMinecraftServerInstance().getGameType().getID(), new SmartMovingConfig());
		SmartCoreEventHandler.Add(new SmartMovingCoreEventHandler());
	}

	@EventHandler
	@SuppressWarnings("unused")
	public void postInit(FMLPostInitializationEvent event)
	{
		net.smart.moving.playerapi.SmartMovingServerPlayerBase.registerPlayerBase();
	}

	@SubscribeEvent
	@SuppressWarnings({ "static-method", "unused" })
	public void tickStart(ClientTickEvent event)
	{
		SmartMovingContext.onTickInGame();
	}

	@SubscribeEvent
	@SuppressWarnings("static-method")
	public void onPacketData(ServerCustomPacketEvent event)
	{
		SmartMovingPacketStream.receivePacket(event.packet, SmartMovingServerComm.instance, net.smart.moving.playerapi.SmartMovingServerPlayerBase.getPlayerBase(((NetHandlerPlayServer)event.handler).playerEntity));
	}

	@SubscribeEvent
	@SuppressWarnings("static-method")
	public void onPacketData(ClientCustomPacketEvent event)
	{
		SmartMovingPacketStream.receivePacket(event.packet, SmartMovingComm.instance, null);
	}

	public void registerGameTicks()
	{
		FMLCommonHandler.instance().bus().register(this);
	}

	@SuppressWarnings("static-method")
	public Object getInstance(EntityPlayer entityPlayer)
	{
		return SmartMovingFactory.getInstance(entityPlayer);
	}

	@SuppressWarnings("static-method")
	public void checkForPresentModsAndInitializeOptions()
	{
		List<ModContainer> modList = Loader.instance().getActiveModList();
		boolean hasRedPowerWiring = false;
		boolean hasBuildCraftTransport = false;
		boolean hasFiniteLiquid = false;
		boolean hasBetterThanWolves = false;
		boolean hasSinglePlayerCommands = false;
		boolean hasRopesPlus = false;
		boolean hasASGrapplingHook = false;
		boolean hasBetterMisc = false;

		for(int i = 0; i < modList.size(); i++)
		{
			ModContainer mod = modList.get(i);
			String name = mod.getName();

			if(name.contains("RedPowerWiring"))
				hasRedPowerWiring = true;
			else if(name.contains("BuildCraftTransport"))
				hasBuildCraftTransport = true;
			else if(name.contains("Liquid"))
				hasFiniteLiquid = true;
			else if(name.contains("FCBetterThanWolves"))
				hasBetterThanWolves = true;
			else if(name.contains("SinglePlayerCommands"))
				hasSinglePlayerCommands = true;
			else if(name.contains("ASGrapplingHook"))
				hasASGrapplingHook = true;
			else if(name.contains("BetterMisc"))
				hasBetterMisc = true;
		}

		hasRopesPlus = Reflect.CheckClasses(SmartMovingMod.class, SmartMovingInstall.RopesPlusCore);

		SmartMovingOptions.initialize(hasRedPowerWiring, hasBuildCraftTransport, hasFiniteLiquid, hasBetterThanWolves, hasSinglePlayerCommands, hasRopesPlus, hasASGrapplingHook, hasBetterMisc);
	}
}