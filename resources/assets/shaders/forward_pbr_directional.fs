//PBR Dir Fragment Shader specially written for ShadowS Project
//Copyright © 2019-2020 by Krogenit
#version 330 core
out vec4 outputColor;

#define DIRECTIONAL_LIGHTS 1

in vec2 texCoords;
in vec2 lightMapCoords;
in vec4 color;
in vec3 position;
in vec3 normal;
in vec3 tangent;
in vec3 delta;

uniform bool useTexture;
uniform sampler2D diffuse;
uniform bool useLightMap;
uniform sampler2D lightMap;
uniform sampler2D normalMap;
uniform sampler2D specularMap;
uniform sampler2D emissionMap;
uniform sampler2D glossMap;

uniform bool useNormalMapping;
uniform bool useSpecularMapping;
uniform bool useEmissionMapping;
uniform bool useGlossMapping;

uniform float emissionPower;

uniform bool useDirectLight;
uniform bool usePointLights;

const float PI = 3.14159265359;

uniform struct DirLight {
	vec3 dir;
	vec3 color;
	float specular;
} directLight[DIRECTIONAL_LIGHTS];

float DistributionGGX(vec3 N, vec3 H, float roughness) {
    float a      = roughness*roughness;
    float a2     = a*a;
    float NdotH  = max(dot(N, H), 0.0);
    float NdotH2 = NdotH*NdotH;
	
    float num   = a2;
    float denom = (NdotH2 * (a2 - 1.0) + 1.0);
    denom = PI * denom * denom;
	
    return num / denom;
}

float GeometrySchlickGGX(float NdotV, float roughness) {
    float r = (roughness + 1.0);
    float k = (r*r) / 8.0;

    float num   = NdotV;
    float denom = NdotV * (1.0 - k) + k;
	
    return num / denom;
}

float GeometrySmith(vec3 N, vec3 V, vec3 L, float roughness) {
    float NdotV = max(dot(N, V), 0.0);
    float NdotL = max(dot(N, L), 0.0);
    float ggx2  = GeometrySchlickGGX(NdotV, roughness);
    float ggx1  = GeometrySchlickGGX(NdotL, roughness);
	
    return ggx1 * ggx2;
}
vec3 fresnelSchlick(float cosTheta, vec3 F0) {
    return F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0);
}  

void main() {
	vec4 diffuseColor = vec4(1.0);
	if(useTexture) {
		diffuseColor = texture(diffuse, texCoords) * color;
	} else {
		diffuseColor = color;
	}
	
	vec3 lightMapColor = vec3(1.0, 1.0, 1.0);
	if(useLightMap) {
		lightMapColor = texture(lightMap, lightMapCoords).xyz;
	}
	
	vec3 em = vec3(0.0, 0.0, 0.0);
	if(useEmissionMapping) {
		em = texture(emissionMap, texCoords).rgb * emissionPower;
	}
	
	if(useDirectLight) {
		vec3 unitNormal = normal;
		float metallic = 0.01;
		float roughness = 0.9; 
		
		if(useNormalMapping) {
			vec4 normalTex = texture(normalMap, texCoords);
			vec4 normalMapValue = 2.0 * normalTex - 1.0;
			vec3 bitang = cross(normal, tangent);
			unitNormal = normalize(normalMapValue.rgb);
			
			mat3 toTangentSpace = mat3(
				tangent, bitang, normal
			);
				
			unitNormal = normalize(toTangentSpace * unitNormal);
		}
			
		if(useSpecularMapping) {
			metallic = clamp(texture(specularMap, texCoords).r, 0.001, 0.999);
		}
			
		if(useGlossMapping) {
			roughness = clamp(1.0 - texture(glossMap, texCoords).r, 0.2, 0.999);
		}

		vec3 albedo = diffuseColor.rgb;
		
		vec3 N = normalize(unitNormal.xyz);
		vec3 V = normalize(-position.xyz);
		vec3 F0 = vec3(0.04); 
		float ao = 0.99;
		
		F0 = mix(F0, albedo, metallic);
		vec3 L = normalize(delta);
		vec3 H = normalize(V + L);
		vec3 radiance     = directLight[0].color;     
		float NDF = DistributionGGX(N, H, roughness);        
		float G   = GeometrySmith(N, V, L, roughness);      
		vec3 F    = fresnelSchlick(max(dot(H, V), 0.0), F0);   
		vec3 kS = F;
		vec3 kD = vec3(1.0) - kS;
		kD *= 1.0 - metallic;	  
		vec3 numerator    = NDF * G * F;
		float NdotL = max(dot(N, L), 0.0);    
		float denominator = 4.0 * max(dot(N, V), 0.0) * NdotL;
		vec3 specular     = numerator / max(denominator, 0.001);  

		vec3 Lo = (kD * albedo / PI + specular) * radiance * NdotL; 
		vec3 ambient = vec3(0.03) * albedo * ao;
		vec3 color = ambient + Lo;
		
		vec3 lightMapColorTest = vec3(1.0, 1.0, 1.0);
		if(useLightMap) {
			lightMapColorTest = texture(lightMap, vec2(0.05, lightMapCoords.y)).xyz;
		}
	   
		outputColor = vec4(diffuseColor.rgb * lightMapColor + color * lightMapColorTest + em, diffuseColor.w);
	} else {
		outputColor =  vec4(diffuseColor.rgb * lightMapColor, diffuseColor.w);
	}
}