//Vertex Shader specially written for ShadowS Project
//Copyright © 2019-2020 by Krogenit
#version 120

varying vec2 texCoords;
varying vec4 color;

void main() 
{	
	color = gl_Color;
	texCoords = gl_MultiTexCoord0.xy;
	gl_Position = gl_ProjectionMatrix * gl_ModelViewMatrix * gl_Vertex;
}