package ru.xlv.post.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.common.network.IPacketOutServer;
import ru.xlv.post.common.PostIO;
import ru.xlv.post.common.PostObject;

import java.io.IOException;

@NoArgsConstructor
@AllArgsConstructor
public class PacketPostNew implements IPacketOutServer {

    private PostObject postObject;

    @Override
    public void write(EntityPlayer entityPlayer, ByteBufOutputStream bbos) throws IOException {
        PostIO.writePostObject(postObject, bbos);
    }
}
