package ru.xlv.post.network;

import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.common.network.IPacketInOnServer;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.post.XlvsPostMod;

import java.io.IOException;

@NoArgsConstructor
public class PacketPostMarkViewed implements IPacketInOnServer {
    @Override
    public void read(EntityPlayer entityPlayer, ByteBufInputStream bbis) throws IOException {
        String uuid = bbis.readUTF();
        XlvsPostMod.INSTANCE.getPostHandler().markPostViewed(entityPlayer.getCommandSenderName(), uuid);
    }
}
