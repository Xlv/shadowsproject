package ru.xlv.post.network;

import io.netty.buffer.ByteBufOutputStream;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.common.network.IPacketCallbackOnServer;
import ru.xlv.core.common.network.PacketCallbackSender;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.post.XlvsPostMod;
import ru.xlv.post.handle.result.PostTakeAttachmentsResult;

import java.io.IOException;

public class PacketPostTakeAttachment implements IPacketCallbackOnServer {

    private PostTakeAttachmentsResult postTakeAttachmentsResult;

    @Override
    public void read(EntityPlayer entityPlayer, ByteBufInputStream bbis, PacketCallbackSender packetCallbackSender) throws IOException {
        String uuid = bbis.readUTF();
        XlvsPostMod.INSTANCE.getPostHandler().takePostAttachments(entityPlayer, uuid).thenAccept(postTakeAttachmentsResult1 -> {
            postTakeAttachmentsResult = postTakeAttachmentsResult1;
            packetCallbackSender.send();
        });
    }

    @Override
    public void write(EntityPlayer entityPlayer, ByteBufOutputStream bbos) throws IOException {
        bbos.writeUTF(postTakeAttachmentsResult.getResponseMessage());
    }

    @Override
    public boolean handleCallback() {
        return true;
    }
}
