package ru.xlv.post.handle.result;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ru.xlv.post.XlvsPostMod;

@Getter
@RequiredArgsConstructor
public enum PostSendResult {

    SUCCESS(XlvsPostMod.INSTANCE.getLocalization().getResponseSendResultSuccessMessage()),
    UNKNOWN(XlvsPostMod.INSTANCE.getLocalization().getResponseSendResultUnknownErrorMessage()),
    DATABASE_ERROR(XlvsPostMod.INSTANCE.getLocalization().getResponseSendResultDatabaseErrorMessage());

    private final String responseMessage;
}
