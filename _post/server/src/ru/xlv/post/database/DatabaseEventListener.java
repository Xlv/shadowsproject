package ru.xlv.post.database;

import ru.xlv.core.database.IDatabaseEventListener;
import ru.xlv.core.util.Maps;
import ru.xlv.post.XlvsPostMod;
import ru.xlv.post.common.PostObject;

public class DatabaseEventListener implements IDatabaseEventListener<PostObjectModel> {
    @Override
    public void onInsert(PostObjectModel object) {
        Maps.addElemToMappedList(XlvsPostMod.INSTANCE.getPostHandler().getPostObjectMap(), object.getPostObject().getSender(), object.getPostObject());
        Maps.addElemToMappedList(XlvsPostMod.INSTANCE.getPostHandler().getPostObjectMap(), object.getPostObject().getRecipient(), object.getPostObject());
    }

    @Override
    public void onDelete(PostObjectModel object) {
        PostObject postObject1 = XlvsPostMod.INSTANCE.getPostHandler().getPostObject(object.getOwnerName(), object.getPostObject().getUuid().toString());
        if (postObject1 != null) {
            Maps.removeElemFromMappedList(XlvsPostMod.INSTANCE.getPostHandler().getPostObjectMap(), object.getOwnerName(), postObject1);
        }
    }

    @Override
    public void onUpdate(PostObjectModel object) {}
}
