package ru.xlv.post.database;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.changestream.ChangeStreamDocument;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import org.bson.Document;
import ru.xlv.core.database.IDatabaseEventListener;
import ru.xlv.core.database.mongodb.ConfigMongoDB;
import ru.xlv.core.database.mongodb.MongoProvider;
import ru.xlv.core.util.IConverter;
import ru.xlv.core.util.Maps;
import ru.xlv.post.common.PostAttachmentCredits;
import ru.xlv.post.common.PostAttachmentItemStack;
import ru.xlv.post.common.PostObject;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

public class DatabaseManager {

    private static final String POST_SENDER_KEY = "sender";
    private static final String POST_RECIPIENT_KEY = "recipient";
    private static final String POST_ARTICLE_KEY = "article";
    private static final String POST_TEXT_KEY = "text";
    private static final String POST_ATTACHMENTS_ITEM_STACK_KEY = "attachments_item_stack";
    private static final String POST_ATTACHMENTS_CURRENCY_KEY = "attachments_currency";
    private static final String POST_UUID_KEY = "uuid";
    private static final String POST_OWNER_KEY = "owner";
    private static final String POST_CREATION_TIME_KEY = "creationTime";

    private final IConverter<PostObjectModel, Document> postConverter = new IConverter<PostObjectModel, Document>() {
        @Override
        public Document convertTo(PostObjectModel postObject) {
            List<PostAttachmentItemStack> attachmentItemStacks = new ArrayList<>();
            List<PostAttachmentCredits> attachmentCurrencies = new ArrayList<>();
            postObject.getPostObject().getAttachments().forEach(postAttachment -> {
                if(postAttachment instanceof PostAttachmentItemStack) {
                    attachmentItemStacks.add((PostAttachmentItemStack) postAttachment);
                } else if(postAttachment instanceof PostAttachmentCredits) {
                    attachmentCurrencies.add((PostAttachmentCredits) postAttachment);
                }
            });
            return new Document()
                    .append(POST_SENDER_KEY, postObject.getPostObject().getSender())
                    .append(POST_RECIPIENT_KEY, postObject.getPostObject().getRecipient())
                    .append(POST_ARTICLE_KEY, postObject.getPostObject().getTitle())
                    .append(POST_TEXT_KEY, postObject.getPostObject().getText())
                    .append(POST_ATTACHMENTS_ITEM_STACK_KEY, attachmentItemStacks)
                    .append(POST_ATTACHMENTS_CURRENCY_KEY, attachmentCurrencies)
                    .append(POST_OWNER_KEY, postObject.getOwnerName())
                    .append(POST_CREATION_TIME_KEY, postObject.getPostObject().getCreationTimeMills())
                    .append(POST_UUID_KEY, postObject.getPostObject().getUuid().toString());
        }

        @Override
        public PostObjectModel convertFrom(Document document) {
            String uuid = document.getString(POST_UUID_KEY);
            String article = document.getString(POST_ARTICLE_KEY);
            String text = document.getString(POST_TEXT_KEY);
            String sender = document.getString(POST_SENDER_KEY);
            String recipient = document.getString(POST_RECIPIENT_KEY);
            PostObject postObject = new PostObject(UUID.fromString(uuid), article, text, sender, recipient);
            postObject.setCreationTimeMills(document.getLong(POST_CREATION_TIME_KEY));
            List<PostAttachmentItemStack> attachments = document.getList(POST_ATTACHMENTS_ITEM_STACK_KEY, PostAttachmentItemStack.class);
            if (attachments != null) {
                postObject.getAttachments().addAll(attachments);
            }
            List<PostAttachmentCredits> attachments1 = document.getList(POST_ATTACHMENTS_CURRENCY_KEY, PostAttachmentCredits.class);
            if (attachments1 != null) {
                postObject.getAttachments().addAll(attachments1);
            }
            return new PostObjectModel(document.getString(POST_OWNER_KEY), postObject);
        }
    };

    private ExecutorService executorService;

    private MongoProvider mongoProvider;
    private MongoCollection<Document> postCollection;

    public void init(IDatabaseEventListener<PostObjectModel> databaseEventListener) {
        executorService = Executors.newFixedThreadPool(16);
        mongoProvider = new MongoProvider(new ConfigMongoDB("config/post/database.json"));
        mongoProvider.init();
        postCollection = mongoProvider.getDatabase().getCollection("post");
        executorService.submit(() -> postCollection.watch(
                Collections.singletonList(
                        Aggregates.match(
                                Filters.in("operationType", Arrays.asList("insert", "update", "replace", "delete"))
                        )
                )
        ).forEach((Consumer<? super ChangeStreamDocument<Document>>) documentChangeStreamDocument -> {
            switch (documentChangeStreamDocument.getOperationType()) {
                case UPDATE:
                    databaseEventListener.onUpdate(postConverter.convertFrom(documentChangeStreamDocument.getFullDocument()));
                    break;
                case REPLACE:
                case DELETE:
                    databaseEventListener.onDelete(postConverter.convertFrom(documentChangeStreamDocument.getFullDocument()));
                    break;
                case INSERT:
                    databaseEventListener.onInsert(postConverter.convertFrom(documentChangeStreamDocument.getFullDocument()));
            }
        }));
    }

    public void shutdown() {
        mongoProvider.shutdown();
        executorService.shutdown();
    }

    public void sendPostObjectSync(PostObject postObject) {
        postCollection.insertOne(postConverter.convertTo(new PostObjectModel(postObject.getSender(), postObject)));
        postCollection.insertOne(postConverter.convertTo(new PostObjectModel(postObject.getRecipient(), postObject)));
    }

    public CompletableFuture<Boolean> updatePostObject(String username, PostObject postObject) {
        return CompletableFuture.supplyAsync(() -> {
            UpdateResult updateResult = postCollection.updateOne(Filters.eq(POST_UUID_KEY, postObject.getUuid().toString()), new Document("$set", postConverter.convertTo(new PostObjectModel(username, postObject))));
            return updateResult.getModifiedCount() > 0;
        }, executorService);
    }

    public CompletableFuture<Boolean> removePostObject(String username, PostObject postObject) {
        return CompletableFuture.supplyAsync(() -> {
            DeleteResult deleteResult = postCollection.deleteOne(Filters.and(Filters.eq(POST_UUID_KEY, postObject), Filters.eq(POST_OWNER_KEY, username)));
            return deleteResult.getDeletedCount() > 0;
        });
    }

//    public CompletableFuture<List<PostObject>> getAllPost(String username) {
//        return CompletableFuture.supplyAsync(() -> {
//            List<PostObject> auctionLots = new ArrayList<>();
//            postCollection.find(
//                    Filters.or(
//                            Filters.eq(POST_SENDER_KEY, username),
//                            Filters.eq(POST_RECIPIENT_KEY, username)
//                    )
//            ).forEach((Consumer<? super Document>) document -> auctionLots.add(postConverter.convertFrom(document)));
//            return auctionLots;
//        }, executorService);
//    }

    public CompletableFuture<Map<String, List<PostObject>>> getAllPost() {
        return CompletableFuture.supplyAsync(this::getAllPostSync, executorService);
    }

    public Map<String, List<PostObject>> getAllPostSync() {
        Map<String, List<PostObject>> map = new HashMap<>();
        postCollection.find().forEach((Consumer<? super Document>) document -> {
            PostObjectModel postObjectModel = postConverter.convertFrom(document);
            Maps.addElemToMappedList(map, postObjectModel.getOwnerName(), postObjectModel.getPostObject());
        });
        return map;
    }
}
