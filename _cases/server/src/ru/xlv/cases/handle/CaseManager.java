package ru.xlv.cases.handle;

import lombok.Getter;
import ru.xlv.cases.util.ConfigCases;
import ru.xlv.core.item.ItemStackFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@Getter
public class CaseManager {

    private final List<Case> caseList = new ArrayList<>();

    public CaseManager(ConfigCases config) {
        caseList.addAll(config.getCases()
                .stream()
                .map(caseModel -> new Case(
                        caseModel.getName(),
                        caseModel.getPrice(),
                        caseModel.getImageUrl(),
                        caseModel.getItems()
                                .stream()
                                .map(caseItemModel -> new CaseItem(ItemStackFactory.create(caseItemModel.getUnlocalizedName(), caseItemModel.getAmount(), caseItemModel.getMetadata(), caseItemModel.getJsonNbtTagCompound()), caseItemModel.getChance()))
                                .collect(Collectors.toList())
                        )
                )
                .collect(Collectors.toList())
        );
    }

    CaseItem getRandomItemFromCase(Case aCase) {
        List<CaseItem> itemsFin = new ArrayList<>();
        aCase.getItems().forEach(caseItem -> {
            for (int i = 0; i < caseItem.getChance(); i++) {
                itemsFin.add(caseItem);
            }
        });
        Collections.shuffle(itemsFin);
        return itemsFin.get(randInt(0, itemsFin.size() - 1));
    }

    public static int randInt(int min, int max) {
        return new Random().nextInt((max - min) + 1) + min;
    }
}
