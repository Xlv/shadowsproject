package ru.xlv.cases.handle;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import ru.xlv.cases.handle.result.CaseRollResult;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.inventory.result.AddItemResult;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.post.XlvsPostMod;
import ru.xlv.post.common.PostAttachmentItemStack;
import ru.xlv.post.common.PostObject;
import ru.xlv.post.util.PostFactory;

@Getter
@RequiredArgsConstructor
public class CaseHandler {

    private final CaseManager caseManager;

    public CaseRollResult roll(EntityPlayer entityPlayer, int caseIndex) {
        if(caseIndex < 0 || caseIndex >= caseManager.getCaseList().size()) return null;
        ServerPlayer serverPlayer = XlvsCore.INSTANCE.getPlayerManager().getPlayer(entityPlayer);
        Case caseById = caseManager.getCaseList().get(caseIndex);
        if (serverPlayer.getSelectedCharacter().getWallet().consumeCredits(caseById.getPrice())) {
            CaseItem caseItem = caseManager.getRandomItemFromCase(caseById);
            ItemStack itemStack = caseItem.getItemStack().copy();
            AddItemResult addItemResult = serverPlayer.getSelectedCharacter().getMatrixInventory().addItem(itemStack);
            if(addItemResult != AddItemResult.SUCCESS) {
                PostObject postObject = PostFactory.createAdminPostObject("LA la la lala la la la tancuyt zvezdy i luna", serverPlayer.getPlayerName());
                postObject.getAttachments().add(new PostAttachmentItemStack(itemStack));
                XlvsPostMod.INSTANCE.getPostHandler().sendPost(postObject);
            }
            return CaseRollResult.builder().type(CaseRollResult.Type.SUCCESS).params(new Object[] {itemStack}).build();
        } else {
            return CaseRollResult.builder().type(CaseRollResult.Type.NOT_ENOUGH_MONEY).params(new Object[] {serverPlayer.getSelectedCharacter().getWallet().getCredits()}).build();
        }
    }
}
