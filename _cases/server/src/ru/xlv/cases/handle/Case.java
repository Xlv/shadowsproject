package ru.xlv.cases.handle;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.List;

@Getter
@RequiredArgsConstructor
public class Case {

    private final String name;
    private final int price;
    private final String imageUrl;
    private final List<CaseItem> items;
}
