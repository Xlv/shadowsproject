package ru.xlv.cases.pojo;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Getter
@RequiredArgsConstructor
public class Case {

    private final String name;
    private final int price;
    private final String texture;
    private final List<CaseItem> items = new ArrayList<>();
}



