package ru.xlv.cases;

import cpw.mods.fml.client.registry.ClientRegistry;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.relauncher.Side;
import net.minecraft.client.settings.KeyBinding;
import org.lwjgl.input.Keyboard;
import ru.xlv.cases.events.KeyHandler;
import ru.xlv.cases.network.*;
import ru.xlv.core.XlvsCore;

@Mod(
        modid = XlvsCasesMod.MODID,
        version = "1.0",
        name = XlvsCasesMod.MODID
)
public class XlvsCasesMod {

    public static final String MODID = "xlvscases";

    public static KeyBinding KeyTest;

    @Mod.Instance(MODID)
    public static XlvsCasesMod INSTANCE;

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        NetworkRegistry.INSTANCE.newSimpleChannel("CasesShopChanel").registerMessage(CasesMainPacket.Handler.class, CasesMainPacket.class, 0, Side.CLIENT);
        NetworkRegistry.INSTANCE.newSimpleChannel("CasesListChanel").registerMessage(CasesListPacket.Handler.class, CasesListPacket.class, 0, Side.CLIENT);
        NetworkRegistry.INSTANCE.newSimpleChannel("CasesCurChanel").registerMessage(CasesViewPacket.Handler.class, CasesViewPacket.class, 0, Side.CLIENT);
        FMLCommonHandler.instance().bus().register(new KeyHandler());
        XlvsCore.INSTANCE.getPacketHandler().getPacketRegistry().register(MODID,
                new PacketCaseRoll(),
                new PacketCaseListSync()
        );
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {
        KeyTest = new KeyBinding("key.friendsgui", Keyboard.KEY_Y, "key.categories.oshop");
        ClientRegistry.registerKeyBinding(KeyTest);
    }

}
