package ru.xlv.core.util;

import lombok.experimental.UtilityClass;
import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;

import javax.annotation.Nonnull;

@UtilityClass
public class SoundUtils {

    public void playGuiSound(@Nonnull String key) {
        playGuiSound(key, 1.0F);
    }

    public void playGuiSound(@Nonnull String key, float f) {
        Minecraft.getMinecraft().getSoundHandler().playSound(PositionedSoundRecord.func_147674_a(new ResourceLocation(key), f));
    }

    public void playGuiSound(@Nonnull SoundType soundType) {
        playGuiSound(soundType, 1.0F);
    }

    public void playGuiSound(@Nonnull SoundType soundType, float f) {
        Minecraft.getMinecraft().getSoundHandler().playSound(PositionedSoundRecord.func_147674_a(new ResourceLocation(soundType.getSoundKey()), f));
    }

    public void playSound(@Nonnull SoundType soundType) {
        playSound(Minecraft.getMinecraft().thePlayer, soundType);
    }

    public void playSound(@Nonnull Entity entity, @Nonnull SoundType soundType) {
        playSound(entity, soundType.getSoundKey());
    }

    public void playSound(@Nonnull World world, double x, double y, double z, @Nonnull SoundType soundType) {
        playSound(world, x, y, z, soundType.getSoundKey());
    }

    public void playSound(@Nonnull World world, double x, double y, double z, @Nonnull SoundType soundType, float f, float f1, boolean b) {
        playSound(world, x, y, z, soundType.getSoundKey(), f, f1, b);
    }

    public void playSound(@Nonnull String key) {
        playSound(Minecraft.getMinecraft().thePlayer, key);
    }

    public void playSound(@Nonnull Entity entity, @Nonnull String key) {
        playSound(entity.worldObj, entity.posX, entity.posY, entity.posZ, key);
    }

    public void playSound(@Nonnull World world, double x, double y, double z, @Nonnull String key) {
        playSound(world, x, y, z, key, 1.0F, 1.0F, false);
    }

    public void playSound(@Nonnull World world, double x, double y, double z, @Nonnull String key, float f, float f1, boolean b) {
        if (!key.contains(":")) {
            key = SoundType.DEFAULT_DOMAIN + key;
        }
        world.playSound(x, y, z, key, f, f1, b);
    }
}
