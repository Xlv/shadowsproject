package ru.xlv.core.resource;

import lombok.SneakyThrows;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.texture.TextureManager;
import ru.krogenit.png_loader.PNGTextureLoader;
import ru.krogenit.utils.Utils;
import ru.xlv.core.renderer.texture.Texture;

import javax.imageio.ImageIO;

import static org.lwjgl.opengl.GL13.GL_CLAMP_TO_BORDER;

public class ResourceLoaderPNG extends ResourceLoader<PNGTextureLoader.ImageSimpleData> {

    private static final TextureManager textureManager = Minecraft.getMinecraft().getTextureManager();

    public ResourceLoaderPNG() {
        super("png");
    }

    @SneakyThrows
    @Override
    public PNGTextureLoader.ImageSimpleData loadAsync(ResourceLocationStateful resourceLocation) {
        return PNGTextureLoader.loadImageData(ImageIO.read(Utils.getInputStreamFromZip(resourceLocation.getResourceLocation())), GL_CLAMP_TO_BORDER);
    }

    @Override
    public void loadSync0(ResourceLocationStateful resourceLocationStateful, PNGTextureLoader.ImageSimpleData imageSimpleData) {
        textureManager.loadTexture(resourceLocationStateful.getResourceLocation(), new Texture(imageSimpleData));
    }

    @Override
    public boolean isLoaded(ResourceLocationStateful resourceLocationStateful) {
        return textureManager.getTexture(resourceLocationStateful.getResourceLocation()) != null;
    }

    @Override
    public void deleteResource(ResourceLocationStateful resourceLocationStateful) {
        textureManager.deleteTexture(resourceLocationStateful.getResourceLocation());
    }

    @Override
    public void deleteAll() {
        throw new UnsupportedOperationException();
    }
}
