package ru.krogenit.client.gui.item;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class WeaponStat {
    private final String value, description;
}

