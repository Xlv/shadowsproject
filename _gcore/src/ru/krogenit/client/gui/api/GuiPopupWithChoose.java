package ru.krogenit.client.gui.api;

import net.minecraft.client.gui.GuiButton;
import org.lwjgl.util.vector.Vector3f;

public class GuiPopupWithChoose extends GuiPopup {

    private final String buttonText1, buttonText2;

    public GuiPopupWithChoose(IGuiWithPopup guiWithPopup, String header, String info, Vector3f color, String buttonText1, String buttonText2, float x, float y) {
        super(guiWithPopup, header, info, color, x, y);
        this.buttonText1 = buttonText1;
        this.buttonText2 = buttonText2;
        initGui();
    }

    @Override
    protected void createButtons() {
        float buttonWidth = 171;
        float buttonHeight = 39;
        float x = this.x;
        float y = -24 + this.y;
        GuiButtonAnimated button = new GuiButtonAnimated(0, ScaleGui.getCenterX(x, buttonWidth),  ScaleGui.getCenterY(y, buttonHeight), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), buttonText1);
        button.setTexture(textureTreeButtonAccept);
        button.setTextureHover(textureTreeButtonHover);
        button.setMaskTexture(textureTreeButtonMask);
        buttonList.add(button);

        y += buttonHeight * 1.35f;
        button = new GuiButtonAnimated(1, ScaleGui.getCenterX(x, buttonWidth),  ScaleGui.getCenterY(y, buttonHeight), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), buttonText2);
        button.setTexture(textureTreeButtonCancel);
        button.setTextureHover(textureTreeButtonHover);
        button.setMaskTexture(textureTreeButtonMask);
        buttonList.add(button);
    }

    @Override
    protected void actionPerformed(GuiButton guiButton) {
        guiWithPopup.popupAction(guiButton);
    }
}
