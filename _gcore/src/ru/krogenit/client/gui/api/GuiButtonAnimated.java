package ru.krogenit.client.gui.api;

import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector3f;
import ru.krogenit.utils.AnimationHelper;
import ru.xlv.core.util.SoundType;
import ru.xlv.core.util.SoundUtils;
import ru.xlv.customfont.FontType;

public class GuiButtonAnimated extends GuiButtonAdvanced {

    private float animation;
    protected float blending;
    protected ResourceLocation maskTexture;
    private final Vector3f maskColor = new Vector3f(0.2196f, 0.7686f, 0.8901f);

    public GuiButtonAnimated(int buttonId, float x, float y, float widthIn, float heightIn, String buttonText) {
        super(buttonId, x, y, widthIn, heightIn, buttonText);
    }

    public void setMaskColor(float r, float g, float b) {
        this.maskColor.x = r;
        this.maskColor.y = g;
        this.maskColor.z = b;
    }

    public void drawButtonEffect(int mouseX, int mouseY) {
        if(visible) {
            if (enabled) {
                boolean hovered = isHovered(mouseX, mouseY);
                if(hovered && !this.hovered) {
                    SoundUtils.playGuiSound(SoundType.BUTTON_HOVER);
                }
                this.hovered = hovered;
                if (hovered || animation > 0) {
                    if(hovered) {
                        animation += AnimationHelper.getAnimationSpeed() * 0.04f;
                        if (animation > 1f) {
                            animation = 1f;
                        }
                    } else {
                        animation -= AnimationHelper.getAnimationSpeed() * 0.04f;
                        if(animation < 0)
                            animation = 0;
                    }

                    float off = height / 8f;
                    float x = xPosition - off;
                    float y = yPosition - off;
                    float width = this.width + off * 2;
                    float height = this.height + off * 2;
                    GuiDrawUtils.clearMaskBuffer(0, 0, ScaleGui.screenWidth, ScaleGui.screenHeight);
                    GL11.glColor4f(maskColor.x ,maskColor.y, maskColor.z, 0.6f * animation * 2f);
                    GuiDrawUtils.drawMaskingButtonEffect(maskTexture, x, y, width, height, x - 4, y - 8, width + 8, height * 1.35f + 1, 90 + animation * 90f);
                    GL11.glColor4f(1f ,1f, 1f, 1f);
                }
            }
        }
    }

    @Override
    public void drawButton(int mouseX, int mouseY) {
        if(visible) {
            boolean hovered = isHovered(mouseX, mouseY);
            if (enabled) {
                drawButtonEffect(mouseX, mouseY);
                if (hovered) {
                    blending += AnimationHelper.getAnimationSpeed() * 0.1f;
                    if (blending > 1) blending = 1;
                } else {
                    blending -= AnimationHelper.getAnimationSpeed() * 0.1f;
                    if (blending < 0) blending = 0f;
                }

                GuiDrawUtils.drawRect(texture, this.xPosition, this.yPosition, this.width, this.height, 1f, 1f, 1f, 1f - blending);
                if (textureHover != null) GuiDrawUtils.drawRect(textureHover, this.xPosition, this.yPosition, this.width, this.height, 1f, 1f, 1f, blending);
                drawText(mc, 0, 0);
            } else {
                if(animation > 0) {
                    animation -= AnimationHelper.getAnimationSpeed() * 0.04f;
                    if(animation < 0)
                        animation = 0;
                }
                if(hovered) GuiDrawUtils.drawRect(texture, this.xPosition, this.yPosition, this.width, this.height, 0.25f, 0.25f, 0.25f, 1f);
                else GuiDrawUtils.drawRect(texture, this.xPosition, this.yPosition, this.width, this.height, 0f, 0f, 0f, 1f);
                drawText(mc, 0, 0);
            }
        }
    }

    protected void drawText(Minecraft mc, int xOffset, int yOffset) {
        int j = 14737632;
        if (packedFGColour != 0) {
            j = packedFGColour;
        } else if (!this.enabled) {
            j = 10526880;
        }

        float offsetY = ScaleGui.get(3);
        float textScale = 0.833f;
        float scaleAnim = width / widthBase;

        GuiDrawUtils.drawCenteredStringNoXYScale(FontType.HelveticaNeueCyrBold, displayString, this.xPosition + this.width / 2.0f + xOffset,
                this.yPosition + this.height / 2.0f + yOffset - offsetY, textScale * scaleAnim, j);
    }

    public void setMaskTexture(ResourceLocation maskTexture) {
        this.maskTexture = maskTexture;
    }
}
