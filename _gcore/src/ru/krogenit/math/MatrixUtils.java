package ru.krogenit.math;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Quaternion;
import org.lwjgl.util.vector.Vector3f;

public class MatrixUtils {

    private final static float DEG2RAD = (float) (Math.PI / 180f);
    private final static Vector3f left = new Vector3f();
    private final static Vector3f up = new Vector3f();
    private final static Vector3f forward = new Vector3f();

    public static Matrix4f toRotationMatrix(Quaternion q, Matrix4f dest)
    {
        if (dest == null) dest = new Matrix4f();

        // Normalize the quaternion
        q.normalise(q);

        // The length of the quaternion
        float s = 2f / q.length();

        // Convert the quaternion to matrix
        dest.m00 = 1f - s * (q.y * q.y + q.z * q.z);
        dest.m10 = s * (q.x * q.y + q.w * q.z);
        dest.m20 = s * (q.x * q.z - q.w * q.y);

        dest.m01 = s * (q.x * q.y - q.w * q.z);
        dest.m11 = 1f - s * (q.x * q.x + q.z * q.z);
        dest.m21 = s * (q.y * q.z + q.w * q.x);

        dest.m02 = s * (q.x * q.z + q.w * q.y);
        dest.m12 = s * (q.y * q.z - q.w * q.x);
        dest.m22 = 1f - s * (q.x * q.x + q.y * q.y);

        return dest;
    }

    public static Matrix4f angleToMatrix(Vector3f angle, Matrix4f dest) {
        float sx, sy, sz, cx, cy, cz, theta;

        // rotation angle about X-axis (pitch)
        theta = angle.x;
        sx = (float) Math.sin(theta);
        cx = (float) Math.cos(theta);

        // rotation angle about Y-axis (yaw)
        theta = angle.y;
        sy = (float) Math.sin(theta);
        cy = (float) Math.cos(theta);

        // rotation angle about Z-axis (roll)
        theta = angle.z;
        sz = (float) Math.sin(theta);
        cz = (float) Math.cos(theta);

        // determine left axis
        left.x = cy * cz;
        left.y = sx * sy * cz + cx * sz;
        left.z = -cx * sy * cz + sx * sz;

        // determine up axis
        up.x = -cy * sz;
        up.y = -sx * sy * sz + cx * cz;
        up.z = cx * sy * sz + sx * cz;

        // determine forward axis
        forward.x = sy;
        forward.y = -sx * cy;
        forward.z = cx * cy;

        // construct rotation matrix
        if (dest == null) dest = new Matrix4f();

        dest.m00 = left.x;
        dest.m01 = left.y;
        dest.m02 = left.z;
        dest.m10 = up.x;
        dest.m11 = up.y;
        dest.m12 = up.z;
        dest.m20 = forward.x;
        dest.m21 = forward.y;
        dest.m22 = forward.z;

        return dest;
    }
}
