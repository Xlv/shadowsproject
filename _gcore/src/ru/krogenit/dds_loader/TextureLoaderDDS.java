package ru.krogenit.dds_loader;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.texture.ITextureObject;
import net.minecraft.client.renderer.texture.SimpleTexture;
import net.minecraft.client.renderer.texture.TextureUtil;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.EXTTextureFilterAnisotropic;
import org.lwjgl.opengl.GLContext;
import ru.krogenit.shaders.pbr.IPBR;
import ru.krogenit.utils.Utils;
import ru.xlv.core.resource.ResourceLoadingState;
import ru.xlv.core.resource.ResourceLocationStateful;
import ru.xlv.core.resource.ResourceManager;
import ru.xlv.core.util.Flex;

import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL12.*;
import static org.lwjgl.opengl.GL13.*;
import static org.lwjgl.opengl.GL14.GL_TEXTURE_LOD_BIAS;

public class TextureLoaderDDS {

	private static final Map<ResourceLocation, ITextureObject> LOADED_TEXTURES = new HashMap<>();
	private static final Map<ResourceLocationStateful, ITextureObject> LOADED_TEXTURES_STATEFUL = new HashMap<>();

	private static int DUMMY_TEXTURE_ID;

	public static void bindTexture(TextureDDS textureDDS) {
		if(canBindTexture(textureDDS)) {
			bindTexture(textureDDS.getTextureId(), 0);
		}
	}

	public static void bindNormalMap(TextureDDS textureDDS, IPBR shader) {
		if(canBindTexture(textureDDS)) {
			shader.setNormalMapping(true);
			bindTexture(textureDDS.getTextureId(), GL_TEXTURE2);
		}
	}

	public static void bindSpecularMap(TextureDDS textureDDS, IPBR shader) {
		if(canBindTexture(textureDDS)) {
			shader.setSpecularMapping(true);
			bindTexture(textureDDS.getTextureId(), GL_TEXTURE3);
		}
	}

	public static void bindEmissionMap(TextureDDS textureDDS, IPBR shader, float power) {
		if(canBindTexture(textureDDS)) {
			shader.setEmissionMapping(true);
			shader.setEmissionPower(power);
			bindTexture(textureDDS.getTextureId(), GL_TEXTURE4);
		}
	}

	public static void bindGlossMap(TextureDDS textureDDS, IPBR shader) {
		if(canBindTexture(textureDDS)) {
			shader.setGlossMapping(true);
			bindTexture(textureDDS.getTextureId(), GL_TEXTURE5);
		}
	}

	public static void bindTexture(ResourceLocation loc) {
		bindTexture0(loc, 0);
	}

	public static void bindNormalMap(ResourceLocation loc) {
		bindTexture0(loc, GL_TEXTURE2);
	}

	public static void bindSpecularMap(ResourceLocation loc) {
		bindTexture0(loc, GL_TEXTURE3);
	}

	public static void bindEmissionMap(ResourceLocation loc) {
		bindTexture0(loc, GL_TEXTURE4);
	}

	public static void bindGlossMap(ResourceLocation loc) {
		bindTexture0(loc, GL_TEXTURE5);
	}

	private static void bindTexture(int id, int texture) {
		if (texture != 0) glActiveTexture(texture);
		glBindTexture(GL_TEXTURE_2D, id);
	}

	private static void bindTexture0(ResourceLocation resourceLocation, int texture) {
		ITextureObject textureObject = LOADED_TEXTURES.get(resourceLocation);
		if(textureObject == null) {
			textureObject = new SimpleTexture(resourceLocation);
			loadTexture(resourceLocation, textureObject);
		} else {
			if(texture != 0) {
				glActiveTexture(texture);
			}
			glBindTexture(GL_TEXTURE_2D, textureObject.getGlTextureId());
		}
	}

	public static boolean deleteTexture(ResourceLocation resourceLocation) {
		ITextureObject remove = LOADED_TEXTURES.remove(resourceLocation);
		if (remove != null) {
			glDeleteTextures(remove.getGlTextureId());
			return true;
		} else {
			return Flex.removeIf(LOADED_TEXTURES_STATEFUL,
					resourceLocationITextureObjectEntry -> resourceLocationITextureObjectEntry.getKey().getResourceLocation().equals(resourceLocation),
					resourceLocationITextureObjectEntry -> {
						glDeleteTextures(resourceLocationITextureObjectEntry.getValue().getGlTextureId());
						resourceLocationITextureObjectEntry.getKey().setLoadingState(ResourceLoadingState.WAIT);
					});
		}
	}

	public static void deleteAllTextures() {
		Flex.removeAll(LOADED_TEXTURES, resourceLocationITextureObjectEntry -> glDeleteTextures(resourceLocationITextureObjectEntry.getValue().getGlTextureId()));
		Flex.removeAll(LOADED_TEXTURES_STATEFUL, resourceLocationITextureObjectEntry -> {
			glDeleteTextures(resourceLocationITextureObjectEntry.getValue().getGlTextureId());
			resourceLocationITextureObjectEntry.getKey().setLoadingState(ResourceLoadingState.WAIT);
		});
	}

	public static void unbind() {
		glActiveTexture(GL_TEXTURE0);
	}

	public static void loadTexture(ResourceLocation resourceLocation, ITextureObject textureObject) {
		loadTexture(loadDDSFile(resourceLocation), resourceLocation, textureObject);
	}

	public static DDSFile loadDDSFile(ResourceLocation resourceLocation) {
		DDSFile ddsFile = new DDSFile();
		ddsFile.loadFile(Utils.getInputStreamFromZip(resourceLocation));
		return ddsFile;
	}

	public static void loadTexture(DDSFile ddsFile, ResourceLocation resourceLocation, ITextureObject textureObject) {
		try {
			createTexture(ddsFile, textureObject.getGlTextureId());
			LOADED_TEXTURES.put(resourceLocation, textureObject);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Не удалось загрузить текстуру " + resourceLocation.toString());
		}
	}

	public static void loadTexture(DDSFile ddsFile, ResourceLocationStateful resourceLocationStateful, ITextureObject textureObject) {
		try {
			createTexture(ddsFile, textureObject.getGlTextureId());
			LOADED_TEXTURES_STATEFUL.put(resourceLocationStateful, textureObject);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Не удалось загрузить текстуру " + resourceLocationStateful.toString());
		}
	}

	public static ITextureObject getTexture(ResourceLocation resourceLocation) {
		return LOADED_TEXTURES.get(resourceLocation);
	}

	public static void createTexture(DDSFile dds, int id) {
		glBindTexture(GL_TEXTURE_2D, id);
		glCompressedTexImage2D(GL_TEXTURE_2D, 0, dds.getDXTFormat(), dds.getWidth(), dds.getHeight(), 0, dds.getBuffer());
		int mipMapCount = dds.getMipMapCount() - 1;
		int textureMagFilter = GL_LINEAR;
		int textureMinFilter = (mipMapCount > 0) ? GL_LINEAR_MIPMAP_LINEAR : GL_LINEAR;
		if(mipMapCount > 0) {
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, mipMapCount);
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_LOD, 0.0F);
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_LOD, (float)mipMapCount);
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_LOD_BIAS, 0.0F);

			int width = dds.getWidth();
			int height = dds.getHeight();
			for(int i=0;i<mipMapCount;i++) {
				width /= 2;
				height /= 2;
				ByteBuffer mipmapBuffer = dds.getMipMapLevel(i);
				glCompressedTexImage2D(GL_TEXTURE_2D, i + 1, dds.getDXTFormat(), width, height, 0, mipmapBuffer);
			}
		}

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, textureMagFilter);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, textureMinFilter);
		if(GLContext.getCapabilities().GL_EXT_texture_filter_anisotropic) {
			float amount = Minecraft.getMinecraft().gameSettings.anisotropicFiltering;
			glTexParameterf(GL_TEXTURE_2D, EXTTextureFilterAnisotropic.GL_TEXTURE_MAX_ANISOTROPY_EXT, amount);
		}

		dds.clear();
	}

	private static void createDummyTexture() {
		int width = 4;
		int height = 4;
		int color1 = 0x777777;
		int[] colors1 = new int[] { -color1, -color1, -color1, -color1};
		int[] textureData = new int[width * height * 3];
		TextureUtil.allocateTexture(DUMMY_TEXTURE_ID = glGenTextures(), width, height);
		for (int i = 0; i < width; ++i) {
			System.arraycopy(colors1, 0, textureData, height * i, colors1.length);
		}
		TextureUtil.uploadTexture(DUMMY_TEXTURE_ID, textureData, width, height);
	}

	private static boolean canBindTexture(TextureDDS textureDDS) {
		textureDDS.resetLifeTime();
		if(textureDDS.isLoaded()) {
			return true;
		} else {
			ResourceManager.loadResource(textureDDS);
			if(DUMMY_TEXTURE_ID == 0) createDummyTexture();
			bindTexture(DUMMY_TEXTURE_ID, 0);
			return false;
		}
	}
}
