package ru.krogenit.dds_loader;

import org.lwjgl.BufferUtils;

import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;

import static org.lwjgl.opengl.EXTTextureCompressionS3TC.*;

public class DDSFile {

	private final boolean printDebug = false;
	private static final int DDS_MAGIC = 0x20534444;
	private int dwMagic;
	private DDSHeader header;
	private List<ByteBuffer> bdata;
	private List<ByteBuffer> bdata2;
	private int imageSize;
	private int dxtFormat;
	private boolean isCubeMap;

	public void clear() {
		if (bdata != null) {
			for (ByteBuffer buffer : bdata) {
				buffer.clear();
			}

			bdata.clear();
			bdata = null;
		}

		if (bdata2 != null) {
			for (ByteBuffer buffer : bdata2) {
				buffer.clear();
			}

			bdata2.clear();
			bdata2 = null;
		}
	}

	void loadFile(InputStream fis) {
		bdata = new ArrayList<>();
		bdata2 = new ArrayList<>(); // TODO: Not properly implemented yet.

		try {
			int totalByteCount = fis.available();
			if (printDebug)
				System.out.println("Total bytes: " + totalByteCount);

			byte[] bMagic = new byte[4];
			fis.read(bMagic);
			dwMagic = newByteBuffer(bMagic).getInt();

			if (dwMagic != DDS_MAGIC) {
				System.err.println("Wrong magic word! This is not a DDS file.");
				fis.close();
				return;
			}

			byte[] bHeader = new byte[124];
			fis.read(bHeader);
			header = new DDSHeader(newByteBuffer(bHeader), printDebug);

			int blockSize = 16;
			if (header.ddspf.sFourCC.equalsIgnoreCase("DXT1")) {
				dxtFormat = GL_COMPRESSED_RGB_S3TC_DXT1_EXT;
				blockSize = 8;
			} else if (header.ddspf.sFourCC.equalsIgnoreCase("DXT3")) {
				dxtFormat = GL_COMPRESSED_RGBA_S3TC_DXT3_EXT;
			} else if (header.ddspf.sFourCC.equalsIgnoreCase("DXT5")) {
				dxtFormat = GL_COMPRESSED_RGBA_S3TC_DXT5_EXT;
			} else if (header.ddspf.sFourCC.equalsIgnoreCase("DX10")) {
				System.err.println("Uses DX10 extended header, which is not supported!");
				fis.close();
				return;
			} else {
				System.err.println("Surface format unknown or not supported: " + header.ddspf.sFourCC);
			}

			int surfaceCount;
			totalByteCount -= 128;

			if (header.hasCaps2CubeMap) {
				surfaceCount = 6;
				isCubeMap = true;
			} else {
				surfaceCount = 1;
				isCubeMap = false;
			}

			imageSize = calculatePitch(blockSize);

			int size = header.dwPitchOrLinearSize;

			if (printDebug)
				System.out.println("Calculated pitch: " + imageSize);
			if (printDebug)
				System.out.println("Included PitchOrLinearSize: " + header.dwPitchOrLinearSize);
			if (printDebug)
				System.out.println("Mipmap count: " + header.dwMipMapCount);

			for (int i = 0; i < surfaceCount; i++) {
				byte[] bytes = new byte[size];

				if (printDebug)
					System.out.println("Getting main surface " + i + ". Bytes: " + bytes.length);

				fis.read(bytes);
				totalByteCount -= bytes.length;
				bdata.add(newByteBuffer(bytes));

				if (header.hasFlagMipMapCount) {
					int size2 = Math.max(size / 4, blockSize);
					int maxMipMaps = header.dwMipMapCount - 1;//Math.min(5, header.dwMipMapCount - 1);
					for (int j = 0; j < maxMipMaps; j++) {
						byte[] bytes2 = new byte[size2];

						if (printDebug)
							System.out.println("Getting secondary surface " + j + ". Bytes: " + bytes2.length);

						fis.read(bytes2);
						totalByteCount -= bytes2.length;
						bdata2.add(newByteBuffer(bytes2));
						size2 = Math.max(size2 / 4, blockSize);
					}
				}
			}

			if (printDebug)
				System.out.printf("Remaining bytes: %d (%d)%n", fis.available(), totalByteCount);
			fis.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private int calculatePitch(int blockSize) {
		return Math.max(1, ((header.dwWidth + 3) / 4)) * blockSize;
	}

	public static ByteBuffer newByteBuffer1(byte[] data) {
		ByteBuffer buffer = BufferUtils.createByteBuffer(data.length);
		buffer.put(data);
		buffer.flip();
		return buffer;
	}

	private static ByteBuffer newByteBuffer(byte[] data) {
		ByteBuffer buffer = ByteBuffer.allocateDirect(data.length).order(ByteOrder.nativeOrder());
		buffer.put(data);
		buffer.flip();
		return buffer;
	}

	public int getWidth() {
		return header.dwWidth;
	}

	public int getHeight() {
		return header.dwHeight;
	}

	public ByteBuffer getBuffer() {
		return getMainBuffer();
	}

	private ByteBuffer getMainBuffer() {
		return bdata.get(0);
	}

	public int getMipMapCount() {
		return this.header.dwMipMapCount;
	}

	public ByteBuffer getMipMapLevel(int level) {
		level = Math.min(Math.min(header.dwMipMapCount - 1, level), Math.max(level, 0));
		return this.bdata2.get(level);
	}

	public ByteBuffer getCubeMapPositiveX() {
		if (!header.hasCaps2CubeMap)
			return null;
		return bdata.get(0);
	}

	public ByteBuffer getCubeMapNegativeX() {
		if (!header.hasCaps2CubeMap)
			return null;
		return bdata.get(1);
	}

	public ByteBuffer getCubeMapPositiveY() {
		if (!header.hasCaps2CubeMap)
			return null;
		return bdata.get(2);
	}

	public ByteBuffer getCubeMapNegativeY() {
		if (!header.hasCaps2CubeMap)
			return null;
		return bdata.get(3);
	}

	public ByteBuffer getCubeMapPositiveZ() {
		if (!header.hasCaps2CubeMap)
			return null;
		return bdata.get(4);
	}

	public ByteBuffer getCubeMapNegativeZ() {
		if (!header.hasCaps2CubeMap)
			return null;
		return bdata.get(5);
	}

	public ByteBuffer getCubeMapMipPXLevel(int level) {
		level = Math.min(Math.min(header.dwMipMapCount, level), Math.max(level, 0));
		return this.bdata2.get((level * 1) - 1);
	}

	public ByteBuffer getCubeMapMipNXLevel(int level) {
		level = Math.min(Math.min(header.dwMipMapCount, level), Math.max(level, 0));
		return this.bdata2.get((level * 2) - 1);
	}

	public ByteBuffer getCubeMapMipPYLevel(int level) {
		level = Math.min(Math.min(header.dwMipMapCount, level), Math.max(level, 0));
		if (printDebug)
			System.out.println((level * 3) - 1);
		return this.bdata2.get((level * 3) - 1);
	}

	public ByteBuffer getCubeMapMipNYLevel(int level) {
		level = Math.min(Math.min(header.dwMipMapCount, level), Math.max(level, 0));
		if (printDebug)
			System.out.println((level * 4) - 1);
		return this.bdata2.get((level * 4) - 1);
	}

	public ByteBuffer getCubeMapMipPZLevel(int level) {
		level = Math.min(Math.min(header.dwMipMapCount, level), Math.max(level, 0));
		if (printDebug)
			System.out.println((level * 5) - 1);
		return this.bdata2.get((level * 5) - 1);
	}

	public ByteBuffer getCubeMapMipNZLevel(int level) {
		level = Math.min(Math.min(header.dwMipMapCount, level), Math.max(level, 0));
		if (printDebug)
			System.out.println((level * 6) - 1);
		return this.bdata2.get((level * 6) - 1);
	}

	int getDXTFormat() {
		return dxtFormat;
	}

	public int getPitchOrLinearSize() {
		return imageSize;
	}

	public boolean isCubeMap() {
		return isCubeMap;
	}
}
