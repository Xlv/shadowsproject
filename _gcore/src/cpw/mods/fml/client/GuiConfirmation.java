package cpw.mods.fml.client;

import cpw.mods.fml.common.StartupQuery;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiOptionButton;
import net.minecraft.client.resources.I18n;

public class GuiConfirmation extends GuiNotification
{
    public GuiConfirmation(StartupQuery query)
    {
        super(query);
    }

    /**
     * Adds the buttons (and other controls) to the screen in question.
     */
    @SuppressWarnings("unchecked")
    @Override
    public void initGui()
    {
        this.buttonList.add(new GuiOptionButton(0, this.width / 2 - 155, this.height - 38, I18n.format("gui.yes")));
        this.buttonList.add(new GuiOptionButton(1, this.width / 2 - 155 + 160, this.height - 38, I18n.format("gui.no")));
    }

    @Override
    protected void actionPerformed(GuiButton guiButton)
    {
        if (guiButton.enabled && (guiButton.id == 0 || guiButton.id == 1))
        {
            FMLClientHandler.instance().showGuiScreen(null);
            query.setResult(guiButton.id == 0);
            query.finish();
        }
    }
}