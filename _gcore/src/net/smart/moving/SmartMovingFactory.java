// ==================================================================
// This file is part of Smart Moving.
//
// Smart Moving is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Smart Moving is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Smart Moving. If not, see <http://www.gnu.org/licenses/>.
// ==================================================================

package net.smart.moving;

import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityOtherPlayerMP;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;

import java.util.Hashtable;
import java.util.Iterator;

public class SmartMovingFactory extends SmartMovingContext {
	private static final SmartMovingFactory SMART_MOVING_FACTORY = new SmartMovingFactory();

	private Hashtable<Integer, SmartMovingOther> otherSmartMovings;

	public static void handleMultiPlayerTick(Minecraft minecraft) {
		SMART_MOVING_FACTORY.doHandleMultiPlayerTick(minecraft);
	}

	public static SmartMoving getInstance(EntityPlayer entityPlayer) {
		return SMART_MOVING_FACTORY.doGetInstance(entityPlayer);
	}

	public static SmartMoving getOtherSmartMoving(int entityId) {
		return SMART_MOVING_FACTORY.doGetOtherSmartMoving(entityId);
	}

	public static SmartMovingOther getOtherSmartMoving(EntityOtherPlayerMP entity) {
		return SMART_MOVING_FACTORY.doGetOtherSmartMoving(entity);
	}

	protected void doHandleMultiPlayerTick(Minecraft minecraft) {
		for (Object o : minecraft.theWorld.playerEntities) {
			Entity player = (Entity) o;
			if (player instanceof EntityOtherPlayerMP) {
				EntityOtherPlayerMP otherPlayer = (EntityOtherPlayerMP) player;
				SmartMovingOther moving = doGetOtherSmartMoving(otherPlayer);
				moving.spawnParticles(minecraft, otherPlayer.posX - otherPlayer.prevPosX, otherPlayer.posZ - otherPlayer.prevPosZ);
				moving.foundAlive = true;
			}
		}

		if (otherSmartMovings == null || otherSmartMovings.isEmpty())
			return;

		Iterator<Integer> entityIds = otherSmartMovings.keySet().iterator();
		while (entityIds.hasNext()) {
			Integer entityId = entityIds.next();
			SmartMovingOther moving = otherSmartMovings.get(entityId);
			if (moving.foundAlive)
				moving.foundAlive = false;
			else
				entityIds.remove();
		}
	}

	protected SmartMoving doGetInstance(EntityPlayer entityPlayer) {
		if (entityPlayer instanceof EntityOtherPlayerMP)
			return doGetOtherSmartMoving(entityPlayer.getEntityId());
		else if (entityPlayer instanceof EntityPlayerSP)
			return ((EntityPlayerSP) entityPlayer).getMoving();
		return null;
	}

	protected SmartMoving doGetOtherSmartMoving(int entityId) {
		SmartMoving moving = tryGetOtherSmartMoving(entityId);
		if (moving == null) {
			Entity entity = Minecraft.getMinecraft().theWorld.getEntityByID(entityId);
			if (entity instanceof EntityOtherPlayerMP)
				moving = addOtherSmartMoving((EntityOtherPlayerMP) entity);
		}
		return moving;
	}

	protected SmartMovingOther doGetOtherSmartMoving(EntityOtherPlayerMP entity) {
		SmartMovingOther moving = tryGetOtherSmartMoving(entity.getEntityId());
		if (moving == null)
			moving = addOtherSmartMoving(entity);
		return moving;
	}

	protected final SmartMovingOther tryGetOtherSmartMoving(int entityId) {
		if (otherSmartMovings == null)
			otherSmartMovings = new Hashtable<>();
		return otherSmartMovings.get(entityId);
	}

	protected final SmartMovingOther addOtherSmartMoving(EntityOtherPlayerMP entity) {
		SmartMovingOther moving = new SmartMovingOther(entity);
		otherSmartMovings.put(entity.getEntityId(), moving);
		return moving;
	}
}