package ru.xlv.mgame.arena.duel;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ru.xlv.mgame.XlvsMGameMod;

@Getter
@RequiredArgsConstructor
public enum DuelInviteResult {

    SUCCESS(XlvsMGameMod.INSTANCE.getDuelLocalization().getResponseInviteResultSuccessMessage()),
    PLAYER_NOT_FOUND(XlvsMGameMod.INSTANCE.getDuelLocalization().getResponseInviteResultPlayerNotFoundMessage()),
    INVITE_SPAM(XlvsMGameMod.INSTANCE.getDuelLocalization().getResponseInviteResultInviteSpamMessage()),
    UNKNOWN(XlvsMGameMod.INSTANCE.getDuelLocalization().getResponseInviteResultUnknownMessage());

    private final String responseMessage;
}
