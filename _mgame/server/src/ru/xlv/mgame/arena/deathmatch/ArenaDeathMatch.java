package ru.xlv.mgame.arena.deathmatch;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import gnu.trove.map.TObjectIntMap;
import gnu.trove.map.hash.TObjectIntHashMap;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.XlvsCoreCommon;
import ru.xlv.core.event.PlayerKilledEvent;
import ru.xlv.core.event.ServerPlayerLogoutEvent;
import ru.xlv.core.event.ServerPlayerRespawnEvent;
import ru.xlv.core.item.ItemStackFactory;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.core.util.JsonUtils;
import ru.xlv.mgame.arena.Arena;
import ru.xlv.mgame.arena.ArenaState;
import ru.xlv.mgame.arena.PlayerRatingManager;
import ru.xlv.mgame.util.ArenaLocalization;

import java.util.Map;
import java.util.Random;
import java.util.TreeMap;

@Getter
@RequiredArgsConstructor
public class ArenaDeathMatch extends Arena {

    private final int registrationTimer = 0, preparationTimer = 3, matchProcessTimer = 120, postMatchTimer = 5;

    private final TObjectIntMap<ServerPlayer> playerScoreMap = new TObjectIntHashMap<>();
    private final Random random = new Random();

    private final DeathMatchConfig config;
    private final ArenaLocalization localization;
    private final PlayerRatingManager playerRatingManager;

    @Override
    public void start() {
        super.start();
        XlvsCoreCommon.EVENT_BUS.register(this);
    }

    @Override
    public void stop() {
        super.stop();
        XlvsCoreCommon.EVENT_BUS.unregister(this);
    }

    @Override
    public void updatePerTick() {
        super.updatePerTick();
        switch (getArenaState()) {
            case REGISTRATION:
                if(getPlayerList().size() > 1) {
                    setArenaState(ArenaState.MATCH_PROCESS);
                }
                break;
            case MATCH_PROCESS:
                processInitState(() -> sendMessageToAllPlayers("MATCH PROCESS"));
                controlStateTimeout();
                break;
            case POST_MATCH:
                processInitState(() -> {
                    sendMessageToAllPlayers("POST MATCH");
                    calculateResults();
                });
                controlStateTimeout();
                break;
            case DONE:
                processInitState(() -> sendMessageToAllPlayers("DONE"));
                stop();
        }
    }

    @SubscribeEvent
    @SuppressWarnings("unused")
    public void event(PlayerKilledEvent event) {
        if(event.getKillerServerPlayer() == event.getTargetServerPlayer()) return;
        ServerPlayer killer = null;
        ServerPlayer target = null;
        for (ServerPlayer serverPlayer : getPlayerList()) {
            if (serverPlayer == event.getTargetServerPlayer()) {
                target = serverPlayer;
            } else if (serverPlayer == event.getKillerServerPlayer()) {
                killer = serverPlayer;
            }
        }
        if(killer != null && target != null) {
            playerScoreMap.adjustOrPutValue(killer, config.getScorePerKill(), config.getScorePerKill());
            sendMessageToAllPlayers(localization.getFormatted(localization.getPlayerKilledPlayer(), killer.getPlayerName(), target.getPlayerName()));
        }
    }

    @SubscribeEvent
    @SuppressWarnings("unused")
    public void event(ServerPlayerRespawnEvent event) {
        synchronized (getPlayerList()) {
            if(getPlayerList().contains(event.getServerPlayer())) {
                respawnPlayer(event.getServerPlayer());
            }
        }
    }

    @SubscribeEvent
    public void event(ServerPlayerLogoutEvent event) {
        synchronized (getPlayerList()) {
            getPlayerList().removeIf(serverPlayer -> serverPlayer == event.getServerPlayer());
        }
    }

    private void calculateResults() {
        Map<String, Integer> map = new TreeMap<>();
        playerScoreMap.forEachEntry((a, b) -> {
            map.put(a.getPlayerName(), b);
            return true;
        });
        int i = 0;
        for (String s : map.keySet()) {
            ServerPlayer playerByName = XlvsCore.INSTANCE.getPlayerManager().getPlayer(s);
            if(playerByName != null) {
                if(i < 3) {
                    int score = map.get(s);
                    int credits = (int) (config.getCreditsPerScore() * score);
                    playerByName.getSelectedCharacter().getWallet().addCredits(credits);
                    playerRatingManager.addRating(playerByName, config.getRatingPerScore() * score);
                    sendMessageToAllPlayers(localization.getFormatted(localization.getPlayerReceivedReward(), s, credits, score));
                } else {
                    break;
                }
            }
            i++;
        }
    }

    private void respawnPlayer(ServerPlayer serverPlayer) {
        movePlayer(serverPlayer, config.getSpawnWorldPosition().get(random.nextInt(config.getSpawnWorldPosition().size())));
        config.getItemsPerMatch().forEach(itemStackModel -> {
            ItemStack itemStack = ItemStackFactory.create(Item.getItemById(itemStackModel.getId()), itemStackModel.getCount(), itemStackModel.getDamage(), null);
            itemStack.setTagCompound(JsonUtils.jsonToNBT(itemStackModel.getStringedNbtTagCompound()));
            giveArenaItem(serverPlayer, itemStack);
        });
    }

    @Override
    public boolean addPlayer(ServerPlayer serverPlayer) {
        boolean b = super.addPlayer(serverPlayer);
        if(b) {
            respawnPlayer(serverPlayer);
        }
        return b;
    }
}
