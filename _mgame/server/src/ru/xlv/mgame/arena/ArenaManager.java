package ru.xlv.mgame.arena;

import lombok.Getter;
import ru.xlv.core.schedule.Scheduled;
import ru.xlv.core.schedule.ScheduledAnnotationExecutor;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Getter
public class ArenaManager<T extends Arena> {

    private final List<T> arenaList = new ArrayList<>();

    public ArenaManager() {
        new ScheduledAnnotationExecutor().register(this);
    }

    protected void onArenaStopped(T arena) {}

    @SuppressWarnings("unused")
    @Scheduled(period = 50L)
    public void updatePerTick() {
        synchronized (arenaList) {
            Iterator<T> iterator = arenaList.iterator();
            while (iterator.hasNext()) {
                T next = iterator.next();
                if(next.isActive()) {
                    next.updatePerTick();
                } else {
                    iterator.remove();
                    onArenaStopped(next);
                }
            }
        }
    }

    @SuppressWarnings("unused")
    @Scheduled(period = 1000L)
    public void updatePerSecond() {
        synchronized (arenaList) {
            arenaList.stream().filter(Arena::isActive).forEach(Arena::updatePerSecond);
        }
    }
}
