package ru.krogenit.hud;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import ru.krogenit.CoreInterface;
import ru.krogenit.client.gui.api.GuiPopup;
import ru.krogenit.client.gui.api.GuiPopupWithChoose;
import ru.xlv.group.event.EventGroupInvite;

public class EventsHUD {

    @SubscribeEvent
    public void event(EventGroupInvite event) {
        HudOverlayElement hud = CoreInterface.instance.getHud();
        hud.setPopup(new GuiPopupWithChoose(hud, "ПРИГЛАШЕНИЕ В ГРУППУ ОТ " + event.getUserName(), "", GuiPopup.green, "ПРИНЯТЬ", "ОТКЛОНИТЬ", 1600, 700));
    }
}

