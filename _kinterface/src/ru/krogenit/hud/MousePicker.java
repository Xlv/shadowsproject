package ru.krogenit.hud;

import net.minecraft.client.Minecraft;
import org.lwjgl.BufferUtils;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

import java.nio.FloatBuffer;

public class MousePicker {
	
	private static final Minecraft mc = Minecraft.getMinecraft();
	
	private final FloatBuffer modelViewMatrix = BufferUtils.createFloatBuffer(16);
	private final FloatBuffer prjectionMatrix = BufferUtils.createFloatBuffer(16);
	private Vector3f currentRay = new Vector3f();
	
	private final Matrix4f projMatrix = new Matrix4f();
	private final Matrix4f mvMatrix = new Matrix4f();
	
	public MousePicker() {
		
	}
	
	public void update() {
		GL11.glGetFloat(GL11.GL_PROJECTION_MATRIX, (FloatBuffer) prjectionMatrix.position(0));
		projMatrix.load(prjectionMatrix);
		GL11.glGetFloat(GL11.GL_MODELVIEW_MATRIX, (FloatBuffer) modelViewMatrix.position(0));
		mvMatrix.load(modelViewMatrix);
		currentRay = calculateMouseRay();
	}
	
	private Vector3f calculateMouseRay() {
		float mouseX = Mouse.getX();
		float mouseY = Mouse.getY();
		Vector2f normalizedCoords = getNormalizedDeviceCoords(mouseX, mouseY);
		Vector4f clipCoords = new Vector4f(normalizedCoords.x, normalizedCoords.y, -1f, 1f);
		Vector4f eyeCoords = toEyeCoords(clipCoords);
		Vector3f worldRay = toWorldCoords(eyeCoords);
		return worldRay;
	}
	
	private Vector3f toWorldCoords(Vector4f eyeCoords) {
		Matrix4f invertedView = Matrix4f.invert(mvMatrix, null);
		Vector4f rayWorld = Matrix4f.transform(invertedView, eyeCoords, null);
		Vector3f mouseRay = new Vector3f(rayWorld.x, rayWorld.y, rayWorld.z);
		mouseRay.normalise();
		return mouseRay;
	}
	
	private Vector4f toEyeCoords(Vector4f clipCoords) {
		Matrix4f invertedProjection = Matrix4f.invert(projMatrix, null);
		Vector4f eyeCoords = Matrix4f.transform(invertedProjection, clipCoords, null);
		return new Vector4f(eyeCoords.x, eyeCoords.y, -1f, 0f);
	}
	
	private Vector2f getNormalizedDeviceCoords(float mouseX, float mouseY) {
		float x = (2f*mouseX) / mc.displayWidth - 1f;
		float y = (2f * mouseY) / mc.displayHeight - 1f;
		return new Vector2f(x, y);
	}
	
	public Vector3f getCurrentRay() {
		return currentRay;
	}
}
