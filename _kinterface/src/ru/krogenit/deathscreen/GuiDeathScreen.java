package ru.krogenit.deathscreen;

import gnu.trove.map.TObjectDoubleMap;
import gnu.trove.map.hash.TObjectDoubleHashMap;
import net.minecraft.client.entity.AbstractClientPlayer;
import net.minecraft.client.entity.EntityOtherPlayerMP;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.IProgressMeter;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.entity.RenderPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.network.play.client.C16PacketClientStatus;
import net.minecraft.stats.StatFileWriter;
import net.minecraft.stats.StatList;
import net.minecraftforge.client.event.RenderPlayerEvent;
import net.smart.moving.SmartMovingContext;
import net.smart.moving.SmartMovingFactory;
import net.smart.moving.SmartMovingOther;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector3f;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.*;
import ru.krogenit.inventory.GuiMatrixPlayerInventory;
import ru.krogenit.inventory.GuiMatrixSlot;
import ru.krogenit.inventory.GuiMatrixSlotSpecial;
import ru.krogenit.util.DecimalUtils;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.inventory.MatrixInventory;
import ru.xlv.core.common.item.ItemArmor;
import ru.xlv.core.common.player.character.CharacterAttribute;
import ru.xlv.core.common.player.character.CharacterAttributeType;
import ru.xlv.core.network.PacketPlayerAttributesGet;
import ru.xlv.core.player.ClientPlayer;
import ru.xlv.core.player.ClientPlayerManager;
import ru.xlv.core.player.stat.Stat;
import ru.xlv.customfont.FontType;
import ru.xlv.mochar.XlvsMainMod;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.lwjgl.opengl.GL11.*;

public class GuiDeathScreen extends AbstractGuiScreenAdvanced implements IProgressMeter {

    private boolean isExpanded, showEquip;
    private final List<GuiDamageSource> damageSources = new ArrayList<>();
    private final List<GuiStatWithIcon> achievements = new ArrayList<>();
    private final Vector3f lightPosition = new Vector3f(0f, -50f, 100f);
    private final Vector3f lightColor = new Vector3f(5f, 0, 0);
    private final TObjectDoubleMap<CharacterAttributeType> armorAttributes = new TObjectDoubleHashMap<>();
    private final Map<MatrixInventory.SlotType, GuiMatrixSlot> nonMatrixSlots = new HashMap<>();
    private static AbstractClientPlayer killerPlayer;
    private final Map<CharacterAttributeType, CharacterAttribute> killerAttributes = new HashMap<>();
    private int respawnTimer;
    private final StatFileWriter statWriter;
    private boolean initKiller;

    public GuiDeathScreen() {
        statWriter = mc.thePlayer.getStatFileWriter();

        achievements.add(new GuiStatWithIcon(TextureRegister.textureDeathScreenIconTravel, 30, 30, "ПРОЙДЕННЫЙ ПУТЬ", "0"));
        achievements.add(new GuiStatWithIcon(TextureRegister.textureDeathScreenIconLocations, 30, 30, "ПОСЕЩЕНО ЛОКАЦИЙ", "0"));
        achievements.add(new GuiStatWithIcon(TextureRegister.textureDeathScreenIconPlayers, 30, 30, "УБИТО ИГРОКОВ", "0"));
        achievements.add(new GuiStatWithIcon(TextureRegister.textureDeathScreenIconMonsters, 30, 30, "УБИТО МОНСТРОВ", "0"));
        achievements.add(new GuiStatWithIcon(TextureRegister.textureDeathScreenIconBoss, 30, 30, "УБИТО РЕЙДОВЫХ БОССОВ", "0"));
        achievements.add(new GuiStatWithIcon(TextureRegister.textureDeathScreenIconMoney, 30, 30, "ЗАРАБОТАНО ВАЛЮТЫ", "0"));
        achievements.add(new GuiStatWithIcon(TextureRegister.textureInvIconExplosion, 30, 30, "ДОБЫТО РЕСУРСОВ", "0"));
        achievements.add(new GuiStatWithIcon(TextureRegister.textureDeathScreenIconMissions, 30, 30, "ВЫПОЛНЕНО ЗАДАНИЙ", "0"));
        achievements.add(new GuiStatWithIcon(TextureRegister.textureDeathScreenIconCombat, 30, 30, "ПОЛУЧЕНО БОЕВОГО ОПЫТА", "0"));
        achievements.add(new GuiStatWithIcon(TextureRegister.textureDeathScreenIconResearch, 30, 30, "ПОЛУЧЕНО ОПЫТА ИССЛЕДОВАНИЙ", "0"));
        achievements.add(new GuiStatWithIcon(TextureRegister.textureDeathScreenIconSurvive, 30, 30, "ПОЛУЧЕНО ОПЫТА ВЫЖИВАНИЯ", "0"));

        XlvsCore.INSTANCE.getStatManager().sync().thenAcceptSync(stats -> {
            int totalQuests = getStatByName("TOTAL_QUEST_COMPLETED_COMBAT", stats).getStatProvider().getStatValue().intValue() +
                    getStatByName("TOTAL_QUEST_COMPLETED_EXPLORE", stats).getStatProvider().getStatValue().intValue() +
                    getStatByName("TOTAL_QUEST_COMPLETED_SURVIVE", stats).getStatProvider().getStatValue().intValue();
            achievements.get(1).setValue("" + getStatByName("TOTAL_LOCATION_DISCOVERED", stats).getStatProvider().getStatValue().intValue());
            achievements.get(2).setValue("" + getStatByName("TOTAL_PLAYERS_KILLED", stats).getStatProvider().getStatValue().intValue());
            achievements.get(5).setValue("" + getStatByName("TOTAL_CREDIT_RECEIVED", stats).getStatProvider().getStatValue().intValue());
            achievements.get(7).setValue("" + totalQuests);
        });

        respawnTimer = 0 * 20;
    }

    private Stat getStatByName(String name, List<Stat> stats) {
        for(Stat stat : stats) {
            if(stat.getName().equals(name)) {
                return stat;
            }
        }

        return stats.get(0);
    }

    private void initSlots() {
        nonMatrixSlots.clear();

        for (MatrixInventory.SlotType value : MatrixInventory.SlotType.values()) {
            ItemStack itemStack = killerPlayer.inventory.getStackInSlot(value.getAssociatedSlotIndex());
            if(value.ordinal() < MatrixInventory.SlotType.HOT_SLOT0.ordinal()) {
                float addX = 0f;
                float addY = 0f;
                switch (value) {
                    case BACKPACK: addX -= 132f; addY -= 38f; break;
                    case ADD_WEAPON: addX -= 208f; addY -= 142f; break;
                    case MELEE_WEAPON: addX -= 131f; addY -= 142f; break;
                    case MAIN_WEAPON: addX -= 131f; addY -= 219f; break;
                    case SECOND_WEAPON: addX -= 131f; addY -= 325f; break;
                    case HEAD: addX -= 456; addY += 3; break;
                    case BODY: addX -= 455; addY -= 73; break;
                    case BRACERS: addX -= 455; addY -= 148; break;
                    case LEGS: addX -= 455; addY -= 224; break;
                    case FEET: addX -= 455; addY -= 299; break;
                }
                GuiMatrixPlayerInventory.addSlot(new GuiMatrixSlotSpecial(0, 0, 0, 0, itemStack, value, null), value, nonMatrixSlots, addX, addY, 0.45f);
            }
        }
    }

    private void recalculateProtectionStats() {
        armorAttributes.clear();
        for(CharacterAttributeType characterAttribute : GuiMatrixPlayerInventory.armorAttributeTypes) {
            armorAttributes.put(characterAttribute, 0);
        }
        armorAttributes.put(CharacterAttributeType.BALLISTIC_PROTECTION, 0);
        armorAttributes.put(CharacterAttributeType.CUT_PROTECTION, 0);

        for (int i = MatrixInventory.SlotType.HEAD.ordinal(); i <= MatrixInventory.SlotType.FEET.ordinal(); i++) {
            MatrixInventory.SlotType slotType = MatrixInventory.SlotType.values()[i];
            GuiMatrixSlot guiMatrixSlot = nonMatrixSlots.get(slotType);
            if(guiMatrixSlot != null) {
                ItemStack itemStack = guiMatrixSlot.getItemStack();
                if(itemStack != null && itemStack.getItem() instanceof ItemArmor) {
                    ItemArmor itemArmor = (ItemArmor) itemStack.getItem();
                    for(CharacterAttributeType characterAttribute : GuiMatrixPlayerInventory.armorAttributeTypes) {
                        double value = itemArmor.getCharacterAttributeBoost(characterAttribute);
                        armorAttributes.put(characterAttribute, armorAttributes.get(characterAttribute) + value);
                    }
                    double value = itemArmor.getCharacterAttributeBoost(CharacterAttributeType.BALLISTIC_PROTECTION);
                    armorAttributes.put(CharacterAttributeType.BALLISTIC_PROTECTION, armorAttributes.get(CharacterAttributeType.BALLISTIC_PROTECTION) + value);
                    value = itemArmor.getCharacterAttributeBoost(CharacterAttributeType.CUT_PROTECTION);
                    armorAttributes.put(CharacterAttributeType.CUT_PROTECTION, armorAttributes.get(CharacterAttributeType.CUT_PROTECTION) + value);
                }
            }
        }
    }

    @Override
    public void initGui() {
        super.initGui();
        this.mc.getNetHandler().addToSendQueue(new C16PacketClientStatus(C16PacketClientStatus.EnumState.REQUEST_STATS));
        initButtons();
        if(killerPlayer != null) initSlots();
        damageSources.clear();
//        damageSources.add(new GuiDamageSource(TextureRegister.textureDeathScreenIconBullet, 48, 48, 148, 58.7));
//        damageSources.add(new GuiDamageSource(TextureRegister.textureSkillActiveLearned, 48, 48, 54, 23.5));
//        damageSources.add(new GuiDamageSource(TextureRegister.textureSkillPassive,48, 48,  34, 43.5));
//        damageSources.add(new GuiDamageSource(TextureRegister.textureSkillPassive,48, 48, 67, 12.3));
    }

    private void initButtons() {
        buttonList.clear();

        float x = 1570;
        float y = 222;
        float width = 21;
        float height = 21;
        GuiButtonAdvanced guiButtonAdvanced = new GuiButtonAdvanced(0, ScaleGui.getCenterX(x), ScaleGui.getCenterY(y), ScaleGui.get(width), ScaleGui.get(height), "");
        guiButtonAdvanced.setTexture( isExpanded ? TextureRegister.textureDeathScreenButtonCollapse : TextureRegister.textureDeathScreenButtonExpand);
        buttonList.add(guiButtonAdvanced);

        if(isExpanded) {
            x = 857;
            y = 821;
        } else {
            x = 1326;
            y = 322;
        }
        width = 207;
        height = 39;
        GuiButtonAnimated guiButtonAnimated = new GuiButtonAnimated(1, ScaleGui.getCenterX(x), ScaleGui.getCenterY(y), ScaleGui.get(width), ScaleGui.get(height), respawnTimer + " СЕКУНД");
        guiButtonAnimated.setTexture(TextureRegister.textureDeathScreenButtonDeathScreen);
        guiButtonAnimated.setTextureHover(TextureRegister.textureDeathScreenButtonDeathScreenHover);
        guiButtonAnimated.setMaskTexture(TextureRegister.textureTreeButtonMask);
        guiButtonAnimated.setMaskColor(226 / 255f, 0f, 26 / 255f);
        buttonList.add(guiButtonAnimated);

        x = 441;
        y = 645;
        width = 171;
        height = 39;
        guiButtonAnimated = new GuiButtonAnimated(2, ScaleGui.getCenterX(x), ScaleGui.getCenterY(y), ScaleGui.get(width), ScaleGui.get(height), showEquip ? "ЗАКРЫТЬ" : "ЭКИПИРОВКА");
        guiButtonAnimated.setTexture(TextureRegister.textureTreeButtonAccept);
        guiButtonAnimated.setTextureHover(TextureRegister.textureTreeButtonHover);
        guiButtonAnimated.setMaskTexture(TextureRegister.textureTreeButtonMask);
        guiButtonAnimated.enabled = guiButtonAnimated.visible = isExpanded;
        buttonList.add(guiButtonAnimated);
    }

    @Override
    protected void actionPerformed(GuiButton guiButton) {
        if(guiButton.id == 0) {
            this.isExpanded = !isExpanded;
            ((GuiButtonAdvanced)buttonList.get(0)).setTexture(isExpanded ? TextureRegister.textureDeathScreenButtonCollapse : TextureRegister.textureDeathScreenButtonExpand);
            GuiButtonAnimated guiButtonAnimated = (GuiButtonAnimated) buttonList.get(1);
            if(isExpanded) {
                guiButtonAnimated.xPosition = ScaleGui.getCenterX(857);
                guiButtonAnimated.yPosition = ScaleGui.getCenterY(821);
            } else {
                guiButtonAnimated.xPosition = ScaleGui.getCenterX(1326);
                guiButtonAnimated.yPosition = ScaleGui.getCenterY(322);
            }
            GuiButton guiButton1 = buttonList.get(2);
            guiButton1.enabled = guiButton1.visible = isExpanded;
        } else if(guiButton.id == 1 && respawnTimer <= 0) {
            this.mc.thePlayer.respawnPlayer();
            this.mc.displayGuiScreen(null);
            killerPlayer = null;
        } else if(guiButton.id == 2 && killerPlayer != null) {
            showEquip = !showEquip;
            guiButton.displayString = showEquip ? "ЗАКРЫТЬ" : "ЭКИПИРОВКА";
            if(showEquip) {
                this.lightColor.x = 4f;
                this.lightColor.y = 4f;
                this.lightColor.z = 5f;
            } else {
                this.lightColor.x = 5f;
                this.lightColor.y = 0.0f;
                this.lightColor.z = 0.0f;
            }
        }
    }

    private void drawHpManaEnergy() {
        float x = 111;
        float y = 236;
        float scale = 0.645f;
        GL11.glColor4f(1f,1f,1f,1f);
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvIconHpGaus, x , y, 50 * scale, 50 * scale);
        y += 48 * scale;
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvIconManaGaus, x , y, 55 * scale, 55 * scale);
        y += 51 * scale;
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvIconStaminaGaus, x , y, 45 * scale, 65 * scale);

        x = 111;
        y = 236;
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvIconHp, x, y, 33 * scale, 33 * scale);
        y += 48 * scale;
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvIconMana, x, y, 40 * scale, 40 * scale);
        y += 51 * scale;
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvIconStamina, x, y, 26 * scale, 41 * scale);

        x += 20f;
        y = 229;
        float fs = 60 / 22f * scale;
        String shp = "" + (int) killerPlayer.getHealth();
        float sl = FontType.Marske.getFontContainer().width(shp) * fs;
        GuiDrawUtils.drawStringCenter(FontType.Marske, shp, x, y, fs, 0xffffff);
        fs = 22 / 22f * scale;
        String hpRegen = "" + (int) (killerAttributes.containsKey(CharacterAttributeType.HEALTH_REGEN) ? killerAttributes.get(CharacterAttributeType.HEALTH_REGEN).getValue() : 0);
        float regenOffsetX = 2 * scale;
        float regenOffsetY = 6 * scale;
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, hpRegen, x + sl + regenOffsetX, y + regenOffsetY, fs, 0xffffff);
        fs = 60 / 22f * scale;
        y+= 50 * scale;

        String smana = "" + (int) (killerAttributes.containsKey(CharacterAttributeType.MANA) ? killerAttributes.get(CharacterAttributeType.MANA).getValue() : 0);
        sl = FontType.Marske.getFontContainer().width(smana) * fs;
        GuiDrawUtils.drawStringCenter(FontType.Marske, smana, x, y, fs, 0xffffff);
        fs = 22 / 22f * scale;
        String manaRegen = "" + (int) (killerAttributes.containsKey(CharacterAttributeType.MANA_REGEN) ? killerAttributes.get(CharacterAttributeType.MANA_REGEN).getValue() : 0);
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, manaRegen, x + sl + regenOffsetX, y + regenOffsetY, fs, 0xffffff);
        y+= 50 * scale;
        fs = 60 / 22f * scale;

        SmartMovingOther moving = SmartMovingFactory.getOtherSmartMoving((EntityOtherPlayerMP) killerPlayer);
        float maxExhaustion = SmartMovingContext.Client.getMaximumExhaustion();
        float fitness = maxExhaustion - Math.min(moving.exhaustion, maxExhaustion);
        String senergy = "" + (int) (fitness);

        sl = FontType.Marske.getFontContainer().width(senergy) * fs;
        GuiDrawUtils.drawStringCenter(FontType.Marske, senergy, x, y, fs, 0xffffff);
        fs = 22 / 22f * scale;
        String energyRegen = "" + (int) (killerAttributes.containsKey(CharacterAttributeType.STAMINA_REGEN) ? killerAttributes.get(CharacterAttributeType.STAMINA_REGEN).getValue() : 0);
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, energyRegen, x + sl + regenOffsetX, y + regenOffsetY, fs, 0xffffff);
    }

    private void drawMiscArmorStats() {
        float scale = 0.645f;
        glColor4f(1f, 1f, 1f, 1f);
        float iconWidth = 22;
        float iconHeight = 27;
        float x = 301;
        float y = 236;
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvIconArmor, x , y, iconWidth, iconHeight);
        y += 32;
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvIconCutDefence, x , y, iconWidth, iconHeight);

        x = 279;
        y = 235;
        float fs = 30 / 22f * scale;
        String armor = DecimalUtils.getFormattedStringWithOneDigit(armorAttributes.get(CharacterAttributeType.BALLISTIC_PROTECTION));
        GuiDrawUtils.drawRightStringCenter(FontType.HelveticaNeueCyrBold, armor, x, y, fs, 0xffffff);
        y += 32;
        String defence = DecimalUtils.getFormattedStringWithOneDigit(armorAttributes.get(CharacterAttributeType.CUT_PROTECTION));
        GuiDrawUtils.drawRightStringCenter(FontType.HelveticaNeueCyrBold, defence, x, y, fs, 0xffffff);
    }

    private void drawStats() {
        float y = 371;
        float fs = 40 / 32f;
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, "СОПРОТИВЛЕНИЕ", 91, y, fs, 0x626262);

        float x = 90;
        y = 389;
        float iconWidth = 20;
        float iconHeight = 20;
        float yOffset = 21;
        GL11.glColor4f(1f,1f,1f,1f);
        GuiDrawUtils.drawCenter(TextureRegister.textureInvIconEnergy, x, y, iconWidth, iconHeight);
        y += yOffset;
        GuiDrawUtils.drawCenter(TextureRegister.textureInvIconHeat, x, y, iconWidth, iconHeight);
        y += yOffset;
        GuiDrawUtils.drawCenter(TextureRegister.textureInvIconFire, x, y, iconWidth, iconHeight);
        y += yOffset;
        GuiDrawUtils.drawCenter(TextureRegister.textureInvIconElectricity, x, y, iconWidth, iconHeight);
        y += yOffset;
        GuiDrawUtils.drawCenter(TextureRegister.textureInvIconToxins, x, y, iconWidth, iconHeight);
        y += yOffset;
        GuiDrawUtils.drawCenter(TextureRegister.textureInvIconRadiation, x, y, iconWidth, iconHeight);
        y += yOffset;
        GuiDrawUtils.drawCenter(TextureRegister.textureInvIconExplosion, x, y, iconWidth, iconHeight);

        x = 119;
        y = 398;
        fs = 22 / 32f;
        for(CharacterAttributeType characterAttributeType : GuiMatrixPlayerInventory.armorAttributeTypes) {
            GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, characterAttributeType.getDisplayName().toUpperCase(), x, y, fs, 0xB4B4B4);
            y+=yOffset;
        }

        x = 296;
        y = 398;
        fs = 28 / 32f;
        for(CharacterAttributeType characterAttributeType : GuiMatrixPlayerInventory.armorAttributeTypes) {
            GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrMedium, DecimalUtils.getFormattedStringWithOneDigit(armorAttributes.get(characterAttributeType)), x, y, fs, 0xffffff);
            y+=yOffset;
        }
    }

    private void drawEquip() {
        float scale = 0.645f;
        String playerName = killerPlayer.getDisplayName().toUpperCase();
        float x = 97;
        float y = 200;
        float fs = 60 / 32f * scale;
        float sl = FontType.HelveticaNeueCyrLight.getFontContainer().width(playerName) * fs;
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, playerName, x, y, fs, 0x626262);

        String lvl = "LVL " + XlvsMainMod.INSTANCE.getClientMainPlayer().getLvl();
        fs = 30 / 32f * scale;
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, lvl, x + sl + 12.8f * scale, y + 6.418f * scale, fs, 0x626262);

        drawMiscArmorStats();
        drawHpManaEnergy();
        drawStats();

        GuiDrawUtils.drawCenter(TextureRegister.textureInvBorderLeft, 332, 225, 1f, 454);

        float iconHeight = 61;
        float yOffset = iconHeight;
        float iconWidth = 53;
        iconHeight = 3;
        x = 575;
        y = 286;
        GuiDrawUtils.drawCenter(TextureRegister.textureInvEquipmentLine, x, y, iconWidth, iconHeight);
        y+=yOffset;
        x = 608;
        iconWidth = 23;
        GuiDrawUtils.drawCenter(TextureRegister.textureInvEquipmentLine, x, y, iconWidth, iconHeight);
        y+=yOffset;
        GuiDrawUtils.drawCenter(TextureRegister.textureInvEquipmentLine, x, y, iconWidth, iconHeight);
        iconWidth = 65;
        x = 566;
        y+=yOffset;
        GuiDrawUtils.drawCenter(TextureRegister.textureInvEquipmentLine, x, y, iconWidth, iconHeight);
        y+=yOffset;
        GuiDrawUtils.drawCenter(TextureRegister.textureInvEquipmentLine, x, y, iconWidth, iconHeight);

        nonMatrixSlots.forEach((slotType, guiMatrixSlot) -> guiMatrixSlot.render(0,0,true));
        glColor4f(1f, 1f, 1f, 1f);
    }

    private void drawExpandedInfo() {
        GuiDrawUtils.drawCenter(TextureRegister.textureDeathScreenElementDevider, 328, 730, 1263, 2);
        if(showEquip) {
            glDisable(GL_TEXTURE_2D);
            GuiDrawUtils.renderCuttedRect(ScaleGui.getCenterX(47), ScaleGui.getCenterY(154), ScaleGui.getCenterX(47 + 713), ScaleGui.getCenterY(154 + 587), ScaleGui.get(45f),  ScaleGui.get(205f),  ScaleGui.get(350f), 18/255f, 18/255f, 18/255f, 0.95f, 0.1f, 0.1f, 0.1f, 0.95f);
            glEnable(GL_TEXTURE_2D);
        }
        GL11.glEnable(GL_SCISSOR_TEST);
        int sx = (int) ScaleGui.getCenterX(showEquip ? 47 : 288);
        int sy = (int) (ScaleGui.screenHeight - ScaleGui.getCenterY(924));
        int swidth = (int) ScaleGui.get(showEquip ? 1583 : 1343);
        int sheight = (int) ScaleGui.get(showEquip ? 771 : 743);
        GL11.glScissor(sx, sy, swidth, sheight);
        float scale = 2.2f;
        float iconWidth = 1366 / scale;
        float iconHeight = 356 / scale;
        float x = 527;
        float y = 560;
        glColor4f(1f, 1f, 1f, 0.25f);
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvPlayerFloor, x, y, iconWidth, iconHeight);
        iconWidth = 1803 / scale;
        iconHeight = 493 / scale;
        glColor4f(226/255f, 0f, 26/255f, 1f);
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvPlayerFloor1, x, y, iconWidth, iconHeight);
        iconWidth = 542 / scale;
        iconHeight = 1167 / scale;
        y -= 220;
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvPlayerLight, x, y, iconWidth, iconHeight);
        iconWidth = 1385 / scale;
        iconHeight = 369 / scale;
        y += 220;
        for (int i = 0; i < 5; i++) GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvPlayerFloor2, x, y, iconWidth, iconHeight);
        glColor4f(1f, 1f, 1f, 1f);
        if(killerPlayer != null) {
            scale = ScaleGui.get(135f);
            renderEntity(ScaleGui.getCenterX(x), ScaleGui.getCenterY(y - 220), scale);
        }
        GL11.glDisable(GL_SCISSOR_TEST);

        if(showEquip && killerPlayer != null) {
            drawEquip();
        }

        GuiDrawUtils.drawCenteredStringCenter(FontType.HelveticaNeueCyrLight, "ВЫ МОЖЕТЕ ВОСКРЕСНУТЬ ЧЕРЕЗ...", 960, 791, 30 / 32f, 0xffffff);
        GuiDrawUtils.drawCenter(TextureRegister.textureDeathScreenElementDeviderSmall, 772, 289, 819, 2);
        GuiDrawUtils.drawCenter(TextureRegister.textureDeathScreenElementDeviderSmall, 772, 379, 819, 2);

        if(killerPlayer != null) {
            String s = killerPlayer.getDisplayName().toUpperCase();
            ClientPlayer player = ClientPlayerManager.INSTANCE.getPlayer(killerPlayer);
            float fs = 60 / 32f;
            GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, s, 772, 226, fs, 0xffffff);
            float l = FontType.HelveticaNeueCyrLight.getFontContainer().width(s) * fs;
            fs = 30 / 32f;
            GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, player.getCharacterType().getDisplayName().toUpperCase(), 772, 260, fs, 0xffffff);
            GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, "LVL " + player.getLevel(), 782 + l, 234, fs, 0x666666);
            GuiDrawUtils.drawCenteredStringCenter(FontType.HelveticaNeueCyrLight, "УБИЙЦА", 526, 614, 36 / 32f, 0x666666);
        }

        x = 772;
        y = 310;
        for(GuiDamageSource guiDamageSource : damageSources) {
            guiDamageSource.draw(x, y);
            x += 220;
        }

        x = 770;
        y = 416;
        float fs = 60 / 32f;
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, "ДОСТИЖЕНИЯ", x, y, fs, 0xffffff);
        x = 772;
        y = 453;
        for(GuiStatWithIcon stat : achievements) {
            stat.draw(x, y);
            y += 32;
            if(y > 668) {
                y = 453;
                x += 446;
            }
        }
    }

    private void drawBackgroundThings() {
        drawDefaultBackground();
        GuiDrawUtils.drawCenter(TextureRegister.textureNPCDialogBorderBot, 115, 142, 1688, 78);
        GuiDrawUtils.drawCenter(TextureRegister.textureNPCDialogBorderTop, 115, 888, 1688, 78);
        float x = 36;
        float y = 291;
        float fs = 18 / 32f;
        GuiDrawUtils.drawSplittedStringCenterXBot(FontType.DeadSpace, "Lorem ipsum\u0003dolor sit amet, consec\n" +
                "tetur adipisicing\n" +
                "elit, sed do eiusmod\n" +
                "tempor in\n" +
                "cididunt ut lab\n" +
                "ore et \n" +
                "magna aliqua. U\n" +
                "t enim ad\n" +
                " minim veniam, q\n" +
                "uis nostrud exercit\n" +
                "ation ul\n" +
                "lamco labor\n" +
                "is nisi ut aliqui\n" +
                "p ex ea c\n" +
                "ommodo consequa\n" +
                "t. Dui\n" +
                "s aute ir\n" +
                "ure ", x, y, fs, ScaleGui.get(150), -1, 0x626262);

        x = 1875;
        y = 437;
        GuiDrawUtils.drawSplittedRightStringCenterXBot(FontType.DeadSpace, "Lorem ipsum\u0003dolor sit amet, consec\n" +
                "tetur adipisicing\n" +
                "elit, sed do eiusmod\n" +
                "tempor in\n" +
                "cididunt ut lab\n" +
                "ore et \n" +
                "magna aliqua. U\n" +
                "t enim ad\n" +
                " minim veniam, q\n" +
                "uis nostrud exercit\n" +
                "ation ul\n" +
                "lamco labor\n" +
                "is nisi ut aliqui\n" +
                "p ex ea c\n" +
                "ommodo consequa\n" +
                "t. Dui\n" +
                "s aute ir\n" +
                "ure\n" +
                "Lorem ipsum\u0003dolor sit amet, consec\n" +
                "tetur adipisicing\n" +
                "elit, sed do eiusmod\n" +
                "tempor in\n" +
                "cididunt ut lab\n" +
                "ore et \n" +
                "magna aliqua. U\n" +
                "t enim ad\n" +
                " minim veniam, q\n" +
                "uis nostrud exercit\n" +
                "ation ul\n" +
                "lamco labor\n" +
                "is nisi ut aliqui\n" +
                "p ex ea c\n" +
                "ommodo consequa\n" +
                "t. Dui\n" +
                "s aute ir\n" +
                "ure ", x, y, fs, ScaleGui.get(150), -1, 0x626262);

        fs = 60 / 32f;
        x = 960;
        y = 90f;
        GuiDrawUtils.drawCenteredStringCenter(FontType.HelveticaNeueCyrLight, "ВАС УБИЛИ!", x, y, fs, 0xffffff);

        GL11.glDisable(GL11.GL_TEXTURE_2D);
        GL11.glColor4f(0f, 0f, 0f, 0.7f);
        y = ScaleGui.getCenterY(182);
        if(isExpanded) {
            GuiDrawUtils.drawCenter(288, 182, 1343, 743);
            x = ScaleGui.getCenterX(288);
            float width = ScaleGui.get(1343);
            float height = ScaleGui.get(371.5f);
            GuiDrawUtils.drawGradient(x, y, width, height, 226 / 255f, 0f, 26 / 255f, 0.2f, 0f, 0f, 0f, 0f);
            GuiDrawUtils.drawGradient(x, y + height, width, height, 0f, 0f, 0f, 0f, 226 / 255f, 0f, 26 / 255f, 0.2f);
        } else {
            GuiDrawUtils.drawCenter(1220, 182, 411, 228);
            x = ScaleGui.getCenterX(1220);
            float width = ScaleGui.get(411);
            float height = ScaleGui.get(114);
            GuiDrawUtils.drawGradient(x, y, width, height, 226 / 255f, 0f, 26 / 255f, 0.2f, 0f, 0f, 0f, 0f);
            GuiDrawUtils.drawGradient(x, y + height, width, height, 0f, 0f, 0f, 0f, 226 / 255f, 0f, 26 / 255f, 0.2f);
        }
        GL11.glEnable(GL11.GL_TEXTURE_2D);

        if(isExpanded) {
            drawExpandedInfo();
        } else {
            GuiDrawUtils.drawCenteredStringCenter(FontType.HelveticaNeueCyrLight, "ВЫ МОЖЕТЕ ВОСКРЕСНУТЬ ЧЕРЕЗ...", 1425, 290, 30 / 32f, 0xffffff);
        }
        GL11.glColor4f(1f, 1f, 1f, 1f);
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTick) {
        if(mc.thePlayer.getHealth() > 0) {
            mc.displayGuiScreen(null);
        }

        if(!initKiller && killerPlayer != null) {
            XlvsCore.INSTANCE.getPacketHandler().sendPacketEffectiveCallback(new PacketPlayerAttributesGet(killerPlayer.getDisplayName())).thenAcceptSync(characterAttributes -> {
                characterAttributes.forEach(characterAttribute -> killerAttributes.put(characterAttribute.getType(), characterAttribute));
            });
            initSlots();
            initKiller = true;
        }

        super.drawScreen(mouseX, mouseY, partialTick);
        drawBackgroundThings();
        drawButtons(mouseX, mouseY, partialTick);
    }

    private void renderEntity(float x, float y, float scale) {
        GuiDrawUtils.drawPlayer(killerPlayer, x, y, -10f, scale, 180f, 0.5f, 0.5f, 0.5f, lightPosition, lightColor, new RenderPlayerEvent.Specials.Post(killerPlayer, (RenderPlayer) RenderManager.instance.getEntityRenderObject(killerPlayer), 0f));
    }

    @Override
    public void mouseClicked(int mouseX, int mouseY, int mouseButton) {
        super.mouseClicked(mouseX, mouseY, mouseButton);
    }

    public static void setKillerPlayer(AbstractClientPlayer killerPlayer) {
        GuiDeathScreen.killerPlayer = killerPlayer;
    }

    @Override
    public void updateScreen() {
        if(respawnTimer > 0) {
            respawnTimer--;
            int seconds = respawnTimer / 20 + 1;
            buttonList.get(1).displayString = seconds + " " + getTimerSeconds(seconds);
        } else {
            buttonList.get(1).displayString = "ВОСКРЕСНУТЬ";
        }
    }

    private String getTimerSeconds(int timer) {
        if(timer == 1) {
            return "СЕКУНДУ";
        } else if(timer > 1 && timer < 5)  {
            return "СЕКУНДЫ";
        } else if(timer > 4 && timer < 20) {
            return "СЕКУНД";
        }

        return "СЕКУНДУ";
    }

    @Override
    public void keyTyped(char character, int key) {

    }

    @Override
    public void func_146509_g() {
        achievements.get(0).setValue(StatList.distanceWalkedStat.func_75968_a(statWriter.writeStat(StatList.distanceWalkedStat)));
        achievements.get(3).setValue(StatList.mobKillsStat.func_75968_a(statWriter.writeStat(StatList.mobKillsStat)));
    }
}
