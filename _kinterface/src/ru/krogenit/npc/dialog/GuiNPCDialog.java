package ru.krogenit.npc.dialog;

import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.IChatComponent;
import noppes.npcs.NoppesStringUtils;
import noppes.npcs.NoppesUtilPlayer;
import noppes.npcs.client.NoppesUtil;
import noppes.npcs.client.TextBlockClient;
import noppes.npcs.client.controllers.MusicController;
import noppes.npcs.client.gui.player.GuiDialogInteract;
import noppes.npcs.client.gui.util.IGuiClose;
import noppes.npcs.constants.EnumOptionType;
import noppes.npcs.constants.EnumPlayerPacket;
import noppes.npcs.controllers.Dialog;
import noppes.npcs.controllers.DialogOption;
import noppes.npcs.entity.EntityNPCInterface;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.AbstractGuiScreenAdvanced;
import ru.krogenit.client.gui.api.GuiDrawUtils;
import ru.krogenit.client.gui.api.ScaleGui;
import ru.krogenit.npc.GuiNPCButtonSwitcher;
import ru.krogenit.npc.quest.GuiNPCQuests;
import ru.krogenit.utils.AnimationHelper;
import ru.xlv.customfont.FontType;

import java.util.ArrayList;
import java.util.List;

import static org.lwjgl.opengl.GL11.*;

public class GuiNPCDialog extends AbstractGuiScreenAdvanced implements IGuiClose {

    protected float scroll, scrollAnim, lastScroll;
    protected float scrollTotalHeight, scrollViewHeight, scrollTextureHeight;
    protected float scrollX, scrollY, scrollWidth, scrollHeight;
    protected boolean mouseScrolling;
    protected float mouseStartY;

    private final EntityClientPlayerMP player = Minecraft.getMinecraft().thePlayer;
    private final EntityNPCInterface npc;
    private boolean closeOnEsc;
    private List<Integer> options = new ArrayList<>();
    private final List<TextBlockClient> lines = new ArrayList<>();
    private boolean isGrabbed = false;
    private Dialog dialog;
    private int selected = 0;
    private boolean addedDialog = false;

    public GuiNPCDialog(GuiDialogInteract parent) {
        super(ScaleGui.FULL_HD);
        this.npc = parent.npc;
        this.dialog = parent.getDialog();
        this.appendDialog(dialog);
    }

    @Override
    public void initGui() {
        super.initGui();
        buttonList.clear();
        initButtons();
        scrollViewHeight = ScaleGui.get(164);
        this.isGrabbed = false;
        this.grabMouse(this.dialog.showWheel);
    }

    private void initButtons() {
        float xOffset = 74;
        float x = 1362;
        float y = 543;
        float buttonWidth = 47;
        float buttonHeight = 47;
        GuiNPCButtonSwitcher b = new GuiNPCButtonSwitcher(0, ScaleGui.getCenterX(x), ScaleGui.getBot(y), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight));
        b.setTexture(TextureRegister.textureNPCButtonDialog);
        b.setMaskTexture(TextureRegister.textureNPCButtonMask);
        buttonList.add(b);
        x += xOffset;
        b = new GuiNPCButtonSwitcher(1, ScaleGui.getCenterX(x), ScaleGui.getBot(y), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight));
        b.setTexture(TextureRegister.textureNPCButtonTrade);
        b.setMaskTexture(TextureRegister.textureNPCButtonMask);
        buttonList.add(b);
        x += xOffset;
        b = new GuiNPCButtonSwitcher(2, ScaleGui.getCenterX(x), ScaleGui.getBot(y), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight));
        b.setTexture(TextureRegister.textureNPCButtonSave);
        b.setMaskTexture(TextureRegister.textureNPCButtonMask);
        buttonList.add(b);
        x += xOffset;
        b = new GuiNPCButtonSwitcher(3, ScaleGui.getCenterX(x), ScaleGui.getBot(y), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight));
        b.setTexture(TextureRegister.textureNPCButtonLocked);
        b.setMaskTexture(TextureRegister.textureNPCButtonMask);
        b.enabled = false;
        buttonList.add(b);
    }

    @Override
    protected void actionPerformed(GuiButton guiButton) {
        if(guiButton.id == 2) {
            mc.displayGuiScreen(new GuiNPCQuests(npc));
        }
    }

    private void drawScroll() {
        if (scrollAnim < scroll) {
            scrollAnim = AnimationHelper.updateSlowEndAnim(scrollAnim, scroll, 0.25f, 0.1f);
        } else if (scrollAnim > scroll) {
            scrollAnim = AnimationHelper.updateSlowEndAnim(scrollAnim, scroll, -0.25f, -0.1f);
        }

        float x = ScaleGui.getCenterX(1604);
        float y = ScaleGui.getBot(669);
        float iconWidth = ScaleGui.get(7);
        float iconHeight = ScaleGui.get(4);
        GuiDrawUtils.drawRect(TextureRegister.textureInvArrowTop, x, y, iconWidth, iconHeight);
        y += iconHeight + ScaleGui.get(3);
        scrollTextureHeight = ScaleGui.get(113);
        float scrollValue = scrollViewHeight / scrollTotalHeight;
        float scrollYValue = scrollTextureHeight / scrollTotalHeight;
        if (scrollValue > 1) scrollValue = 1f;
        if (scrollYValue > 1) scrollYValue = 1f;
        iconHeight = scrollTextureHeight * scrollValue;
        y += scrollAnim * scrollYValue;
        scrollX = x;
        scrollY = y;
        scrollWidth = iconWidth;
        scrollHeight = iconHeight;
        GuiDrawUtils.drawRect(TextureRegister.textureInvScrollBody, x, y, iconWidth, iconHeight);
        y = ScaleGui.getBot(792);
        iconHeight = ScaleGui.get(4);
        GuiDrawUtils.drawRect(TextureRegister.textureInvArrowBot, x, y, iconWidth, iconHeight);
    }

    private void drawOptions(int mouseY) {
        float x = 300;
        float y = 848;
        float offsetY = 25f;
        float max = ScaleGui.getBot(y + options.size() * 25f);
        float startY = ScaleGui.getBot(y - offsetY / 2f);

        if (mouseY >= startY && mouseY < max) {
            int count = (int) ((mouseY - startY) / ScaleGui.get(offsetY));

            if (count < this.options.size()) {
                this.selected = count;
            }
        } else if(closeOnEsc && mouseY >= max + ScaleGui.get(10f) && mouseY < max + ScaleGui.get(45f)) {
            this.selected = -2;
        } else {
            this.selected = -1;
        }

        float fs = 30 / 32f;
        for (int k = 0; k < this.options.size(); ++k) {
            int id = this.options.get(k);
            DialogOption option = this.dialog.options.get(id);
            if (this.selected == k) {
                GL11.glDisable(GL_TEXTURE_2D);
                GL11.glColor4f(0.172f, 0.172f, 0.172f, 0.8f);
                GuiDrawUtils.drawCenterXBot(x - 4, y - 10, 1325, 25);
                GL11.glEnable(GL_TEXTURE_2D);
            }

            GuiDrawUtils.drawStringCenterXBot(FontType.HelveticaNeueCyrLight, EnumChatFormatting.AQUA + "" + (k+1) + ". " + EnumChatFormatting.WHITE + NoppesStringUtils.formatText(option.title, this.player, this.npc), x, y, fs, option.optionColor);
            y += offsetY;
        }

        if(this.closeOnEsc) {
            y += offsetY;
            if(selected == -2) {
                GL11.glDisable(GL_TEXTURE_2D);
                GL11.glColor4f(0.172f, 0.172f, 0.172f, 0.8f);
                GuiDrawUtils.drawCenterXBot(x - 4, y - 10, 1325, 25);
                GL11.glEnable(GL_TEXTURE_2D);
            }

            GuiDrawUtils.drawStringCenterXBot(FontType.HelveticaNeueCyrLight, EnumChatFormatting.AQUA + "ESC. " + EnumChatFormatting.WHITE + "Выйти из диалога", x, y, fs, 0xffffff);
        }
    }

    private void drawBackgroundThings(int mouseX, int mouseY, float partialTick) {
        GL11.glColor4f(1f, 1f, 1f, 1f);
        GuiDrawUtils.drawCenter(TextureRegister.textureNPCDialogBorderTop, 115, 60, 1688, 78);
        GuiDrawUtils.drawCenterXBot(TextureRegister.textureNPCDialogBorderBot, 115, 610, 1688, 78);
        GuiDrawUtils.drawCenterXBot(TextureRegister.textureNPCBorderBotIconsCover, 1231, 521, 498, 91);
        GuiDrawUtils.drawCenterXBot(TextureRegister.textureNPCDeviderLine, 308, 848 + dialog.options.size() * 25f, 1303, 2);
        drawScroll();
        drawButtons(mouseX, mouseY, partialTick);
        float x = 292;
        float y = 554;
        float fs = 60 / 32f;
        GuiDrawUtils.drawStringCenterXBot(FontType.HelveticaNeueCyrLight, npc.display.name.toUpperCase(), x, y, fs, 0xffffff);
        fs = 30 / 32f;
        y += 28;
//        GuiDrawUtils.drawStringCenterXBot(FontType.HelveticaNeueCyrLight, "ТОРГОВЕЦ ОРУЖИЕМ", x, y, fs, 0xffffff);
        x = 36;
        y = 107;
        fs = 18 / 32f;
        GuiDrawUtils.drawSplittedStringCenterXBot(FontType.DeadSpace, "Lorem ipsum\u0003dolor sit amet, consec\n" +
                "tetur adipisicing\n" +
                "elit, sed do eiusmod\n" +
                "tempor in\n" +
                "cididunt ut lab\n" +
                "ore et \n" +
                "magna aliqua. U\n" +
                "t enim ad\n" +
                " minim veniam, q\n" +
                "uis nostrud exercit\n" +
                "ation ul\n" +
                "lamco labor\n" +
                "is nisi ut aliqui\n" +
                "p ex ea c\n" +
                "ommodo consequa\n" +
                "t. Dui\n" +
                "s aute ir\n" +
                "ure ", x, y, fs, ScaleGui.get(150), -1, 0x626262);

        x = 1875;
        GuiDrawUtils.drawSplittedRightStringCenterXBot(FontType.DeadSpace, "Lorem ipsum\u0003dolor sit amet, consec\n" +
                "tetur adipisicing\n" +
                "elit, sed do eiusmod\n" +
                "tempor in\n" +
                "cididunt ut lab\n" +
                "ore et \n" +
                "magna aliqua. U\n" +
                "t enim ad\n" +
                " minim veniam, q\n" +
                "uis nostrud exercit\n" +
                "ation ul\n" +
                "lamco labor\n" +
                "is nisi ut aliqui\n" +
                "p ex ea c\n" +
                "ommodo consequa\n" +
                "t. Dui\n" +
                "s aute ir\n" +
                "ure\n" +
                "Lorem ipsum\u0003dolor sit amet, consec\n" +
                "tetur adipisicing\n" +
                "elit, sed do eiusmod\n" +
                "tempor in\n" +
                "cididunt ut lab\n" +
                "ore et \n" +
                "magna aliqua. U\n" +
                "t enim ad\n" +
                " minim veniam, q\n" +
                "uis nostrud exercit\n" +
                "ation ul\n" +
                "lamco labor\n" +
                "is nisi ut aliqui\n" +
                "p ex ea c\n" +
                "ommodo consequa\n" +
                "t. Dui\n" +
                "s aute ir\n" +
                "ure ", x, y, fs, ScaleGui.get(150), -1, 0x626262);
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTick) {
        super.drawScreen(mouseX, mouseY, partialTick);
        drawDefaultBackground();

        GL11.glDisable(GL11.GL_TEXTURE_2D);
        GL11.glColor4f(0f, 0f, 0f, 0.8f);
        GuiDrawUtils.drawCenterXBot(288, 831, 1342, 249);
        GuiDrawUtils.drawCenterXBot(288, 649, 1342, 166);
        GL11.glEnable(GL11.GL_TEXTURE_2D);

        drawBackgroundThings(mouseX, mouseY, partialTick);
        GL11.glColor4f(1f, 1f, 1f, 1f);
        float x = 314;
        float diff = scrollTotalHeight - scrollViewHeight;
        float y = 795 + (diff > 0 ? diff : 0);
        float fs = 30 / 32f;
        float startY = y;

        GL11.glEnable(GL_SCISSOR_TEST);
        int sx = (int) ScaleGui.getCenterX(288);
        int sy = (int) (ScaleGui.get(265));
        int swidth = (int) ScaleGui.get(1342);
        int sheight = (int) ScaleGui.get(165);
        GL11.glScissor(sx, sy, swidth, sheight);
        float nickWidth = 0;
        for(TextBlockClient textBlockClient : lines) {
            nickWidth = Math.max(nickWidth, FontType.HelveticaNeueCyrLight.getFontContainer().width(textBlockClient.getName()) * fs);
        }
        x+=nickWidth;

        for(int i = lines.size() - 1; i >= 0; i--) {
            TextBlockClient textBlockClient = lines.get(i);
            for (int j = textBlockClient.lines.size()-1; j >= 0; j--) {
                IChatComponent iChatComponent = textBlockClient.lines.get(j);
                GuiDrawUtils.drawStringCenterXBot(FontType.HelveticaNeueCyrLight, EnumChatFormatting.WHITE + iChatComponent.getFormattedText(), x, y - scrollAnim, fs, textBlockClient.color);
                y -= 17f;
            }
            y+=17;
            String name = textBlockClient.getName();
            if(name.equals(mc.thePlayer.getDisplayName())) {
                name = EnumChatFormatting.AQUA + name;
            } else {
                name = EnumChatFormatting.GOLD + name;
            }
            float l = FontType.HelveticaNeueCyrLight.getFontContainer().width(name + ": ") * fs;
            GuiDrawUtils.drawStringCenterXBot(FontType.HelveticaNeueCyrLight, name + ": ", x -4 - l, y - scrollAnim, fs, textBlockClient.color);
            y -= 17f;
        }
        scrollTotalHeight = ScaleGui.get(startY - y + 17);
        if(addedDialog) {
            addedDialog = false;
            diff = scrollTotalHeight - scrollViewHeight;
            if(diff > 0) scroll = diff;
        }
        GL11.glDisable(GL_SCISSOR_TEST);
        GL11.glDisable(GL_TEXTURE_2D);
        GL11.glShadeModel(GL_SMOOTH);
        x = ScaleGui.getCenterX(288);
        y = ScaleGui.getBot(650);
        float width = ScaleGui.get(1342);
        float height = ScaleGui.get(165);
        Tessellator tessellator = Tessellator.instance;
        tessellator.startDrawingQuads();
        tessellator.setColorRGBA_F(0f, 0f, 0f, 0.0f);
        tessellator.addVertex(x, y + height,0f);
        tessellator.addVertex(x + width, y + height, 0);
        tessellator.setColorRGBA_F(0f, 0f, 0f, 0.6f);
        tessellator.addVertex(x + width, y, 0f);
        tessellator.addVertex(x, y, 0f);
        tessellator.draw();
        GL11.glShadeModel(GL_FLAT);
        GL11.glEnable(GL_TEXTURE_2D);

        if (!this.dialog.showWheel) {
            this.drawOptions(mouseY);
        }
    }

    public void appendDialog(Dialog dialog) {
        this.closeOnEsc = !dialog.disableEsc;
        this.dialog = dialog;
        this.options = new ArrayList<>();

        if (dialog.sound != null && !dialog.sound.isEmpty()) {
            MusicController.Instance.stopMusic();
            MusicController.Instance.playSound(dialog.sound, (float) this.npc.posX, (float) this.npc.posY, (float) this.npc.posZ);
        }

        TextBlockClient textBlockClient = new TextBlockClient(this.npc, dialog.text, 360, 14737632, this.player, this.npc);
        this.lines.add(textBlockClient);
        addedDialog = true;

        for (int slot : dialog.options.keySet()) {
            DialogOption option = dialog.options.get(slot);

            if (option != null && option.optionType != EnumOptionType.Disabled) {
                this.options.add(slot);
            }
        }

        this.grabMouse(dialog.showWheel);
    }

    private void handleDialogSelection() {
        int optionId = -1;

        if (this.dialog.showWheel) {
            optionId = this.selected;
        } else if (!this.options.isEmpty()) {
            optionId = this.options.get(this.selected);
        }

        NoppesUtilPlayer.sendData(EnumPlayerPacket.Dialog, this.dialog.id, optionId);

        if (this.dialog != null && this.dialog.hasOtherOptions() && !this.options.isEmpty()) {
            DialogOption option = this.dialog.options.get(optionId);

            if (option != null && option.optionType == EnumOptionType.DialogOption) {
                this.lines.add(new TextBlockClient(this.player.getDisplayName(), option.title, 280, option.optionColor, this.player, this.npc));
                NoppesUtil.clickSound();
            } else {
                this.closed();
                this.close();
            }
        } else {
            this.closed();
            this.close();
        }
    }

    @Override
    public void mouseClicked(int mouseX, int mouseY, int mouseButton) {
        super.mouseClicked(mouseX, mouseY, mouseButton);
        if (mouseButton == 0) {
            if(this.selected == -2) {
                NoppesUtilPlayer.sendData(EnumPlayerPacket.Dialog, this.dialog.id, -1);
                this.closed();
                this.close();
            } else if (this.selected == -1 && this.options.isEmpty() || this.selected >= 0) {
                this.handleDialogSelection();
            }

            if (mouseX > scrollX && mouseY > scrollY && mouseX < scrollX + scrollWidth && mouseY < scrollY + scrollHeight) {
                mouseStartY = mouseY;
                mouseScrolling = true;
                lastScroll = scroll;
            }
        }
    }

    @Override
    public void mouseClickMove(int mouseX, int mouseY, int lastButtonClicked, long timeSinceMouseClick) {
        if (mouseScrolling) {
            float heightDiff = scrollTotalHeight - scrollViewHeight;
            if (heightDiff > 0) {
                float diff = scrollTextureHeight / scrollTotalHeight;
                scroll = lastScroll + (mouseY - mouseStartY) / diff;

                if (scroll < 0) {
                    scroll = 0;
                } else if (scroll > scrollTotalHeight - scrollViewHeight) {
                    scroll = scrollTotalHeight - scrollViewHeight;
                }
            }
        }
    }

    @Override
    public void mouseMovedOrUp(int mouseX, int mouseY, int which) {
        mouseScrolling = false;
    }

    @Override
    public void handleMouseInput() {
        super.handleMouseInput();
        int d = Mouse.getEventDWheel();
        int mouseX = Mouse.getX();
        int mouseY = this.height - Mouse.getY() - 1;
        if (d != 0) {
            float x = ScaleGui.getCenterX(288);
            float y = ScaleGui.getBot(650);
            float width = ScaleGui.get(1342);
            float height = ScaleGui.get(165);
            if (mouseX >= x && mouseY >= y && mouseX < x + width && mouseY < y + height) {
                float diff = scrollTotalHeight - scrollViewHeight;
                if (diff > 0) {
                    scroll -= d / 2f;
                    if (scroll < 0) {
                        scroll = 0;
                    } else if (scroll > diff) {
                        scroll = diff;
                    }
                }
            }
        }
    }

    public void keyTyped(char character, int key) {
        if (key == this.mc.gameSettings.keyBindForward.getKeyCode() || key == 200) {
            --this.selected;
        }

        if (key == this.mc.gameSettings.keyBindBack.getKeyCode() || key == 208) {
            ++this.selected;
        }

        if (key == 28) {
            this.handleDialogSelection();
        }

        if (this.closeOnEsc && (key == 1 || key == this.mc.gameSettings.keyBindInventory.getKeyCode())) {
            NoppesUtilPlayer.sendData(EnumPlayerPacket.Dialog, this.dialog.id, -1);
            this.closed();
            this.close();
        }

        super.keyTyped(character, key);
    }

    private void closed() {
        this.grabMouse(false);
        NoppesUtilPlayer.sendData(EnumPlayerPacket.CheckQuestCompletionWithOpenDialog, npc.getEntityId());
    }

    public void grabMouse(boolean grab) {
        if (grab && !this.isGrabbed) {
            Minecraft.getMinecraft().mouseHelper.grabMouseCursor();
            this.isGrabbed = true;
        } else if (!grab && this.isGrabbed) {
            Minecraft.getMinecraft().mouseHelper.ungrabMouseCursor();
            this.isGrabbed = false;
        }
    }

    public void close() {
        this.mc.displayGuiScreen(null);
        this.mc.setIngameFocus();
    }

    public void setClose(int i, NBTTagCompound data) {
        this.grabMouse(false);
    }

    public boolean doesGuiPauseGame()
    {
        return false;
    }
}
