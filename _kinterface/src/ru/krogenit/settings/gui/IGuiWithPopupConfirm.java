package ru.krogenit.settings.gui;

public interface IGuiWithPopupConfirm {
    void applySettings();
    void cancel();
    float getCurrentAspect();
}
