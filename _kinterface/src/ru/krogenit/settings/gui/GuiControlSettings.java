package ru.krogenit.settings.gui;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiOptionButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.resources.I18n;
import net.minecraft.client.settings.GameSettings;
import net.minecraft.client.settings.KeyBinding;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.AbstractGuiScreenAdvanced;
import ru.krogenit.client.gui.api.GuiButtonAnimated;
import ru.krogenit.client.gui.api.GuiDrawUtils;
import ru.krogenit.client.gui.api.ScaleGui;
import ru.krogenit.settings.gui.button.GuiButtonSetting;
import ru.krogenit.settings.gui.slider.GuiSliderSetting;
import ru.xlv.customfont.FontType;

public class GuiControlSettings extends AbstractGuiScreenAdvanced implements IGuiWithPopupConfirm {
    private static final GameSettings.Options[] OPTIONS = new GameSettings.Options[]{GameSettings.Options.INVERT_MOUSE, GameSettings.Options.SENSITIVITY, GameSettings.Options.TOGGLE_LAY};

    private final GuiScreen parentScreen;
    protected String header;

    private final GameSettings options;

    public KeyBinding buttonId = null;
    public long time;
    private GuiKeyBindingListCustom keyBindingList;
    private GuiButtonAnimated resetButton;

    public GuiControlSettings(GuiScreen guiScreen) {
        super(ScaleGui.SXGA);
        this.parentScreen = guiScreen;
        this.options = Minecraft.getMinecraft().gameSettings;
    }

    public void initGui() {
        super.initGui();
        buttonList.clear();
        this.keyBindingList = new GuiKeyBindingListCustom(this, this.mc);
        float buttonWidth = ScaleGui.get(270);
        float buttonHeight = ScaleGui.get(39);
        float x = 960 - 40 - 270;
        float y = 1020;
        GuiButtonAnimated b = new GuiButtonAnimated(200, ScaleGui.getCenterX(x), ScaleGui.getCenterY(y), buttonWidth, buttonHeight, I18n.format("gui.done").toUpperCase());
        b.setTexture(TextureRegister.textureESCButton);
        b.setTextureHover(TextureRegister.textureESCButtonHover);
        b.setMaskTexture(TextureRegister.textureESCButtonMask);
        this.buttonList.add(b);
        x = 960 + 40;
        this.resetButton = new GuiButtonAnimated(201, ScaleGui.getCenterX(x), ScaleGui.getCenterY(y), buttonWidth, buttonHeight, I18n.format("controls.resetAll").toUpperCase());
        resetButton.setTexture(TextureRegister.textureESCButton);
        resetButton.setTextureHover(TextureRegister.textureESCButtonHover);
        resetButton.setMaskTexture(TextureRegister.textureESCButtonMask);
        this.buttonList.add(resetButton);
        this.header = I18n.format("controls.title").toUpperCase();
        x = 960;
        y = 40;
        for (GameSettings.Options options : OPTIONS) {
            if (options.getEnumFloat()) {
                GuiSliderSetting s = new GuiSliderSetting(options.returnEnumOrdinal(), ScaleGui.getCenterX(x) - buttonWidth / 2f, ScaleGui.getCenterY(y), buttonWidth, buttonHeight, options);
                s.setTexture(TextureRegister.textureSettingsButton);
                this.buttonList.add(s);
            } else {
                GuiButtonSetting b1 = new GuiButtonSetting(options.returnEnumOrdinal(), ScaleGui.getCenterX(x) - buttonWidth / 2f, ScaleGui.getCenterY(y), buttonWidth, buttonHeight, options, null);
                b1.setTexture(TextureRegister.textureSettingsButton);
                this.buttonList.add(b1);
            }
            y += 45;
        }
    }

    protected void actionPerformed(GuiButton guiButton) {
        if (guiButton.id == 200) {
            this.mc.displayGuiScreen(this.parentScreen);
            applySettings();
        } else if (guiButton.id == 201) {
            for (KeyBinding keybinding : this.mc.gameSettings.keyBindings) {
                keybinding.setKeyCode(keybinding.getKeyCodeDefault());
            }

            KeyBinding.resetKeyBindingArrayAndHash();
        } else if (guiButton.id < 200 && guiButton instanceof GuiOptionButton) {
            this.options.setOptionValue(((GuiOptionButton) guiButton).returnEnumOptions(), 1);
            guiButton.displayString = this.options.getKeyBinding(GameSettings.Options.getEnumOptions(guiButton.id));
        }
    }

    public void mouseClicked(int mouseX, int mouseY, int mouseButton) {
        for (Object o : buttonList) {
            if (o instanceof GuiButtonSetting) {
                GuiButtonSetting b = (GuiButtonSetting) o;
                if (b.isPressed) b.isPressed = b.isHovered(mouseX, mouseY);
            }
        }

        if (this.buttonId != null) {
            this.options.setOptionKeyBinding(this.buttonId, -100 + mouseButton);
            this.buttonId = null;
            KeyBinding.resetKeyBindingArrayAndHash();
        } else if (mouseButton != 0 || !this.keyBindingList.func_148179_a(mouseX, mouseY, mouseButton)) {
            super.mouseClicked(mouseX, mouseY, mouseButton);
        }
    }

    public void mouseMovedOrUp(int mouseX, int mouseY, int which) {
        if (which != 0 || !this.keyBindingList.func_148181_b(mouseX, mouseY, which)) {
            super.mouseMovedOrUp(mouseX, mouseY, which);
        }
    }

    public void keyTyped(char character, int key) {
        if (this.buttonId != null) {
            if (key == 1) {
                this.options.setOptionKeyBinding(this.buttonId, 0);
            } else {
                this.options.setOptionKeyBinding(this.buttonId, key);
            }

            this.buttonId = null;
            this.time = Minecraft.getSystemTime();
            KeyBinding.resetKeyBindingArrayAndHash();
        } else {
            super.keyTyped(character, key);
        }
    }

    public void drawScreen(int mouseX, int mouseY, float partialTick) {
        super.drawScreen(mouseX, mouseY, partialTick);
        this.drawDefaultBackground();
        this.keyBindingList.drawScreen(mouseX, mouseY, partialTick);
        GuiDrawUtils.drawCenteredStringCenter(FontType.HelveticaNeueCyrLight, this.header, 960, 14, 1.5f, 16777215);
        drawButtons(mouseX, mouseY, partialTick);
        boolean flag = true;

        for (KeyBinding keybinding : this.options.keyBindings) {
            if (keybinding.getKeyCode() != keybinding.getKeyCodeDefault()) {
                flag = false;
                break;
            }
        }

        this.resetButton.enabled = !flag;
        super.drawScreen(mouseX, mouseY, partialTick);
    }

    @Override
    public void applySettings() {
        for (GuiButton o : this.buttonList) {
            if (o instanceof GuiButtonSetting) {
                GuiButtonSetting b = (GuiButtonSetting) o;
                mc.gameSettings.setValue(b.getOption(), b.getCurrentValue());
            } else if (o instanceof GuiSliderSetting) {
                GuiSliderSetting b = (GuiSliderSetting) o;
                mc.gameSettings.setOptionFloatValue(b.getOption(), b.getOption().denormalizeValue(b.value));
            }
        }

        mc.gameSettings.saveOptions();
    }

    @Override
    public void cancel() {

    }

    @Override
    public float getCurrentAspect() {
        return minAspect;
    }
}
