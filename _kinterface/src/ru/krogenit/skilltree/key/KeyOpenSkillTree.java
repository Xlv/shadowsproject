package ru.krogenit.skilltree.key;

import net.minecraft.client.settings.KeyBinding;
import ru.krogenit.client.key.AbstractKey;
import ru.krogenit.skilltree.gui.GuiSkillTree;

public class KeyOpenSkillTree extends AbstractKey {
    private boolean keyDown = false;
    private boolean keyUp = true;

    public KeyOpenSkillTree(KeyBinding keyBindings) {
        super(keyBindings);
    }

    @Override
    public void keyDown() {
        if (mc.currentScreen == null && !keyDown && mc.thePlayer != null) {
            mc.displayGuiScreen(new GuiSkillTree());
        }
    }

    @Override
    public void keyUp() {
        if (!keyUp) {
            keyDown = false;
            keyUp = true;
        }
    }
}
