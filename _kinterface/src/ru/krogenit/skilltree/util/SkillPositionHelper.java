package ru.krogenit.skilltree.util;

import org.lwjgl.util.vector.Vector2f;
import ru.xlv.core.skill.Skill;

public class SkillPositionHelper {
    public static Vector2f getSkillPosition(Skill skill) {
        switch (skill.getId()) {
            case 0: return new Vector2f(81, -426);
            case 1: return new Vector2f(192, -532);
            case 2: return new Vector2f(260, -532);
            case 3: return new Vector2f(328, -532);

            case 4: return new Vector2f(192, -426);
            case 5: return new Vector2f(328, -468);
            case 6: return new Vector2f(396, -468);
            case 7: return new Vector2f(464, -468);

            case 8: return new Vector2f(82, -175);
            case 9: return new Vector2f(192, -64);
            case 10: return new Vector2f(260, -64);
            case 11: return new Vector2f(328, -64);

            case 12: return new Vector2f(192, -175);
            case 13: return new Vector2f(336, -132);
            case 14: return new Vector2f(404, -132);
            case 15: return new Vector2f(472, -132);

            case 16: return new Vector2f(192, -302);
            case 17: return new Vector2f(124, -302);
            case 18: return new Vector2f(52, -302);
            case 19: return new Vector2f(-21, -302);

            case 20: return new Vector2f(519, -302);
            case 21: return new Vector2f(587, -302);
            case 22: return new Vector2f(655, -302);

            case 24: return new Vector2f(-161, -383);
            case 25: return new Vector2f(-254, -383);
            case 26: return new Vector2f(-331, -383);
            case 27: return new Vector2f(-407, -383);

            case 28: return new Vector2f(-161, -150);
            case 29: return new Vector2f(-254, -150);
            case 30: return new Vector2f(-331, -150);
            case 31: return new Vector2f(-407, -150);

            case 32: return new Vector2f(-243, -277);
            case 33: return new Vector2f(-320, -277);
            case 34: return new Vector2f(-397, -277);
            case 35: return new Vector2f(-473, -277);

            case 36: return new Vector2f(-558, -277);
            case 37: return new Vector2f(-651, -277);
            case 38: return new Vector2f(-745, -277);

            case 39: return new Vector2f(-556, 113);
            case 40: return new Vector2f(-437, 62);
            case 41: return new Vector2f(-309, 62);
            case 42: return new Vector2f(-182, 62);

            case 43: return new Vector2f(-556, 343);
            case 44: return new Vector2f(-437, 402);
            case 45: return new Vector2f(-309, 402);
            case 46: return new Vector2f(-173, 402);

            case 47: return new Vector2f(463, 113);
            case 48: return new Vector2f(362, 62);
            case 49: return new Vector2f(268, 62);
            case 50: return new Vector2f(175, 62);

            case 51: return new Vector2f(464, 343);
            case 52: return new Vector2f(362, 402);
            case 53: return new Vector2f(269, 402);
            case 54: return new Vector2f(175, 402);

            case 55: return new Vector2f(34, 232);
            case 56: return new Vector2f(34, 292);
            case 57: return new Vector2f(34, 351);

            case 58: return new Vector2f(229, 504);
            case 59: return new Vector2f(339, 504);
            case 60: return new Vector2f(450, 504);
            case 61: return new Vector2f(560, 504);

            case 62: return new Vector2f(229, 580);
            case 63: return new Vector2f(339, 580);
            case 64: return new Vector2f(450, 580);
            case 65: return new Vector2f(560, 580);
            default: return new Vector2f(0,0);
        }
    }
}
