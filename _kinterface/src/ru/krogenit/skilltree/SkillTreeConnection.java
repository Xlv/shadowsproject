package ru.krogenit.skilltree;

import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector2f;
import ru.krogenit.skilltree.gui.GuiSkill;
import ru.krogenit.skilltree.type.EnumSkillTreeAvailableType;

public class SkillTreeConnection {

    private final GuiSkill parent;
    private final GuiSkill child;
    private final Vector2f[] points;
    private final ConnectionAnim[] connectionAnims;

    public SkillTreeConnection(GuiSkill parent, GuiSkill child) {
        this.parent = parent;
        this.child = child;
        this.points = new Vector2f[4];
        this.connectionAnims = new ConnectionAnim[3];
        for (int i = 0; i < points.length; i++) points[i] = new Vector2f();
        calculatePoints();
        for (int i = 0; i < connectionAnims.length; i++)
            connectionAnims[i] = new ConnectionAnim(points[i], points[i + 1]);
    }

    private void calculatePoints() {
        float x1 = parent.xPosition + parent.width / 2f;
        float y1 = parent.yPosition + parent.height / 2f;
        float x2 = child.xPosition + child.width / 2f;
        float y2 = child.yPosition + child.height / 2f;
        float centerX = (x1 + x2) / 2f;

        points[0].x = x1;
        points[0].y = y1;
        points[1].x = centerX;
        points[1].y = y1;
        points[2].x = centerX;
        points[2].y = y2;
        points[3].x = x2;
        points[3].y = y2;
    }

    public void drawLines() {
        calculatePoints();
        if (child.getAvailableType() == EnumSkillTreeAvailableType.AVAILABLE) {
            GL11.glColor4f(35 / 255f, 121 / 255f, 140 / 255f, 1f);
        } else if (child.getAvailableType() == EnumSkillTreeAvailableType.LOCKED) {
            GL11.glColor4f(92 / 255f, 92 / 255f, 92 / 255f, 1f);
        } else {
            GL11.glColor4f(0.8f, 0.8f, 0.8f, 0.95f);
        }

        for (int i = 0; i < points.length - 1; i++) {
            GL11.glVertex2f(points[i].x, points[i].y);
            GL11.glVertex2f(points[i + 1].x, points[i + 1].y);
        }
    }

    public void drawEffects(float lineWidth, Vector2f treePosAnim, float scaleAnim) {
        if (child.getAvailableType() == EnumSkillTreeAvailableType.AVAILABLE) {
            GL11.glColor4f(35 / 155f, 121 / 155f, 140 / 155f, 1f);
        } else if (child.getAvailableType() == EnumSkillTreeAvailableType.LOCKED) {
            return;
        } else {
            GL11.glColor4f(1f, 1f, 1f, 1f);
        }

        for (ConnectionAnim connectionAnim : connectionAnims) {
            connectionAnim.update();
            if (!connectionAnim.isHalfComplete()) break;
        }

        boolean animationComplete = true;
        for (ConnectionAnim connectionAnim : connectionAnims) {
            if (!connectionAnim.isComplete()) {
                connectionAnim.draw(lineWidth, treePosAnim, scaleAnim);
                animationComplete = false;
            }
        }

        if (animationComplete) {
            for (ConnectionAnim connectionAnim : connectionAnims)
                connectionAnim.reset();
        }
    }

    public GuiSkill getChild() {
        return child;
    }

    public GuiSkill getParent() {
        return parent;
    }
}
