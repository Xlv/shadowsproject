package ru.krogenit.skilltree.gui;

import net.minecraft.client.gui.GuiButton;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.AbstractGuiScreenAdvanced;
import ru.krogenit.client.gui.api.GuiButtonAnimated;
import ru.krogenit.client.gui.api.GuiDrawUtils;
import ru.krogenit.client.gui.api.ScaleGui;
import ru.xlv.customfont.FontType;

public class GuiConfirmSaveBuild extends AbstractGuiScreenAdvanced {

    private final GuiSkillTree parent;
    private final GuiButtonBuildSkillTree buildButton;

    public GuiConfirmSaveBuild(GuiSkillTree parent, GuiButtonBuildSkillTree buildButton) {
        this.parent = parent;
        this.mc = parent.mc;
        this.buildButton = buildButton;
        initGui();
    }

    @Override
    public void initGui() {
        super.initGui();
        buttonList.clear();

        float buttonWidth = 171;
        float buttonHeight = 39;
        float x = 960;
        float y = -24 + 540;
        GuiButtonAnimated button = new GuiButtonAnimated(0, ScaleGui.getCenterX(x, buttonWidth),  ScaleGui.getCenterY(y, buttonHeight), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "ПОДТВЕРДИТЬ");
        button.setTexture(TextureRegister.textureTreeButtonAccept);
        button.setTextureHover(TextureRegister.textureTreeButtonHover);
        button.setMaskTexture(TextureRegister.textureTreeButtonMask);
        buttonList.add(button);

        y += buttonHeight * 1.35f;
        button = new GuiButtonAnimated(1, ScaleGui.getCenterX(x, buttonWidth),  ScaleGui.getCenterY(y, buttonHeight), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "ОТМЕНИТЬ");
        button.setTexture(TextureRegister.textureTreeButtonCancel);
        button.setTextureHover(TextureRegister.textureTreeButtonHover);
        button.setMaskTexture(TextureRegister.textureTreeButtonMask);
        buttonList.add(button);
    }

    @Override
    protected void actionPerformed(GuiButton guiButton) {
        if(guiButton.id == 0) {
            parent.changeBuild(true, buildButton.id);
            parent.setPopup(null);
        } else {
            parent.changeBuild(false, buildButton.id);
            parent.setPopup(null);
        }
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTick) {
        GuiDrawUtils.renderBuildConfirmPopup(960-317, 540-220, 960+317, 540+90, ScaleGui.get(27f), 27/255f, 48/255f, 23/255f);
        drawButtons(mouseX, mouseY, partialTick);
        float x = 960;
        float y = 540-164;
        float fs = 1.3f;
        GuiDrawUtils.drawCenteredStringCenter(FontType.HelveticaNeueCyrLight, "СОХРАНИТЬ НАБОР НАВЫКОВ №"+(parent.getCurrentBuild()+1) + "?", x, y, fs, 0xffffff);

        float iconWidth = 502;
        float iconHeight = 1;
        y += 25;
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureTreeBorderConfirm, x , y, iconWidth, iconHeight);
        y += 22;
        fs = 0.85f;
        GuiDrawUtils.drawCenteredStringCenter(FontType.HelveticaNeueCyrLight, "Сохраненные наборы можно использовать в любой момент игры, если вы не", x, y, fs, 0xffffff);
        y+=FontType.HelveticaNeueCyrLight.getFontContainer().height() * fs;
        GuiDrawUtils.drawCenteredStringCenter(FontType.HelveticaNeueCyrLight, "находитесь в состоянии боя. Прочтение способности проходит в течение 10 секунд.", x, y, fs, 0xffffff);
        y+=FontType.HelveticaNeueCyrLight.getFontContainer().height() * fs + 6;
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureTreeBorderConfirm, x , y, iconWidth, iconHeight);
    }
}
