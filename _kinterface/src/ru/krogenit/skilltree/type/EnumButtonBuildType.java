package ru.krogenit.skilltree.type;

public enum EnumButtonBuildType {
    LOCKED, AVAILABLE, CURRENT;
}
