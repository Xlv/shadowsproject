package ru.krogenit.skilltree.type;

public enum EnumSlotSkillPassiveType {
    LOCKED, AVAILABLE;
}
