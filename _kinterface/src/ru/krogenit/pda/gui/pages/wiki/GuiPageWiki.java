package ru.krogenit.pda.gui.pages.wiki;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.*;
import ru.krogenit.pda.gui.GuiPda;
import ru.krogenit.utils.AnimationHelper;
import ru.xlv.customfont.FontType;

import java.util.ArrayList;
import java.util.List;

import static org.lwjgl.opengl.GL11.glScissor;

public class GuiPageWiki extends AbstractGuiScreenAdvanced {

    private final GuiPda pda;
    private float firstPoint, fs1;

    private float scroll, scrollAnim, lastScroll;
    private float scrollTotalHeight, scrollViewHeight, scrollTextureHeight;
    private float scrollX, scrollY, scrollWidth, scrollHeight;
    private boolean mouseScrolling;
    private float mouseStartY;
    private final List<GuiSlotWiki> wikiSlots = new ArrayList<>();

    private final EnumWikiSection section;
    private final String header;
    private GuiPageWiki previous;

    private static final List<WikiItem> wikiArmorItems = new ArrayList<>();

    static {
        List<ResourceLocation> textures = new ArrayList<>();
        textures.add(TextureRegister.texturePDAWikiItem6);
        wikiArmorItems.add(new WikiItem(textures, "SPECR-I-O-T", EnumWikiItemType.ARMORED_SUIT, EnumWikiSection.ARMOR));
        textures = new ArrayList<>();
        textures.add(TextureRegister.texturePDAWikiItem7);
        wikiArmorItems.add(new WikiItem(textures, "SHELL", EnumWikiItemType.SPACESUIT, EnumWikiSection.ARMOR));
        textures = new ArrayList<>();
        textures.add(TextureRegister.texturePDAWikiItem8);
        wikiArmorItems.add(new WikiItem(textures, "SILENCE", EnumWikiItemType.ARMORED_SUIT, EnumWikiSection.ARMOR));
        textures = new ArrayList<>();
        textures.add(TextureRegister.texturePDAWikiItem9);
        wikiArmorItems.add(new WikiItem(textures, "DISASTER", EnumWikiItemType.ARMORED_SUIT, EnumWikiSection.ARMOR));
        textures = new ArrayList<>();
        textures.add(TextureRegister.texturePDAWikiItem10);
        wikiArmorItems.add(new WikiItem(textures, "PALADIN", EnumWikiItemType.ARMORED_SUIT, EnumWikiSection.ARMOR));
        textures = new ArrayList<>();
        textures.add(TextureRegister.texturePDAWikiItem11);
        wikiArmorItems.add(new WikiItem(textures, "CENSOR", EnumWikiItemType.ARMORED_SUIT, EnumWikiSection.ARMOR));

    }

    public GuiPageWiki(float minAspect, GuiPda pda, EnumWikiSection section) {
        super(minAspect);
        this.pda = pda;
        this.header = section.getLocalizedName();
        this.section = section;
    }

    @Override
    public void initGui() {
        super.initGui();
        buttonList.clear();
        wikiSlots.clear();
        scrollViewHeight = 570;

        float buttonWidth = 228;
        float buttonHeight = 40;
        float x = ScaleGui.getCenterX(1594, buttonWidth);
        float y = ScaleGui.getCenterY(202, buttonHeight);
        GuiButtonAnimated b = new GuiButtonAnimated(0, x, y, ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "СЛУЧАЙНАЯ СТАТЬЯ");
        b.setTexture(TextureRegister.textureESCButton);
        b.setTextureHover(TextureRegister.textureESCButtonHover);
        b.setMaskTexture(TextureRegister.textureESCButtonMask);
        buttonList.add(b);

        buttonWidth = 38;
        buttonHeight = 37;
        x = 1548;
        y = 885;
        GuiButtonAdvanced t = new GuiButtonAdvanced(1, ScaleGui.getCenterX(x, buttonWidth), ScaleGui.getCenterY(y, buttonHeight), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "");
        t.setTexture(TextureRegister.texturePDAWikiButtonHome);
        t.setTextureHover(TextureRegister.texturePDAWikiButtonHomeHover);
        buttonList.add(t);
        x += buttonWidth + 9;
        t = new GuiButtonAdvanced(2, ScaleGui.getCenterX(x, buttonWidth), ScaleGui.getCenterY(y, buttonHeight), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "");
        t.setTexture(TextureRegister.texturePDAWikiButtonBack);
        t.setTextureHover(TextureRegister.texturePDAWikiButtonBackHover);
        buttonList.add(t);
        x += buttonWidth + 9;
        t = new GuiButtonAdvanced(3, ScaleGui.getCenterX(x, buttonWidth), ScaleGui.getCenterY(y, buttonHeight), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "");
        t.setTexture(TextureRegister.texturePDAWikiButtonLeft);
        t.setTextureHover(TextureRegister.texturePDAWikiButtonLeftHover);
        buttonList.add(t);
        x += buttonWidth + 9;
        t = new GuiButtonAdvanced(4, ScaleGui.getCenterX(x, buttonWidth), ScaleGui.getCenterY(y, buttonHeight), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "");
        t.setTexture(TextureRegister.texturePDAWikiButtonRight);
        t.setTextureHover(TextureRegister.texturePDAWikiButtonRightHover);
        buttonList.add(t);

        buttonWidth = 342;
        buttonHeight = 275;
        float startX = 783;
        x = startX;
        y = 398;
        float yStart = y;
        float yOffset = buttonHeight + 19;
        float xOffset = buttonWidth + 19;

        if(section == EnumWikiSection.ALL) {
            GuiSlotWiki s = new GuiSlotWiki(EnumWikiSection.LOCATIONS, ScaleGui.getCenterX(x, buttonWidth), ScaleGui.getCenterY(y, buttonHeight), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight));
            s.setTexture(TextureRegister.texturePDAWikiItem1);
            s.setOpened(mc.theWorld.rand.nextInt(8));
            s.setTotal(mc.theWorld.rand.nextInt(20) + 10);
            s.setTotalToNextRank(s.getOpened() + 10);
            s.setWikiSectionRank(EnumWikiSectionRank.NOVICE);
            wikiSlots.add(s);
            x += xOffset;
            s = new GuiSlotWiki(EnumWikiSection.CREATURES, ScaleGui.getCenterX(x, buttonWidth), ScaleGui.getCenterY(y, buttonHeight), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight));
            s.setTexture(TextureRegister.texturePDAWikiItem2);
            s.setOpened(mc.theWorld.rand.nextInt(8));
            s.setTotal(mc.theWorld.rand.nextInt(20) + 10);
            s.setTotalToNextRank(s.getOpened() + 10);
            s.setWikiSectionRank(EnumWikiSectionRank.NOVICE);
            wikiSlots.add(s);
            x += xOffset;
            s = new GuiSlotWiki(EnumWikiSection.ARMOR, ScaleGui.getCenterX(x, buttonWidth), ScaleGui.getCenterY(y, buttonHeight), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight));
            s.setTexture(TextureRegister.texturePDAWikiItem3);
            s.setOpened(mc.theWorld.rand.nextInt(8));
            s.setTotal(mc.theWorld.rand.nextInt(20) + 10);
            s.setTotalToNextRank(s.getOpened() + 10);
            s.setWikiSectionRank(EnumWikiSectionRank.NOVICE);
            wikiSlots.add(s);
            y += yOffset;
            x = 783;
            s = new GuiSlotWiki(EnumWikiSection.WEAPON, ScaleGui.getCenterX(x, buttonWidth), ScaleGui.getCenterY(y, buttonHeight), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight));
            s.setTexture(TextureRegister.texturePDAWikiItem4);
            s.setOpened(mc.theWorld.rand.nextInt(8));
            s.setTotal(mc.theWorld.rand.nextInt(20) + 10);
            s.setTotalToNextRank(s.getOpened() + 10);
            s.setWikiSectionRank(EnumWikiSectionRank.NOVICE);
            wikiSlots.add(s);
            y += yOffset;
            x = 783;
            s = new GuiSlotWiki(EnumWikiSection.MECHANICS, ScaleGui.getCenterX(x, buttonWidth), ScaleGui.getCenterY(y, buttonHeight), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight));
            s.setTexture(TextureRegister.texturePDAWikiItem5);
            s.setOpened(mc.theWorld.rand.nextInt(8));
            s.setTotal(mc.theWorld.rand.nextInt(20) + 10);
            s.setTotalToNextRank(s.getOpened() + 10);
            s.setWikiSectionRank(EnumWikiSectionRank.NOVICE);
            wikiSlots.add(s);
        } else if(section == EnumWikiSection.ARMOR) {
            for(WikiItem item : wikiArmorItems) {
                if(item.getSection() == section) {
                    GuiSlotWikiItem s = new GuiSlotWikiItem(item, ScaleGui.getCenterX(x, buttonWidth), ScaleGui.getCenterY(y, buttonHeight), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight));
                    s.setTexture(item.getTextures().get(0));
                    wikiSlots.add(s);
                    x += xOffset;

                    if(x > 1724) {
                        x = startX;
                        y += yOffset;
                    }
                }
            }
            y -= yOffset;
        }

        scrollTotalHeight = y + buttonHeight - yStart;
    }

    @Override
    protected void actionPerformed(GuiButton b) {
//        if(b instanceof GuiSlotWikiItem) {
//
//        } else
            if(b instanceof GuiSlotWiki) {
            pda.showWikiPage(((GuiSlotWiki) b).getWikiSection(), this);
        } else {
            if(b.id == 0) {
                pda.showRandomWikiPage();
            } else if(b.id == 1) {
                pda.showWikiPage(EnumWikiSection.ALL, this);
            } else if(b.id == 2) {
                if(previous != null) {
                    pda.showPreviousWikiPage(previous);
                }
            } else if(b.id == 3) {
                if(section != EnumWikiSection.ALL) pda.wikiLeftButton(this);
            } else if(b.id == 4) {
                if(section != EnumWikiSection.ALL) pda.wikiRightButton(this);
            }
        }

    }

    private void updateAnimation() {
        firstPoint = AnimationHelper.updateSlowEndAnim(firstPoint, 1f, 0.1f, 0.0001f);
        if(firstPoint > 0.9f) {
            fs1 = AnimationHelper.updateSlowEndAnim(fs1, 1f, 0.1f, 0.0001f);
        }
    }

    private void renderScroll() {
        if(scrollAnim < scroll) {
            scrollAnim = AnimationHelper.updateSlowEndAnim(scrollAnim, scroll, 0.25f, 0.1f);
        } else if(scrollAnim > scroll) {
            scrollAnim = AnimationHelper.updateSlowEndAnim(scrollAnim, scroll, -0.25f, -0.1f);
        }

        float x = 1704;
        float y = 262;
        float iconWidth = 7;
        float iconHeight = 4;
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvArrowTop, x, y, iconWidth, iconHeight);
        y += iconHeight + 2;
        scrollTextureHeight = 555;
        float scrollValue = scrollViewHeight / scrollTotalHeight;
        float scrollYValue = scrollTextureHeight / scrollTotalHeight;
        if(scrollValue > 1) scrollValue = 1f;
        if(scrollYValue > 1) scrollYValue = 1f;
        iconHeight = scrollTextureHeight * scrollValue;
        y += iconHeight / 2f + scrollAnim * scrollYValue;
        scrollX = ScaleGui.getCenterX(x, iconWidth);
        scrollY = ScaleGui.getCenterY(y, iconHeight);
        scrollWidth = ScaleGui.get(iconWidth);
        scrollHeight = ScaleGui.get(iconHeight);
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvScrollBody, x, y, iconWidth, iconHeight);
        y = 828;
        iconHeight = 4;
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvArrowBot, x, y, iconWidth, iconHeight);
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTick) {
        updateAnimation();
        GL11.glColor4f(1f, 1f, 1f,1f);
        GuiDrawUtils.drawCenterCentered(TextureRegister.texturePDAProfileBorderDevider, 1160, 241, 1096 * fs1, 2);
        GuiDrawUtils.drawCenterCentered(TextureRegister.texturePDAProfileBorderDevider, 1160, 851, 1096 * fs1, 2);
        renderScroll();

        for (GuiSlotWiki slot : wikiSlots) {
            slot.addYPosition(ScaleGui.get(-scrollAnim));
        }

        float x = 630;
        float y = 201;
        float iconWidth = 37;
        float stringXOffset = 27;
        float fs = 1.15f  * fs1;
        GuiDrawUtils.drawCenterCentered(TextureRegister.texturePDAIconCircle, x, y, iconWidth * firstPoint, iconWidth * firstPoint);
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, header, x + stringXOffset, y - 3, fs, 0xffffff);

        y = ScaleGui.getCenterY(231, 0);
        float height = ScaleGui.getCenterY(839, 0) - y;
        glScissor(pda.scissorX, (int)y,pda.scissorWidth , (int)height);
        for(GuiSlotWiki slot : wikiSlots) {
            slot.drawButton(mouseX, mouseY);
        }
        glScissor(pda.scissorX, pda.scissorY, pda.scissorWidth, pda.scissorHeight);

        drawButtons(mouseX, mouseY, partialTick);
    }

    @Override
    public void mouseClicked(int mouseX, int mouseY, int mouseButton) {
        if (mouseButton == 0) {
            for (GuiButton guibutton : this.buttonList) {
                if (guibutton.mousePressed(this.mc, mouseX, mouseY)) {
                    this.selectedButton = guibutton;
                    guibutton.playClickSound(this.mc.getSoundHandler());
                    this.actionPerformed(guibutton);
                    return;
                }
            }

            for(GuiSlotWiki slot : wikiSlots) {
                if(slot.mousePressed(mc, mouseX, mouseY)) {
                    slot.playClickSound(this.mc.getSoundHandler());
                    this.actionPerformed(slot);
                    return;
                }
            }

            if(mouseX > scrollX && mouseY > scrollY && mouseX < scrollX + scrollWidth && mouseY < scrollY + scrollHeight) {
                mouseStartY = mouseY;
                mouseScrolling = true;
                lastScroll = scroll;
            }
        }
    }

    @Override
    public void mouseClickMove(int mouseX, int mouseY, int lastButtonClicked, long timeSinceMouseClick) {
        if(mouseScrolling) {
            float heightDiff = scrollTotalHeight - scrollViewHeight;
            if(heightDiff > 0) {
                float diff = ScaleGui.get(scrollTextureHeight) / scrollTotalHeight;
                scroll = lastScroll + (mouseY - mouseStartY) / diff;

                if (scroll < 0) {
                    scroll = 0;
                } else if (scroll > scrollTotalHeight - scrollViewHeight) {
                    scroll = scrollTotalHeight - scrollViewHeight;
                }
            }
        }
    }

    @Override
    public void mouseMovedOrUp(int mouseX, int mouseY, int which) {
        mouseScrolling = false;
    }

    @Override
    public void scrollInput(int mouseX, int mouseY, int d) {
        if (d != 0) {
            float diff = scrollTotalHeight - scrollViewHeight;
            if(diff > 0) {
                scroll -= d / 2f;
                if(scroll < 0) {
                    scroll = 0;
                } else if(scroll > diff) {
                    scroll = diff;
                }
            }
        }
    }

    public void setPrevious(GuiPageWiki previous) {
        this.previous = previous;
    }

    public EnumWikiSection getSection() {
        return section;
    }
}
