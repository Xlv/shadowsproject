package ru.krogenit.pda.gui.pages.wiki;

public enum EnumWikiItemType {

    ARMORED_SUIT("БРОНЕКОСТЮМ"), SPACESUIT("СКАФАНДР");

    private String localized;

    EnumWikiItemType(String localized) {
        this.localized = localized;
    }

    public String getLocalizedName() {
        return localized;
    }
}
