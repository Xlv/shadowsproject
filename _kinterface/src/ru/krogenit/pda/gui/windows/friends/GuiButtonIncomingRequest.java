package ru.krogenit.pda.gui.windows.friends;

import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.EnumStringRenderType;
import ru.krogenit.pda.gui.windows.ButtonContent;
import ru.krogenit.pda.gui.windows.IGuiWindowWithSubActions;
import ru.krogenit.pda.gui.windows.players.GuiButtonPlayer;
import ru.xlv.core.common.player.character.CharacterType;

public class GuiButtonIncomingRequest extends GuiButtonPlayer {

    public GuiButtonIncomingRequest(int id, float x, float y, float width, float height, IGuiWindowWithSubActions guiWindow, String playerName,
                                    CharacterType characterType, int level) {
        super(id, x, y, width, height, guiWindow, playerName, characterType, level, 0, true, false, false);
        contents.clear();
        addContent(ButtonContent.newBuilder().setString("" + id).setStringRenderType(EnumStringRenderType.CENTERED).setWidth(38).build());
        addContent(ButtonContent.newBuilder().setString(playerName).setWidth(283).build());
        addContent(ButtonContent.newBuilder().setTexture(TextureRegister.getClassIcon(characterType)).setWidth(38).build());
        addContent(ButtonContent.newBuilder().setString("" + level).setStringRenderType(EnumStringRenderType.CENTERED).setWidth(38).build());
        addContent(ButtonContent.newBuilder().setTexture(TextureRegister.texturePDAWinFriendsIconAccept).setId(0).setWidth(38).setTextureOffset(6).build());
        addContent(ButtonContent.newBuilder().setTexture(TextureRegister.texturePDAWinFriendsIconDecline).setId(1).setWidth(38).setTextureOffset(6).build());
    }
}
