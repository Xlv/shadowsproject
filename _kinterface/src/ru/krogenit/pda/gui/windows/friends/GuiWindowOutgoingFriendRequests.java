package ru.krogenit.pda.gui.windows.friends;

import net.minecraft.client.gui.GuiButton;
import org.lwjgl.input.Keyboard;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.GuiButtonAnimated;
import ru.krogenit.client.gui.api.ScaleGui;
import ru.krogenit.pda.EnumPdaLeftMenuSection;
import ru.krogenit.pda.gui.GuiPda;
import ru.krogenit.pda.gui.windows.EnumWindowType;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.player.character.CharacterType;
import ru.xlv.core.player.ClientPlayer;
import ru.xlv.friend.common.FriendRelation;
import ru.xlv.friend.network.PacketFriendPlayerListGet;

import java.util.List;

public class GuiWindowOutgoingFriendRequests extends GuiWindowIncomingFriendRequests {

    public GuiWindowOutgoingFriendRequests(GuiPda pda, float width, float height) {
        super(pda, width, height);
        setHeader("ИСХОДЯЩИЕ ЗАЯВКИ");
        setLeftCornerDesc("FILE №81022012_CODE_REQ");
        setBackgroundTexture(TextureRegister.texturePDAWinAchieveBg);
        this.windowType = EnumWindowType.OUTGOING_FRIEND_REQUESTS;
    }

    @Override
    public void initButtons() {
        float buttonWidth = 159;
        float buttonHeight = 39;
        float x = ScaleGui.get(458);
        float y = ScaleGui.get(71);
        GuiButtonAnimated b = new GuiButtonAnimated(2, x, y, ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "ВХОДЯЩИЕ");
        b.setTexture(TextureRegister.textureTreeButtonAccept);
        b.setTextureHover(TextureRegister.textureTreeButtonHover);
        b.setMaskTexture(TextureRegister.textureTreeButtonMask);
        buttonList.add(b);
    }

    @Override
    protected void initPlayers() {
        float buttonWidth = ScaleGui.get(996);
        float buttonHeight = ScaleGui.get(37);

        int id = 1;
        List<FriendRelation> allOutgoingInvites = friendHandler.getAllOutgoingInvites();
        for(FriendRelation friendRelation : allOutgoingInvites) {
            //TODO: получение уровня и класса
            GuiButtonOutgoingRequest s = new GuiButtonOutgoingRequest(id, 0, 0, buttonWidth, buttonHeight, this, friendRelation.getTarget(), CharacterType.MEDIC,
                    0);
            players.add(s);
            playerByName.put(friendRelation.getTarget(), s);
            id++;
        }
    }

    @Override
    protected void getNewInfo(int offset) {
        XlvsCore.INSTANCE.getPacketHandler().sendPacketEffectiveCallback(new PacketFriendPlayerListGet(PacketFriendPlayerListGet.FilterType.OUTGOING_INVITE, offset)).thenAcceptSync(clientPlayers -> {
            if(clientPlayers.size() > 0) {
                for (ClientPlayer clientPlayer : clientPlayers) {
                    GuiButtonOutgoingRequest request = (GuiButtonOutgoingRequest) playerByName.get(clientPlayer.getPlayerName());
                    if (request != null) {
                        request.setCharacterType(clientPlayer.getCharacterType());
                        request.setLevel(clientPlayer.getLevel());
                    }
                }
            }
        });

        requestedElements += 20;
    }

    @Override
    protected void onFriendsSynced() {
        super.onFriendsSynced();
    }

    @Override
    protected void actionPerformed(GuiButton b) {
        if(b.id == 2) {
            pda.showIncomingFriendRequests(); return;
        }
        super.actionPerformed(b);
    }

    @Override
    public void onWindowClosed() {
        Keyboard.enableRepeatEvents(false);
        if(!pda.isWindowOpened(EnumWindowType.INCOMING_FRIEND_REQUESTS))
            pda.setLeftMenuSubButtonPressed(EnumPdaLeftMenuSection.COMMUNITY_REQUESTS, false);
    }
}
