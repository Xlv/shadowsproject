package ru.krogenit.pda.gui.windows.players;

import lombok.Setter;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiPlayerInfo;
import net.minecraft.client.network.NetHandlerPlayClient;
import net.minecraft.util.AxisAlignedBB;
import org.apache.commons.lang3.StringUtils;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.*;
import ru.krogenit.pda.EnumPdaLeftMenuSection;
import ru.krogenit.pda.gui.GuiPda;
import ru.krogenit.pda.gui.windows.*;
import ru.krogenit.pda.gui.windows.mail.GuiWindowWriteMessage;
import ru.krogenit.shop.field.GuiSearchField;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.player.ClientMainPlayer;
import ru.xlv.core.player.ClientPlayer;
import ru.xlv.core.player.ClientPlayerManager;
import ru.xlv.customfont.FontContainer;
import ru.xlv.customfont.FontType;
import ru.xlv.friend.XlvsFriendMod;
import ru.xlv.friend.handle.FriendHandler;
import ru.xlv.friend.network.PacketFriendInviteRequest;
import ru.xlv.friend.network.PacketFriendRemove;
import ru.xlv.friend.network.PacketFriendSync;
import ru.xlv.group.XlvsGroupMod;
import ru.xlv.group.common.Group;
import ru.xlv.group.network.PacketGroupInvite;
import ru.xlv.group.network.PacketGroupSync;
import ru.xlv.mochar.XlvsMainMod;

import java.util.*;

import static org.lwjgl.opengl.GL11.glScissor;

public class GuiWindowPlayers extends GuiWindow implements IGuiSearch, IGuiSortable, IGuiWindowWithSubActions {

    protected GuiSearchField searchField;
    @Setter
    private int playersCount;
    @Setter
    private long ping;
    private boolean searchable;

    protected final List<GuiButtonPlayer> players = new ArrayList<>();
    protected final Map<String, GuiButtonPlayer> playerByName = new HashMap<>();
    protected final List<GuiButtonPlayer> filteredResults = new ArrayList<>();
    protected final FriendHandler friendHandler = XlvsFriendMod.INSTANCE.getFriendHandler();
    protected int requestedElements = 0;

    public GuiWindowPlayers(GuiPda pda, float width, float height, boolean searchable) {
        this(pda, width, height);
        this.searchable = searchable;
    }

    public GuiWindowPlayers(GuiPda pda, float width, float height) {
        super(EnumWindowType.PLAYERS, pda, width, height);
        syncFriends();
        syncGroups();
        setHeader("СЕРВЕР");
        setLeftCornerDesc("FILE №89065012_CODE_SERVER");
        setBackgroundTexture(TextureRegister.texturePDAWinPlayersBg);
        this.searchable = true;
        ping = pda.getPing();
        playersCount = pda.getPlayersCount();
    }

    @Override
    public void initGui() {
        super.initGui();
        initButtons();

        if(searchable) searchField = new GuiSearchField(ScaleGui.get(45), ScaleGui.get(150),ScaleGui.get(278), ScaleGui.get(37), ScaleGui.get(28 / 32f), "Поиск по никнейму...", this);
        initColumnButtons();
        refreshValues();
        Keyboard.enableRepeatEvents(true);
        getNewInfo(requestedElements);
    }

    protected void refreshValues() {
        players.clear();
        playerByName.clear();
        filteredResults.clear();
        initPlayers();
        filteredResults.addAll(players);
        updateValuesPosition();
    }

    protected void initColumnButtons() {
        float x = ScaleGui.get(93);
        float y = ScaleGui.get(220);
        float buttonWidth = 283;
        float buttonHeight = 30;
        GuiWinButtonColumn b = new GuiWinButtonColumn(4, x, y, ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "NICK", this);
        buttonList.add(b);
        buttonWidth = 37;
        x += ScaleGui.get(293);
        b = new GuiWinButtonColumn(5, x, y, ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "CL", this);
        buttonList.add(b);
        x += ScaleGui.get(47);
        b = new GuiWinButtonColumn(6, x, y, ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "LVL", this);
        buttonList.add(b);
        x += ScaleGui.get(47);
        b = new GuiWinButtonColumn(7, x, y, ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "ST", this);
        buttonList.add(b);
    }

    protected void initButtons() {
        float buttonWidth = 159;
        float buttonHeight = 39;
        float x = ScaleGui.get(733);
        float y = ScaleGui.get(70);
        GuiButtonAnimated button = new GuiButtonAnimated(2, x, y, ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "ЗАЯВКИ");
        button.setTexture(TextureRegister.textureTreeButtonAccept);
        button.setTextureHover(TextureRegister.textureTreeButtonHover);
        button.setMaskTexture(TextureRegister.textureTreeButtonMask);
        buttonList.add(button);
        x += ScaleGui.get(178);
        button = new GuiButtonAnimated(3, x, y, ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "ДРУЗЬЯ");
        button.setTexture(TextureRegister.textureTreeButtonAccept);
        button.setTextureHover(TextureRegister.textureTreeButtonHover);
        button.setMaskTexture(TextureRegister.textureTreeButtonMask);
        buttonList.add(button);
    }

    @Override
    protected void actionPerformed(GuiButton b) {
        if(b.id == 3) {
            pda.showFriends(); return;
        } else if(b.id == 2) {
            pda.showIncomingFriendRequests(); return;
        }
        super.actionPerformed(b);
    }

    protected void initPlayers() {
        float buttonWidth = ScaleGui.get(996);
        float buttonHeight = ScaleGui.get(37);
        NetHandlerPlayClient handler = mc.thePlayer.sendQueue;
        List<GuiPlayerInfo> playerInfoList = (List<GuiPlayerInfo>)handler.playerInfoList;
        Group group = XlvsGroupMod.INSTANCE.getGroup();
        int id = 1;
        long now = System.currentTimeMillis();
        for(GuiPlayerInfo playerInfo : playerInfoList) {
            String name = playerInfo.name;
            ClientPlayer clientPlayer = ClientPlayerManager.INSTANCE.getPlayerByName(playerInfo.name);
            if(clientPlayer != null) {
                String playerName = clientPlayer.getPlayerName();
                GuiButtonPlayer s = new GuiButtonPlayer(id, 0, 0, buttonWidth, buttonHeight, this, playerName, clientPlayer.getCharacterType(),
                        clientPlayer.getLevel(), now, friendHandler.getAllFriendNames().contains(playerName), group != null && group.getPlayers().contains(playerName), false);
                players.add(s);
                playerByName.put(playerName, s);
                id++;
            } else if(name.equals(mc.thePlayer.getDisplayName())) {
                String playerName = mc.thePlayer.getDisplayName();
                ClientMainPlayer mainPlayer = XlvsMainMod.INSTANCE.getClientMainPlayer();
                GuiButtonPlayer s = new GuiButtonPlayer(id, 0, 0, buttonWidth, buttonHeight, this, playerName, mainPlayer.getCharacterType(),
                        mainPlayer.getLvl(), now, false, false, false);
                players.add(s);
                playerByName.put(playerName, s);
                id++;
            }
        }
    }

    protected void onFriendsSynced() {
        refreshValues();
    }

    protected void syncFriends() {
        XlvsCore.INSTANCE.getPacketHandler().sendPacketEffectiveCallback(new PacketFriendSync()).thenAcceptSync(result -> {
            if(result.isSuccess()) {
                friendHandler.sync(result.getFriendRelations());
                onFriendsSynced();
            }
        });
    }

    protected void onGroupSynced() {
        refreshValues();
    }

    protected void syncGroups() {
        XlvsCore.INSTANCE.getPacketHandler().sendPacketEffectiveCallback(new PacketGroupSync()).thenAcceptSync(success -> {
            if(success) {
                onGroupSynced();
            }
        });
    }

    protected void getNewInfo(int offset) {

    }

    protected void updateValuesPosition() {
        float x = ScaleGui.get(44);
        float y = ScaleGui.get(250);
        float startY = y;
        float yOffset = ScaleGui.get(42);
        int id = 0;
        for (GuiButtonPlayer p : filteredResults) {
            p.id = id; p.setXPosition(x); p.setYPosition(y);
            y += yOffset;
            id++;
        }

        guiScroll.setScrollViewHeight(ScaleGui.get(577));
        guiScroll.setScrollTotalHeight(y - startY);
    }

    @Override
    public void search(String value) {
        filteredResults.clear();

        for(GuiButtonPlayer b : players) {
            if(StringUtils.containsIgnoreCase(b.getPlayerName(), value)) filteredResults.add(b);
        }

        updateValuesPosition();
    }

    @Override
    public void sort(int id, EnumSortType state) {
        for(GuiButton guiButton : buttonList) {
            if(guiButton.id != id) {
                if(guiButton instanceof GuiWinButtonColumn) {
                    GuiWinButtonColumn b = (GuiWinButtonColumn) guiButton;
                    b.resetSort();
                }
            }
        }

        if(state == EnumSortType.NONE) {
            filteredResults.clear();
            filteredResults.addAll(players);
        } else {
            switch (id) {
                case 4:
                    if(state == EnumSortType.ASCENDING) filteredResults.sort(Comparator.comparing(GuiButtonPlayer::getPlayerName));
                    else filteredResults.sort(Comparator.comparing(GuiButtonPlayer::getPlayerName).reversed());
                    break;
                case 5:
                    if(state == EnumSortType.ASCENDING) filteredResults.sort(Comparator.comparingInt(o -> o.getCharacterType().ordinal()));
                    else filteredResults.sort((o1, o2) -> Collections.reverseOrder().compare(o1.getCharacterType().ordinal(), o2.getCharacterType().ordinal()));
                    break;
                case 6:
                    if(state == EnumSortType.ASCENDING) filteredResults.sort(Comparator.comparingInt(GuiButtonPlayer::getLevel));
                    else filteredResults.sort(Comparator.comparingInt(GuiButtonPlayer::getLevel).reversed());
                    break;
                case 7:
                    if(state == EnumSortType.ASCENDING) filteredResults.sort(Comparator.comparingLong(GuiButtonPlayer::getLastOnline));
                    else filteredResults.sort(Comparator.comparingLong(GuiButtonPlayer::getLastOnline).reversed());
            }
        }

        updateValuesPosition();
    }

    @Override
    public void subAction(GuiWinButtonWithSubs button, int id) {
        GuiButtonPlayer buttonPlayer = (GuiButtonPlayer) button;
        if(buttonPlayer.getPlayerName().equals(mc.thePlayer.getDisplayName())) return;
        if(id == 0) {
            pda.showOrCreateWindow(EnumWindowType.MAIL_WRITE, new GuiWindowWriteMessage(pda, buttonPlayer.getPlayerName(), ""), false);
            pda.setLeftMenuSubButtonPressed(EnumPdaLeftMenuSection.MAIL_WRITE, true);
        } else if(id == 1) {
            pda.showProfile(buttonPlayer.getPlayerName());
        } else if(id == 2) {
            XlvsCore.INSTANCE.getPacketHandler().sendPacketEffectiveCallback(new PacketGroupInvite(buttonPlayer.getPlayerName())).thenAcceptSync(s -> {
                //TODO: обработка респонс меседжа
                pda.setPopup(new GuiPopup(pda, s, "",GuiPopup.green));
            });
        } else if(id == 3) {
            if(buttonPlayer.isFriend()) {
                XlvsCore.INSTANCE.getPacketHandler().sendPacketEffectiveCallback(new PacketFriendRemove(buttonPlayer.getPlayerName())).thenAcceptSync(s -> {
                    //TODO: обработка респонс меседжа;
                    pda.setPopup(new GuiPopup(pda, s, "",GuiPopup.green));
                });
            } else {
                XlvsCore.INSTANCE.getPacketHandler().sendPacketEffectiveCallback(new PacketFriendInviteRequest(buttonPlayer.getPlayerName())).thenAcceptSync(s -> {
                    if(s.isSuccess()) {
                        pda.setPopup(new GuiPopup(pda, "ЗАЯВКА УСПЕШНО ОТПРАВЛЕНА", s.getResponseMessage(), GuiPopup.green));
                    } else {
                        pda.setPopup(new GuiPopup(pda, "ПРОИЗОШЛА ОШИБКА", s.getResponseMessage(), GuiPopup.red));
                    }
                    //TODO: обработка респонс меседжа;

                });
            }
        } else if(id == 4) {
            //blacklist
        }
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTick) {
        super.drawScreen(mouseX, mouseY, partialTick);
        drawScroll();
        drawBackgroundThings();
        if(searchable) drawSearchField(mouseX, mouseY);
        drawPlayers(mouseX, mouseY);
    }

    protected void drawScroll() {
        guiScroll.drawScroll(windowXAnim + ScaleGui.get(1064),
                windowYAnim + ScaleGui.get(250),
                ScaleGui.get(528)  * secondPoint * collapseAnim,
                windowYAnim + ScaleGui.get(788));
    }

    protected void drawBackgroundThings() {
        GuiDrawUtils.drawRect(TextureRegister.texturePDAWinPlayersDevider, windowXAnim + ScaleGui.get(44), windowYAnim + ScaleGui.get(128), ScaleGui.get(1026), ScaleGui.get(2));
        GuiDrawUtils.drawRect(TextureRegister.texturePDAWinPlayersDevider, windowXAnim + ScaleGui.get(44), windowYAnim + ScaleGui.get(208), ScaleGui.get(1026), ScaleGui.get(2));

        float x = windowXAnim + ScaleGui.get(62);
        float y = windowYAnim + ScaleGui.get(91);
        float iconWidth = ScaleGui.get(37);
        float stringMinusYOffset = ScaleGui.get(14);
        float stringPositiveYOffset = ScaleGui.get(10);
        float stringXOffset = ScaleGui.get(27);
        float fs = ScaleGui.get(1.2f) * fs1;
        GuiDrawUtils.drawRectCentered(TextureRegister.texturePDAIconCircle, x, y, iconWidth * firstPoint, iconWidth * firstPoint);
        String s = "ИГРОКОВ: ";
        FontContainer fontContainer = FontType.HelveticaNeueCyrLight.getFontContainer();
        GuiDrawUtils.drawStringNoScale(fontContainer, s, x + stringXOffset, y - stringMinusYOffset, fs, 0xffffff);
        GuiDrawUtils.drawStringNoScale(fontContainer, "" + playersCount, x + stringXOffset + fontContainer.width(s) * fs, y - stringMinusYOffset, fs, 0x1BC3EC);
        s = "ПИНГ: ";
        GuiDrawUtils.drawStringNoScale(fontContainer, s, x + stringXOffset, y + stringPositiveYOffset, fs, 0xffffff);
        GuiDrawUtils.drawStringNoScale(fontContainer, (this.ping + " МС"), x + stringXOffset + fontContainer.width(s) * fs, y + stringPositiveYOffset, fs, 0x1BC3EC);

        fs = 28 / 32f;
        GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrLight, "№", windowXAnim + ScaleGui.get(57), windowYAnim + ScaleGui.get(234.5f), fs, 0x666666);
    }

    protected void drawSearchField(int mouseX, int mouseY) {
        searchField.addXPosition(windowXAnim);
        searchField.addYPosition(windowYAnim);
        searchField.drawTextBox(mouseX, mouseY);
    }

    protected void drawPlayers(int mouseX, int mouseY) {
        GL11.glEnable(GL11.GL_SCISSOR_TEST);
        scissorX = (int) (windowXAnim);
        float height = ScaleGui.get(578);
        float height1 = ScaleGui.get(windowHeight - 14);
        scissorHeight = (int) (height * firstPoint * collapseAnim);
        scissorY = (int) (mc.displayHeight - (windowCenterYAnim + halfHeight - ScaleGui.get(27)) + height1 * (1.0f - firstPoint) + height1 * (1.0f - collapseAnim));
        scissorWidth = (int) scaledWidth;
        glScissor(scissorX, scissorY, scissorWidth, scissorHeight);
        scissorAABB = AxisAlignedBB.getBoundingBox(scissorX, windowYAnim + ScaleGui.get(249), -100, scissorX + scissorWidth, windowYAnim + ScaleGui.get(827), 100);
        int id = 0;
        for(GuiButtonPlayer b : filteredResults) {
            b.addXPosition(windowXAnim);
            b.addYPosition(windowYAnim - guiScroll.getScrollAnim());

            if(scissorAABB.intersectsWith(AxisAlignedBB.getBoundingBox(b.xPosition, b.yPosition, 0, b.xPosition + b.width, b.yPosition + b.height, 1))) {
                b.drawButton(mouseX, mouseY);
                if(id + 1 == requestedElements) {
                    getNewInfo(requestedElements);
                }
            }

            id++;
        }

        GL11.glDisable(GL11.GL_SCISSOR_TEST);
    }

    @Override
    public boolean mouseClickedWindow(int mouseX, int mouseY, int mouseButton) {
        if(searchable) searchField.mouseClicked(mouseX, mouseY, mouseButton);
        for(GuiButtonPlayer b : players) {
            if(scissorAABB.intersectsWith(AxisAlignedBB.getBoundingBox(b.xPosition, b.yPosition, 0, b.xPosition + b.width, b.yPosition + b.height, 0))
                    && b.mousePressed(mc, mouseX, mouseY)) {
                return true;
            }
        }

        return super.mouseClickedWindow(mouseX, mouseY, mouseButton);
    }

    @Override
    public void updateScreen() {
        super.updateScreen();
        if(searchable) searchField.updateCursorCounter();
    }

    @Override
    public void keyTyped(char character, int key) {
        super.keyTyped(character, key);
        if(searchable) searchField.textboxKeyTyped(character, key);
    }

    @Override
    public void setCollapsed(boolean collapsed) {
        super.setCollapsed(collapsed);
        if(!collapsed) {
            Keyboard.enableRepeatEvents(true);
        }
    }

    @Override
    public void onWindowClosed() {
        Keyboard.enableRepeatEvents(false);
        pda.setLeftMenuSubButtonPressed(EnumPdaLeftMenuSection.COMMUNITY_SERVER, false);
    }

    @Override
    public void onWindowCollapsed() {
        Keyboard.enableRepeatEvents(false);
    }
}
