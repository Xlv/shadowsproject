package ru.krogenit.modifi.slot;

public enum EnumModifySlotType {
    SIGHT, TRIGGER, SILENCER, BUTT, MAGAZINE, CASE, AMMO;
}
