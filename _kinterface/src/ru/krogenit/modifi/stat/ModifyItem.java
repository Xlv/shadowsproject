package ru.krogenit.modifi.stat;

import lombok.AllArgsConstructor;
import lombok.Getter;
import net.minecraft.item.ItemStack;

@Getter
@AllArgsConstructor
public class ModifyItem {
    private final ItemStack itemStack;
    private final ModifyStat[] stats;
}
