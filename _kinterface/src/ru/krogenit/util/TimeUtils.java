package ru.krogenit.util;

import net.minecraft.util.EnumChatFormatting;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Date;

public class TimeUtils {

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm");

    public static String formatTimeMills(long time) {
        return dateFormat.format(new Date(time));
    }

    public static String getLastOnlineTime(long lastOnlineTime) {
        long diff = System.currentTimeMillis() - lastOnlineTime;
        if(diff <= 1000) {
            return EnumChatFormatting.GREEN + "ON";
        }
        LocalDate lastDate = Instant.ofEpochMilli(lastOnlineTime).atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate currentDate = LocalDate.now();
        Period period = Period.between(lastDate, currentDate);

        int years = period.getYears();
        if(years > 100) years = 100;
        if(years > 0) return years + "Г";
        int months = period.getMonths();
        if(months > 0) return  months + "М";
        int days = period.getDays();
        if(days > 0) return days + "Д";

        long seconds = diff / 1000;
        if(seconds > 60) {
            long minutes = seconds / 60;
            if (minutes > 60) {
                return minutes / 60 + "Ч";
            } else return minutes + "М";
        } else return seconds + "С";
//        long diff = System.currentTimeMillis() - lastOnlineTime;
//        if(diff <= 1000) {
//            return EnumChatFormatting.GREEN + "ON";
//        } else {
//            long seconds = lastOnlineTime / 1000;
//            if(seconds > 60) {
//                long minutes = seconds / 60;
//                if(minutes > 60) {
//                    long hours = minutes / 60;
//                    if(hours > 24) {
//                        float days = hours / 24;
//                        if(days > 30) {
//                            float months = days / 30;
//                            if(months > 12) {
//                                float years =
//                            }
//                        } return hours / 24 + "Д";
//                    } return hours + "Ч";
//                } return minutes + "М";
//            } return seconds + "С";
//        }
    }
}
